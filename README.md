# :blue_book: Soccerting Project

### 목차
### [1.소개(Introduce)](#1-소개)  
### [2.사용한기술(Tech)](#2-사용한기술)  
### [3.사진(Picture)](#3-사진) 

<br>

![ic_main_logo](https://user-images.githubusercontent.com/39984656/61764892-b6724880-ae15-11e9-9ee9-47d4460392e5.png)     
<br>
<br>



# 1 소개  

>  ### - **"축구를 할때 꼭 1명이 늘 부족해요"**  
>  ### - **"저희팀은 다른팀과 경기를 뛰고싶은데, 상대를 어디서 구해야할지 모르겠어요"**  

<br>

### **축구를 하고 싶은데 같이 할 사람이 없나요?**  
### **인원이 부족한가요?**  
### **동네 축구친구를 사귀고 싶으신가요?**
### **저희 “싸커팅”이 찾아드리겠습니다!**              

<br>
<br>


### *클럽 시스템*    
```
- 지역과 나이 대를 입력하면 원하는 클럽을 창설할 수 있어요!
- 감독과 코치에게 클럽 권한이 부여됩니다!
- 클럽회원들끼리 채팅이 가능합니다. 자유롭게 대화도 가능!
- 클럽 포지션 스쿼드(포메이션)을 제공!
- 클럽 전용 게시판 제공!  
```

### *클럽 검색 시스템*      
```
- 원하는 지역, 나이대로 가입 가능한 클럽을 찾을 수 있어요!    
```

### *클럽 간 매칭*       
```
- 5:5부터 11:11까지 매칭하고 싶은 인원수를 조절할 수 있어요!
- 지역, 인원수, 날짜로 클럽 간 매칭을 할 수 있어요!
- 매칭 된 클럽과 채팅이 가능합니다!  
```

### *용병시스템*    
```
- 클럽이 부담스러우신 분들! 
- 클럽에 가입되어 있어도 자유롭게 즐기고 싶으신 분들!
  원하는 대로 매칭이 가능합니다! (최대 4개까지 가능합니다.)  
```
    
<br>
     
### **싸커팅앱을 이용하여 소통하여 보세요!!**         
     
<br>   
<br>     
<br>   
         
  
# 2 사용한기술  

> ### :blue_book: **language : kotlin**
> ### :orange_book: **architecture : MVVM**
> ### :notebook_with_decorative_cover: **etc : RxJava, Firebase, Databinding, CI(Jenkins, gitlab), JatPack(Navigation) ...**        

  
<br>
<br>
<br>
    
# 3 사진  
      
<img src="https://user-images.githubusercontent.com/39984656/61852028-69fd3a80-aef3-11e9-9500-4c15466937d5.png" width="500" height="900" />
<img src="https://user-images.githubusercontent.com/39984656/61852035-6e295800-aef3-11e9-92f5-102d7e902689.png" width="500" height="900" />    
<br>
<img src="https://user-images.githubusercontent.com/39984656/61852049-78e3ed00-aef3-11e9-8882-54ce00696cac.png" width="500" height="900" />
<img src="https://user-images.githubusercontent.com/39984656/61845844-da9a5c00-aedf-11e9-9b8b-b462f679ed13.png" width="500" height="900" />    
<br>
<img src="https://user-images.githubusercontent.com/39984656/61845846-de2de300-aedf-11e9-89cd-3a1ce58eee89.png" width="500" height="900" />
<img src="https://user-images.githubusercontent.com/39984656/61845867-ebe36880-aedf-11e9-981a-4ce276bc79c1.png" width="500" height="900" />    
<br>
<img src="https://user-images.githubusercontent.com/39984656/61845868-ebe36880-aedf-11e9-9cd6-ca2b3a740800.png" width="500" height="900" />    
<img src="https://user-images.githubusercontent.com/39984656/61852053-7a151a00-aef3-11e9-84dc-e1bb93cb3111.png" width="500" height="900" />    

  
<br>
<br>

---
---
---

### [목차로이동](#목차)       