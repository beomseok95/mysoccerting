package ks.bs.mysoccerting.rxactivitytest


import io.reactivex.observers.TestObserver
import ks.bs.mysoccerting.rx.caller.ActivityObservable
import ks.bs.mysoccerting.test.JustCallWithCountActivity
import org.junit.Test

class JustCallWithCountActivityTest {

    @Test
    fun recursiveCall() {
        val testObserver = TestObserver<JustCallWithCountActivity.ExtraData>()
        val extraData = JustCallWithCountActivity.ExtraData(2)

        ActivityObservable<JustCallWithCountActivity.ExtraData>(
            null,
            JustCallWithCountActivity::class.java,
            extraData,
            extraData.javaClass
        )
                .createObservable()
                .subscribe(testObserver)

        testObserver.await()
        testObserver.assertNoErrors()
        

    }
}