package ks.bs.mysoccerting.rxactivitytest

import android.os.Bundle
import io.reactivex.observers.TestObserver
import ks.bs.mysoccerting.exception.CanceledByUserException
import ks.bs.mysoccerting.rx.caller.ActivityBundleObservable
import ks.bs.mysoccerting.test.JustCancelActivity
import org.junit.Test

class JustCancelActivityTest {
    @Test
    fun shouldThrowExceptionCancel() {

        val testObserver = TestObserver<Bundle>()

        val bundle = Bundle()
        ActivityBundleObservable(null, JustCancelActivity::class.java, bundle)
                .createObservable()
                .subscribe(testObserver)

        testObserver.await()
        testObserver.assertValueCount(0)
        testObserver.assertError(CanceledByUserException::class.java)

    }
}