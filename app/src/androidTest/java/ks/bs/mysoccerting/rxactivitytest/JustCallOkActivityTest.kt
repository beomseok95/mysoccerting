package ks.bs.mysoccerting.rxactivitytest

import android.os.Bundle
import io.reactivex.observers.TestObserver
import ks.bs.mysoccerting.rx.caller.ActivityBundleObservable
import ks.bs.mysoccerting.test.JustCallOkActivity
import org.junit.Test

class JustCallOkActivityTest {

    @Test
    fun 문제없이부르고부르고() {

        val testObserver = TestObserver<Bundle>()
        ActivityBundleObservable(null, JustCallOkActivity::class.java, Bundle())
                .createObservable()
                .subscribe(testObserver)

        testObserver.apply {
            await()
            assertComplete()
            assertNoErrors()
        }
        
    }
}