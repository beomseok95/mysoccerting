package ks.bs.mysoccerting.rxactivitytest

import android.os.Bundle
import io.reactivex.observers.TestObserver
import junit.framework.Assert.assertEquals
import ks.bs.mysoccerting.rx.caller.ActivityBundleObservable

import ks.bs.mysoccerting.test.JustOkActivity
import org.junit.Test

class ObservableActivityBundleTest {

    @Test
    fun doFinally가불려져야하지요() {

        val testObserver = TestObserver<Bundle>()

        var disposed = false

        val bundle = Bundle()
        ActivityBundleObservable(null, JustOkActivity::class.java, bundle)
                .createObservable()
                .doFinally {
                    disposed = true
                }
                .subscribe(testObserver)

        testObserver.await()
        testObserver.assertNoErrors()
        testObserver.assertValueCount(1)
        testObserver.assertComplete()

        assertEquals("doFinally()가 실행되지 않았다.", true, disposed)
    }
}