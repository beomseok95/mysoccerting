package ks.bs.mysoccerting.fcm

import com.google.firebase.iid.FirebaseInstanceId
import com.google.gson.Gson
import io.reactivex.Single
import ks.bs.mysoccerting.api.ApiService
import ks.bs.mysoccerting.api.Header
import ks.bs.mysoccerting.firebase.RxRealTimeDB
import ks.bs.mysoccerting.model.ModelPush
import ks.bs.mysoccerting.rx.networkThread
import ks.bs.mysoccerting.scene.login.LoginManager
import ks.bs.mysoccerting.support.DontCare
import ks.bs.mysoccerting.util.TLog
import okhttp3.OkHttpClient

object FcmHelper {

    fun sendMessage(destinationUid: String, title: String = "알림", message: String): Single<ModelPush.Response> {
        return getToken(destinationUid)
            .flatMap { token ->
                val pushModel = ModelPush(
                    to = token,
                    notification = ModelPush.Notification(
                        body = message,
                        title = title
                    ),
                    data = ModelPush.Data(
                        uid = destinationUid
                    )
                )

                ApiService.fcm()
                    .pushFcm(hader = Header.create(), paramater = pushModel)
                    .networkThread()
            }
    }


    private fun getToken(destinationUid: String): Single<String> {
        val path = "fcm/token/$destinationUid/pushtoken"
        return RxRealTimeDB.toSingle(path) { "" }
    }

    fun registerPushToken(): Single<DontCare> {
        return createToken()
            .flatMap { toekn -> RxRealTimeDB.push("fcm/token/${LoginManager.auth.uid}/pushtoken", toekn) }
    }


    private fun createToken(): Single<String> {

        return Single.create { emitter ->
            FirebaseInstanceId.getInstance()
                .instanceId
                .addOnSuccessListener { instanceIdResult ->
                    emitter.onSuccess(instanceIdResult.token)
                }
                .addOnFailureListener { e ->
                    emitter.onError(e)
                }
        }

    }

}