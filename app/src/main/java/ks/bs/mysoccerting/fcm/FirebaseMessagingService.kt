package ks.bs.mysoccerting.fcm

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.media.RingtoneManager
import android.os.Build
import androidx.core.app.NotificationCompat
import com.firebase.jobdispatcher.FirebaseJobDispatcher
import com.firebase.jobdispatcher.GooglePlayDriver
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import ks.bs.mysoccerting.R
import ks.bs.mysoccerting.prefs.Prefs
import ks.bs.mysoccerting.scene.home.MainActivity
import ks.bs.mysoccerting.util.TLog
import android.net.Uri
import com.facebook.login.LoginManager
import ks.bs.mysoccerting.model.ModelPush
import ks.bs.mysoccerting.rx.toJson
import ks.bs.mysoccerting.scene.alarm.AlarmActivity


class FirebaseMessagingService : FirebaseMessagingService() {
    override fun onMessageReceived(remoteMessage: RemoteMessage?) {

        remoteMessage?.data?.isNotEmpty()
            .let { longTermTask ->
                if (longTermTask!!)
                    scheduleJob() //10 초이상작업
                else
                    handleNow()  //짧은작덥

            }

        remoteMessage?.data?.let { data ->
            remoteMessage.notification?.let {
                if (Prefs.settings.fcmEnabled && Prefs.settings.isLogined && Prefs.settings.loginUid == data["uid"])
                    sendNotification(it.body!!)
            }
        }

    }


    private fun sendNotification(messageBody: String) {
        val intent = Intent(this, AlarmActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        val pendingIntent = PendingIntent.getActivity(
            this, 0, intent,
            PendingIntent.FLAG_ONE_SHOT
        )

        val channelId = getString(R.string.default_notification_channel_id)
//        val defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
        val soundUri = Uri.parse(
            "android.resource://" +
                    applicationContext.packageName +
                    "/" +
                    R.raw.noti_sound
        )

        val notificationBuilder = NotificationCompat.Builder(this, channelId)
            .setSmallIcon(R.mipmap.ic_launcher)
            .setContentTitle("Soccerting")
            .setContentText(messageBody)
            .setAutoCancel(true)
            .setSound(soundUri)
            .setContentIntent(pendingIntent)

        val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        // android Oreo 알림 채널이 필요합니다
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channel = NotificationChannel(
                channelId,
                "Channel human readable title",
                NotificationManager.IMPORTANCE_DEFAULT
            )
            notificationManager.createNotificationChannel(channel)
        }

        notificationManager.notify(0 /*알림ID */, notificationBuilder.build())
    }

    private fun scheduleJob() { //장기작업(10초이상)일때 처리하는 메소드
        val dispatcher = FirebaseJobDispatcher(GooglePlayDriver(this))
        val myJob = dispatcher.newJobBuilder()
            .setService(NotificationJobService::class.java)
            .setTag("my-job-tag")
            .build()
        dispatcher.schedule(myJob)
    }

    private fun handleNow() {
        TLog.i("Short lived task is done.")
    }
}