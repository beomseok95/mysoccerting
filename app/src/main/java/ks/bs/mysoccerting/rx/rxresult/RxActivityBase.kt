package ks.bs.mysoccerting.rx.rxresult

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import ks.bs.mysoccerting.exception.CanceledByUserException
import ks.bs.mysoccerting.scene.base.ActivityBase
import java.lang.reflect.Type


@SuppressLint("Registered")
open class RxActivityBase : ActivityBase() {
    inline fun <reified T> getInput(): T {
        return getInput(T::class.java)
    }

    fun <T> getInput(clazz: Type): T {
        return intent.getObject(clazz) as T
    }

    fun finishWithResponse(any: Any) {
        setResult(AppCompatActivity.RESULT_OK, Intent().putObject(any))
        finish()
    }

    fun finishWithCancel() {
        finishWithException(CanceledByUserException())
    }

    override fun finishWithException(t: Throwable) {
        setResult(Activity.RESULT_CANCELED, Intent().putObject(t))
        finish()
    }
}

