package ks.bs.mysoccerting.rx.caller

import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import com.google.gson.reflect.TypeToken
import io.reactivex.Single
import io.reactivex.SingleEmitter
import ks.bs.mysoccerting.scene.base.ActivityBase
import ks.bs.mysoccerting.util.AppInstance
import ks.bs.mysoccerting.util.GsonHelper
import ks.bs.mysoccerting.util.TLog
import java.lang.reflect.Type

/**
 *
 * Object 처리용
 * 모든 JSON 변환 가능한 객체에 사용 가능하다.
 * 단 하나의 EXTRA만 사용한다.
 *
 */
abstract class ActivityObjectSingle<TOUT> : ActivityBase() {
    inline fun <reified TIN> parseRequest()
            : TIN = getObject(
        intent,
        object : TypeToken<TIN>() {}.type
    )!!

    fun finishWithResponse(response: TOUT) {
        setResult(RESULT_OK, putObject(Intent(), response))
        finish()
    }

    fun cancelWithResponse(response: TOUT) {
        setResult(RESULT_CANCELED,
            putObject(Intent(), response)
        )
        finish()
    }

    companion object {

        inline fun <reified TOUT> create(
            context: Context?,
            clazz: Class<*>,
            inputObject: Any?
        ): Single<TOUT> = create<TOUT>(
            context,
            ComponentName(context.get().packageName, clazz.canonicalName!!),
            inputObject
        )

        inline fun <reified TOUT> create(
            context: Context?,
            componentName: ComponentName,
            inputObject: Any?
        ): Single<TOUT> = create<TOUT>(
            context,
            componentName,
            inputObject,
            object : TypeToken<TOUT>() {}.type
        )

        fun <TOUT> create(
            context: Context?,
            componentName: ComponentName,
            inputObject: Any?,
            classOfResponse: Type
        ): Single<TOUT> {
            var receiver: SingleObjectReceiver<TOUT>? = null

            return Single.defer {
                Single.create<TOUT> { emitter ->

                    receiver = startNewReceiver<TOUT>(
                        context,
                        emitter,
                        classOfResponse
                    )

                    startActivityCaller(
                        context,
                        receiver!!,
                        componentName,
                        inputObject
                    )

                }.doFinally {
                    if (receiver == null) {
                       //TToast.showDebug("doFinally / 리시버 해제 요청이 두번 들어왔음.")
                    } else {
                        context.get().unregisterReceiver(receiver)
                        receiver = null
                    }
                }
            }
        }

        private fun <TOUT> startNewReceiver(
            context: Context?,
            emitter: SingleEmitter<TOUT>,
            classOfResponse: Type
        ): SingleObjectReceiver<TOUT> {
            val receiver = SingleObjectReceiver<TOUT>(emitter, classOfResponse)

            val intentFilter = IntentFilter()
            intentFilter.addAction(receiver.name)
            context.get().registerReceiver(receiver, intentFilter)

            return receiver
        }


        private fun <TOUT> startActivityCaller(
            context: Context?,
            receiver: SingleObjectReceiver<TOUT>,
            componentName: ComponentName,
            inputObject: Any?
        ) {
            val intent = Intent(context.get(), ActivityCaller::class.java)

            putInputObject(intent, inputObject)

            TLog.d("### RECEIVER NAME: ${receiver.name}")

            intent.putExtra(ActivityCaller.EXTRA_COMPONENT_NAME, componentName)
            intent.putExtra(ActivityCaller.EXTRA_RECEIVER_NAME, receiver.name)

            if (context == null) {
                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
            }

            context.get().startActivity(intent)
        }

        fun putInputObject(inputIntent: Intent, inputObject: Any?): Intent =
            putObject(inputIntent, inputObject)

        const val EXTRA_OBJECT_OBSERVABLE = "NEXT-M-DATA"

        fun <T> putObject(intent: Intent, obj: T?): Intent {
            if (obj == null) {
                return intent
            }
            val json: String = GsonHelper.toJson(obj)

            intent.putExtra(EXTRA_OBJECT_OBSERVABLE, json)

            return intent
        }

        inline fun <reified T> getObject(intent: Intent): T? {
            return getObject<T>(
                intent,
                object : TypeToken<T>() {}.type
            )
        }

        inline fun <reified T> getObject(bundle: Bundle): T? {
            return getObject(
                bundle,
                object : TypeToken<T>() {}.type
            )
        }

        fun <T> getObject(intent: Intent, classOfT: Type): T? {
            val bundle = intent.extras ?: return null

            return getObject<T>(bundle, classOfT)
        }

        fun <T> getObject(bundle: Bundle, classOfT: Type): T? {
            if (!bundle.containsKey(EXTRA_OBJECT_OBSERVABLE)) {
                return null
            }

            val json: String = bundle.getString(EXTRA_OBJECT_OBSERVABLE)!!

            return GsonHelper.fromJson<T>(json, classOfT)
        }

        fun Context?.get(): Context {
            return this ?: AppInstance.get()
        }

    }
}

