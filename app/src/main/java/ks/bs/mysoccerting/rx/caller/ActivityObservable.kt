package ks.bs.mysoccerting.rx.caller

import android.content.*
import android.os.Bundle
import com.google.gson.JsonObject
import io.reactivex.Observable
import io.reactivex.ObservableEmitter
import ks.bs.mysoccerting.util.AppInstance
import ks.bs.mysoccerting.util.GsonHelper
import ks.bs.mysoccerting.util.TLog
import java.lang.reflect.Type

/**
 * Created by ian on 2018. 3. 18..
 * 주고 받는 데이타를 모두 스트링으로 전환하여 Gson toJson / fromJson 사용함에 주의
 *
 * intent 방식용 (모든 값은 스트링만 가능함)
 *  "KEY1":"VALUE1"
 *  "KEY2":"VALUE2"
 *
 * 기존 스트링 기반의 EXTRA 사용하는 Activity 와 호환된다.
 * 데이타 Field 만큼의 EXTRA 사용한다.
 */
open class ActivityObservable<TOUT>(
        val context: Context?,
        private val componentName: ComponentName,
        private val inputObject: Any?,
        private val classOfT: Type
) {
    constructor(context: Context?, clazz: Class<*>, inputObject: Any?, classOfT: Type)
            : this(context, ComponentName(AppInstance.get().packageName, clazz.canonicalName!!), inputObject, classOfT)

    private val receiverName = "${ActivityObservable::class.java.canonicalName}.${generateRandomKey()}.RECEIVER"
    private var receiver: BroadcastReceiver? = null

    fun createObservable(): Observable<TOUT> {

        return Observable.defer {
            Observable.create<TOUT> { emitter ->

                receiver = startNewReceiver(emitter)

                startActivityCaller(componentName, inputObject)

            }.doFinally {
                if (receiver == null) {
                   // TToast.showDebug("doFinally / 리시버 해제 요청이 두번 들어왔음.")
                }
                receiver?.let { AppInstance.get().unregisterReceiver(it) }
                receiver = null
            }
        }
    }


    private fun startNewReceiver(emitter: ObservableEmitter<TOUT>): BroadcastReceiver {
        val receiver = object : BroadcastReceiver() {
            override fun onReceive(context: Context, resultIntent: Intent) {
                if (emitter.isDisposed) {
                    return
                }

                try {
                    onReceiveResult(resultIntent, emitter)
                } catch (e: Throwable) {
                    emitter.onError(e)
                }
            }
        }
        val intentFilter = IntentFilter()
        intentFilter.addAction(receiverName)
        AppInstance.get().registerReceiver(receiver, intentFilter)

        return receiver
    }

    private fun startActivityCaller(componentName: ComponentName, inputObject: Any?) {
        val intent = Intent(AppInstance.get(), ActivityCaller::class.java)

        putInputObject(intent, inputObject)

        TLog.d("### RANDOM $receiverName")

        intent.putExtra(ActivityCaller.EXTRA_COMPONENT_NAME, componentName)
        intent.putExtra(ActivityCaller.EXTRA_RECEIVER_NAME, receiverName)

        if (context in listOf(null, AppInstance.get().applicationContext, AppInstance.get().baseContext)) {
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
            AppInstance.get().startActivity(intent)
        } else {
            context!!.startActivity(intent)
        }
    }

    private fun onReceiveResult(resultIntent: Intent, emitter: ObservableEmitter<TOUT>) {
        if (resultIntent.hasExtra(ActivityCaller.RESULT_EXTRA_ERROR)) {
            emitter.onError(resultIntent.getSerializableExtra(ActivityCaller.RESULT_EXTRA_ERROR) as Throwable)
            return
        }

        val obj: TOUT? = getOutputObject(resultIntent, classOfT)
        if (obj != null) {
            emitter.onNext(obj)
        }

        emitter.onComplete()
    }

    protected open fun putInputObject(inputIntent: Intent, inputObject: Any?): Intent =
        putObject(inputIntent, inputObject)

    protected open fun getOutputObject(resultIntent: Intent, classOfT: Type): TOUT? {
        if (classOfT == Bundle::class.java) {
            return resultIntent.extras as TOUT
        }
        return getObject(resultIntent, classOfT)

    }

    companion object {
        private var sequentialIndex: Int = 0
        private fun generateRandomKey(): String = "SEQ.${++sequentialIndex}.RAND.${(Math.random() * 10000).toInt()}"

        fun <T> putObject(intent: Intent, obj: T?): Intent {
            if (obj == null) {
                return intent
            }
            val jsonObject: JsonObject = GsonHelper.fromObject(obj)

            jsonObject.keySet().forEach { key ->
                val value = jsonObject.get(key)

                if (value.isJsonPrimitive) {
                    intent.putExtra(key, value.asString)
                } else {
                    intent.putExtra(key, value.toString())
                }
            }

            return intent
        }

        fun <T> getObject(intent: Intent, classOfT: Type): T? {
            val extras = intent.extras ?: return null

            val jsonObject = JsonObject()
            extras.keySet().forEach { name ->
                val value = extras[name]
                jsonObject.addProperty(name, value.toString())
            }

            val json = jsonObject.toString()

            return GsonHelper.fromJson<T>(json, classOfT)
        }

    }

}