package ks.bs.mysoccerting.rx.caller

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import io.reactivex.ObservableEmitter
import ks.bs.mysoccerting.exception.CanceledByUserException
import java.lang.reflect.Type

class ObservableObjectReceiver<TOUT>(val emitter: ObservableEmitter<TOUT>, val classOfT: Type) : BroadcastReceiver() {
    val name = generateReceiverName()

    override fun onReceive(context: Context, resultIntent: Intent) {
        if (emitter.isDisposed) {
            return
        }

        try {
            onReceiveResult<TOUT>(resultIntent, emitter, classOfT)
        } catch (e: Throwable) {
            emitter.onError(e)
        }
    }

    private fun <TOUT> onReceiveResult(resultIntent: Intent, emitter: ObservableEmitter<TOUT>, classOfT: Type) {
        if (resultIntent.hasExtra(ActivityCaller.RESULT_EXTRA_ERROR)) {
            emitter.onError(resultIntent.getSerializableExtra(ActivityCaller.RESULT_EXTRA_ERROR) as Throwable)
            return
        }

        val outputObject = getOutputObject<TOUT>(resultIntent, classOfT)

        if (outputObject == null) {
            emitter.onError(CanceledByUserException())
            return
        }

        emitter.onNext(outputObject)
        emitter.onComplete()
    }

    fun <TOUT> getOutputObject(resultIntent: Intent, classOfT: Type): TOUT? {
        return ActivityObjectObservable.getObject<TOUT>(resultIntent, classOfT)
    }

    companion object {
        private var sequentialIndex: Int = 0
        private fun generateRandomKey(): String = "SEQ.${++sequentialIndex}.RAND.${(Math.random() * 10000).toInt()}"
        private fun generateReceiverName() = "${ActivityObjectObservable::class.java.canonicalName}.${generateRandomKey()}.RECEIVER"
    }
}