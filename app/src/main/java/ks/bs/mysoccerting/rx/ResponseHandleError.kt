package ks.bs.mysoccerting.rx

interface ResponseHandleError {
    fun isSuccess(): Boolean
    fun isFailed() = !isSuccess()
    fun getErrorCode(): String = ""
    fun getErrorMessage(): String = ""
}