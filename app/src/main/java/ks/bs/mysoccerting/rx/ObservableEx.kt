package ks.bs.mysoccerting.rx

import io.reactivex.Observable

class ObservableEx {
    companion object {
        fun <T> safeCallable(supplier: () -> T): Observable<T> {
            return io.reactivex.Observable.create<T> { emitter ->
                try {
                    val value = supplier()

                    if (emitter.isDisposed) {
                        return@create
                    }

                    emitter.onNext(value)
                    emitter.onComplete()

                } catch (t: Throwable) {
                    if (emitter.isDisposed) {
                        return@create
                    }

                    emitter.onError(t)
                }
            }
        }
    }
}
