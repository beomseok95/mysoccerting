package ks.bs.mysoccerting.rx

import android.app.Dialog
import android.app.ProgressDialog
import android.content.Context
import android.os.Bundle
import android.view.View
import android.view.animation.AnimationUtils
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import ks.bs.mysoccerting.R
import ks.bs.mysoccerting.customview.dialog.SoccerBallDialog
import ks.bs.mysoccerting.util.GsonHelper
import ks.bs.mysoccerting.util.getString
import java.util.*
import java.util.concurrent.TimeUnit

fun <T> Observable<T>.networkThread(): Observable<T> {
    return subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())

}

fun <T> Observable<T>.mainThread(): Observable<T> {
    return subscribeOn(AndroidSchedulers.mainThread())
        .observeOn(AndroidSchedulers.mainThread())
}

fun <T> Observable<T>.throwErrorOnEmpty(function: () -> Throwable): Observable<T> {
    return switchIfEmpty(Observable.create<T> { throw function() })
}

fun toDialogMessageElse(throwable: Throwable): String {
    throwable.message?.let {
        return it
    }

    return throwable.javaClass.simpleName
}

fun Any.toJson(pretty: Boolean = true): String {
    if (this is Bundle) {
        return toJsonBundle(this, pretty)
    }

    if (pretty)
        return GsonHelper.pretty.toJson(this)
    else
        return GsonHelper.toJson(this)
}

private fun toJsonBundle(bundle: Bundle, pretty: Boolean): String {

    val map = Hashtable<String, Any>()

    bundle.keySet().forEach {
        map[it] = bundle[it]
    }

    return map.toJson(pretty)
}

fun <T> Observable<T>.ignoreInterruption(): Observable<T> {
    return onErrorResumeNext { t: Throwable ->
        if (t is InterruptedException) {
            return@onErrorResumeNext Observable.empty()
        }

        return@onErrorResumeNext Observable.error(t)
    }
}

fun <T : View> T.onQuickClickWithAnimation(function: (view: T) -> Unit): T {
    return onClickWithAnimation(200, TimeUnit.MILLISECONDS, function)
}

fun <T : View> T.onClickWithAnimation(function: (view: T) -> Unit): T {
    return onClickWithAnimation(2000, TimeUnit.MILLISECONDS, function)
}

fun <T : View> T.onClickWithAnimation(delayForClickable: Long, timeUnit: TimeUnit, function: (view: T) -> Unit): T {
    setOnClickListener { view ->

        if (!view.isClickable)
            return@setOnClickListener
        view.isClickable = false

        startAnimation(AnimationUtils.loadAnimation(context, R.anim.animation_button_click))

        val delayForAnimation = 200L
        if (timeUnit == TimeUnit.MILLISECONDS)
            require(delayForClickable >= delayForAnimation) { "딜레이가 최소 200 milliseconds 이상이어야 함. $delayForClickable" }

        Observable.timer(delayForAnimation, TimeUnit.MILLISECONDS)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                function.invoke(this)
            }

        Observable.timer(delayForClickable, timeUnit)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                view.isClickable = true

            }

    }

    return this
}

fun <T> Observable<T>.withProgressAnimation(context: Context?, functionOnCancel: (() -> Unit)? = null): Observable<T> {
    var dialog: Dialog? = null

    return networkThread()
        .doOnSubscribe { disposable ->
            AndroidSchedulers.mainThread().scheduleDirect {
                dialog = showProgressDialog(context, disposable, functionOnCancel)
            }
        }
        .doFinally {
            dialog?.safeDismiss()
        }
}

fun showProgressDialog(context: Context?, disposable: Disposable, functionOnCancel: (() -> Unit)?): Dialog {
    require(context != null) { "context need  not null!!" }
    val dialog = ProgressDialog(context)

    dialog.setOnCancelListener {
        disposable.dispose()
        functionOnCancel?.invoke()
    }
    val cancelable = functionOnCancel != null
    dialog.setCancelable(cancelable)
    dialog.setCanceledOnTouchOutside(cancelable)

    dialog.show()


    return dialog
}


/*fun showProgressDialog(context: Context?, disposable: Disposable, functionOnCancel: (() -> Unit)?): Dialog {
    val dialog = ProgressDialog(context)

    dialog.setMessage(R.string.loading_message.getString())
    dialog.setOnCancelListener {
        disposable.dispose()
        functionOnCancel?.invoke()
    }
    val cancelable = functionOnCancel != null
    dialog.setCancelable(cancelable)
    dialog.setCanceledOnTouchOutside(cancelable)

    dialog.show()


    return dialog
}*/


fun <T : View> T.onClickWithBallAnimation(function: (view: T) -> Unit): T {
    return onClickWithAnimation(1000, TimeUnit.MILLISECONDS, function)
}

fun <T : View> T.onClickWithBallAnimation(delayForClickable: Long, timeUnit: TimeUnit, function: (view: T) -> Unit): T {
    setOnClickListener { view ->

        if (!view.isClickable)
            return@setOnClickListener
        view.isClickable = false

        startAnimation(AnimationUtils.loadAnimation(context, R.anim.animation_button_click))

        val delayForAnimation = 200L
        if (timeUnit == TimeUnit.MILLISECONDS)
            require(delayForClickable >= delayForAnimation) { "딜레이가 최소 200 milliseconds 이상이어야 함. $delayForClickable" }

        Observable.timer(delayForAnimation, TimeUnit.MILLISECONDS)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                function.invoke(this)
            }

        Observable.timer(delayForClickable, timeUnit)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                view.isClickable = true

            }

    }

    return this
}

fun <T> Observable<T>.withProgressBallAnimation(
    context: Context?,
    functionOnCancel: (() -> Unit)? = null
): Observable<T> {
    var dialog: Dialog? = null

    return networkThread()
        .doOnSubscribe { disposable ->
            AndroidSchedulers.mainThread().scheduleDirect {
                dialog = showSoccerBallProgressDialog(context, disposable, functionOnCancel)
            }
        }
        .doFinally {
            dialog?.safeDismiss()
        }
}

fun showSoccerBallProgressDialog(context: Context?, disposable: Disposable, functionOnCancel: (() -> Unit)?): Dialog {
    require(context != null) { "context need  not null!!" }
    val dialog = SoccerBallDialog(context)

    dialog.setOnCancelListener {
        disposable.dispose()
        functionOnCancel?.invoke()
    }
    val cancelable = functionOnCancel != null
    dialog.setCancelable(cancelable)
    dialog.setCanceledOnTouchOutside(cancelable)

    dialog.show()


    return dialog
}

fun Dialog?.safeDismiss() {
    if (this?.isShowing == true) {
        dismiss()

    }
}