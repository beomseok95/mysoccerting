package ks.bs.mysoccerting.rx

import io.reactivex.Completable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class CompletableEx {
    companion object {
        fun safeCallable(): Completable {
            return Completable.create { emitter ->
                try {
                    if (emitter.isDisposed) {
                        return@create
                    }

                    emitter.onComplete()

                } catch (t: Throwable) {
                    if (emitter.isDisposed) {
                        return@create
                    }

                    emitter.onError(t)
                }
            }
        }
    }
}

fun Completable.networkThread(): Completable {
    return subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
}

fun Completable.mainThread(): Completable {
    return subscribeOn(AndroidSchedulers.mainThread())
            .observeOn(AndroidSchedulers.mainThread())
}

