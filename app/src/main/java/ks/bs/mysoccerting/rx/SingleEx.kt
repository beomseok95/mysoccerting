package ks.bs.mysoccerting.rx

import android.app.Dialog
import android.app.ProgressDialog
import android.content.Context
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import ks.bs.mysoccerting.customview.dialog.SoccerBallDialog
import ks.bs.mysoccerting.exception.ErrorCodeException

fun <T> Single<T>.networkThread(): Single<T> {
    return subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
}

fun <T> Single<T>.mainThread(): Single<T> {
    return subscribeOn(AndroidSchedulers.mainThread())
        .observeOn(AndroidSchedulers.mainThread())
}

fun <T> Single<T>.withProgressAnimation(context: Context?, functionOnCancel: (() -> Unit)? = null): Single<T> {

    var dialog: Dialog? = null

    return networkThread()
        .doOnSubscribe { disposable ->
            AndroidSchedulers.mainThread().scheduleDirect {
                dialog = showProgressDialog(context, disposable, functionOnCancel)
            }
        }
        .doFinally {
            dialog.safeDismiss()
        }
}

fun <T> Single<T>.withProgressDialog(
    context: Context?,
    title: String,
    message: String,
    functionOnCancel: (() -> Unit)? = null
): Single<T> {

    var dialog: Dialog? = null
    var disposable: Disposable? = null
    return networkThread()
        .doOnSubscribe {
            AndroidSchedulers.mainThread().scheduleDirect {
                val cancelable = functionOnCancel != null

                val dial = ProgressDialog(context)
                dial.setTitle(title)
                dial.setMessage(message)
                dial.setCanceledOnTouchOutside(cancelable)
                dial.setCancelable(cancelable)


                if (cancelable) {
                    dial.setOnCancelListener {
                        disposable?.dispose()
                        functionOnCancel?.invoke()
                    }
                }
                dialog = dial
                dial.show()
            }
            disposable = it
        }
        .doFinally {
            dialog?.safeDismiss()
        }
}


fun <T> Single<T>.withProgressBallAnimation(context: Context?, functionOnCancel: (() -> Unit)? = null): Single<T> {

    var dialog: Dialog? = null

    return networkThread()
        .doOnSubscribe { disposable ->
            AndroidSchedulers.mainThread().scheduleDirect {
                dialog = showSoccerBallProgressDialog(context, disposable, functionOnCancel)
            }
        }
        .doFinally {
            dialog.safeDismiss()
        }
}

fun <T> Single<T>.withProgressBallDialog(
    context: Context?,
//    title: String,
//    message: String,
    functionOnCancel: (() -> Unit)? = null
): Single<T> {

    var dialog: Dialog? = null
    var disposable: Disposable? = null
    return networkThread()
        .doOnSubscribe {
            AndroidSchedulers.mainThread().scheduleDirect {
                val cancelable = functionOnCancel != null

                require(context != null) { "context need  not null!!" }
                
                val dial = SoccerBallDialog(context)
                dial.setCanceledOnTouchOutside(cancelable)
                dial.setCancelable(cancelable)


                if (cancelable) {
                    dial.setOnCancelListener {
                        disposable?.dispose()
                        functionOnCancel?.invoke()
                    }
                }
                dialog = dial
                dial.show()
            }
            disposable = it
        }
        .doFinally {
            dialog?.safeDismiss()
        }
}


fun <T : ResponseHandleError> Single<T>.throwExceptionOnFailed(): Single<T> =
    map {
        if (it.isFailed()) throw ErrorCodeException(it)
        it
    }


object SingleEx {
    fun <T> safeCallable(supplier: () -> T): Single<T> {
        return Single.create<T> { emitter ->
            try {
                val value = supplier()

                if (emitter.isDisposed) {
                    return@create
                }

                emitter.onSuccess(value)

            } catch (t: Throwable) {
                if (emitter.isDisposed) {
                    return@create
                }

                emitter.onError(t)
            }
        }
    }
}