package ks.bs.mysoccerting.rx.caller

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import ks.bs.mysoccerting.exception.CanceledByUserException
import ks.bs.mysoccerting.exception.ErrorLogException
import ks.bs.mysoccerting.util.TLog
import java.io.Serializable

/**
 * Created by ian on 2018. 3. 15..
 */
class ActivityCaller : Activity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val newIntent = Intent(Intent.ACTION_MAIN)
        if (intent.extras != null)
            newIntent.putExtras(intent.extras!!)
        newIntent.addCategory(Intent.CATEGORY_LAUNCHER)
        newIntent.component = intent.getParcelableExtra(EXTRA_COMPONENT_NAME)

        newIntent.removeExtra(EXTRA_COMPONENT_NAME)
        newIntent.removeExtra(EXTRA_RECEIVER_NAME)

        startActivityForResult(newIntent, REQUEST_CALL_APP)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        val broadcastData = Intent(intent.getStringExtra(EXTRA_RECEIVER_NAME))

        when {
            requestCode != REQUEST_CALL_APP ->
                replaceResultException(
                    broadcastData,
                    data,
                    ErrorLogException("Unidentified onActivityResult for requestCode:$requestCode, reqsultCode:$resultCode, data:$data")
                )
            resultCode == RESULT_CANCELED ->
                replaceResultException(
                    broadcastData,
                    data,
                    CanceledByUserException()
                )
            resultCode != RESULT_OK ->
                replaceResultException(
                    broadcastData,
                    data,
                    ErrorLogException("resultCode is not RESULT_OK : $resultCode")
                )
            else ->
                if (data != null) {
                    TLog.i("data is Null ? -> ${data.extras == null}")
                    broadcastData.putExtras(data.extras!!)
                } else {
                    broadcastData.putExtras(Bundle())
                }
        }

        sendBroadcast(broadcastData)

        finish()
    }

    /**
     * TODO CompositeException 처리 필요할 수도 있음.
     */
    private fun replaceResultException(broadcastData: Intent, data: Intent?, serializable: Serializable) {
        if (data != null && data.hasExtra(RESULT_EXTRA_ERROR)) {
            broadcastData.putExtras(data.extras!!)
        } else {
            broadcastData.putExtra(RESULT_EXTRA_ERROR, serializable)
        }
    }

    companion object {
        val EXTRA_COMPONENT_NAME = "${this::class.java.canonicalName}.EXTRA_COMPONENT_NAME"
        val EXTRA_RECEIVER_NAME = "${this::class.java.canonicalName}.EXTRA_RECEIVER_NAME"
        val RESULT_EXTRA_ERROR = "${this::class.java.canonicalName}.EXTRA_RESULT_ERROR"

        private const val REQUEST_CALL_APP = 10001
    }
}