package ks.bs.mysoccerting.rx.caller

import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.os.Bundle
import ks.bs.mysoccerting.util.AppInstance
import java.lang.reflect.Type

/**
 * Bundle 주고 받기 전용
 */
class ActivityBundleObservable(
        context: Context?,
        componentName: ComponentName,
        bundle: Bundle?
) : ActivityObservable<Bundle>(context, componentName, bundle, Bundle::class.java) {
    constructor(context: Context?, clazz: Class<*>, bundle: Bundle?)
            : this(context, ComponentName(AppInstance.get().packageName, clazz.canonicalName!!), bundle)

    constructor(context: Context?, clazz: Class<*>, inputIntent: Intent) : this(context, clazz, inputIntent.extras)

    override fun putInputObject(inputIntent: Intent, inputObject: Any?): Intent {
        if (inputObject == null) {
            return inputIntent
        }

        inputIntent.putExtras(inputObject as Bundle)
        return inputIntent
    }

    override fun getOutputObject(resultIntent: Intent, classOfT: Type): Bundle? {
        if (resultIntent.extras != null) {
            return resultIntent.extras
        }
        return null
    }
}