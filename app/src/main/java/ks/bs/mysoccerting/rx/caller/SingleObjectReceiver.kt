package ks.bs.mysoccerting.rx.caller

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import io.reactivex.SingleEmitter
import ks.bs.mysoccerting.exception.CanceledByUserException
import java.lang.reflect.Type

class SingleObjectReceiver<TOUT>(val emitter: SingleEmitter<TOUT>, val classOfT: Type) : BroadcastReceiver() {
    val name = generateReceiverName()

    override fun onReceive(context: Context, resultIntent: Intent) {
        if (emitter.isDisposed) {
            return
        }

        try {
            onReceiveResult(resultIntent, emitter, classOfT)
        } catch (t: Throwable) {
            emitter.onError(t)
        }
    }

    private fun <TOUT> onReceiveResult(resultIntent: Intent, emitter: SingleEmitter<TOUT>, classOfT: Type) {
        if (resultIntent.hasExtra(ActivityCaller.RESULT_EXTRA_ERROR)) {
            emitter.onError(resultIntent.getSerializableExtra(ActivityCaller.RESULT_EXTRA_ERROR) as Throwable)
            return
        }

        val outputObject = getOutputObject<TOUT>(resultIntent, classOfT)

        if (outputObject == null) {
            emitter.onError(CanceledByUserException())
            return
        }

        emitter.onSuccess(outputObject)
    }

    fun <TOUT> getOutputObject(resultIntent: Intent, classOfT: Type): TOUT? {
        return ActivityObjectSingle.getObject<TOUT>(resultIntent, classOfT)
    }

    companion object {
        private var sequentialIndex: Int = 0
        private fun generateRandomKey(): String = "SEQ.${++sequentialIndex}.RAND.${(Math.random() * 10000).toInt()}"
        private fun generateReceiverName() = "${ActivityObjectSingle::class.java.canonicalName}.${generateRandomKey()}.RECEIVER"
    }
}