package ks.bs.mysoccerting.util

import ks.bs.mysoccerting.model.TimeValue
import ks.bs.mysoccerting.model.time.LocalTime
import java.text.SimpleDateFormat
import java.util.*


object DateEx {
    val sec = 60
    val min = 60
    val hour = 24
    val day = 30
    val month = 12


    @JvmStatic
    fun convertDate(timeStamp: Long) = Date(timeStamp)

    @JvmStatic
    fun convertDateString(timeStamp: Long): String {
        val date = convertDate(timeStamp)
        val dateFormat = SimpleDateFormat("작성시간: yy년 MM월 dd일 HH:mm:ss")
        return dateFormat.format(date)
    }

    @JvmStatic
    fun convertHopeDateString(timeStamp: Long): String {
        val date = convertDate(timeStamp)
        val dateFormat = SimpleDateFormat("희망일\n MM월 dd일")
        return dateFormat.format(date)
    }

    @JvmStatic
    fun convertBoardDateString(timeStamp: Long): String {
        val date = convertDate(timeStamp)
        val dateFormat = SimpleDateFormat("yy.MM.dd")
        return dateFormat.format(date)
    }

    @JvmStatic
    fun convertRecentDate(timeStamp: Long): String {
        val curTime = System.currentTimeMillis()
        var diffTime = (curTime - timeStamp) / 1000
        var msg: String? = null
        if (diffTime < TimeValue.SEC.value)
            msg = "방금 전"
        else {
            for (i in TimeValue.values()) {
                diffTime /= i.value
                if (diffTime < i.maximum) {
                    msg = "$diffTime${i.msg}"
                    break
                }
            }
        }
        return msg!!
    }


    @JvmStatic
    fun calculateTimeAgo(date: String): String {


        val curTime = System.currentTimeMillis()
        val inputTime = LocalTime(date).time
        val diffTime = (curTime - inputTime) / 1000

        return when {
            diffTime < sec -> "$diffTime 초전"
            diffTime / sec < min -> "${diffTime / sec} 분전 "
            diffTime / sec / min < hour -> "${diffTime / sec / min} 시간전 "
            diffTime / sec / min / hour < day -> "${diffTime / sec / min / hour} 일전 "
            else -> "${diffTime / sec / min / hour / day} 달전 "
        }
    }

    fun calculateDday(dateTime: Long): Long {
        val oneday = 86_400_000
        val currentTime = LocalTime().time
        val targetTime = dateTime
        val calDay = (targetTime - currentTime) / oneday
        return calDay + 1
    }
}

fun Long.toConvertDate() = Date(this)

fun Long.toConvertDateString(): String {
    val date = this.toConvertDate()
    val dateFormat = SimpleDateFormat("글쓴시간: yy년 MM월 dd일 HH:mm:ss")
    return dateFormat.format(date)
}

fun Long.toConvertHopeDateString(): String {
    val date = this.toConvertDate()
    val dateFormat = SimpleDateFormat("희망일\n MM월 dd일")
    return dateFormat.format(date)
}


