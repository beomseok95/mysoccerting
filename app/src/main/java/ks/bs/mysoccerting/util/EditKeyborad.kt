package ks.bs.mysoccerting.util

import android.content.Context
import androidx.databinding.ObservableField
import android.view.inputmethod.InputMethodManager
import android.widget.TextView

interface EditKeyborad {
    var searchText: ObservableField<String>

    val editTextActionListener: TextView.OnEditorActionListener

    fun hideKeyBorad(v: TextView?) {
        val imm = v?.context?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(v.windowToken, 0)
    }
}