package ks.bs.mysoccerting.util

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkInfo
import kr.nextm.lib.TToast

object NetworkStatus {

    private const val TYPE_WIFI = 1
    private const val TYPE_MOBILE = 2
    private const val TYPE_NOT_CONNECTED = 3

    private fun check(context: Context): Int {
        val cm: ConnectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val networkInfo: NetworkInfo? = cm.activeNetworkInfo ?: null

        if (networkInfo != null) {
            val type = networkInfo.type
            if (type == ConnectivityManager.TYPE_MOBILE) {//mobile check
                return TYPE_MOBILE
            } else if (type == ConnectivityManager.TYPE_WIFI) {//wifi check
                return TYPE_WIFI
            }
        }
        return TYPE_NOT_CONNECTED  //none
    }

    fun checkAndAlert(context: Context){
        when(check(context)){
            TYPE_NOT_CONNECTED->notConnectToast()
            else->{}
        }
    }

    private fun notConnectToast(){
        TToast.show("인터넷 연결상태를 확인해주세요.")
    }
}