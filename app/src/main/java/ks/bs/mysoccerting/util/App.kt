package ks.bs.mysoccerting.util

import android.app.Application
import android.content.Context
import android.content.pm.PackageManager
import android.util.Base64
import android.util.Log
import com.crashlytics.android.Crashlytics
import com.kakao.auth.KakaoSDK
import com.kakao.util.helper.Utility.getPackageInfo
import io.fabric.sdk.android.Fabric
import ks.bs.mysoccerting.adapter.KakaoSDKAdapter
import ks.bs.mysoccerting.test.koin.appModul
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin
import org.koin.core.logger.PrintLogger
import rx_activity_result2.RxActivityResult
import java.security.MessageDigest
import java.security.NoSuchAlgorithmException

class App : Application() {

    override fun onCreate() {
        super.onCreate()
        instance = this

        RxActivityResult.register(this)

        initKakaoAdapter()

        getKeyHash(this)

        Fabric.with(this, Crashlytics())

        startKoin {
            androidContext(this@App)
            modules(appModul)
            logger(PrintLogger())
        }
    }

    private fun initKakaoAdapter() {
        // Kakao Sdk 초기화
        KakaoSDK.init(KakaoSDKAdapter())
    }

    override fun onTerminate() {

        super.onTerminate()

        instance = null

    }


    companion object {
        private var instance: App? = null

        @JvmStatic
        fun getApplicationContext(): App {
            return instance ?: throw IllegalStateException("This Application does not inherit com.kakao.App")
        }

        fun getKeyHash(context: Context) {
            val TAG = "KAKAO_HASH_KEY "
            val packageInfo = getPackageInfo(context, PackageManager.GET_SIGNATURES) ?: return
            for (signature in packageInfo.signatures) {
                try {
                    val md = MessageDigest.getInstance("SHA")
                    md.update(signature.toByteArray())
                    Log.d(TAG, "key_hash=" + Base64.encodeToString(md.digest(), Base64.NO_WRAP))
                } catch (e: NoSuchAlgorithmException) {
                    Log.w(TAG, "Unable to get MessageDigest. signature=$signature", e)
                }

            }

        }
    }
}