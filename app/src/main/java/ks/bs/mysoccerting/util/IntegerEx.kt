package ks.bs.mysoccerting.util

import android.view.View
import ks.bs.mysoccerting.R
import ks.bs.mysoccerting.model.type.AgeType


object IntegerEx {

    @JvmStatic
    fun convertAgeStr(intValue: Int): String {
        return intValue.toString() + "대"
    }

    @JvmStatic
    fun visibilityCheck(input: String?) = if (input.isNullOrEmpty()) View.GONE else View.VISIBLE

}

fun Int.toAge() = this.toString() + "대"

fun isAgeAll(age: String?): Int {
    if(age.isNullOrEmpty()||age=="ALL")
        return 0

    return age.toInt()
}
fun isAgeAll(age : Int):String{
    if (age == 0)
        return "ALL"
    return age.toString()
}

fun Int.toColor() = when {
    this in 1..19 -> R.color.teens
    this in 20..29 -> R.color.twenties
    this in 30..39 -> R.color.thirties
    this in 40..49 -> R.color.forties
    this in 50..59 -> R.color.fifties
    this in 60..69 -> R.color.sixty
    this in 70..79 -> R.color.seventy
    else -> R.color.allAges
}

fun Int.convertStandardAge() =
    when {
        this in 1..19 -> "10대"
        this in 20..29 -> "20대"
        this in 30..39 -> "30대"
        this in 40..49 -> "40대"
        this in 50..59 -> "50대"
        this in 60..69 -> "60대"
        this in 70..79 -> "70대"
        else -> "ALL"
    }


fun Int.getAgeType() = when {
    this in 1..19 -> AgeType.TEENS
    this in 20..29 -> AgeType.TWENTIES
    this in 30..39 -> AgeType.THIRTIES
    this in 40..49 -> AgeType.FORTIES
    this in 50..59 -> AgeType.FIFTIES
    this in 60..69 -> AgeType.SIXTY
    this in 70..79 -> AgeType.SEVENTY
    else -> AgeType.ALL
}
