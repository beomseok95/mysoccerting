package ks.bs.mysoccerting.util

import android.util.DisplayMetrics
import android.util.TypedValue
import ks.bs.mysoccerting.exception.ErrorLogException
import org.jetbrains.anko.windowManager
import kotlin.math.min


fun Number.dpToPixels(): Int {

    AppInstance.get().resources.displayMetrics.setTo(fixDp())
    return TypedValue.applyDimension(
            TypedValue.COMPLEX_UNIT_DIP,
            toFloat(),
            AppInstance.get().resources.displayMetrics
    ).toInt()
}

private fun fixDp(): DisplayMetrics {
    val metrics = DisplayMetrics()
    AppInstance.get().windowManager.defaultDisplay.getMetrics(metrics)
    val baseSizeDp = 560

    val r1 = baseSizeDp / 160f

    val sizeDp = min(metrics.widthPixels, metrics.heightPixels)
    val r2 = sizeDp / metrics.densityDpi.toFloat()
//        if (Math.abs(r1 - r2) > 1f) {
    val properDpi = (sizeDp / r1).toInt()
    val properDensity = properDpi / 160f
    metrics.densityDpi = properDpi
    metrics.density = properDensity
    metrics.scaledDensity = properDensity
    return metrics
}

fun Number.dpToPixelsWithFixDp(): Int {

    return TypedValue.applyDimension(
        TypedValue.COMPLEX_UNIT_DIP,
        toFloat(),
        AppInstance.get().resources.displayMetrics
    ).toInt()
}

fun Number.pixelsToDp(): Float {
    return toFloat() / (AppInstance.get().resources.displayMetrics.densityDpi.toFloat() / DisplayMetrics.DENSITY_DEFAULT)
}


/**
 * 유저가 임의로 폰트 크기 변경하는 경우, UI 틀어지는 현상 발생하므로, SP 단위 사용 금지
 */
fun Number.spToPixels(): Int {
    throw ErrorLogException("유저가 임의로 폰트 크기 변경하는 경우, UI 틀어지는 현상 발생하므로, SP 단위 사용 금지")
}
