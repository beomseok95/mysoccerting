package ks.bs.mysoccerting.util

import android.app.AlarmManager
import android.app.Application
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import ks.bs.mysoccerting.scene.login.LoginActivity
import java.lang.ref.WeakReference


object AppInstance {

    var instance: WeakReference<Application>? = null

    fun get(): Application {
        if (instance == null || instance!!.get() == null) {
            instance = WeakReference(getApplication())
        }
        return instance!!.get()!!
    }

    fun getApplicationContext() = get().applicationContext


    private fun getApplication(): Application {
        val className = Class.forName("android.app.ActivityThread")
        val method = className.getMethod("currentApplication")
        val result = method.invoke(null, *arrayOfNulls<Any>(0))

        return result as Application
    }

    fun restart() {
        reserveStartSplashActivity(get())
        System.exit(0)
    }

    private fun reserveStartSplashActivity(context: Context) {
        val startingActivity = Intent(context, LoginActivity::class.java)
        val mPendingIntentId = 123456
        val mPendingIntent = PendingIntent.getActivity(
            context, mPendingIntentId, startingActivity,
            PendingIntent.FLAG_CANCEL_CURRENT
        )
        val mgr = context.getSystemService(Context.ALARM_SERVICE) as AlarmManager
        mgr.set(AlarmManager.RTC, System.currentTimeMillis() + 100, mPendingIntent)
    }
}