package ks.bs.mysoccerting.util

import com.ptrstovka.calendarview2.CalendarDay


fun CalendarDay.convertFormat(): String {
    return "${this.year}-${this.month+1}-${this.day}"
}

fun String.transToMonthAndDay(): String {
    val tempDate = this.split("-")
    return "${tempDate[1]}월${tempDate[2]}일"

}