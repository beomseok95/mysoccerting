package ks.bs.mysoccerting.util

import java.util.regex.Pattern

object ValidateCheck {
    val VALID_EMAIL_ADDRESS_REGEX =
        Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE)

    val VALID_PASSWOLD_REGEX_ALPHA_NUM = Pattern.compile("^[a-zA-Z0-9!@.#$%^&*?_~]{6,16}$")

    val VALID_NICKNAME_REGEX_NUM = Pattern.compile("^[가-힣-a-zA-Z0-9]{3,10}$")

    val VALID_CLUB_NAME_REGEX_NUM = Pattern.compile("^[가-힣-a-zA-Z0-9]{3,15}$")

    val VALIDE_CLUB_INTRO_REGEXT = Pattern.compile("^[ㄱ-ㅎ|가-힣-a-zA-Z0-9!@.#\$%^&*?_~ㅏ-ㅣ \n ]{10,60}$")


    fun validateEmail(emailStr: String): Boolean {
        val matcher = VALID_EMAIL_ADDRESS_REGEX.matcher(emailStr)
        return matcher.find()
    }

    fun validatePassword(pwStr: String): Boolean {
        val matcher = VALID_PASSWOLD_REGEX_ALPHA_NUM.matcher(pwStr)
        return matcher.matches()
    }

    fun validateNickName(nickNameStr: String): Boolean {
        val matcher = VALID_NICKNAME_REGEX_NUM.matcher(nickNameStr)
        return matcher.matches()
    }

    fun validateClubName(nickNameStr: String): Boolean {
        val matcher = VALID_CLUB_NAME_REGEX_NUM.matcher(nickNameStr)
        return matcher.matches()
    }

    fun validateClubIntro(nickNameStr: String): Boolean {
        val matcher = VALIDE_CLUB_INTRO_REGEXT.matcher(nickNameStr)
        return matcher.matches()
    }
}