package ks.bs.mysoccerting.util

import android.content.Context
import android.graphics.drawable.Drawable
import android.net.Uri

fun Uri.toDrawable(context: Context): Drawable {
    val inputStream = context.contentResolver.openInputStream(this)
    return Drawable.createFromStream(inputStream, this.toString())
}