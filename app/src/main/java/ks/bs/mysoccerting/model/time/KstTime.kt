package kr.nextm.api.model.type.time

import ks.bs.mysoccerting.model.time.LocalTime
import java.util.*

class KstTime(date: Date) : LocalTime(date, kstTimeZone) {

    constructor() : this(Calendar.getInstance().time)
    constructor(timeInMillis: Long) : this(Date(timeInMillis))
    constructor(input: String) : this(LocalTime.parseWithTimeZone(input, kstTimeZone))

    companion object {
        val kstTimeZone: TimeZone = TimeZone.getTimeZone("Asia/Seoul")
    }
}
