package ks.bs.mysoccerting.model.type

enum class AlarmType(val code: Int, val value: String, val text: String) {
    COMMENT(code = 0, value = "COMMENT", text = "님이 댓글을 작성하였습니다."),
    CLUB_SIGNUP(code = 1, value = "CLUB_SIGNUP", text = "님이 클럽가입을 신청하였습니다."),
    CLUB_SIGNUP_COMPLETE(code = 2, value = "CLUB_SIGNUP_COMPLETE", text = "클럽가입신청이 수락되었습니다."),
    CLUB_SIGNUP_REJECT(code = 3, value = "CLUB_SIGNUP_REJECT", text = "클럽가입신청이 거절되었습니다."),
    CLUB_MERCENARY_SIGNUP_REQUEST(code = 4, value = "CLUB_MERCENARY_SIGNUP_REQUEST", text = "님이 용병등록을 신청하였습니다."),
    CLUB_MERCENARY_SIGNUP_COMPLETE(code = 5, value = "CLUB_MERCENARY_SIGNUP_COMPLETE", text = "용병등록 신청이 수락되었습니다."),
    CLUB_MERCENARY_SIGNUP_REJECT(code = 6, value = "CLUB_MERCENARY_SIGNUP_REJECT", text = "용병등록 신청이 거절되었습니다."),
    MERCENARY_INVITATION_REQUEST(code = 7, value = "MERCENARY_INVITATION_REQUEST", text = "클럽 용병으로 초대하였습니다."),
    MERCENARY_INVITATION_COMPLETE(code = 8, value = "MERCENARY_INVITATION_COMPLETE", text = "클럽 용병초대를 수락하였습니다."),
    MATCHING_APPLY(code = 9, value = "MATCHING_APPLY", text = "매칭을 신청하였습니다."),
    MATCHING_COMPLETE(code = 10, value = "MATCHING_COMPLETE", text = "클럽과 매칭이 성사되었습니다."),
    MATCHING_REJECT(code = 11, value = "MATCHING_REJECT", text = "클럽과의 매칭이 거절되었습니다."),
    CHANGE_CLUB_GRADE(code = 12,value = "CHANGE_CLUB_GRADE",text = "클럽 계급이 변경되었습니다."),
    CLUB_FIRED(code = 13,value = "CLUB_FIRED",text = "클럽에서 추방되었습니다.")
}

