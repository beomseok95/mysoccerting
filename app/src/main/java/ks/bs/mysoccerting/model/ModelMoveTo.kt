package ks.bs.mysoccerting.model

data class ModelMoveTo(
    val locationcode: String = "",
    val clubPullPath: String? = null,
    val clubContentId: String? = null
)