package kr.nextm.api.model.type

import ks.bs.mysoccerting.model.OutputType

class JsonElementAsObject(obj: Any) : JsonElementAs(obj, OutputType.ToObject)
