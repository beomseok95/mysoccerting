package ks.bs.mysoccerting.model.type

enum class MovePageLocationType(val locationCode:String) {
    CLUB_DETAIL("club_detail"),
    PROFILE("profile")
}