package ks.bs.mysoccerting.model

import com.google.firebase.database.Exclude

data class ModelClub(
    var clubMarkImageUrl: String? = null,
    var clubName: String = "",
    var clubIntro: String = "",
    var clubMemberNumber: Int = 1,
    var activityArea: String = "",
    var activityAge: Int = 0,
    val clubMembers: MutableList<ModelMember> = mutableListOf(),
    var clubPositionList: HashMap<String, String>? = null,
    val createAt: String = "",
    var masterUid: String = "",
    val contentId: String = "",
    var membershipApplicationList: MutableList<ModelUser>? = null,
    val timeline: MutableMap<String, ModelClubTimeline>? = null,
    var mercenaryApplicationList: MutableList<ModelMember>? = mutableListOf(),
    val mercenaryMember: MutableList<ModelMember>? = mutableListOf(),
    var mercenaryNumber: Int = 0,
    var matchApplicationList: MutableList<MatchApplicationInfo>? = null,
    val matchUpList: MutableMap<String, MatchInfo>? = null,
    val assistantList: MutableList<String>? = null

) {
    data class ModelMember(
        var nickName: String = "",
        val uid: String = "",
        var memberImageUrl: String? = null,
        var position: String? = null,
        var age: String? = null,
        var gradeType: String = "",
        val mercenary: Boolean = false
    )

    data class MatchApplicationInfo(
        val clubMarkImageUrl: String? = null,
        val clubName: String = "",
        val hopeDate: String = "",
        val peopleNumber: String = "",
        val area: String = "",
        val clubMasterUid: String = "",
        @Exclude val _senderImageUrl: String? = null,
        @Exclude val _timeStamp: Long = 0,
        @Exclude val _senderUid: String = "",
        @Exclude val _senderNickName: String? = "",
        @Exclude val _senderClubUid: String? = null,
        @Exclude val _senderEmail: String? = "",
        @Exclude val _reciverUid: String = "",
        @Exclude val _message: String = "",
        @Exclude val _clubUid: String? = null,
        @Exclude val _matchInfo: MatchDetailInfo? = null
    ) : ModelAlaram(
        senderImageUrl = _senderImageUrl,
        timeStamp = _timeStamp,
        senderUid = _senderUid,
        senderNickName = _senderNickName,
        senderClubUid = _senderClubUid,
        senderEmail = _senderEmail,
        reciverUid = _reciverUid,
        message = _message,
        clubUid = _clubUid,
        matchInfo = _matchInfo
    )

    data class MatchInfo(
        val clubMarkImageUrl1: String? = null,
        val clubMarkImageUrl2: String? = null,
        val clubName1: String = "",
        val clubName2: String = "",
        val hopeDate: String = "",
        var hopeDateTimeStamp: Long = 0,
        val peopleNumber: String = "",
        val area: String = "",
        val clubMasterUid1: String = "",
        val clubMasterUid2: String = "",
        val clubUid1: String = "",
        val clubUid2: String = "",
        val matchingCompleteTime: Long = 0,
        val contentId: String = ""
    )


    data class ClubInfoChangeData(
        val club: String = "",
        val clubJonined: Boolean = false,
        val clubImageUrl: String? = null,
        val clubUid: String = "",
        val clubMasterUid: String = "",
        val clubIntro: String = "",
        val activityAge: Int = 0,
        val activityArea: String = "",
        val assitantList: MutableList<String>? = null
    )
}


