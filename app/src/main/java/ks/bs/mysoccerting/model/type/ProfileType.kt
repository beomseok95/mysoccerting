package ks.bs.mysoccerting.model.type

import android.graphics.drawable.Drawable
import com.google.gson.annotations.SerializedName
import kr.nextm.lib.getDrawable
import ks.bs.mysoccerting.R

enum class ProfileType(val code:Int,val image: Drawable) {
    @SerializedName("0")
    TYPE0(0, R.drawable.ic_profile_default1.getDrawable()),
    @SerializedName("1")
    TYPE1(1, R.drawable.ic_profile_default2.getDrawable()),
    @SerializedName("2")
    TYPE2(2, R.drawable.ic_profile_default3.getDrawable())
}