package ks.bs.mysoccerting.model

import ks.bs.mysoccerting.R

enum class PositionColor(var position: String, var bgColor: Int) {
    FW("FW", R.drawable.bg_position_fw),
    MF("MF", R.drawable.bg_position_mf),
    DF("DF", R.drawable.bg_position_df),
    GK("GK", R.drawable.bg_position_gk),
    ALL("ALL", R.drawable.bg_position_all);

}
