package ks.bs.mysoccerting.model

import androidx.databinding.ObservableArrayMap
import java.util.*

object ModelPosition {
    enum class Pos(pos: String) {
        FW("A"),
        AM("B"),
        CM("C"),
        DM("D"),
        DF("E"),
        GK("F")
    }

    var tagToTextList = ObservableArrayMap<String, List<String>>()
    var textToTag = ObservableArrayMap<String, String>()
    var posALL = listOf("AFW", "AAM", "ACM", "ADM", "ADF")
    fun getPosList(): ObservableArrayMap<String, Boolean> {
        val posList = ObservableArrayMap<String, Boolean>()
        val posFW = listOf("LF", "LCF", "CF", "RCF", "RF")
        val posAM = listOf("LW", "LAM", "AM", "RAM", "RW")
        val posCM = listOf("LM", "LCM", "CM", "RCM", "RM")
        val posDM = listOf("LWB", "LDM", "DM", "RDM", "RWB")
        val posDF = listOf("LB", "LCB", "CB", "RCB", "RB")
        val posGK = "GK"
        val posAL = "ALL"

        val posArr = listOf(posFW, posAM, posCM, posDM, posDF)

        for (i in 0..4) tagToTextList[posALL[i]] = posArr[i]
        for (i in posALL) {
            posList[i] = false
            textToTag[i] = i
        }
        for (i in posALL)
            for (j in (tagToTextList[i])!!) {
                posList[j] = false
            }
        for (i in posALL)
            for (j in (tagToTextList[i])!!) {
                textToTag[j] = i
            }

        posList[posGK] = false
        posList[posAL] = false
        posList["direction"] = false
        textToTag["ALL"] = "ALL"
        textToTag["GK"] = "AGK"
        return posList
    }


    fun isPressedAll(posList: ObservableArrayMap<String, Boolean>) {
        posList["ALL"] =
            posList["AFW"]!! && posList["AAM"]!! && posList["ACM"]!! && posList["ADM"]!! && posList["ADF"]!! && posList["GK"]!!
    }

    fun isPressedLine(posList: ObservableArrayMap<String, Boolean>, tag: String) {
        if (tag.isNotEmpty()) {
            var isCheckedAll = true
            for (i in 0..4) {
                isCheckedAll = isCheckedAll && posList[(tagToTextList[tag])!![i]]!!
            }
            posList[tag] = isCheckedAll
        }
    }

    fun pressAll(posList: ObservableArrayMap<String, Boolean>) {
        for (pos in posList) posList[pos.key] = posList["ALL"]!!
    }

    fun pressLine(posList: ObservableArrayMap<String, Boolean>, tag: String) {
        posList[tag] = !(posList[tag])!!
        for (i in (tagToTextList[tag]!!)) posList[i] = posList[tag]
    }

    fun getFilterList(posList: HashMap<String, Boolean>): MutableList<String> {
        val list = mutableListOf<String>()

        if (posList["ALL"]!!) {
            list.add("ALL")
        } else {
            for (isChecked in posList) {
                if (isChecked.value && !list.contains(textToTag[isChecked.key]!!.substring(1))) {
                    list.add(textToTag[isChecked.key]!!.substring(1))
                }

            }
            Collections.sort(list, object : Comparator<String> {
                override fun compare(pos1: String, pos2: String): Int {
                    return Pos.valueOf(pos1).compareTo(Pos.valueOf(pos2))
                }
            })
            val mfList = listOf("AM", "CM", "DM")
            if (list.containsAll(mfList)) {
                list.removeAll(mfList)
                list.add(1, "MF")
            }
        }
        return list
    }

    fun observableToHash(posList: ObservableArrayMap<String, Boolean>): HashMap<String, Boolean> {
        val hashList = HashMap<String, Boolean>()
        posList.forEach {
            hashList[it.key] = it.value
        }
        return hashList
    }

    fun hashToObservable(posList: HashMap<String, Boolean>): ObservableArrayMap<String, Boolean> {
        val observableList = ObservableArrayMap<String, Boolean>()
        posList.forEach {
            observableList[it.key] = it.value
        }
        return observableList
    }
}