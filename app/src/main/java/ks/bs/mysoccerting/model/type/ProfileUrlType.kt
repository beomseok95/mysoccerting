package ks.bs.mysoccerting.model.type

import android.graphics.drawable.Drawable
import com.google.gson.annotations.SerializedName
import kr.nextm.lib.getDrawable
import ks.bs.mysoccerting.R

enum class ProfileUrlType(val code:Int, val url: String) {
    @SerializedName("0")
    TYPE0(0, "https://firebasestorage.googleapis.com/v0/b/mysoccerting.appspot.com/o/userProfileImages%2Fic_profile_default1.png?alt=media&token=9084cd7f-ada9-4d07-beab-5c7b27c19d67"),
    @SerializedName("1")
    TYPE1(1, "https://firebasestorage.googleapis.com/v0/b/mysoccerting.appspot.com/o/userProfileImages%2Fic_profile_default2.png?alt=media&token=eda2fcc9-5b28-442c-aba1-aab6caaf465d"),
    @SerializedName("2")
    TYPE2(2, "https://firebasestorage.googleapis.com/v0/b/mysoccerting.appspot.com/o/userProfileImages%2Fic_profile_default3.png?alt=media&token=5c6abf07-cc1c-4a4c-9224-239a910ccd73")
}