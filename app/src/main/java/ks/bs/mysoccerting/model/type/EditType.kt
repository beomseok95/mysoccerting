package ks.bs.mysoccerting.model.type

enum class EditType(val hint: String,val info :String,val tag:String) {
    ALIAS("별명을 입력해주세요","별명","ALIAS"),
    AGE("나이을 입력해주세요","나이","AGE"),
    SEX("성별을 입력해주세요","성별","SEX"),
    CLUB_NAME("클럽 이름을 입력해주세요","클럽 이름","CLUB_NAME"),
    ACTIVITY_AGE("활동 나이대를 입력해주세요","활동 나이대","ACTIVITY_AGE"),
    INTRO("소개글을 입력해주세요","소개글","INTRO")
}