package ks.bs.mysoccerting.model.type

import android.graphics.drawable.Drawable
import android.provider.ContactsContract
import kr.nextm.lib.getDrawable
import ks.bs.mysoccerting.R

enum class ClubGradeType(val grade: Int, val gradeName: String, val imageDrawable: Drawable) {
    COACH(3, "감독", R.drawable.ic_grade_coach.getDrawable()),
    ASSISTANT(2, "코치", R.drawable.ic_grade_assistant.getDrawable()),
    PLAYER(1, "선수", R.drawable.ic_grade_player.getDrawable()),
    MERCENARY(0, "용병", R.drawable.bg_white.getDrawable());

    companion object {
        fun gradeToDrawable(grade: Int = 0, gradeName: String? = ""): Drawable {
            val drawable = R.drawable.bg_white.getDrawable()
            values().forEach {
                if (it.grade == grade || it.gradeName == gradeName) {
                    return it.imageDrawable
                }
            }
            return drawable
        }

        fun gradeTypeToDrawable(gradeType: String = ""): Drawable {
            var drawable = R.drawable.bg_white.getDrawable()
            if (gradeType.isNotEmpty())
                drawable = valueOf(gradeType).imageDrawable
            return drawable
        }

        fun gradeTypeToName(gradeType: String): String {
            if (gradeType.isNotEmpty())
                valueOf(gradeType).name
            return ""
        }

        fun toGrade(gradeName: String = "", imageDrawable: Drawable? = null): String {
            var gradeType = "PLAYER"
            values().forEach {
                if (it.gradeName == gradeName || it.imageDrawable == imageDrawable)
                    gradeType = it.name
            }
            return gradeType
        }

    }

    data class ModelGradeChange(
        val isDelegated: Boolean? = null,
        val isMercenary: Boolean? = null,
        val count: Int? = null,
        val gradeType: String? = null,
        val nickName: String? = null
    )
}