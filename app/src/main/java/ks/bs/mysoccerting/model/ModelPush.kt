package ks.bs.mysoccerting.model


data class ModelPush(
    var to: String? = null,                             //PushToken을 입력하는 부분 푸시를 받는 사용자
    var notification: Notification? = Notification(),   //백그라운드 푸시 호출하는 변수
    val data: Data? = null
) {
    data class Notification(
        var body: String? = null,
        var title: String? = null
    )

    data class Data(
        val uid: String = ""
    )

    data class Response(
        val canonical_ids: Int = 0,
        val failure: Int = 0,
        val multicast_id: Long = 0,
        val results: List<Result> = listOf(),
        val success: Int = 0
    )

    data class Result(
        val message_id: String = ""
    )
}
