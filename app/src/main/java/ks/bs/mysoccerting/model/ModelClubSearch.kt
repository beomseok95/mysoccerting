package ks.bs.mysoccerting.model

data class ModelClubSearch(
    val clubImageUrl: String? = "",
    val clubName: String? = "",
    val hopeDate: String? = "",
    val content: String = "",
    val position: String? = null,
    val timestamp: Long = 0,
    val uid: String = "",
    var commentcount: Int = 0,
    val hopeArea: String = "",
    val contentId: String = "",
    var clubMasterUid: String = ""
) : ModelBase()