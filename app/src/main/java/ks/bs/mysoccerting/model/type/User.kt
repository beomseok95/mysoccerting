package ks.bs.mysoccerting.model.type

enum class User(info: String) {
    NEW_USER("새로운유저"),
    ALREADY_REGISTERD("기등록유저"),
    DEFAULT("디폴트")
    ;
}