package ks.bs.mysoccerting.model


enum class OutputType {
    ToString,
    ToObject
}