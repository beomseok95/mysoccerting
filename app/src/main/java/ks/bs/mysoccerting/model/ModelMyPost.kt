package ks.bs.mysoccerting.model

import ks.bs.mysoccerting.model.type.PostType

data class ModelMyPost(
    val contentId: String="",
    val type: PostType=PostType.NONE,
    val mPostPath: String="",
    val content : String="",
    val timestamp: Long=0
)