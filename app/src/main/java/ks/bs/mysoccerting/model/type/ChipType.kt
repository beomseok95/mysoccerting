package ks.bs.mysoccerting.model.type

enum class ChipType(val tag: String) {
    POSITION("position"),
    AREA("area"),
    AGE("age")
}