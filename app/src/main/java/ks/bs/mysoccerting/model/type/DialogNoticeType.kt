package ks.bs.mysoccerting.model.type

enum class DialogNoticeType {
    LOGIN,
    NEEDCLUB,
    AREADY_CLUB,
    NEED_GRADE,
    NEED_GRADE_INVITATION;
}