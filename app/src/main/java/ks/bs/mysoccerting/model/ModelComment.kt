package ks.bs.mysoccerting.model

data class ModelComment(
    val writerProfileUrl: String? = null,
    val writerNickName: String? = null,
    var content: String? = null,
    var writerEmail: String? = null,
    var writerUid: String? = null,
    var timestamp: Long = 0,
    val contentId: String? = null,// documentPath,
    val postConetntId: String? = null,
    val clubUid: String = "",
    var isWriter: Boolean = false
)