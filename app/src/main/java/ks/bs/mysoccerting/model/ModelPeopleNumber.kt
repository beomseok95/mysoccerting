package ks.bs.mysoccerting.model

import android.widget.ArrayAdapter
import androidx.databinding.ObservableField

data class ModelPeopleNumber(
    var id: Int = 0,
    var pos: ObservableField<Int> = ObservableField(6),
    var peopleNumber: ObservableField<String> = ObservableField("11 vs 11"),
    var numberList: MutableList<String> = mutableListOf(),
    var adapter: ArrayAdapter<String>
)