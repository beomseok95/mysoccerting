package ks.bs.mysoccerting.model

data class ModelReport(
    val title: String = "",
    val content: String = "",
    val date: String = "",
    val uid: String? = null,
    val contentId: String = ""
)

