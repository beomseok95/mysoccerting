package ks.bs.mysoccerting.model.type

enum class YesNo {
    Y,
    N;

    fun toBoolean() = this == Y

    fun isYes() = toBoolean()
    fun isNo() = !toBoolean()

    fun isPositive() = toBoolean()
    fun isNegative() = !toBoolean()

    companion object {
        fun of(v: Boolean): YesNo = if (v) Y else N

    }
}

fun Boolean.yesNo(): YesNo = if (this) YesNo.Y else YesNo.N
