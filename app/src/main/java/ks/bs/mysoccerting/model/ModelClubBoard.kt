package ks.bs.mysoccerting.model

data class ModelClubBoard(
    var title: String = "",
    var profileImageUrl: String? = "",
    var nickName: String = "",
    var content: String = "",
    var timestamp: Long = 0,
    var comments: HashMap<String, ModelComment>? = null,
    var contentId: String = "",
    var uid: String = "",
    var commentcount: Int = 0,
    val clubUid: String = ""
)