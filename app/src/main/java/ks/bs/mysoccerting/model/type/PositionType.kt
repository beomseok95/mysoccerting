package ks.bs.mysoccerting.model.type

import android.graphics.drawable.Drawable
import kr.nextm.lib.getDrawable
import ks.bs.mysoccerting.R

enum class PositionType(val code: String, val iconDrawable: Drawable, val bgColor: Int) {
    FW("FW", R.drawable.ic_ball_fw.getDrawable(), R.color.FW),
    MF("MF", R.drawable.ic_ball_mf.getDrawable(), R.color.MF),
    AM("AM", R.drawable.ic_ball_mf.getDrawable(), R.color.MF),
    CM("CM", R.drawable.ic_ball_mf.getDrawable(), R.color.MF),
    DM("DM", R.drawable.ic_ball_mf.getDrawable(),  R.color.MF),
    DF("DF", R.drawable.ic_ball_df.getDrawable(),  R.color.DF),
    GK("GK", R.drawable.ic_ball_gk.getDrawable(), R.color.GK),
    ALL("ALL", R.drawable.ic_ball_all.getDrawable(),  R.color.ALL)
}