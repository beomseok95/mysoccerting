package kr.nextm.api.model.type.time


import ks.bs.mysoccerting.model.time.LocalTime
import java.util.*

class GmtTime(date: Date) : LocalTime(date, gmtTimeZone) {

    constructor() : this(Calendar.getInstance().time)
    constructor(input: String) : this(LocalTime.parseWithTimeZone(input, gmtTimeZone))

    companion object {
        val gmtTimeZone: TimeZone = TimeZone.getTimeZone("GMT")
    }

}
