package kr.nextm.api.model.type

import ks.bs.mysoccerting.model.OutputType
import ks.bs.mysoccerting.util.GsonHelper
import java.io.Serializable

open class JsonElementAs(val obj: Any, val outputType: OutputType) : Serializable {
    fun <T> get(): T = obj as T

    override fun toString() = GsonHelper.toJson(obj)
}
