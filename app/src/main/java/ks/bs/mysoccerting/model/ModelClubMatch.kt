package ks.bs.mysoccerting.model

data class ModelClubMatch(
    val clubImageUrl: String? = null,
    val clubName: String? = null,
    val content: String = "",
    val timestamp: Long = 0,
    val uid: String = "",
    val contentId: String = "",
    var commentcount: Int = 0,
    val hopeDate: String = "",
    val matchHopeDateTimeStamp: Long = 0,
    val matcingArea: String = "",
    val matchingPeopleNumbewr: String? = null,
    val age: String? = "",
    val clubUid: String = ""
) {
    data class ApplyResult(
        val ok: Boolean = false,
        val duplicate: Boolean = false,
        val over: Boolean = false,
        val cancel: Boolean = false
    )
}