package ks.bs.mysoccerting.model

open class ModelAlaram(
    open val senderImageUrl: String? = null,
    open val timeStamp: Long = 0,
    open val alaramTypeCode: Int = 0,
    open val commentCallType: String? = null,
    /** DetailItemActivity CallTag*/
    open val postPullPath: String = "",
    open val senderUid: String = "",
    open val senderNickName: String? = "",
    open val senderClubUid: String? = null,
    open val senderEmail: String? = "",
    open val reciverUid: String = "",
    open val message: String = "",
    open val divider: Divider? = null,
    open val clubUid: String? = null,
    open val matchInfo: MatchDetailInfo? = null
) {
    data class Divider(
        val text: String = "",
        val isDividerShowing: Boolean = false
    )

    data class MatchDetailInfo(
        val hopeDate: String = "",
        val area: String = "",
        val peopleNumber: String = "",
        val hopeDateTimeStamp: Long = 0
    )


    companion object {
        fun dividerModelCreate(text: String) = ModelAlaram(
            divider = Divider(
                text = text,
                isDividerShowing = true
            )
        )
    }
}