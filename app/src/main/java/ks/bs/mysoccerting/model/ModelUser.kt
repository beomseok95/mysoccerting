package ks.bs.mysoccerting.model

data class ModelUser(
    var uid: String? = null,
    var nickName: String? = null,
    var age: Int? = null,
    var email: String? = null,
    var area: String? = null,
    var sex: String? = null,
    var profileImageUrl: String? = null,
    var position: List<String>? = null,
    var club: String? = null,
    var clubJonined: Boolean = false,
    var clubImageUrl: String? = null,
    var clubUid: String? = null,
    var clubMasterUid: String? = null,
    var clubAssistantList: MutableList<String>? = null,
    var mercenaryClubInfo: MutableList<ModelMercenaryClub>? = null
) {
    data class ModelMercenaryClub(
        var clubMarkImageUrl: String? = null,
        var clubName: String = "",
        val masterUid: String = "",
        val contentId: String = ""
    )

}