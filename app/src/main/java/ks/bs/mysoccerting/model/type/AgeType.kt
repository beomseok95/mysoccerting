package ks.bs.mysoccerting.model.type

import android.graphics.drawable.Drawable
import kr.nextm.lib.getColor
import kr.nextm.lib.getDrawable
import ks.bs.mysoccerting.R

enum class AgeType(val age: String,val color: Int,val icon:Drawable) {
    TEENS("teens", R.color.teens.getColor(),R.drawable.circle_teen.getDrawable()),
    TWENTIES("twenties", R.color.twenties.getColor(),R.drawable.circle_twenties.getDrawable()),
    THIRTIES("thirties", R.color.thirties.getColor(),R.drawable.circle_thirties.getDrawable()),
    FORTIES("forties", R.color.forties.getColor(),R.drawable.circle_forties.getDrawable()),
    FIFTIES("fifties", R.color.fifties.getColor(),R.drawable.circle_fifties.getDrawable()),
    SIXTY("sixty", R.color.sixty.getColor(),R.drawable.circle_sixty.getDrawable()),
    SEVENTY("seventy", R.color.seventy.getColor(),R.drawable.circle_seventy.getDrawable()),
    ALL("all", R.color.allAges.getColor(),R.drawable.circle_all.getDrawable()),
}