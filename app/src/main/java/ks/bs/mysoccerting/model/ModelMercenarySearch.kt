package ks.bs.mysoccerting.model

data class ModelMercenarySearch(
    val profileImageUrl: String? = "",
    val nickName: String? = "",
    val age: String = "20대",
    val hopeDate: String = "",
    val content: String = "",
    val position: String? = null,
    val uid: String = "",
    var commentcount: Int = 0,
    val hopeArea: String = "",
    val contentId: String = "",
    val timestamp: Long = 0,
    val clubMasterUid: String = ""

) : ModelBase()