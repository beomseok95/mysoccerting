package ks.bs.mysoccerting.model.type

import ks.bs.mysoccerting.firebase.FirebaseUrls

enum class PostType(val type: String, val path: String) {
    CLUB_MATCH("clubMatch", FirebaseUrls.clubMatch),
    CLUB_SEARCH("clubSearch", FirebaseUrls.clubSeach),
    MERCENARY_SEARCH("mercenarySearch", FirebaseUrls.mercenarySeach),
    CLUB_BOARD("board", FirebaseUrls.clubBoard),
    MY_POST("myPost",FirebaseUrls.myPost),
    NONE("none","none")
}