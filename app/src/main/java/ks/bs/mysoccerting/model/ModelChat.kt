package ks.bs.mysoccerting.model

data class ModelChat(
    val uid: String = "",
    val content: String = "",
    val nickName: String = "",
    val profileImageUrl: String? = null,
    val clubName: String? = null,
    var timeStamp: Long = 0
)