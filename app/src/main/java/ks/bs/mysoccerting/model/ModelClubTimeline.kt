package ks.bs.mysoccerting.model

import ks.bs.mysoccerting.model.type.TimelineType

data class ModelClubTimeline(
    val nickName: String = "",
    val uid: String = "",
    val contentUid: String = "",
    val memberImageUrl: String? = null,
    val content: String? = null,
    var timestamp: Long? = 0,
    val type: TimelineType = TimelineType.NONE,
    val contentPath: String? = null
)