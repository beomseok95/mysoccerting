package ks.bs.mysoccerting.model

data class ModelEditData(
    val tag: String = "",
    val data: String = "",
    val hint: String = "",
    val isAge: Boolean = false
)