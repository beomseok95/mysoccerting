package ks.bs.mysoccerting.model

enum class CollectionPath(val collectionPath: String) {
    CLUB_MATCH("clubMatch"),
    MERCENARY_SEARCH("mercenarySearch"),
    CLUB_SEARCH("clubSearch"),
    COMMENT("comments"),
    CLUB("list"),
    CLUB_TIMELINE("timeline"),
    CLUB_NAMES("names"),
    CLUB_BOARD("board"),
    USERS("users/info"),
    MY_POST("users/myPost"),
    ANNOYMOUS("etc/annonymousUser"),
    ALARM("alarm"),
    CHAT("chat"),
    MATCH_CHAT("matchChat"),
    REPORT("etc/service/report/bug"),
    SUGGESTIONS("etc/service/report/suggestions")
}