package ks.bs.mysoccerting.model.type

import com.google.gson.annotations.SerializedName

enum class TimelineType(val code: Int, val value: String, val postText: String) {
    @SerializedName("0")
    NONE(0, "NONE", ""),
    @SerializedName("1")
    POST(1, "POST", "님이 게시물을 작성하였습니다."),
    @SerializedName("2")
    APPROVE_SIGN_UP(2,"APPROVE_SIGN_UP","님이 클럽에 가입하였습니다."),
    @SerializedName("3")
    CLUB_RETIREMENT(3,"CLUB_RETIREMENT","님이 클럽을 탈퇴하였습니다."),
    @SerializedName("4")
    MERCENARY_SIGN_UP(4,"MERCENARY_SIGN_UP","님이 용병으로 등록되었습니다."),
    @SerializedName("5")
    MERCENARY_RETIREMENT(5,"MERCENARY_SIGN_UP","님이 용병을 탈퇴하였습니다."),
    @SerializedName("6")
    MATCHING_COMPLETE(6,"MATCHING_COMPLETE","클럽과 매칭이 성사되었습니다."),

}