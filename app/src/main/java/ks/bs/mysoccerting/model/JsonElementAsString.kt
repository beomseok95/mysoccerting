package kr.nextm.api.model.type

import ks.bs.mysoccerting.model.OutputType

class JsonElementAsString(obj: Any) : JsonElementAs(obj, OutputType.ToString)
