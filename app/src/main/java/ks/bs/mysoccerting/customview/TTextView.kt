package ks.bs.mysoccerting.customview

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.LinearLayout
import kotlinx.android.synthetic.main.textview_position.view.*
import ks.bs.mysoccerting.R

/**
 * @JvmOverloads constructor
 *생성자의 parameter가 기본값으로 대체하도록 컴파일러에 지시한다는 의미
 */

class TTextView @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : LinearLayout(context, attrs, defStyleAttr) {
    init {
        LayoutInflater.from(context).inflate(R.layout.textview_position, this, true)
        imageView.text = "ALL"
        imageView.setBackgroundResource(R.drawable.bg_position_all)
    }
}






