package ks.bs.mysoccerting.customview.dialog

import android.app.Application
import android.app.Dialog
import android.content.Context
import android.content.ContextWrapper
import android.content.DialogInterface
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.FragmentManager
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.subjects.SingleSubject
import ks.bs.mysoccerting.R
import ks.bs.mysoccerting.exception.CanceledByUserException
import ks.bs.mysoccerting.rx.rxresult.KEY_FOR_JSON_OBJECT
import ks.bs.mysoccerting.rx.rxresult.getObject
import ks.bs.mysoccerting.rx.rxresult.putObject
import ks.bs.mysoccerting.rx.showProgressDialog
import ks.bs.mysoccerting.scene.base.FragmentBaseViewModel

abstract class RxDialogFragment<T> : DialogFragment(), FragmentBaseViewModel.BaseVmListener {
    private val subject = SingleSubject.create<T>()
    private val disposableOnDestroy = CompositeDisposable()
    private var isEnter = false

    final override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val window = dialog?.window
        requireNotNull(window) {
            "RxDialogFragment: Window Not Attached"
        }
        val parent = window.decorView as ViewGroup
        val view = onCreateCustomView(inflater, parent)

        val param = view.layoutParams ?: ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT
        )

        /**
         *  onCreateView 이후 view의 LayoutParam.width,height은 항상 MATCH_PARENT로 고정된다.
         *  이 시점에만 view의 Layout Size가 보존되므로 해당값으로 dialog window의 크기를 지정한다.
         */
        window.setLayout(param.width, param.height)
        return view

    }

    /**
     *  기존의 DialogFragment.onCreateView는 container 가 항상 null 로 들어온다.
     *  parent가 null인 inflate 메소드는 view의 layoutParam을 null로 반환해 결과적으로 뷰의 크기가 무시되므로,
     *  onCreateView 시점에 항상 존재하는 dialog의 decorView를 container로 넘겨 뷰의 layoutParam을 보존한다.
     */
    protected abstract fun onCreateCustomView(inflater: LayoutInflater, container: ViewGroup): View

    /**
     *  NO_TITLE style 미 적용시 다이얼로그가 잘리는 현상이 있다.
     */
    final override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return Dialog(requireContext(), R.style.RxDialog)
    }

    override fun onCancel(dialog: DialogInterface) {
        super.onCancel(dialog)
        subject.onError(CanceledByUserException())
    }

    override fun onStart() {
        super.onStart()
        dialog?.window?.decorView?.setPadding(
                0,
                0,
                0,
                0
        )

        if (!isEnter) {
            isEnter = true
            onEnter()
        }
    }

    open fun onEnter() {}

    override fun getContext(): Context {
        val context = super.getContext()
        require(context != null) {
            "RxDialogFragment context is null"
        }
        return context
    }

    override fun onDestroy() {
        super.onDestroy()
        disposableOnDestroy.clear()
    }

    fun putObject(obj: Any): RxDialogFragment<T> {
        val bundle = Bundle()
        bundle.putObject(obj)
        arguments = bundle
        return this
    }

    fun toSingle(context: Context): Single<T> {
        return subject
                .doOnSubscribe {
                    AndroidSchedulers.mainThread().scheduleDirect {
                        show(context)
                    }
                }
                .doFinally {
                    if (dialog?.isShowing == true) {
                        dismiss()
                    }
                }
    }

    protected inline fun <reified R> parseObject(): R {
        val item: R? = arguments?.getObject(R::class.java)
        require(item != null) {
            "RxDialogFragment parseObject Failed.\n" +
                    "targetJson : ${arguments?.getString(KEY_FOR_JSON_OBJECT)}"
        }
        return item
    }

    private fun show(context: Context) {
        show(getFragmentManager(context), javaClass.canonicalName)
    }

    protected fun finishWithResponse(data: T) {
        if (subject.hasObservers())
            subject.onSuccess(data)
    }

    protected fun finishWithException(e: Throwable) {
        if (subject.hasObservers())
            subject.onError(e)
    }

    private fun getFragmentManager(context: Context): FragmentManager {
        return when (context) {
            is FragmentActivity -> context.supportFragmentManager
            is ContextWrapper -> getFragmentManager(context.baseContext)
            is Application -> throw IllegalStateException("Application Context로 Dialog Fragment를 생성할수 없습니다")
            else -> throw IllegalStateException("알수없는 타입 : ${context.javaClass.canonicalName}")
        }

    }

    protected fun Disposable.disposeOnDestroy() {
        disposableOnDestroy.add(this)
    }

    override fun createDialogForProgress(request: FragmentBaseViewModel.ProgressBarOwner.Request): Dialog {
        return showProgressDialog(requireContext(), request.disposable, request.functionOnCancel)
    }
}