package ks.bs.mysoccerting.customview.dialog

import android.app.Activity
import android.app.Dialog
import android.content.Context
import ks.bs.mysoccerting.BuildConfig
import ks.bs.mysoccerting.R
import ks.bs.mysoccerting.exception.CanceledByUserException
import ks.bs.mysoccerting.exception.ErrorCodeException
import ks.bs.mysoccerting.exception.ErrorLogException
import ks.bs.mysoccerting.exception.toDialogMessage
import ks.bs.mysoccerting.util.TLog
import ks.bs.mysoccerting.util.getString


object TDialog {

    fun show(
        context: Context,
        title: CharSequence,
        message: CharSequence,
        functionOnDismiss: () -> Unit = {}
    ): Dialog {
        return Builder(context, title.toString())
            .setMessage(message.toString())
            .isColored(true)
            .show(functionOnDismiss)
    }

    fun show(
        context: Context,
        message: CharSequence,
        functionOnDismiss: () -> Unit = {}
    ): Dialog {
        return Builder(context)
            .setMessage(message.toString())
            .isColored(true)
            .show(functionOnDismiss)
    }

    fun show(
        context: Context,
        e: ErrorCodeException,
        functionOnDismiss: () -> Unit = {}
    ): Dialog {
        TLog.e(e)
        return show(
            context,
            e.code,
            e.message,
            functionOnDismiss
        )
    }

    fun show(
        context: Context,
        t: Throwable,
        functionOnDismiss: () -> Unit = {}
    ) {
        TLog.e(t)

        if (t is CanceledByUserException) {
            functionOnDismiss()
            return
        }

        //Crashlytics.logException(t)

        show(
            context,
            "${getTitle(t)} <small><small>(${BuildConfig.BUILD_TYPE}-${BuildConfig.VERSION_CODE})</small></small>",
            t.toDialogMessage(),
            functionOnDismiss
        )
    }

    private fun getTitle(t: Throwable) =
        if (t is ErrorLogException) t.title else R.string.default_error_title.getString()

    fun showAndFinish(
        activity: Activity?,
        message: CharSequence
    ) {
        show(activity!!, message) {
            activity.finish()
        }
    }

    fun showAndFinish(
        activity: Activity?,
        message: Throwable
    ) {
        TLog.e(message)
        show(activity!!, message) {
            activity.finish()
        }
    }

    class Builder(context: Context, title: String = "") : DialogObservable.Builder(context, title)

}