package ks.bs.mysoccerting.customview.dialog

import android.graphics.Color
import android.graphics.drawable.ShapeDrawable
import android.graphics.drawable.StateListDrawable
import android.graphics.drawable.shapes.RoundRectShape
import androidx.core.content.ContextCompat
import ks.bs.mysoccerting.R
import ks.bs.mysoccerting.util.AppInstance


class DialogButtonDrawable(isLeftEnd: Boolean, isRightEnd: Boolean) : StateListDrawable() {

    init {
        val left = if (isLeftEnd) 5f else 0f
        val right = if (isRightEnd) 5f else 0f
        val shape = RoundRectShape(
            floatArrayOf(
                0f, 0f,
                0f, 0f,
                right, right,
                left, left
            ), null, null
        )
        val normal = ShapeDrawable(shape)
        normal.paint.color = Color.WHITE
        val pressed = ShapeDrawable(shape)
        pressed.paint.color = ContextCompat.getColor(AppInstance.get(), R.color.colorPrimary)
        val disabled = ShapeDrawable(shape)
        disabled.paint.color = ContextCompat.getColor(AppInstance.get(), R.color.bg_header)
        addState(intArrayOf(android.R.attr.state_pressed), pressed)
        addState(intArrayOf(-android.R.attr.state_enabled), disabled)
        addState(intArrayOf(), normal)
    }

}