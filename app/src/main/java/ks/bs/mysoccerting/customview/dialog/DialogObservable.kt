package ks.bs.mysoccerting.customview.dialog

import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import androidx.core.content.ContextCompat
import androidx.core.math.MathUtils
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.TextView
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject
import kotlinx.android.synthetic.main.view_tdialog.*
import ks.bs.mysoccerting.R
import ks.bs.mysoccerting.rx.onClickWithAnimation
import ks.bs.mysoccerting.util.dpToPixels
import ks.bs.mysoccerting.util.toHtml


class DialogObservable(
    context: Context,
    private val title: String,
    private val contentParam: ContentParam,
    private val buttons: List<DialogButton>,
    private val isColored: Boolean,
    private val cancelable: Boolean
) : Dialog(context) {

    abstract class ContentParam {
        abstract fun onCreate(context: Context, onComplete: (Boolean) -> Unit): View
    }

    data class Result<T : ContentParam>(val ok: Boolean, val content: T)

    open class Builder(private val context: Context, private var title: String = "") {
        private var isColored = false
        fun setTitle(title: String): Builder {
            this.title = title
            return this
        }

        fun setMessage(message: String) = setContent(TextContentParam(message))

        fun <T : ContentParam> setContent(contentParam: T): DialogContentBuilder<T> {
            return DialogContentBuilder(context, contentParam, title, isColored)
        }

        fun isColored(enable: Boolean): Builder {
            isColored = enable
            return this
        }
    }


    private var completed = false
    private val subject = PublishSubject.create<Boolean>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        setContentView(R.layout.view_tdialog)

        initChild()

        initHeaderColor()

        if (!cancelable) {
            setCancelable(false)
            setCanceledOnTouchOutside(false)
            btn_dialog_close.visibility = View.INVISIBLE
        }

        btn_dialog_close.onClickWithAnimation { dismiss() }

        initDialogSize()
    }

    private fun initDialogSize() {
        layout_dialog_root.addOnLayoutChangeListener(object : View.OnLayoutChangeListener {
            override fun onLayoutChange(
                v: View,
                left: Int,
                top: Int,
                right: Int,
                bottom: Int,
                oldLeft: Int,
                oldTop: Int,
                oldRight: Int,
                oldBottom: Int
            ) {
                v.removeOnLayoutChangeListener(this)
                val headerHeight = 40.dpToPixels()
                val buttonsHeight = if (buttons.isEmpty()) {
                    0
                } else {
                    DialogButton.Height
                }
                val sum = headerHeight + buttonsHeight

                val width = 450.dpToPixels()
                val height = MathUtils.clamp(
                    bottom - top,
                    300.dpToPixels() + sum,
                    450.dpToPixels() + sum
                )

                val displayMetrics = context.resources.displayMetrics
                v.layoutParams.width = minOf(width, displayMetrics.widthPixels)
                v.layoutParams.height = minOf(height, displayMetrics.heightPixels)
                window?.setLayout(v.layoutParams.width, v.layoutParams.height)
            }
        })
    }

    private fun initChild() {
        tv_dialog_title.text = title.toHtml()

        layout_dialog_content.addView(contentParam.onCreate(this.context, this::onComplete))

        buttons.forEachIndexed { index, dialogButton ->
            val btn = dialogButton.onCreate(context, this::onComplete).apply {
                background = DialogButtonDrawable(index == 0, index == buttons.lastIndex)
            }

            layout_dialog_button.addView(btn)
        }

        if (buttons.isEmpty() && title.isEmpty()) {
            layout_dialog_root.dividerDrawable = null
        }

        tv_dialog_title.hideIfEmpty()
        layout_dialog_button.hideIfEmpty()
    }

    private fun initHeaderColor() {
        if (isColored) {
            layout_dialog_header.background = ContextCompat.getDrawable(context, R.drawable.bg_setting_dialog_header)!!
            tv_dialog_title.setTextColor(Color.WHITE)
            btn_dialog_close.setColorFilter(Color.WHITE)
        }
    }

    fun toObservable(): Observable<Boolean> {
        show()
        return subject
    }

    fun onComplete(value: Boolean) {
        if (!completed) {
            completed = true
            subject.onNext(value)
            dismiss()
        }
    }

    override fun dismiss() {
        super.dismiss()
        if (!completed) subject.onNext(false)
        subject.onComplete()
    }

    fun show(onDismiss: () -> Unit): Dialog {
        setOnDismissListener { onDismiss() }
        showOnMainThread()
        return this
    }

    private fun ViewGroup.hideIfEmpty() {
        if (childCount == 0) visibility = View.GONE
    }

    private fun TextView.hideIfEmpty() {
        visibility = if (text.isNotEmpty()) View.VISIBLE else View.GONE
    }

}