package ks.bs.mysoccerting.customview.dialog

import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import androidx.core.content.ContextCompat
import android.view.Window
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import kotlinx.android.synthetic.main.dialog_image.*
import ks.bs.mysoccerting.R
import ks.bs.mysoccerting.rx.onClickWithAnimation
import ks.bs.mysoccerting.util.dpToPixels

class ImageDialog(
    context: Context,
    private val title: String,
    private val isSettingsDialog: Boolean,
    private val imgUrl: String,
    private val width: Int,
    private val height: Int
) : Dialog(context) {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        setContentView(R.layout.dialog_image)
        tv_dialog_title.text = title
        btn_dialog_close.onClickWithAnimation { dismiss() }
        initHeaderColor()
        window?.setLayout(width, height)
        Glide.with(context)
            .load(imgUrl)
            .apply(RequestOptions().centerCrop())
            .into(iv_dialog)
    }

    private fun initHeaderColor() {
        if (isSettingsDialog) {
            layout_dialog_header.background = ContextCompat.getDrawable(context, R.drawable.bg_setting_dialog_header)!!
            tv_dialog_title.setTextColor(Color.WHITE)
            btn_dialog_close.setColorFilter(Color.WHITE)
        }
    }

    class Builder(private val context: Context, private var title: String = "") {

        private var isSettingsDialog: Boolean = false
        private var imgUrl: String = ""
        private var width: Int = 600.dpToPixels()
        private var height: Int = 480.dpToPixels()

        fun isSettingDialog(enable: Boolean): Builder {
            isSettingsDialog = enable
            return this
        }

        fun setImgUrl(url: String): Builder {
            imgUrl = url
            return this
        }

        fun setSize(width: Int, height: Int): Builder {
            this.width = width
            this.height = height
            return this
        }

        fun create() = ImageDialog(context, title, isSettingsDialog, imgUrl, width, height)
    }

}