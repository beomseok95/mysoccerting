package ks.bs.mysoccerting.customview.dialog

import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.GradientDrawable
import android.os.Bundle
import androidx.core.math.MathUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.TextView
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject
import kotlinx.android.synthetic.main.settings_sectionitem_switch.view.*
import kotlinx.android.synthetic.main.view_multiple_selection_dialog.*
import ks.bs.mysoccerting.R
import ks.bs.mysoccerting.customview.children
import ks.bs.mysoccerting.exception.CanceledByUserException
import ks.bs.mysoccerting.rx.onClickWithAnimation
import ks.bs.mysoccerting.util.toHtml


class TMultipleSelectionDialog<T> private constructor(context: Context, private val properties: Properties<T>) : Dialog(context) {

    internal val subject = PublishSubject.create<List<T>>()

    data class Properties<T>(
            var title: String = "",
            var elements: List<T> = emptyList(),
            var selections: List<T> = emptyList(),
            var cancelable: Boolean = true,
            var transform: (T) -> String = { it.toString() },
            var headerColor: Int = Color.TRANSPARENT,
            var children: List<(ViewGroup, T) -> View> = emptyList()
    )

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        setContentView(R.layout.view_multiple_selection_dialog)

        buttonClose.onClickWithAnimation { dismiss() }

        textTitle.text = properties.title

        textTitle.hideIfEmpty()
        layoutActionButtons.hideIfEmpty()

        if (properties.headerColor != Color.TRANSPARENT) {
            val header = GradientDrawable()
            header.setColor(properties.headerColor)
            header.cornerRadii = floatArrayOf(5f, 5f, 5f, 5f, 0f, 0f, 0f, 0f)
            layout_dialog_header.background = header
            textTitle.setTextColor(Color.WHITE)
            buttonClose.setColorFilter(Color.WHITE)
        }

        properties.elements.forEach {
            createViewElement(layoutElements, it, properties.children).let { layoutElements.addView(it) }
        }

        buttonCancel.background = DialogButtonDrawable(isLeftEnd = true, isRightEnd = false)
        buttonTouchAll.background = DialogButtonDrawable(isLeftEnd = false, isRightEnd = false)
        buttonOk.background = DialogButtonDrawable(isLeftEnd = false, isRightEnd = true)


        if (properties.cancelable) {
            buttonCancel.setOnClickListener { cancel() }
            setOnCancelListener {
                subject.onError(CanceledByUserException())
            }
        }
        setCancelable(properties.cancelable)

        buttonOk.setOnClickListener {
            dismiss()
            subject.onNext(getResult())
            subject.onComplete()
        }

        buttonTouchAll.setOnClickListener {
            touchAllElements()
        }


        val listener = object : View.OnLayoutChangeListener {
            override fun onLayoutChange(v: View, left: Int, top: Int, right: Int, bottom: Int, oldLeft: Int, oldTop: Int, oldRight: Int, oldBottom: Int) {
                v.removeOnLayoutChangeListener(this)

                v.layoutParams.height = MathUtils.clamp(
                        bottom - top,
                        context.resources.getDimensionPixelSize(R.dimen.dialog_min_height),
                        context.resources.getDimensionPixelSize(R.dimen.dialog_max_height)
                )
            }

        }
        layoutDialogRoot.addOnLayoutChangeListener(listener)
    }

    fun touchAllElements() {

        val isAllSelected = getResult().size == properties.elements.size

        if (isAllSelected) {
            layoutElements.children<View>()
                    .forEach { it.sch_row.isChecked = false }
        } else {
            layoutElements.children<View>()
                    .forEach { it.sch_row.isChecked = true }
        }
    }

    private fun createViewElement(parent: ViewGroup, element: T, children: List<(ViewGroup, T) -> View>): View {
        val view = LayoutInflater.from(context).inflate(R.layout.settings_sectionitem_switch, parent, false)

        view.tv_row_switch_title.text = properties.transform(element).toHtml()

        view.sch_row.isChecked = element in properties.selections

        view.layoutParams.height = ViewGroup.LayoutParams.WRAP_CONTENT

        view.setOnClickListener {
            view.sch_row.let { it.isChecked = !it.isChecked }
        }

        children.forEach {
            view.layoutChildren.addView(it.invoke(view.layoutChildren, element))

        }

        return view
    }

    private fun getResult(): List<T> {
        val selections = mutableListOf<Int>()
        layoutElements.children<View>()
                .forEachIndexed { index, view ->
                    if (view.sch_row.isChecked) {
                        selections.add(index)
                    }
                }

        return properties.elements.filterIndexed { index, _ -> index in selections }
    }


    class Builder<T>(val context: Context) {
        val properties: Properties<T> = Properties()

        fun setTitle(title: String): Builder<T> {
            properties.title = title
            return this
        }

        fun setHeaderColor(headerColor: Int): TMultipleSelectionDialog.Builder<T> {
            properties.headerColor = headerColor
            return this
        }

        fun setElements(elements: List<T>): Builder<T> {
            properties.elements = elements
            return this
        }


        fun setElements(elements: Array<T>): Builder<T> {
            return setElements(elements.toList())
        }

        fun setTransform(transform: (T) -> String): Builder<T> {
            properties.transform = transform
            return this
        }

        fun setSelections(selections: List<T>): Builder<T> {
            properties.selections = selections
            return this
        }

        fun setSelections(functionSelection: (T) -> Boolean): Builder<T> {
            properties.selections = properties.elements.filter(functionSelection)
            return this
        }

        fun setCancelable(cancelable: Boolean): Builder<T> {
            properties.cancelable = cancelable
            return this
        }

        fun addChildView(viewCreator: (ViewGroup, T) -> View): Builder<T> {
            properties.children += viewCreator
            return this
        }

        fun build(): TMultipleSelectionDialog<T> {
            return TMultipleSelectionDialog(context, properties)
        }

        fun show(): TMultipleSelectionDialog<T> {
            val dialog = build()
            dialog.show()
            return dialog
        }

        fun toObservable(): Observable<List<T>> {
            return show().subject
        }
    }

    private fun ViewGroup.hideIfEmpty() {
        if (childCount == 0) visibility = View.GONE
    }

    private fun TextView.hideIfEmpty() {
        visibility = if (text.isNotEmpty()) View.VISIBLE else View.GONE
    }

}