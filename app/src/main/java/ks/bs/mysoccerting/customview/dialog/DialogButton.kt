package ks.bs.mysoccerting.customview.dialog

import android.content.Context
import androidx.annotation.StringRes
import androidx.core.content.ContextCompat
import android.widget.Button
import android.widget.LinearLayout
import ks.bs.mysoccerting.R
import ks.bs.mysoccerting.util.AppInstance
import ks.bs.mysoccerting.util.dpToPixels

class DialogButton {
    private var clickListener: () -> Unit = {}
    private var onNext: Boolean = true
    private var title = ""

    constructor(@StringRes id: Int, onNext: Boolean = true, clickListener: () -> Unit = {}) {
        title = AppInstance.get().getString(id)
        this.clickListener = clickListener
        this.onNext = onNext
    }

    constructor(title: String, onNext: Boolean = true, clickListener: () -> Unit = {}) {
        this.title = title
        this.clickListener = clickListener
        this.onNext = onNext
    }

    fun onCreate(context: Context, onComplete: (Boolean) -> Unit): Button {
        val btn = Button(context)
        btn.text = title
        btn.setOnClickListener {
            clickListener()
            onComplete(onNext)
        }
        btn.setTextColor(ContextCompat.getColorStateList(context, R.color.btn_dialog_text))
        btn.textSize = 24.0f
        btn.layoutParams = LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                Height,
                1f)
        return btn
    }

    companion object {
        fun OK() = DialogButton(android.R.string.ok, true)
        fun CANCEL() = DialogButton(android.R.string.cancel, false)
        val Height = 55.dpToPixels()
    }
}