package ks.bs.mysoccerting.customview

import android.view.View
import android.view.ViewGroup
import android.widget.CompoundButton
import android.widget.LinearLayout
import kotlinx.android.synthetic.main.textview_position.view.*
import ks.bs.mysoccerting.model.PositionColor
import kotlin.reflect.KMutableProperty0

inline fun <reified T> ViewGroup.children(): Iterable<T> {
    return (0 until childCount)
        .map {
            getChildAt(it)
        }
        .filter { it is T }
        .map { it as T }
}

fun CompoundButton.bind(value: KMutableProperty0<Boolean>, functionOnCheckedChanged: (value: Boolean) -> Unit = {}) {
    isChecked = value.get()

    setOnCheckedChangeListener { v, isChecked ->
        if (v.isPressed) {
            value.set(isChecked)
            functionOnCheckedChanged(isChecked)
        }
    }

}

fun Boolean.toVisibility(ifTrue: Int = View.VISIBLE, ifFalse: Int = View.GONE): Int {
    return if (this) ifTrue else ifFalse
}

fun LinearLayout.addPosition(position: String): TTextView {
    val tv = TTextView(context)
    tv.imageView.text = position
    tv.imageView.setBackgroundResource(getPositionColor(position))
    addView(tv)
    return tv
}

fun getPositionColor(position: String): Int {
    when (position) {
        PositionColor.FW.position -> {
            return PositionColor.FW.bgColor
        }
        PositionColor.MF.position -> {
            return PositionColor.MF.bgColor
        }
        PositionColor.DF.position -> {
            return PositionColor.DF.bgColor
        }
        PositionColor.GK.position -> {
            return PositionColor.GK.bgColor
        }
        else -> {
            return PositionColor.ALL.bgColor
        }
    }
}

fun SelectAreaView.setContent(title: String, functionOnClick: (view: SelectAreaView) -> Unit = {}): SelectAreaView {
    val v = SelectAreaView(context)
    v.imageView.text = title
    v.setOnClickListener {
        functionOnClick(v)
    }
    return v
}
fun CloudInfoView.setContent(title: String):CloudInfoView{
    val v = CloudInfoView(context)
    v.imageView.text = title
    return v
}
