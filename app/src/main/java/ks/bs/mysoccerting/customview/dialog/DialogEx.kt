package ks.bs.mysoccerting.customview.dialog

import android.app.Dialog
import android.content.Context
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers

fun Dialog.showOnMainThread() {
    AndroidSchedulers.mainThread().scheduleDirect {
        show()
    }
}

fun <T> List<T>.toSingleSelectionDialog(
    context: Context,
    title: String,
    cancelable: Boolean = false,
    initialIndex: Int = -1,
    transform: (T) -> (String) = { it.toString() },
    isSettingsDialog: Boolean = false
): Single<T> {

    return SingleSelectionDialog.Builder<T>(context, title)
        .setItems(this)
        .setSelectedIndex(initialIndex)
        .setTransform(transform)
        .setCancelable(cancelable)
        .isSettingDialog(isSettingsDialog)
        .toSingle()
}

fun <T> List<T>.toSingleSelectionDialog(
    context: Context,
    title: String,
    cancelable: Boolean = false,
    initialItem: T,
    transform: (T) -> (String) = { it.toString() },
    isSettingsDialog: Boolean = false
): Single<T> {

    return toSingleSelectionDialog(context, title, cancelable, indexOf(initialItem), transform, isSettingsDialog)
}


fun <T> List<T>.toMultipleSelectionDialog(
    context: Context,
    title: String,
    cancelable: Boolean = false,
    selections: List<T> = emptyList(),
    transform: (T) -> (String) = { it.toString() }
): Single<List<T>> {

    return TMultipleSelectionDialog.Builder<T>(context)
        .setTitle(title)
        .setElements(this)
        .setTransform(transform)
        .setSelections(selections)
        .setCancelable(cancelable)
        .toObservable()
        .singleOrError()
}