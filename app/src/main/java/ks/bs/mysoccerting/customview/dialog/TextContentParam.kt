package ks.bs.mysoccerting.customview.dialog

import android.content.Context
import androidx.core.content.ContextCompat
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import ks.bs.mysoccerting.R
import ks.bs.mysoccerting.util.dpToPixels

class TextContentParam(private val text: String) : DialogObservable.ContentParam() {
    override fun onCreate(context: Context, onComplete: (Boolean) -> Unit): View {
        val textView = TextView(context)
        textView.text = text
        textView.textSize = 30f
        textView.setTextColor(ContextCompat.getColor(context, R.color.gray))
        textView.layoutParams = LinearLayout.LayoutParams(
            ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT
        )
            .apply {
                marginStart = 10.dpToPixels()
                marginEnd = 10.dpToPixels()
            }
        textView.minHeight = 300.dpToPixels()
        textView.gravity = Gravity.CENTER
        return textView
    }
}