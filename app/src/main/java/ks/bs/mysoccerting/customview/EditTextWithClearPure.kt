package ks.bs.mysoccerting.customview

import android.content.Context
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.text.Editable
import android.text.TextWatcher
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.View
import androidx.appcompat.widget.AppCompatEditText
import androidx.core.content.res.ResourcesCompat
import androidx.core.graphics.drawable.toBitmap
import ks.bs.mysoccerting.R
import ks.bs.mysoccerting.util.dpToPixels

class EditTextWithClearPure : AppCompatEditText {
    constructor(context: Context) : super(context) {
        setupButton()
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        setupButton()
    }

    constructor(context: Context, attrs: AttributeSet, defStyle: Int) : super(context, attrs, defStyle) {
        setupButton()
    }

    private fun setupButton() {
        isCursorVisible = false
        setPadding(40, 15, 15, 25)

        background = ResourcesCompat.getDrawable(resources, R.drawable.bg_item_base, null)!!

        val clearButtonImage = ResourcesCompat.getDrawable(resources, R.drawable.ic_clear_opaque_24dp, null)!!

        addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                if (s?.isEmpty()!!) {
                    setCompoundDrawablesRelativeWithIntrinsicBounds(null, null, null, null)
                } else {

                    setCompoundDrawablesRelativeWithIntrinsicBounds(
                        null,
                        null,
                        clearButtonImage,
                        null
                    )
                }
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }
        })

        setOnTouchListener(View.OnTouchListener { _, event ->
            if (isMotionActionUp(event) && event.rawX >= (this.right - this.compoundPaddingRight)
            ) {
                this.setText("")
                return@OnTouchListener true
            }

            return@OnTouchListener false
        })
    }

    private fun isMotionActionUp(event: MotionEvent) = event.action == MotionEvent.ACTION_UP

}