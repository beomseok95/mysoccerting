package ks.bs.mysoccerting.customview

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.LinearLayout
import kotlinx.android.synthetic.main.layout_club_detail_tab.view.*
import kr.nextm.lib.getDrawable
import ks.bs.mysoccerting.R

class ClubDetailTab @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0, bgRes: Int
) : LinearLayout(context, attrs, defStyleAttr) {
    init {
        LayoutInflater.from(context).inflate(R.layout.layout_club_detail_tab, this, true)
        tab.background = bgRes.getDrawable()
    }
}