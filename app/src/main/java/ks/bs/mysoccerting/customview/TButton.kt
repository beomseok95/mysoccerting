package ks.bs.mysoccerting.customview

import android.content.Context
import android.content.res.TypedArray
import android.graphics.Color
import androidx.appcompat.widget.AppCompatButton
import android.util.AttributeSet
import android.util.TypedValue
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import ks.bs.mysoccerting.R
import ks.bs.mysoccerting.rx.ignoreInterruption
import java.util.concurrent.TimeUnit

class TButton(context: Context, attrs: AttributeSet?, defStyleAttr: Int) :
    AppCompatButton(context, attrs, defStyleAttr) {
    constructor(context: Context) : this(context, null)
    constructor(context: Context, attrs: AttributeSet?) : this(
        context,
        attrs,
        R.attr.buttonStyle
    )

    init {
        setBackgroundResource(R.drawable.btn_blue)
        setTextColor(Color.WHITE)
        setTextSize(TypedValue.COMPLEX_UNIT_DIP, 25f)
        setAttrs(attrs, defStyleAttr)
    }

    private fun setAttrs(attrs: AttributeSet?, defStyle: Int) {
        val typeArray = context.obtainStyledAttributes(attrs, R.styleable.TButton, defStyle, 0)
        setTypeArray(typeArray)
    }

    private fun setTypeArray(typeArray: TypedArray) {
        val blink = typeArray.getBoolean(R.styleable.TButton_blink, false)
        if (blink) {
            setBlink()
        }
        typeArray.recycle()
    }

    var disposable: Disposable? = null
    private fun setBlink() {
        disposable?.dispose()

        disposable = Observable.interval(500, TimeUnit.MILLISECONDS)
            .observeOn(AndroidSchedulers.mainThread())
            .ignoreInterruption()
            .subscribe {
                if (it.toInt() % 2 == 0) {
                    setBackgroundResource(R.drawable.btn_orange)
                } else {
                    setBackgroundResource(R.drawable.btn_blue)
                }
            }
    }

    override fun onDetachedFromWindow() {
        disposable?.dispose()
        super.onDetachedFromWindow()
    }

}