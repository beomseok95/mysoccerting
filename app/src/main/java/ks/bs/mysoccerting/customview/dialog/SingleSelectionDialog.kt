package ks.bs.mysoccerting.customview.dialog

import android.app.Dialog
import android.content.Context
import android.content.DialogInterface
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import androidx.core.content.ContextCompat
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.RadioButton
import android.widget.RadioGroup
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.subjects.SingleSubject
import kotlinx.android.synthetic.main.dialog_single_selection.*
import ks.bs.mysoccerting.R
import ks.bs.mysoccerting.exception.CanceledByUserException
import ks.bs.mysoccerting.rx.onClickWithAnimation
import ks.bs.mysoccerting.util.dpToPixels

class SingleSelectionDialog<T>(context: Context,
                               private val title: String,
                               private val items: List<T>,
                               private val transform: (T) -> String,
                               private var selectedIndex: Int,
                               private val cancelable: Boolean,
                               private val isSettingsDialog: Boolean) : Dialog(context), DialogInterface.OnDismissListener {

    private val emitter = SingleSubject.create<T>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        setContentView(R.layout.dialog_single_selection)
        tv_dialog_title.text = title
        setOnDismissListener(this)
        btn_dialog_close.onClickWithAnimation { dismiss() }
        initHeaderColor()
        initDialogButton()
        addRadioButton()

        if (!cancelable) {
            setCanceledOnTouchOutside(false)
            setCancelable(false)
            btn_dialog_close.visibility = View.GONE
        }

        window?.setLayout(300.dpToPixels(), 380.dpToPixels())
    }

    override fun setOnDismissListener(listener: DialogInterface.OnDismissListener?) {
        if (listener == this) super.setOnDismissListener(listener)
    }

    override fun onDismiss(dialog: DialogInterface?) {
        if (emitter.hasObservers() && !emitter.hasValue() && !emitter.hasThrowable()) {
            emitter.onError(CanceledByUserException())
        }
    }

    private fun initHeaderColor() {
        if (isSettingsDialog) {
            layout_dialog_header.background = ContextCompat.getDrawable(context, R.drawable.bg_setting_dialog_header)!!
            tv_dialog_title.setTextColor(Color.WHITE)
            btn_dialog_close.setColorFilter(Color.WHITE)
        }
    }

    private fun initDialogButton() {

        btn_cancel.background = DialogButtonDrawable(true, false)
        btn_cancel.setOnClickListener { emitter.onError(CanceledByUserException()) }
        btn_ok.background = DialogButtonDrawable(false, true)
        btn_ok.setOnClickListener {
            val selected = selectedIndex
            if (selected != -1) {
                emitter.onSuccess(items[selected])
            }
        }
        btn_ok.isEnabled = selectedIndex != -1
    }

    private fun addRadioButton() {
        val group = RadioGroup(context)
        group.orientation = RadioGroup.VERTICAL
        group.layoutParams = ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
        items.forEachIndexed { index, item ->
            val radio = RadioButton(context)
            radio.setTextColor(ContextCompat.getColor(context, R.color.gray))
            radio.text = transform(item)
            radio.setOnClickListener {
                radio.isChecked = true
                btn_ok.isEnabled = true
                if (index == selectedIndex) {
                    emitter.onSuccess(item)
                } else selectedIndex = index
            }
            radio.textSize = 20.0f
            val param = RadioGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
            param.setMargins(0, 5.dpToPixels(), 0, 5.dpToPixels())
            radio.layoutParams = param
            group.addView(radio)
            if (index == selectedIndex) {
                btn_ok.isEnabled = true
                group.check(radio.id)
            }
        }
        layout_dialog_content.addView(group)
    }

    fun toSingle(): Single<T> {
        return emitter
                .doOnSubscribe {
                    AndroidSchedulers.mainThread().scheduleDirect {
                        show()
                    }
                }
                .doOnSuccess { dismiss() }
                .doOnError { dismiss() }
    }

    class Builder<T>(private val context: Context, private var title: String = "") {
        private var items: List<T> = emptyList()
        private var transform: (T) -> String = { it.toString() }
        private var selectedIndex: Int = -1
        private var isSettingsDialog: Boolean = false
        private var cancelable: Boolean = false

        fun setItems(items: List<T>): Builder<T> {
            this.items = items
            return this
        }

        fun setTransform(transform: (T) -> String): Builder<T> {
            this.transform = transform
            return this
        }

        fun setSelected(item: T): Builder<T> {
            selectedIndex = items.indexOf(item)
            return this
        }

        fun setSelectedIndex(index: Int): Builder<T> {
            selectedIndex = index
            return this
        }

        fun isSettingDialog(enable: Boolean): Builder<T> {
            isSettingsDialog = enable
            return this
        }

        fun setCancelable(enable: Boolean): Builder<T> {
            cancelable = enable
            return this
        }

        fun toSingle() = SingleSelectionDialog(context, title, items, transform, selectedIndex, isSettingsDialog, cancelable).toSingle()
    }

}