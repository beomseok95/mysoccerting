package ks.bs.mysoccerting.customview.dialog

import android.app.Dialog
import android.content.Context
import io.reactivex.Maybe
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.disposables.Disposable

class DialogContentBuilder<T : DialogObservable.ContentParam>(private val context: Context,
                                                              private val contentParam: T,
                                                              private var title: String,
                                                              private var isColored: Boolean,
                                                              private var cancelable: Boolean = true) {

    private val buttons = ArrayList<DialogButton>()

    fun setTitle(title: String): DialogContentBuilder<T> {
        this.title = title
        return this
    }

    fun <R : DialogObservable.ContentParam> setContent(content: R): DialogContentBuilder<R> {
        val builder = DialogContentBuilder(context, content, title, isColored, cancelable)
        builder.buttons.addAll(buttons)
        return builder
    }

    fun addButton(button: DialogButton): DialogContentBuilder<T> {
        buttons.add(button)
        return this
    }

    fun isColored(enable: Boolean): DialogContentBuilder<T> {
        isColored = enable
        return this
    }

    fun show(onDismiss: () -> Unit = {}): Dialog = create().show(onDismiss)

    fun setCancelable(enable: Boolean): DialogContentBuilder<T> {
        this.cancelable = enable
        return this
    }

    fun toObservable(): Observable<DialogObservable.Result<T>> {
        return create().toObservable().map { DialogObservable.Result(it, contentParam) }
    }

    fun toSingle(): Single<DialogObservable.Result<T>> {
        return toObservable().singleOrError()
    }

    fun singleElement(): Maybe<DialogObservable.Result<T>> {
        return toObservable().singleElement()
    }

    fun subscribe(func: (DialogObservable.Result<T>) -> Unit): Disposable {
        return toObservable().subscribe(func)
    }

    fun create(): DialogObservable {
        return DialogObservable(context, title, contentParam, buttons, isColored, cancelable)
    }
}