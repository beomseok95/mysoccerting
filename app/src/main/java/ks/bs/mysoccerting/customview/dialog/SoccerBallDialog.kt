package ks.bs.mysoccerting.customview.dialog

import android.app.Dialog
import android.content.Context
import android.view.Window
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.layout_soccer_ball.*
import ks.bs.mysoccerting.R

class SoccerBallDialog(context: Context) : Dialog(context) {

    init {
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window?.setBackgroundDrawableResource(android.R.color.transparent)
        setContentView(R.layout.layout_soccer_ball)

        Glide.with(context)
            .load(R.raw.soccer_ball)
            .into(image)
    }


}