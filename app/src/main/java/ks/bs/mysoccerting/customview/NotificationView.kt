package ks.bs.mysoccerting.customview

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.LinearLayout
import androidx.core.view.marginEnd
import ks.bs.mysoccerting.R
import ks.bs.mysoccerting.util.dpToPixels

class NotificationView @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : LinearLayout(context, attrs, defStyleAttr) {
    init {
        LayoutInflater.from(context).inflate(R.layout.layout_badge, this, true)
    }
}