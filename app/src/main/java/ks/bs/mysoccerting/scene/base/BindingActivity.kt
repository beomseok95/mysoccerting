package ks.bs.mysoccerting.scene.base

import android.os.Bundle
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import ks.bs.mysoccerting.BR
import ks.bs.mysoccerting.rx.rxresult.RxActivityBase

abstract class BindingActivity<B : ViewDataBinding> : RxActivityBase() {

    @LayoutRes
    abstract fun layoutRes(): Int

    abstract val viewModel: Any

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        DataBindingUtil.setContentView<B>(this, layoutRes())
            .also {
                it.setVariable(BR.vm, viewModel)
            }

    }
}