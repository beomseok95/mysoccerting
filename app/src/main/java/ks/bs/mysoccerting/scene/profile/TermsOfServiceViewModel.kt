package ks.bs.mysoccerting.scene.profile

import android.view.View
import androidx.databinding.ObservableField
import kr.nextm.lib.TToast
import ks.bs.mysoccerting.BuildConfig
import ks.bs.mysoccerting.rx.rxresult.startActivity
import ks.bs.mysoccerting.rx.rxresult.toIntent

class TermsOfServiceViewModel {

    val version = ObservableField(BuildConfig.VERSION_NAME)

    fun showTermsOfService(v: View) {
        TermsOfServiceActivity::class.java
            .toIntent()
            .startActivity(v.context)
            .subscribe()
    }

    fun showPrivacyPolicy(v: View) {
        PrivacyPolicyActivity::class.java
            .toIntent()
            .startActivity(v.context)
            .subscribe()
    }

    fun showLegalnotice(v: View) {
        TToast.show("법적공지")
    }
}
