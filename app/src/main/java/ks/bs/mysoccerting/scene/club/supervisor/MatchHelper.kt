package ks.bs.mysoccerting.scene.club.supervisor

import android.view.View
import io.reactivex.Maybe
import io.reactivex.Single
import io.reactivex.rxkotlin.zipWith
import ks.bs.mysoccerting.customview.dialog.DialogButton
import ks.bs.mysoccerting.customview.dialog.DialogObservable
import ks.bs.mysoccerting.customview.dialog.TDialog
import ks.bs.mysoccerting.customview.dialog.TextContentParam
import ks.bs.mysoccerting.exception.ErrorCode
import ks.bs.mysoccerting.exception.toDialogMessage
import ks.bs.mysoccerting.fcm.FcmHelper
import ks.bs.mysoccerting.firebase.CrashlyticsHelper
import ks.bs.mysoccerting.firebase.FirebaseUrls
import ks.bs.mysoccerting.firebase.RxRealTimeDB
import ks.bs.mysoccerting.model.*
import ks.bs.mysoccerting.model.time.LocalTime
import ks.bs.mysoccerting.model.time.Pattern
import ks.bs.mysoccerting.model.type.AlarmType
import ks.bs.mysoccerting.model.type.TimelineType
import ks.bs.mysoccerting.rx.withProgressBallDialog
import ks.bs.mysoccerting.scene.login.LoginManager
import ks.bs.mysoccerting.scene.match.MatchingInfoCheckDialog
import ks.bs.mysoccerting.support.DontCare
import ks.bs.mysoccerting.util.wrapWith

object MatchHelper {

    fun accept(view: View, modelAlaram: ModelAlaram) {
        requireNotNull(modelAlaram.senderUid) { ErrorCode.FAIL_NO_SENDER_UID.message }
        requireNotNull(modelAlaram.reciverUid) { ErrorCode.FAIL_NO_RECIVER_UID.message }

        var senderClubModel: ModelClub
        var reciverClubModel: ModelClub
        RxRealTimeDB.toSingle("${FirebaseUrls.club}/${modelAlaram.senderClubUid}") { ModelClub() }
            .zipWith(RxRealTimeDB.toSingle("${FirebaseUrls.club}/${LoginManager.user?.clubUid}") { ModelClub() })
            .flatMap { response ->

                require(response.first.contentId.isNotEmpty()) { ErrorCode.NO_EXIST_CLUB.message }
                require(response.second.contentId.isNotEmpty()) { ErrorCode.FAIL_NO_EXIST_MY_CLUB_CONTENT_ID.message }
                senderClubModel = response.first
                reciverClubModel = response.second

                when {
                    areayMatchedCheck(response) ->
                        TDialog.Builder(view.context)
                            .setMessage("${response.first.clubName} 클럽과는 이미 매칭되어있습니다.")
                            .isColored(true)
                            .addButton(DialogButton.OK())
                            .toSingle()

                    else -> {
                        val matchContentId = RxRealTimeDB.generateRandomKey("match")

                        createMatchUpInfo(response, modelAlaram, matchContentId)
                            .flatMap {
                                createAlaram(
                                    modelAlaram,
                                    reciverClubModel,
                                    senderClubModel,
                                    isReject = false,
                                    matchUpContentId = matchContentId
                                )
                            }
//                            .createFcmMessage(modelAlaram, isReject = false)
                            .createAllMemberFcm(senderClubModel, reciverClubModel)
                            .removeMatchApplicationList(senderClubModel, reciverClubModel, modelAlaram)
                            .createTimeline(senderClubModel, reciverClubModel, modelAlaram)
                            .flatMap {
                                TDialog.Builder(view.context)
                                    .setMessage("매칭이 수락되었습니다.")
                                    .isColored(true)
                                    .addButton(DialogButton.OK())
                                    .toSingle()
                            }
                    }
                }
            }
            .withProgressBallDialog(view.context)
            .subscribe({

            }, { e ->
                TDialog.show(view.context, e.toDialogMessage())
                CrashlyticsHelper.logException(e)
            })
            .let {}
    }

    fun reject(view: View, modelAlaram: ModelAlaram) {
        requireNotNull(modelAlaram.senderUid) { ErrorCode.FAIL_NO_SENDER_UID.message }
        requireNotNull(modelAlaram.reciverUid) { ErrorCode.FAIL_NO_RECIVER_UID.message }

        var senderClubModel: ModelClub
        var reciverClubModel: ModelClub
        RxRealTimeDB.toSingle("${FirebaseUrls.club}/${modelAlaram.senderClubUid}") { ModelClub() }
            .zipWith(RxRealTimeDB.toSingle("${FirebaseUrls.club}/${LoginManager.user?.clubUid}") { ModelClub() })
            .flatMap { response ->

                require(response.first.contentId.isNotEmpty()) { ErrorCode.NO_EXIST_CLUB.message }
                require(response.second.contentId.isNotEmpty()) { ErrorCode.FAIL_NO_EXIST_MY_CLUB_CONTENT_ID.message }
                senderClubModel = response.first
                reciverClubModel = response.second


                createAlaram(modelAlaram, reciverClubModel, senderClubModel, isReject = true)
                    .createFcmMessage(modelAlaram, isReject = true)
                    .removeMatchApplicationList(senderClubModel, reciverClubModel, modelAlaram)
                    .flatMap {
                        TDialog.Builder(view.context)
                            .setMessage("매칭을 거절하였습니다.")
                            .isColored(true)
                            .addButton(DialogButton.OK())
                            .toSingle()
                    }
            }
            .withProgressBallDialog(view.context)
            .subscribe({

            }, { e ->
                TDialog.show(view.context, e.toDialogMessage())
                CrashlyticsHelper.logException(e)
            })
            .let {}
    }

    private fun createAlaram(
        modelAlaram: ModelAlaram,
        reciverClubModel: ModelClub,
        senderClubModel: ModelClub,
        isReject: Boolean,
        matchUpContentId: String? = null
    ): Single<DontCare> {
        return RxRealTimeDB.toSingle("${FirebaseUrls.alram}/${modelAlaram.senderUid}") { mutableListOf<ModelAlaram>() }
            .flatMap {
                it.add(
                    createAlaramModel(
                        reciverClubModel,
                        modelAlaram,
                        isSender = true,
                        isReject = isReject,
                        matchUpContentId = matchUpContentId
                    )
                )
                Single.just(it)
            }
            .flatMap { RxRealTimeDB.push("${FirebaseUrls.alram}/${modelAlaram.senderUid}", it) }
            .flatMap {
                RxRealTimeDB.toSingle("${FirebaseUrls.alram}/${modelAlaram.reciverUid}") { mutableListOf<ModelAlaram>() }
                    .flatMap {
                        it.removeAll { it.senderClubUid == senderClubModel.contentId }
                        it.add(
                            createAlaramModel(
                                senderClubModel,
                                modelAlaram,
                                isSender = false,
                                isReject = isReject,
                                matchUpContentId = matchUpContentId
                            )
                        )
                        Single.just(it)
                    }
            }
            .flatMap { RxRealTimeDB.push("${FirebaseUrls.alram}/${modelAlaram.reciverUid}", it) }
    }

    private fun Single<DontCare>.createFcmMessage(
        modelAlaram: ModelAlaram,
        isReject: Boolean
    ): Single<DontCare> {
        return flatMap { createFcm(modelAlaram, isSender = true, isReject = isReject) }
            .flatMap { createFcm(modelAlaram, isSender = false, isReject = isReject) }
            .flatMap { Single.just(DontCare) }
    }

    private fun Single<DontCare>.createAllMemberFcm(
        senderClubModel: ModelClub,
        reciverClubModel: ModelClub
    ): Single<DontCare> {
        return flatMap {

            senderClubModel.clubMembers.forEach {
                FcmHelper.sendMessage(
                    destinationUid = it.uid,
                    message = "${reciverClubModel.clubName.wrapWith(
                        "[",
                        "]"
                    )} ${AlarmType.MATCHING_COMPLETE.text}"
                )
                    .subscribe()
            }

            reciverClubModel.clubMembers.forEach {
                FcmHelper.sendMessage(
                    destinationUid = it.uid,
                    message = "${senderClubModel.clubName.wrapWith(
                        "[",
                        "]"
                    )} ${AlarmType.MATCHING_COMPLETE.text}"
                )
                    .subscribe()
            }

            this
        }
    }

    private fun createFcm(
        modelAlaram: ModelAlaram,
        isSender: Boolean,
        isReject: Boolean
    ): Single<ModelPush.Response> {
        // true -> 샌더가 받음 , false -> 샌더가 보냄
        val clubName = if (isSender) LoginManager.user?.club else modelAlaram.senderNickName
        val fcmPostText = if (isReject) AlarmType.MATCHING_REJECT.text else AlarmType.MATCHING_COMPLETE.text
        return FcmHelper.sendMessage(
            destinationUid = if (isSender) modelAlaram.senderUid else modelAlaram.reciverUid,
            message = "${clubName?.wrapWith(
                "[",
                "]"
            )} $fcmPostText"
        )
    }

    private fun Single<DontCare>.removeMatchApplicationList(
        senderClubModel: ModelClub,
        reciverClubModel: ModelClub,
        modelAlaram: ModelAlaram
    ): Single<DontCare> {
        val matchApplicationPath =
            "${FirebaseUrls.club}/${reciverClubModel.contentId}/matchApplicationList"

        return flatMap { RxRealTimeDB.toSingle(matchApplicationPath) { mutableListOf(ModelClub.MatchApplicationInfo()) } }
            .flatMap { applicationList ->
                applicationList.removeAll { it.senderClubUid == senderClubModel.contentId && it.timeStamp == modelAlaram.timeStamp }
                RxRealTimeDB.push(matchApplicationPath, applicationList)
            }
    }

    private fun Single<DontCare>.createTimeline(
        senderClubModel: ModelClub,
        reciverClubModel: ModelClub,
        modelAlaram: ModelAlaram
    ): Single<DontCare> {
        return flatMap {
            timelineGenerate(
                isSender = true,
                senderClubModel = senderClubModel,
                reciverClubModel = reciverClubModel,
                modelAlaram = modelAlaram
            )
        }
            .flatMap {
                timelineGenerate(
                    isSender = false,
                    senderClubModel = senderClubModel,
                    reciverClubModel = reciverClubModel,
                    modelAlaram = modelAlaram
                )
            }
    }

    private fun timelineGenerate(
        isSender: Boolean,
        senderClubModel: ModelClub,
        reciverClubModel: ModelClub,
        modelAlaram: ModelAlaram
    ): Single<DontCare> {
        // true -> 샌더가 받음 , false -> 샌더가 보냄
        val contentId = RxRealTimeDB.generateRandomKey("timeline")
        val clubUid = if (isSender) reciverClubModel.contentId else senderClubModel.contentId
        val timelinePath =
            "${FirebaseUrls.club}/$clubUid/${FirebaseUrls.timeLine}/$contentId"

        return RxRealTimeDB.push(
            timelinePath,
            ModelClubTimeline(
                nickName = if (isSender) reciverClubModel.clubName else senderClubModel.clubName,
                uid = if (isSender) modelAlaram.reciverUid else modelAlaram.senderUid,
                contentUid = if (isSender) reciverClubModel.contentId else senderClubModel.contentId,
                memberImageUrl = if (isSender) reciverClubModel.clubMarkImageUrl else senderClubModel.clubMarkImageUrl,
                timestamp = LocalTime().time,
                type = TimelineType.MATCHING_COMPLETE
            )
        )
    }

    private fun createMatchUpInfo(
        response: Pair<ModelClub, ModelClub>,
        modelAlaram: ModelAlaram,
        matchContentId: String
    ): Single<DontCare> {

        return createMatchUpInfos(response, modelAlaram, true, matchContentId)
            .flatMap { createMatchUpInfos(response, modelAlaram, false, matchContentId) }
    }


    private fun createMatchUpInfos(
        response: Pair<ModelClub, ModelClub>,
        modelAlaram: ModelAlaram,
        isSender: Boolean,
        matchContentId: String
    ): Single<DontCare> {
        val clubUid = if (isSender) response.first.contentId else response.second.contentId
        return RxRealTimeDB.push(
            "${FirebaseUrls.club}/$clubUid/matchUpList/$matchContentId",
            ModelClub.MatchInfo(
                clubMarkImageUrl1 = response.first.clubMarkImageUrl,
                clubMarkImageUrl2 = response.second.clubMarkImageUrl,
                clubName1 = response.first.clubName,
                clubName2 = response.second.clubName,
                hopeDate = modelAlaram.matchInfo?.hopeDate!!,
                hopeDateTimeStamp = modelAlaram.matchInfo?.hopeDateTimeStamp!!,
                peopleNumber = modelAlaram.matchInfo?.peopleNumber!!,
                area = modelAlaram.matchInfo?.area!!,
                clubMasterUid1 = response.first.masterUid,
                clubMasterUid2 = response.second.masterUid,
                clubUid1 = response.first.contentId,
                clubUid2 = response.second.contentId,
                matchingCompleteTime = LocalTime().time,
                contentId = matchContentId
            )
        )
    }

    private fun createAlaramModel(
        model: ModelClub,
        modelAlaram: ModelAlaram,
        isSender: Boolean, // true -> 샌더가 받음 , false -> 샌더가 보냄
        isReject: Boolean,
        matchUpContentId: String? = null
    ): ModelAlaram {
        val clubName = if (isSender) LoginManager.user?.club else model.clubName
        val alarmType = if (isReject) AlarmType.MATCHING_REJECT else AlarmType.MATCHING_COMPLETE

        val alarmData = ModelAlaram(
            senderEmail = if (isSender) LoginManager.user?.email else modelAlaram.senderEmail,
            senderImageUrl = if (isSender) LoginManager.user?.email else modelAlaram.senderEmail,
            timeStamp = LocalTime().time,
            senderUid = if (isSender) LoginManager.user?.uid!! else modelAlaram.senderUid,
            senderNickName = clubName,
            reciverUid = if (isSender) modelAlaram.senderUid else modelAlaram.reciverUid,
            alaramTypeCode = alarmType.code,
            message = "${clubName?.wrapWith(
                "[",
                "]"
            )} ${alarmType.text}",
            postPullPath = matchUpContentId ?: "none"
        )
        return alarmData
    }

    private fun areayMatchedCheck(response: Pair<ModelClub, ModelClub>) =
        response.second.matchUpList?.any { it.value.clubUid1 == response.first.contentId } ?: false
                || response.first.matchUpList?.any { it.value.clubUid1 == response.second.contentId } ?: false
                || response.second.matchUpList?.any { it.value.clubUid2 == response.first.contentId } ?: false
                || response.first.matchUpList?.any { it.value.clubUid2 == response.second.contentId } ?: false

    fun apply(view: View, matchModel: ModelClubMatch): Single<Any> {
        require(matchModel.clubUid.isNotEmpty()) { ErrorCode.NO_EXIST_CLUB.message }

        return MatchingInfoCheckDialog()
            .putObject(matchModel)
            .toSingle(view.context)
            .noExistClubCheck(matchModel.clubUid)
            .flatMap { result ->
                if (result)
                    ClubManager.showDialog(
                        view,
                        title = "매칭신청",
                        message = "${matchModel.clubName} 클럽에게 매칭을 신청하시겠습니까?",
                        okBtnText = "신청"
                    )
                        .filter { it.ok }
                        .applyProccess(matchModel)
                        .flatMap { proccessResult -> Single.just(proccessResult) }
                else
                    Single.just(ModelClubMatch.ApplyResult(cancel = true))
            }
            .flatMap { result ->
                when {
                    result.ok -> ClubManager.showDialog(
                        view = view,
                        title = "",
                        message = "매칭신청이 완료되었습니다.",
                        cancelable = false,
                        okBtnText = "확인"
                    )
                    result.duplicate -> ClubManager.showDialog(
                        view = view,
                        title = "",
                        message = "이미 매칭이 성사되었거나,\n매칭신청중입니다.",
                        cancelable = false,
                        okBtnText = "확인"
                    )
                    result.over -> ClubManager.showDialog(
                        view = view,
                        title = "",
                        message = "매칭일이 이미 지났습니다.",
                        cancelable = false,
                        okBtnText = "확인"
                    )
                    else -> Single.just(DontCare)
                }
            }
    }

    private fun Maybe<DialogObservable.Result<TextContentParam>>.applyProccess(matchModel: ModelClubMatch): Single<ModelClubMatch.ApplyResult> {
        return this
            .flatMapSingle {
                if (!isMatchDayOver(matchModel)) {
                    val path = "${FirebaseUrls.club}/${matchModel.clubUid}/matchApplicationList"
                    RxRealTimeDB.toSingle(path) { mutableListOf<ModelClub.MatchApplicationInfo>() }
                        .zipWith(RxRealTimeDB.toSingle("${FirebaseUrls.club}/${matchModel.clubUid}/matchUpList") { mutableMapOf<String, ModelClub.MatchInfo>() })
                        .map { pair ->
                            when {
                                isAlreadyApplied(pair.first) -> pair.first to false
                                isAreadyMatched(pair.second) -> pair.first to false
                                else -> pair.first to true
                            }
                        }
                        .flatMap { availability ->
                            if (availability.second) {
                                send(availability, matchModel, path)
                                    .flatMap { Single.just(ModelClubMatch.ApplyResult(ok = true)) }
                            } else
                                Single.just(ModelClubMatch.ApplyResult(duplicate = true))
                        }
                } else
                    Single.just(ModelClubMatch.ApplyResult(over = true))
            }
    }

    private fun isMatchDayOver(matchModel: ModelClubMatch): Boolean {
        return TimeManager.judgeTimeOutOneDay(
            LocalTime(matchModel.matchHopeDateTimeStamp).formatString(
                Pattern.toStringDefault
            )
        )
    }

    private fun isAreadyMatched(matchList: MutableMap<String, ModelClub.MatchInfo>) =
        matchList.any { it.value.clubUid1 == LoginManager.user?.clubUid || it.value.clubUid2 == LoginManager.user?.clubUid }

    private fun isAlreadyApplied(it: MutableList<ModelClub.MatchApplicationInfo>) =
        it.any { it.clubUid == LoginManager.user?.clubUid }


    private fun send(
        availability: Pair<MutableList<ModelClub.MatchApplicationInfo>, Boolean>,
        matchModel: ModelClubMatch,
        path: String
    ): Single<DontCare> {
        availability.first.add(
            ModelClub.MatchApplicationInfo(
                clubName = LoginManager.user?.club!!,
                hopeDate = matchModel.hopeDate,
                peopleNumber = matchModel.matchingPeopleNumbewr!!,
                area = matchModel.matcingArea,
                clubMasterUid = LoginManager.user?.clubMasterUid!!,
                _clubUid = LoginManager.user?.clubUid!!,
                clubMarkImageUrl = LoginManager.user?.clubImageUrl,
                _timeStamp = LocalTime().time,
                _senderImageUrl = LoginManager.user?.clubImageUrl,
                _senderUid = LoginManager.user?.uid!!,
                _senderNickName = LoginManager.user?.club,
                _senderClubUid = LoginManager.user?.clubUid,
                _senderEmail = LoginManager.user?.email,
                _reciverUid = matchModel.uid,
                _message = "${LoginManager.user?.club}이 ${AlarmType.MATCHING_APPLY.text}",
                _matchInfo = ModelAlaram.MatchDetailInfo(
                    hopeDate = matchModel.hopeDate,
                    area = matchModel.matcingArea,
                    peopleNumber = matchModel.matchingPeopleNumbewr,
                    hopeDateTimeStamp = matchModel.matchHopeDateTimeStamp
                )
            )
        )

        return RxRealTimeDB.push(path, availability.first)
            .flatMap {
                FcmHelper.sendMessage(
                    destinationUid = matchModel.uid,
                    message = "${LoginManager.user?.club?.wrapWith(
                        "[",
                        "]"
                    )} 클럽이 ${AlarmType.MATCHING_APPLY.text}"
                )
            }
            .flatMap {
                val alarmPath = "${FirebaseUrls.alram}/${matchModel.uid}"
                RxRealTimeDB.toSingle(alarmPath) { mutableListOf<ModelAlaram>() }
                    .flatMap { alramList ->

                        val alarmData = ModelAlaram(
                            senderEmail = LoginManager.user?.email,
                            senderImageUrl = LoginManager.user?.clubImageUrl,
                            timeStamp = LocalTime().time,
                            senderUid = LoginManager.user?.uid!!,
                            senderNickName = LoginManager.user?.club,
                            reciverUid = matchModel.uid,
                            alaramTypeCode = AlarmType.MATCHING_APPLY.code,
                            message = "${LoginManager.user?.club} 클럽이 ${AlarmType.MATCHING_APPLY.text}",
                            senderClubUid = LoginManager.user?.clubUid,
                            matchInfo = ModelAlaram.MatchDetailInfo(
                                hopeDate = matchModel.hopeDate,
                                area = matchModel.matcingArea,
                                peopleNumber = matchModel.matchingPeopleNumbewr
                            )
                        )

                        alramList.add(alarmData)
                        RxRealTimeDB.push(alarmPath, alramList)
                    }
            }
    }
}


