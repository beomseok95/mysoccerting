package ks.bs.mysoccerting.scene.club.clubtour

import ks.bs.mysoccerting.model.ModelClub

interface ClubDetailViewPagerInterface {
    fun moveClubBase()
    fun moveBrowse()
}