package ks.bs.mysoccerting.scene.profile

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import ks.bs.mysoccerting.BR
import ks.bs.mysoccerting.R
import ks.bs.mysoccerting.databinding.TermsOfServiceFragmentBinding
import ks.bs.mysoccerting.scene.base.FragmentBase

class TermsOfServiceFragment : FragmentBase() {

    val viewModel = createVm()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding = DataBindingUtil.inflate<TermsOfServiceFragmentBinding>(
            inflater, R.layout.terms_of_service_fragment, container, false
        )
        binding.setVariable(BR.viewModel, viewModel)
        return binding.root

    }

    private fun createVm() = TermsOfServiceViewModel()
}
