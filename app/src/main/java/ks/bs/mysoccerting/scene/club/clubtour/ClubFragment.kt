package ks.bs.mysoccerting.scene.club.clubtour


import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.navigation.fragment.findNavController
import ks.bs.mysoccerting.BR
import ks.bs.mysoccerting.R
import ks.bs.mysoccerting.rx.showProgressDialog
import ks.bs.mysoccerting.scene.base.FragmentBase
import ks.bs.mysoccerting.scene.base.FragmentBaseViewModel
import ks.bs.mysoccerting.scene.login.LoginManager
import ks.bs.mysoccerting.util.TLog


class ClubFragment : FragmentBase() {
    lateinit var binding: ViewDataBinding
    var viewModel = createVm()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        if (!LoginManager.user?.clubUid.isNullOrEmpty()) {
            TLog.d(LoginManager.user?.clubUid!!)
            val action =
                ClubFragmentDirections.actionClubFragmentToClubDetailFragment(
                    LoginManager.user?.clubUid!!
                )
            findNavController().navigate(action)
        }
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_club, container, false)
        binding.setVariable(BR.viewModel, viewModel)

        return binding.root
    }
    private fun createVm() = ClubViewModel(object :
        ClubViewModel.Contract {
        override fun createDialogForProgress(request: FragmentBaseViewModel.ProgressBarOwner.Request): Dialog {
            return showProgressDialog(activity, request.disposable, request.functionOnCancel)
        }
    })

    override fun onDetach() {
        viewModel.onDetach()
        super.onDetach()
    }

}
