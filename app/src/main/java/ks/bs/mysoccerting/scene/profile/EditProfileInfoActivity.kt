package ks.bs.mysoccerting.scene.profile

import android.os.Bundle
import androidx.databinding.DataBindingUtil
import kotlinx.android.synthetic.main.activity_edit_profile_info.*
import ks.bs.mysoccerting.BR
import ks.bs.mysoccerting.R
import ks.bs.mysoccerting.databinding.ActivityEditProfileInfoBinding
import ks.bs.mysoccerting.model.ModelEditData
import ks.bs.mysoccerting.model.type.EditType
import ks.bs.mysoccerting.rx.rxresult.RxActivityBase

class EditProfileInfoActivity : RxActivityBase() {

    val viewModel = createVm()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        DataBindingUtil.setContentView<ActivityEditProfileInfoBinding>(this, R.layout.activity_edit_profile_info)
            .setVariable(BR.viewModel, viewModel)

        initLayout()
    }

    private fun initLayout() {
        val type = getInput<ModelEditData>()

        viewModel.type = EditType.valueOf(type.tag)

        toolbar_title.text = EditType.valueOf(type.tag).info + "입력"

        viewModel.hint.set(type.hint)
        viewModel.editData.set(type.data)
        viewModel.isAge.set(type.isAge)

        if (type.tag == "SEX")
            viewModel.isSex.set(true)
    }

    private fun createVm() = EditProfileInfoViewModel(object : EditProfileInfoViewModel.EditProfileInfoContract {
        override fun save(data: String) {
            finishWithResponse(data)
        }
    })

}
