package ks.bs.mysoccerting.scene.club.detail.setting

import android.view.View
import androidx.databinding.ObservableField
import io.reactivex.rxkotlin.addTo
import kr.nextm.lib.TLog
import kr.nextm.lib.TToast
import ks.bs.mysoccerting.firebase.CrashlyticsHelper
import ks.bs.mysoccerting.firebase.FirebaseUrls
import ks.bs.mysoccerting.firebase.RxRealTimeDB
import ks.bs.mysoccerting.model.ModelClub
import ks.bs.mysoccerting.scene.base.FragmentBaseViewModel
import ks.bs.mysoccerting.scene.login.LoginManager

class MercenaryBottomSheetViewModel(val contract: Contract) : FragmentBaseViewModel(contract) {

    interface Contract : BaseVmListener {
        fun dismissBottomSheet()
    }

    val text = ObservableField<String>()
    val profileImageUrl = ObservableField<String>()

    var model = ModelClub.ModelMember()


    fun initContent(model: ModelClub.ModelMember) {
        this.model = model
        text.set("${model.nickName} 용병등록을 신청하였습니다.")
        profileImageUrl.set(model.memberImageUrl)
    }

    fun deleteAlarm(view: View) {
        RxRealTimeDB.toSingle("${FirebaseUrls.club}/${LoginManager.user?.clubUid}") { ModelClub() }
            .map { modelClub ->
                modelClub.mercenaryApplicationList?.removeAll { it.uid == model.uid }
                modelClub
            }
            .flatMap { model ->
                RxRealTimeDB.push("${FirebaseUrls.club}/${LoginManager.user?.clubUid}", model)
            }
            .withProgressBallAnimation()
            .subscribe({
                contract.dismissBottomSheet()
                TToast.show("삭제를 성공했습니다.")
                TLog.d("delete Alaram success")
            }, { e ->
                contract.dismissBottomSheet()
                TToast.show("삭제를 실패했습니다.")
                CrashlyticsHelper.logException(e)
            })
            .addTo(compositeDisposable)
    }
}