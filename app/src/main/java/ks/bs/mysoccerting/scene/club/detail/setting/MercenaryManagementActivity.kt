package ks.bs.mysoccerting.scene.club.detail.setting

import android.os.Bundle
import androidx.databinding.DataBindingUtil
import kotlinx.android.synthetic.main.activity_membership_management.toolbar_title
import ks.bs.mysoccerting.BR
import ks.bs.mysoccerting.R
import ks.bs.mysoccerting.databinding.ActivityMercenaryManagementBinding
import ks.bs.mysoccerting.scene.base.ActivityBase

class MercenaryManagementActivity : ActivityBase() {

    val viewModel = createVm()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        DataBindingUtil.setContentView<ActivityMercenaryManagementBinding>(
            this,
            R.layout.activity_mercenary_management
        )
            .setVariable(BR.viewModel, viewModel)

        toolbar_title.text = "용병가입신청관리"

        viewModel.loadAffiliateList()
    }

    fun createVm() = MercenaryManagementVm()

    override fun onDestroy() {
        viewModel.clearDisposables()
        super.onDestroy()
    }
}
