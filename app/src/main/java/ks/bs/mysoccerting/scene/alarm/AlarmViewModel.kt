package ks.bs.mysoccerting.scene.alarm

import android.view.View
import androidx.databinding.ObservableBoolean
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.GenericTypeIndicator
import io.reactivex.Observable
import io.reactivex.rxkotlin.addTo
import ks.bs.mysoccerting.BR
import ks.bs.mysoccerting.R
import ks.bs.mysoccerting.databinding.BaseRecyclerViewAdapter
import ks.bs.mysoccerting.databinding.ItemAlarmBinding
import ks.bs.mysoccerting.firebase.FirebaseHelper
import ks.bs.mysoccerting.firebase.FirebaseUrls
import ks.bs.mysoccerting.firebase.RxRealTimeDB
import ks.bs.mysoccerting.model.ModelAlaram
import ks.bs.mysoccerting.model.ModelMoveTo
import ks.bs.mysoccerting.model.time.LocalTime
import ks.bs.mysoccerting.scene.base.ActivityBaseViewModel
import ks.bs.mysoccerting.scene.login.LoginManager
import ks.bs.mysoccerting.util.TLog

class AlarmViewModel(val contract: Contract) : ActivityBaseViewModel() {
    interface Contract {
        fun finishPage(moveLocation: ModelMoveTo)
    }

    val isLogin = ObservableBoolean(true)

    val newAlarmAdapter =
        BaseRecyclerViewAdapter<AlarmItemViewModel, ItemAlarmBinding>(
            R.layout.item_alarm,
            BR.viewModel
        )


    fun loadAlarmData() {
        if (FirebaseAuth.getInstance().currentUser != null) {
            val indecatior = object : GenericTypeIndicator<MutableList<ModelAlaram>>() {}
            RxRealTimeDB.toObservable("${FirebaseUrls.alram}/${LoginManager.user?.uid}", indecatior) { mutableListOf() }
                .newAlarmDivider()
                .setNewAlarmData()
                .priorAlarmDivider()
                .setPriorAlarmData()
                .subscribe({ response ->
                }, { e ->
                    TLog.e(e)
                })
                .addTo(compositeDisposable)
        } else
            isLogin.set(false)
    }


    private fun Observable<MutableList<ModelAlaram>>.newAlarmDivider(): Observable<MutableList<ModelAlaram>> {
        return this.map { response ->
            val oneDay = 86_400_000
            newAlarmAdapter.clear()

            response.any { (LocalTime().time - it.timeStamp) < oneDay }
                .let { result ->
                    if (result)
                        newAlarmAdapter.insert(AlarmItemViewModel(ModelAlaram.dividerModelCreate("새로운 알림"), contract))
                }

            response
        }
    }

    private fun Observable<MutableList<ModelAlaram>>.setNewAlarmData(): Observable<MutableList<ModelAlaram>> {
        return this.map { response ->
            val oneDay = 86_400_000

            response.filter { (LocalTime().time - it.timeStamp) < oneDay }
                .sortedByDescending { it.timeStamp }
                .forEach { newAlarmAdapter.insert(AlarmItemViewModel(it, contract)) }

            response
        }
    }


    private fun Observable<MutableList<ModelAlaram>>.priorAlarmDivider(): Observable<MutableList<ModelAlaram>> {
        return this.map { response ->
            val oneDay = 86_400_000

            response.any { (LocalTime().time - it.timeStamp) > oneDay }
                .let { result ->
                    if (result)
                        newAlarmAdapter.insert(AlarmItemViewModel(ModelAlaram.dividerModelCreate("이전 알림"), contract))
                }

            response
        }
    }

    private fun Observable<MutableList<ModelAlaram>>.setPriorAlarmData(): Observable<MutableList<ModelAlaram>> {
        return this.map { response ->
            val oneDay = 86_400_000

            response.filter { (LocalTime().time - it.timeStamp) > oneDay }
                .sortedByDescending { it.timeStamp }
                .forEach { newAlarmAdapter.insert(AlarmItemViewModel(it, contract)) }

            response
        }
    }

    fun backPressed(view: View) {
        val activity = view.context as AlarmActivity
        activity.onBackPressed()
    }

    fun login(view: View) {
        LoginManager.noUserSafeAction(view.context, compositeDisposable) {}
    }
}