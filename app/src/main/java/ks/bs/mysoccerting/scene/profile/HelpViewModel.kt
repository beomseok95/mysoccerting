package ks.bs.mysoccerting.scene.profile

import android.view.View
import ks.bs.mysoccerting.rx.rxresult.putObject
import ks.bs.mysoccerting.rx.rxresult.startActivity
import ks.bs.mysoccerting.rx.rxresult.toIntent


class HelpViewModel {

    fun sendSuggestions(view: View) {
        ReportActivity::class.java
            .toIntent()
            .putObject(SUGGESTIONS)
            .startActivity(view.context)
            .subscribe()
    }

    fun bugReport(view: View) {
        ReportActivity::class.java
            .toIntent()
            .putObject(REPORT)
            .startActivity(view.context)
            .subscribe()
    }

    companion object{
        const val SUGGESTIONS ="suggestions"
        const val REPORT ="report"
    }
}
