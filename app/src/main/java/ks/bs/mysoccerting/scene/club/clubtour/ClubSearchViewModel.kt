package ks.bs.mysoccerting.scene.club.clubtour

import androidx.databinding.ObservableField
import android.view.inputmethod.EditorInfo
import android.widget.EditText
import android.widget.TextView
import androidx.databinding.ObservableBoolean
import io.reactivex.Single
import io.reactivex.rxkotlin.addTo
import ks.bs.mysoccerting.BR
import ks.bs.mysoccerting.R
import ks.bs.mysoccerting.adapter.GridFilterAdapter
import ks.bs.mysoccerting.databinding.ItemClubSearchBinding
import ks.bs.mysoccerting.firebase.CrashlyticsHelper
import ks.bs.mysoccerting.firebase.FirebaseHelper
import ks.bs.mysoccerting.model.Area
import ks.bs.mysoccerting.scene.base.FragmentBaseViewModel
import ks.bs.mysoccerting.util.EditKeyborad
import ks.bs.mysoccerting.util.TLog

class ClubSearchViewModel(val contract: ClubSearchFragmentContract) : FragmentBaseViewModel(contract), EditKeyborad {
    interface ClubSearchFragmentContract : BaseVmListener {
        fun showAreaDialog(): Single<Area>
    }

    var isDesending = false
    var areaFilterText = ObservableField("위치")
    var areaFilterSelected = ObservableBoolean(false)

    val isFiltered = ObservableBoolean(false)

    val adapter = GridFilterAdapter<ClubSearchItemVm, ItemClubSearchBinding>(
        R.layout.item_club_search, BR.clubModel, 2
    )

    override var searchText = ObservableField("")

    override val editTextActionListener =
        TextView.OnEditorActionListener { v, actionId, _ -> initEditAction(actionId, v) }

    private fun initEditAction(actionId: Int, v: TextView?): Boolean {
        when (actionId) {
            EditorInfo.IME_ACTION_SEARCH -> {
                changeFilter()
                hideKeyBorad(v)
            }

            else -> {
                changeFilter()
                hideKeyBorad(v)
                return false
            }
        }
        return true
    }

    fun getContents() {
        FirebaseHelper.clubBrowseSubject
            .map { it.values.toList() }
            .subscribe({ response ->
                adapter.clear()
                response.forEach { model ->
                    adapter.insert(ClubSearchItemVm(model))
                }
                adapter.items.sortByDescending { it.create_at }
            }, {CrashlyticsHelper.logException(it) })
            .addTo(compositeDisposable)
    }


    fun showAreaDialog() {
        contract.showAreaDialog()
            .subscribe({
                val city = it.selectedCity
                val town = it.selectedTown

                if (town == null && city == null) {
                    areaFilterText.set("")
                    areaFilterSelected.set(false)
                    isFiltered.set(false)
                    return@subscribe
                }
                if (town == null) {
                    val result = "$city"
                    areaFilterText.set(result)
                    areaFilterSelected.set(true)
                    isFiltered.set(true)
                    changeFilter()
                    return@subscribe
                }

                val result = "$city $town"
                areaFilterText.set(result)
                areaFilterSelected.set(true)
                isFiltered.set(true)
                changeFilter()
            }, {
                CrashlyticsHelper.logException(it)
                TLog.e(it.message.toString())
            }).let { addDisposable(it) }
    }


    private fun changeFilter() {
        val area = areaFilterText.get()?.split(" ")

        if (searchText.get()!!.isNotEmpty())
            adapter.changeFilter {
                it.activityArea.contains(area?.get(0) ?: "------------------")
                it.clubIntro.contains(searchText.get()!!) ||
                        it.clubName.contains(searchText.get()!!)
            }
        else
            adapter.changeFilter {
                it.activityArea.contains(area?.get(0) ?: "------------------")
            }

    }

    fun onSearch(editText: EditText) {
        changeFilter()
        hideKeyBorad(editText)
    }

    fun onNumberSort() {
        if (isDesending)
            adapter.items.sortBy { it.clubMemberCount() }
        else
            adapter.items.sortByDescending { it.clubMemberCount() }

        isDesending = !isDesending

        adapter.notifyDataSetChanged()
    }

    fun filterRefresh() {
        isDesending = false
        onNumberSort()
        isFiltered.set(false)
        areaFilterSelected.set(false)
        areaFilterText.set("위치")
        searchText.set("")
        adapter.resetFilter()
    }
}