package ks.bs.mysoccerting.scene.profile

import android.view.View
import androidx.databinding.ObservableField

class PrivacyPolicyVm {

    val url = ObservableField("file:///android_asset/privacy_policy.html")

    fun backPressed(view: View) {
        val activity = view.context as PrivacyPolicyActivity
        activity.onBackPressed()
    }
}