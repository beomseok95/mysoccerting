package ks.bs.mysoccerting.scene.club.detail.board

import android.content.Intent
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import io.reactivex.Single
import kotlinx.android.synthetic.main.activity_board_detail.*
import kr.nextm.lib.TToast
import ks.bs.mysoccerting.BR
import ks.bs.mysoccerting.R
import ks.bs.mysoccerting.customview.dialog.TDialog
import ks.bs.mysoccerting.databinding.ActivityBoardDetailBinding
import ks.bs.mysoccerting.rx.rxresult.RxActivityBase
import ks.bs.mysoccerting.rx.rxresult.startActivityForResult
import ks.bs.mysoccerting.scene.common.CommentAmendActivity

class DetailBoardActivity : RxActivityBase() {
    private val viewModel = createVm()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        DataBindingUtil.setContentView<ActivityBoardDetailBinding>(this, R.layout.activity_board_detail)
            .setVariable(BR.viewModel, viewModel)

        toolbar_title.text = "클럽 게시글 상세"
        viewModel.init(getInput())
    }

    private fun createVm() = DetailBoardViewModel(object :
        DetailBoardViewModel.Contract {
        override fun showToast(message: String) {
            TToast.show(message)
        }

        override fun moveAmendActivity(): Single<String> {
            return Intent(this@DetailBoardActivity, CommentAmendActivity::class.java)
                .startActivityForResult(this@DetailBoardActivity)
        }

        override fun finishPage(moveToClubPage: Boolean) {
            finishWithResponse(moveToClubPage)
        }

        override fun showCommentSubmitDialog(): TDialog.Builder {
            return TDialog.Builder(this@DetailBoardActivity, "알림")
        }

        override fun reloadCommentCount(count: Int) {
            setCommentCount(count)
        }
    })

    private fun setCommentCount(count: Int) {
        viewModel.commentcount.set(count.toString())
    }

    override fun onDestroy() {
        viewModel.onDestroy()
        super.onDestroy()
    }

    override fun onBackPressed() {
        finishWithResponse("")
    }
}