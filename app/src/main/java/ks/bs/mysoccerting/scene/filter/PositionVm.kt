package ks.bs.mysoccerting.scene.filter

import android.view.View
import android.widget.RadioGroup
import androidx.databinding.ObservableBoolean
import kr.nextm.lib.TLog
import ks.bs.mysoccerting.R
import ks.bs.mysoccerting.scene.base.FragmentBase

class PositionVm(val contract: Contract) : FragmentBase() {

    interface Contract {
        fun selectComplete(select: String)
        fun onBackPressed()
    }

    val fwSelected = ObservableBoolean(false)
    val mfSelected = ObservableBoolean(false)
    val dfSelected = ObservableBoolean(false)
    val gkSelected = ObservableBoolean(false)

    var selectPosition = ""

    val radioCheckedListener = RadioGroup.OnCheckedChangeListener { group, checkedId ->
        TLog.d("radio selected id  $checkedId")

        when (checkedId) {
            R.id.btn_radio_fw -> {
                fwSelected.set(true)
                mfSelected.set(false)
                dfSelected.set(false)
                gkSelected.set(false)
                selectPosition = "FW"
            }
            R.id.btn_radio_mf -> {
                fwSelected.set(false)
                mfSelected.set(true)
                dfSelected.set(false)
                gkSelected.set(false)
                selectPosition = "MF"
            }
            R.id.btn_radio_df -> {
                fwSelected.set(false)
                mfSelected.set(false)
                dfSelected.set(true)
                gkSelected.set(false)
                selectPosition = "DF"
            }
            else -> {
                fwSelected.set(false)
                mfSelected.set(false)
                dfSelected.set(false)
                gkSelected.set(true)
                selectPosition = "GK"
            }
        }
    }

    fun cancel(view: View) {
        contract.onBackPressed()

    }

    fun ok(view: View) {
        contract.selectComplete(selectPosition)
    }
}

