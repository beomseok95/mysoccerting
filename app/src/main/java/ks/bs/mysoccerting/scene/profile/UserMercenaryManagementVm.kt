package ks.bs.mysoccerting.scene.profile

import android.view.View
import com.google.firebase.database.GenericTypeIndicator
import io.reactivex.rxkotlin.addTo
import ks.bs.mysoccerting.BR
import ks.bs.mysoccerting.R
import ks.bs.mysoccerting.adapter.GridItemAdapter
import ks.bs.mysoccerting.databinding.ItemMercenaryingClubBinding
import ks.bs.mysoccerting.firebase.CrashlyticsHelper
import ks.bs.mysoccerting.firebase.FirebaseUrls
import ks.bs.mysoccerting.firebase.RxRealTimeDB.toObservable
import ks.bs.mysoccerting.model.ModelUser
import ks.bs.mysoccerting.scene.base.ActivityBaseViewModel
import ks.bs.mysoccerting.scene.login.LoginManager
import ks.bs.mysoccerting.util.TLog

class UserMercenaryManagementVm(val contract: Contract) : ActivityBaseViewModel() {

    interface Contract {
        fun finishPageWithResponse(contentId: String)
    }

    val adapter = GridItemAdapter<ClubItemVm, ItemMercenaryingClubBinding>(
        R.layout.item_mercenarying_club,
        BR.viewModel,
        2
    )

    fun backPressed(view: View) {
        val activity = view.context as UserMercenaryManagementActivity
        activity.onBackPressed()
    }

    fun init() {
        val indecatior = object : GenericTypeIndicator<MutableList<ModelUser.ModelMercenaryClub>>() {}
        toObservable(
            "${FirebaseUrls.users}/${LoginManager.user?.uid}/mercenaryClubInfo",
            indecatior
        ) { mutableListOf() }
            .subscribe({ response ->
                adapter.clear()
                response.forEach { adapter.insert(ClubItemVm(it, contract)) }
            }, { e ->
                CrashlyticsHelper.logException(e)
            })
            .addTo(compositeDisposable)
    }
}