package ks.bs.mysoccerting.scene.club.detail.chat

import android.view.View
import androidx.databinding.ObservableField
import com.google.firebase.database.GenericTypeIndicator
import io.reactivex.Observable
import io.reactivex.rxkotlin.addTo
import ks.bs.mysoccerting.BR
import ks.bs.mysoccerting.R
import ks.bs.mysoccerting.databinding.BaseRecyclerViewAdapter
import ks.bs.mysoccerting.firebase.CrashlyticsHelper
import ks.bs.mysoccerting.firebase.FirebaseUrls
import ks.bs.mysoccerting.firebase.RxRealTimeDB
import ks.bs.mysoccerting.model.ModelChat
import ks.bs.mysoccerting.model.time.LocalTime
import ks.bs.mysoccerting.scene.base.FragmentBaseViewModel
import ks.bs.mysoccerting.scene.club.supervisor.TimeManager
import ks.bs.mysoccerting.scene.club.supervisor.getFormattDate
import ks.bs.mysoccerting.scene.login.LoginManager

class ClubChatViewModel(val contract: Contract) : FragmentBaseViewModel(contract) {
    interface Contract : BaseVmListener {
        fun scrollBottom()
    }

    val adapter = BaseRecyclerViewAdapter<ChatItemVm, ks.bs.mysoccerting.databinding.ItemChatBinding>(
        R.layout.item_chat,
        BR.viewModel
    )

    val chatText = ObservableField("")
    var contentId: String = ""

    fun init(contentId: String) {
        this.contentId = contentId

        val path = "${FirebaseUrls.clubChat}/$contentId"
        val indecator = object : GenericTypeIndicator<HashMap<String, ModelChat>>() {}

        RxRealTimeDB.toObservable(path, indecator) { HashMap() }
            .overTimeChatDelete()
            .map { it.values }
            .subscribe({
                adapter.clear()
                it.forEach { chatModel -> adapter.insert(ChatItemVm(chatModel, false)) }
                adapter.items.sortBy { it.timeStamp }
                contract.scrollBottom()

            }, { e ->
                CrashlyticsHelper.logException(e)
            })
            .addTo(compositeDisposable)
    }

    private fun Observable<HashMap<String, ModelChat>>.overTimeChatDelete(): Observable<HashMap<String, ModelChat>> {
        return this.flatMap {
            it.forEach { chat ->
                if (TimeManager.judgeTimeOutSevenDays(chat.value.timeStamp.getFormattDate()))
                    RxRealTimeDB.remove("${FirebaseUrls.clubChat}/${LoginManager.user?.clubUid}/${chat.key}")
                        .subscribe()
            }

            this
        }
    }

    fun chatSend(view: View) {
        if (chatText.get()!!.isEmpty())
            return

        val contentId = RxRealTimeDB.generateRandomKey("chat")

        val chatModel = ModelChat(
            uid = LoginManager.user?.uid!!,
            content = chatText.get()!!,
            nickName = LoginManager.user?.nickName!!,
            profileImageUrl = LoginManager.user?.profileImageUrl,
            timeStamp = LocalTime().time
        )

        RxRealTimeDB.push("${FirebaseUrls.clubChat}/${this.contentId}/$contentId", chatModel)
            .subscribe({
                chatText.set("")
            }, { e ->
                CrashlyticsHelper.logException(e)
            })
            .addTo(compositeDisposable)
    }

}