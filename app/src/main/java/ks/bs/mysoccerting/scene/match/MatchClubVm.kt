package ks.bs.mysoccerting.scene.match

import ks.bs.mysoccerting.model.ModelClubMatch

class MatchClubVm(val model: ModelClubMatch, val contract: MatchingViewModel.Contract) {

    val clubImageUrl get() = model.clubImageUrl
    val clubName get() = model.clubName
    val age get() = model.age
    val hopeDate get() = "매칭 희망일 : ${model.hopeDate}"
    val content get() = model.content ?: ""
    val uid get() = model.uid
    val commentcount get() = model.commentcount
    val matcingArea get() = model.matcingArea
    val matchingPeopleNumbewr get() = model.matchingPeopleNumbewr
    val contentId get() = model.contentId
    val timestamp get() = model.timestamp

    fun moveItemDetailPage() {
        contract.moveItemDetailPage(model)
            .subscribe()
    }
}