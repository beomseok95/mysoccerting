package ks.bs.mysoccerting.scene.search


import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import io.reactivex.Single
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.activity_post_add.*
import kr.nextm.lib.TToast
import ks.bs.mysoccerting.BR
import ks.bs.mysoccerting.R
import ks.bs.mysoccerting.model.Area
import ks.bs.mysoccerting.model.ModelBase
import ks.bs.mysoccerting.model.ModelClubSearch
import ks.bs.mysoccerting.model.ModelMercenarySearch
import ks.bs.mysoccerting.rx.rxresult.putObject
import ks.bs.mysoccerting.rx.rxresult.startActivityForResult
import ks.bs.mysoccerting.rx.showProgressDialog
import ks.bs.mysoccerting.scene.base.FragmentBase
import ks.bs.mysoccerting.scene.base.FragmentBaseViewModel
import ks.bs.mysoccerting.scene.common.DetailItemActivity
import ks.bs.mysoccerting.scene.common.PostAddActivity
import ks.bs.mysoccerting.scene.filter.AreaDialog
import ks.bs.mysoccerting.scene.filter.CalendarDialog
import ks.bs.mysoccerting.scene.filter.PeopleNumberDialog


class SearchFragment : FragmentBase() {
    var viewModel = createVm()

    lateinit var binding: ViewDataBinding
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_search, container, false)
        binding.setVariable(BR.viewModel, viewModel)
        return binding.root
    }

    private fun createVm() = SearchViewModel(object :
        SearchViewModel.SearchFragmentContract {
        override fun resetViewModel() {
            resetBinding()
        }

        override fun showToast(message: String) {
            TToast.show(message)
        }

        override fun showPeopleNumberDialog(): Single<MutableList<String>> {
            requireNotNull(context){"Context need to  Not null!!"}
            return PeopleNumberDialog()
                .toSingle(context!!)
        }

        override fun showAreaDialog(): Single<Area> {
            requireNotNull(context){"context need not null!!"}
            return AreaDialog()
                .toSingle(context!!)
        }

        override fun showCalendarDialog(): Single<CalendarDialog.Date> {
            requireNotNull(context){"context need to Not Null!!"}
            return CalendarDialog()
                .toSingle(context!!)
        }

        override fun moveItemDetailPageMercenary(item: ModelMercenarySearch): Single<ModelBase> {
            DetailItemActivity.CALL_LOCATION =
                CALL_TAG_SEARCH_MERCENARY
            return Intent(activity, DetailItemActivity::class.java)
                .putObject(item)
                .startActivityForResult(context!!)
        }

        override fun moveItemDetailPageClub(item: ModelClubSearch): Single<ModelBase> {
            DetailItemActivity.CALL_LOCATION =
                CALL_TAG_SEARCH_CLUB
            return Intent(activity, DetailItemActivity::class.java)
                .putObject(item)
                .startActivityForResult(context!!)
        }

        override fun movePostWirteActivity(currentPage: String): Single<Any> {
            return Intent(activity, PostAddActivity::class.java)
                .putObject(currentPage)
                .startActivityForResult(context!!)
        }

        override fun getDisposable(): CompositeDisposable {
            return getViewModelDiposalbe()
        }
        override fun createDialogForProgress(request: FragmentBaseViewModel.ProgressBarOwner.Request): Dialog {
            return showProgressDialog(activity, request.disposable, request.functionOnCancel)
        }

        override fun resetFlexBoxView() {
            plexobx.removeAllViews()
        }
    })

    private fun getViewModelDiposalbe(): CompositeDisposable {
        return viewModel.compositeDisposable
    }

    private fun resetBinding() {
        binding.setVariable(BR.viewModel, viewModel)
    }


    override fun onDetach() {
        viewModel.onDetach()
        super.onDetach()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        viewModel.init()
    }

    companion object {
        const val CALL_TAG_SEARCH_MERCENARY = "Search_Mercenary"
        const val CALL_TAG_SEARCH_CLUB = "Search_Club"
    }
}
