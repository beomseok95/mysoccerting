package ks.bs.mysoccerting.scene.match

import android.view.View
import androidx.databinding.ObservableField
import ks.bs.mysoccerting.model.ModelClubMatch
import ks.bs.mysoccerting.rx.toJson
import ks.bs.mysoccerting.scene.base.FragmentBaseViewModel
import ks.bs.mysoccerting.util.TLog

class MatchingInfoCheckVm(val contract: Contract) : FragmentBaseViewModel(contract) {

    interface Contract : BaseVmListener {
        fun onBackPressed()
        fun response(result: Boolean)
    }

    val cancelText = ObservableField("취소")
    val date = ObservableField<String>()
    val peopleNumber = ObservableField<String>()
    val area = ObservableField<String>()

    fun init(model: ModelClubMatch) {
        date.set(model.hopeDate)
        peopleNumber.set(model.matchingPeopleNumbewr)
        area.set(model.matcingArea)
    }

    fun cancel(view: View) {
        contract.response(false)
    }

    fun ok(view: View) {
        contract.response(true)
    }


}