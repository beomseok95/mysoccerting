package ks.bs.mysoccerting.scene.profile

import android.content.pm.PackageManager
import android.graphics.drawable.Drawable
import android.net.Uri
import android.view.View
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.core.net.toUri
import androidx.databinding.ObservableField
import io.reactivex.Single
import io.reactivex.rxkotlin.addTo
import kr.nextm.lib.TToast
import ks.bs.mysoccerting.exception.ErrorCode
import ks.bs.mysoccerting.firebase.CrashlyticsHelper
import ks.bs.mysoccerting.firebase.FirebaseUrls
import ks.bs.mysoccerting.firebase.RxRealTimeDB
import ks.bs.mysoccerting.firebase.RxStorage
import ks.bs.mysoccerting.model.ModelClub
import ks.bs.mysoccerting.model.ModelEditData
import ks.bs.mysoccerting.model.ModelUser
import ks.bs.mysoccerting.model.type.EditType
import ks.bs.mysoccerting.rx.rxresult.putObject
import ks.bs.mysoccerting.rx.rxresult.startActivityForResult
import ks.bs.mysoccerting.rx.rxresult.toIntent
import ks.bs.mysoccerting.rx.withProgressBallAnimation
import ks.bs.mysoccerting.scene.base.ActivityBaseViewModel
import ks.bs.mysoccerting.scene.club.supervisor.updateClubBoard
import ks.bs.mysoccerting.scene.filter.AreaDialog
import ks.bs.mysoccerting.scene.filter.PositionDialog
import ks.bs.mysoccerting.scene.login.LoginManager
import ks.bs.mysoccerting.support.DontCare
import ks.bs.mysoccerting.util.TLog
import ks.bs.mysoccerting.util.isAgeAll
import ks.bs.mysoccerting.util.toSafeInt

class ProfileSettingViewModel(val contract: ProfileSettingContract) : ActivityBaseViewModel() {
    interface ProfileSettingContract {
        fun getDrawableFromUri(uri: Uri): Drawable
        fun checkExternalStoragePermission()
        fun finishSuccess(images: Pair<String?, String?>)
        fun bringToFront()
    }

    val age = ObservableField<String>()
    val sex = ObservableField<String>()
    val alias = ObservableField<String>()
    val area = ObservableField<String>()
    var positionList = listOf<String>()
    val position = ObservableField<String>()
    val profileImageUrl = ObservableField<String>()
    val backgroundImageUrl = ObservableField<String>()

    var selectProfileImageUri: String? = null
    var selectBackgroundImageUri: String? = null


    fun initUserData() {
        val path = "${FirebaseUrls.users}/${auth.currentUser?.uid}"
        RxRealTimeDB.toSingle(path) { ModelUser() }
            .subscribe({ response ->
                if (response.age != null)
                    age.set(isAgeAll(response.age!!))

                sex.set(response.sex)
                alias.set(response.nickName)
                area.set(response.area)
                position.set(response.position?.joinToString(" "))

                if (response.profileImageUrl != null)
                    profileImageUrl.set(response.profileImageUrl)

            }, { e ->
                CrashlyticsHelper.logException(e)
            })
            .addTo(compositeDisposable)
    }

    fun moveEditPage(view: View, child: TextView, isAge: Boolean = false) {
        EditProfileInfoActivity::class.java
            .toIntent()
            .putObject(
                ModelEditData(
                    tag = view.tag.toString(),
                    data = child.text.toString(),
                    hint = EditType.valueOf(view.tag.toString()).hint,
                    isAge = isAge
                )
            )
            .startActivityForResult<String>(view.context)
            .subscribe({ result ->
                resultAdd(view.tag.toString(), result)
            }, { e ->
                CrashlyticsHelper.logException(e)
            })
            .addTo(compositeDisposable)
    }

    private fun resultAdd(tag: String, result: String) {
        when (tag) {
            EditType.AGE.tag -> age.set(result)
            EditType.SEX.tag -> sex.set(result)
            EditType.ALIAS.tag -> alias.set(result)
        }
    }

    fun showAreaDialog(view: View) {
        AreaDialog()
            .toSingle(view.context)
            .subscribe({
                val city = it.selectedCity
                val town = it.selectedTown

                if (town == null && city == null) {
                    return@subscribe
                }
                if (town == null) {
                    area.set("$city")
                    return@subscribe
                }

                area.set("$city $town")
            }, {
                CrashlyticsHelper.logException(it)
                TLog.e(it.message.toString())
            }).let { addDisposable(it) }
    }


    fun showPositionDialog(view: View) {
        PositionDialog()
            .toSingle(view.context)
            .subscribe({ result ->
                position.set(result)
            }, { e ->
                CrashlyticsHelper.logException(e)
            })
            .addTo(compositeDisposable)
    }

    fun changeProfile(view: View) {
        if (ContextCompat.checkSelfPermission(
                view.context,
                android.Manifest.permission.READ_EXTERNAL_STORAGE
            ) == PackageManager.PERMISSION_GRANTED
        ) {
            TransparentAlbumActivity::class.java
                .toIntent()
                .startActivityForResult<String>(view.context)
                .subscribe({ response ->
                    TLog.d(response)
                    selectProfileImageUri = response
                    profileImageUrl.set(response)

                }, { e ->
                    CrashlyticsHelper.logException(e)
                })
                .addTo(compositeDisposable)
        } else {
            TToast.show("권한없음")
            contract.checkExternalStoragePermission()
        }
    }

    fun ok(view: View) {
        requireNotNull(LoginManager.user) { ErrorCode.FAIL_NO_USER_INFO_SELF.message }

        val nickName = LoginManager.user?.nickName
        val userData = LoginManager.user!!
            .also {
                it.nickName = alias.get()
                it.age = age.get().toSafeInt()
                it.area = area.get()
                it.sex = sex.get()
                it.position = if (position.get() != null) listOf(position.get().toString()) else null
            }


        val profileObservable =
            if (selectProfileImageUri != null)
                RxStorage.push("userProfileImages", selectProfileImageUri!!.toUri())
                    .withProgressBallAnimation(view.context)
            else
                Single.just("")


        profileObservable
            .flatMap {
                val profileImageUrl =
                    if (it.isEmpty())
                        profileImageUrl.get()
                    else
                        it

                userData.profileImageUrl = profileImageUrl

                Single.just(userData)
            }
            .flatMap { data ->
                val path = "${FirebaseUrls.users}/${auth.currentUser?.uid}"

                RxRealTimeDB.push(path, data)
            }
            .updateInClubMemberInfo(nickName!!)
            .updateClubBoard(userData)
            .flatMap {
                val path = "${FirebaseUrls.club}/${LoginManager.user!!.clubUid}"
                RxRealTimeDB.toSingle(path) { ModelClub() }
                    .flatMap { modelClub ->
                        modelClub.clubPositionList?.forEach {
                            if (it.value == nickName)
                                modelClub.clubPositionList?.set(it.key, alias.get().toString())
                        }
                        RxRealTimeDB.push("$path/clubPositionList", modelClub.clubPositionList)
                    }
            }
            .subscribe({
                TLog.d("User Info change success!!")

                LoginManager.saveUserInfo(userData)

                contract.finishSuccess(selectProfileImageUri to selectBackgroundImageUri)
            }, { e ->
                TToast.show("프로필 저장이 실패했습니다.\n$e")
                CrashlyticsHelper.logException(e)
            })
            .addTo(compositeDisposable)
    }

    fun backPressed(view: View) {
        val activity = view.context as ProfileSettingActivity
        activity.onBackPressed()
    }

    private fun <T> Single<T>.updateInClubMemberInfo(beforNickName: String): Single<DontCare> {
        return this.flatMap {
            if (LoginManager.user!!.clubUid?.isNotEmpty() == true) {
                val path = "${FirebaseUrls.club}/${LoginManager.user!!.clubUid}/clubMembers"
                RxRealTimeDB.toSingle(path) { mutableListOf<ModelClub.ModelMember>() }
                    .flatMap { memberList ->

                        memberList.forEachIndexed { index, modelMember ->
                            if( modelMember.uid == LoginManager.user!!.uid ){
                                modelMember.nickName = LoginManager.user!!.nickName ?: ""
                                modelMember.memberImageUrl = LoginManager.user!!.profileImageUrl
                                modelMember.position = LoginManager.user!!.position?.let { it.joinToString(" ") }
                                modelMember.age = LoginManager.user!!.age?.let { it.toString() }

                            }

                        }
                        RxRealTimeDB.push(path, memberList)
                    }
                    .flatMap { RxRealTimeDB.toSingle("${FirebaseUrls.club}/${LoginManager.user!!.clubUid}/clubPositionList") { HashMap<String, String>() } }
                    .flatMap {
                        var findKey = ""

                        it.forEach {
                            if (it.value == beforNickName)
                                findKey = it.key
                        }

                        if (findKey.isNotEmpty())
                            it.put(findKey, LoginManager.user?.nickName!!)

                        if (LoginManager.user?.clubUid != null && !it.isNullOrEmpty())
                            RxRealTimeDB.push("${FirebaseUrls.club}/${LoginManager.user!!.clubUid}/clubPositionList", it)
                        else
                            Single.just(DontCare)
                    }
            } else
                Single.just(DontCare)
        }
    }

}
