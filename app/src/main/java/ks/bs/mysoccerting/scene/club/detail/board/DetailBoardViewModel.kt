package ks.bs.mysoccerting.scene.club.detail.board

import android.view.View
import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import androidx.databinding.ObservableLong
import com.google.firebase.database.GenericTypeIndicator
import io.reactivex.Maybe
import io.reactivex.Single
import io.reactivex.rxkotlin.addTo
import ks.bs.mysoccerting.BR
import ks.bs.mysoccerting.R
import ks.bs.mysoccerting.customview.dialog.DialogButton
import ks.bs.mysoccerting.customview.dialog.TDialog
import ks.bs.mysoccerting.databinding.BaseRecyclerViewAdapter
import ks.bs.mysoccerting.databinding.ItemCommentBinding
import ks.bs.mysoccerting.exception.ErrorCode
import ks.bs.mysoccerting.fcm.FcmHelper
import ks.bs.mysoccerting.firebase.CrashlyticsHelper
import ks.bs.mysoccerting.firebase.FirebaseUrls
import ks.bs.mysoccerting.firebase.RxRealTimeDB
import ks.bs.mysoccerting.model.ModelAlaram
import ks.bs.mysoccerting.model.ModelClubBoard
import ks.bs.mysoccerting.model.ModelComment
import ks.bs.mysoccerting.model.time.LocalTime
import ks.bs.mysoccerting.model.type.AlarmType
import ks.bs.mysoccerting.scene.base.ActivityBaseViewModel
import ks.bs.mysoccerting.scene.club.detail.ClubDetailFragment
import ks.bs.mysoccerting.scene.login.LoginManager
import ks.bs.mysoccerting.support.DontCare
import ks.bs.mysoccerting.util.DateEx
import ks.bs.mysoccerting.util.TLog
import ks.bs.mysoccerting.util.getString

class DetailBoardViewModel(private val contract: Contract) : ActivityBaseViewModel() {
    interface Contract {
        fun showToast(message: String)
        fun showCommentSubmitDialog(): TDialog.Builder
        fun moveAmendActivity(): Single<String>
        fun finishPage(moveToCLubPage: Boolean)
        fun reloadCommentCount(count: Int)
    }

    var commentContent = ObservableField<String>()

    var commentVisibility = ObservableBoolean(true)

    val profileImageUrl = ObservableField<String>()
    val nickName = ObservableField<String>()
    val content = ObservableField<String>()
    val commentcount = ObservableField<String>()
    val timestamp = ObservableLong()
    var commentContentId = ""
    var postWriterUid = ""

    val commentAdapter =
        BaseRecyclerViewAdapter<ClubBoardCommentItemVm, ItemCommentBinding>(
            R.layout.item_comment,
            BR.modelComment
        )

    fun init(model: ModelClubBoard) {
        profileImageUrl.set(model.profileImageUrl)
        content.set(model.content)
        nickName.set(model.nickName)
        commentcount.set(model.commentcount.toString())
        timestamp.set(model.timestamp)
        commentContentId = model.contentId
        postWriterUid = model.uid

        getComment(FirebaseUrls.clubBoard, model.contentId)
    }

    private fun getComment(collectionPath: String, contentDocument: String) {
        val indecatior = object : GenericTypeIndicator<HashMap<String, ModelComment>>() {}
        requireNotNull(ClubDetailFragment.CLUB_UID) { ErrorCode.FAIL_NO_EXIST_MY_CLUB_CONTENT_ID.message }

        TLog.d("ClubDetailFragment.CLUB_UID ${ClubDetailFragment.CLUB_UID}")
        RxRealTimeDB.toObservable(
            "$collectionPath/${ClubDetailFragment.CLUB_UID}/$contentDocument/comments",
            indecatior
        ) { HashMap() }
            .map { response ->
                commentAdapter.clear()
                response
            }
            .map { it.values.toList() }
            .subscribe({ response ->
                response.forEach {
                    if (LoginManager.user?.uid == it.writerUid)
                        it.isWriter = true

                    commentAdapter.insert(ClubBoardCommentItemVm(it, contract))
                }

                commentAdapter.items.sortBy { it.timeStamp }

                commentAdapter.notifyDataSetChanged()
            }, { e ->
                CrashlyticsHelper.logException(e)
            })
            .addTo(compositeDisposable)
    }

    fun convertDateString() = DateEx.convertDateString(timestamp.get())

    fun submitBtnClickEvent(view: View) {
        if (commentContent.get().isNullOrEmpty()) {
            contract.showToast("댓글을 입력하세요")
            return
        }
        LoginManager.noUserSafeAction(view.context, compositeDisposable) {

            TDialog.Builder(view.context, R.string.notice.getString())
                .setMessage("댓글을 작성하시겠습니까?")
                .isColored(true)
                .addButton(DialogButton.CANCEL())
                .addButton(DialogButton.OK())
                .toSingle()
                .filter { it.ok }
                .map {
                    commentVisibility.set(true)
                    addComment(FirebaseUrls.clubBoard)
                }
                .subscribe()
                .addTo(compositeDisposable)
        }
    }

    private fun addComment(collectionPath: String) {
        requireNotNull(commentContentId) { ErrorCode.FAIL_NO_POST_CONTENT_ID.message }

        val document = RxRealTimeDB.generateRandomKey("comment")
        val data = ModelComment(
            writerEmail = LoginManager.user?.email,//댓글을 입력한 유저아이디를 입력
            content = commentContent.get().toString(),
            writerUid = LoginManager.user?.uid,//댓글을 입력한 유저의 UID 입력
            timestamp = System.currentTimeMillis(),
            contentId = document,
            writerNickName = LoginManager.user?.nickName,
            writerProfileUrl = LoginManager.user?.profileImageUrl,
            postConetntId = commentContentId,
            clubUid = LoginManager.user?.clubUid!!
        )
        val path = "$collectionPath/${LoginManager.user?.clubUid}/$commentContentId/comments/$document"
        RxRealTimeDB.push(path, data)
            .flatMap { matchingClubCommentCountSetting(1) }
            .filter { LoginManager.user?.uid != postWriterUid }
            .flatMap { Maybe.just(DontCare) }
            .flatMapSingle { RxRealTimeDB.toSingle("${FirebaseUrls.alram}/$postWriterUid") { mutableListOf<ModelAlaram>() } }
            .flatMap { responseList ->
                val alarmData = ModelAlaram(
                    senderEmail = LoginManager.user?.email,
                    senderImageUrl = LoginManager.user?.profileImageUrl,
                    timeStamp = LocalTime().time,
                    senderUid = LoginManager.user?.uid!!,
                    senderNickName = LoginManager.user?.nickName,
                    reciverUid = postWriterUid,
                    alaramTypeCode = AlarmType.COMMENT.code,
                    message = "${LoginManager.user?.nickName} ${AlarmType.COMMENT.text}",
                    postPullPath = "$collectionPath/${LoginManager.user?.clubUid}/$commentContentId"
                )

                responseList.add(alarmData)

                RxRealTimeDB.push("${FirebaseUrls.alram}/$postWriterUid", responseList)
            }
            .flatMap {
                FcmHelper.sendMessage(
                    destinationUid = postWriterUid,
                    message = "${LoginManager.user?.nickName} ${AlarmType.COMMENT.text}"
                )
            }
            .subscribe({
                contract.showToast("댓글이 등록되었습니다.")
                commentContent.set("")
            }, { e ->
                CrashlyticsHelper.logException(e)
            }).let { addDisposable(it) }
    }

    private fun matchingClubCommentCountSetting(num: Int): Single<Int> {
        require(ClubDetailFragment.CLUB_UID.isNotEmpty()) { "CLUB_UID need to not null" }

        val path = "${FirebaseUrls.clubBoard}/${ClubDetailFragment.CLUB_UID}/$commentContentId/commentcount"
        return RxRealTimeDB.counter(path, num)
            .flatMap { commentCount ->
                commentcount.set(commentCount.toString())
                Single.just(commentCount)
            }
    }

    fun commentBtnClkEvent() {
        commentVisibility.set(!commentVisibility.get())
    }

    fun backPressed(view: View) {
        val activity = view.context as DetailBoardActivity
        activity.onBackPressed()
    }
}