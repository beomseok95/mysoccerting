package ks.bs.mysoccerting.scene.profile


import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import kr.nextm.lib.TToast
import ks.bs.mysoccerting.BR
import ks.bs.mysoccerting.R
import ks.bs.mysoccerting.customview.dialog.TDialog
import ks.bs.mysoccerting.databinding.FragmentProfileBinding
import ks.bs.mysoccerting.rx.showProgressDialog
import ks.bs.mysoccerting.scene.base.FragmentBase
import ks.bs.mysoccerting.scene.base.FragmentBaseViewModel

class ProfileFragment : FragmentBase() {
    var viewModel = createVm()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val binding =
            DataBindingUtil.inflate<FragmentProfileBinding>(
                inflater,
                R.layout.fragment_profile,
                container,
                false
            )

        binding.setVariable(BR.viewModel, viewModel)

        viewModel.getUserInfo()

        return binding.root
    }

    private fun createVm() = ProfileViewModel(object :
        ProfileViewModel.ProfileFragmentContract {

        override fun showToast(message: String) {
            TToast.show(message)
        }

        override fun finish() {
            activity?.finish()
        }

        override fun createDialogForProgress(request: FragmentBaseViewModel.ProgressBarOwner.Request): Dialog {
            return showProgressDialog(activity, request.disposable, request.functionOnCancel)
        }

    })


    override fun onDetach() {
        viewModel.onDetach()
        super.onDetach()
    }
}
