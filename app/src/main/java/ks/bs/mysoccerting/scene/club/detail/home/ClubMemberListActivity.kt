package ks.bs.mysoccerting.scene.club.detail.home

import android.os.Bundle
import androidx.databinding.DataBindingUtil
import kr.nextm.lib.TToast
import ks.bs.mysoccerting.R
import ks.bs.mysoccerting.databinding.ActivityClubMemberListBinding
import ks.bs.mysoccerting.model.ModelClub
import ks.bs.mysoccerting.rx.rxresult.RxActivityBase
import ks.bs.mysoccerting.rx.rxresult.getObject

class ClubMemberListActivity : RxActivityBase() {

    private val viewModel = createVm()

    private fun createVm() = ClubMemberListViewModel(object :
        ClubMemberListViewModel.ClubMemberListContract {
        override fun backPressEvent() {
            finishWithCancel()
        }
    })

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        DataBindingUtil.setContentView<ActivityClubMemberListBinding>(this, R.layout.activity_club_member_list)
            .let {
                it.viewModel = viewModel
            }

        viewModel.getContents(
            modelClub = intent.getObject(),
            type = intent.getStringExtra("TYPE")
        )
    }

    override fun onBackPressed() {
        finishWithResponse("")
    }
}