package ks.bs.mysoccerting.scene.club.detail.home

import android.view.View
import io.reactivex.rxkotlin.addTo
import ks.bs.mysoccerting.BR
import ks.bs.mysoccerting.R
import ks.bs.mysoccerting.databinding.BaseRecyclerViewAdapter
import ks.bs.mysoccerting.databinding.ItemMatchUpListBinding
import ks.bs.mysoccerting.firebase.CrashlyticsHelper
import ks.bs.mysoccerting.firebase.FirebaseUrls
import ks.bs.mysoccerting.firebase.RxRealTimeDB
import ks.bs.mysoccerting.model.ModelClub
import ks.bs.mysoccerting.rx.rxresult.startActivity
import ks.bs.mysoccerting.rx.rxresult.toIntent
import ks.bs.mysoccerting.scene.base.ActivityBaseViewModel
import ks.bs.mysoccerting.scene.club.detail.chat.ChatActivity
import ks.bs.mysoccerting.util.TLog

class MatchUpVm : ActivityBaseViewModel() {

    val adapter = BaseRecyclerViewAdapter<MatchUpItemVm, ItemMatchUpListBinding>(R.layout.item_match_up_list, BR.vm)

    fun backPressed(view: View) {
        val activity = view.context as MatchUpActivity
        activity.onBackPressed()
    }

    fun init(clubUid: String) {
        RxRealTimeDB.toObservable("${FirebaseUrls.club}/$clubUid/matchUpList") { mutableMapOf<String, ModelClub.MatchInfo>() }
            .subscribe({ it ->
                adapter.clear()

                it.values.forEach { matchInfo ->
                    adapter.insert(MatchUpItemVm(matchInfo))
                }

                adapter.items.sortBy { it.hopeDateTimeStamp }

            }, { e ->
                CrashlyticsHelper.logException(e)
            })
            .addTo(compositeDisposable)
    }
}