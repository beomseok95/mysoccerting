package ks.bs.mysoccerting.scene.lifecycle

interface ActivityLifeCycle {
    fun onDestroy()
}