package ks.bs.mysoccerting.scene.club.detail.home

import androidx.databinding.ObservableField
import ks.bs.mysoccerting.BR
import ks.bs.mysoccerting.R
import ks.bs.mysoccerting.databinding.BaseRecyclerViewAdapter
import ks.bs.mysoccerting.databinding.ItemClubMemberListBinding
import ks.bs.mysoccerting.model.ModelClub
import ks.bs.mysoccerting.scene.base.ActivityBaseViewModel

class ClubMemberListViewModel(private val contract: ClubMemberListContract) : ActivityBaseViewModel() {
    interface ClubMemberListContract {
        fun backPressEvent()
    }

    var modelClub = ModelClub()
    var item = mutableListOf<ModelClub.ModelMember>()
    val adapter = BaseRecyclerViewAdapter<ClubPeopleItemVm, ItemClubMemberListBinding>(
        R.layout.item_club_member_list, BR.viewModel
    )
    var title = ObservableField<String>()

    fun backPressBtnClkEvent() {
        contract.backPressEvent()
    }

    fun getContents(modelClub: ModelClub, type: String) {
        when (type) {
            ClubDetailHomeViewModel.MEMBER -> {
                for (i in modelClub.clubMembers.indices) {
                    adapter.insert(ClubPeopleItemVm(modelClub.clubMembers[i], modelClub.contentId, i))
                }
                title.set("클럽 맴버")
            }
            ClubDetailHomeViewModel.MERCENARY -> {
                if (modelClub.mercenaryMember != null) {
                    for (i in modelClub.mercenaryMember.indices) {
                        adapter.insert(ClubPeopleItemVm(modelClub.mercenaryMember[i], modelClub.contentId, i))
                    }
                }
                title.set("클럽 용병")

            }

        }
    }
}