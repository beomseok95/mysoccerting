package ks.bs.mysoccerting.scene.profile

import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import ks.bs.mysoccerting.BR
import ks.bs.mysoccerting.R
import ks.bs.mysoccerting.databinding.AccountFragmentBinding
import ks.bs.mysoccerting.rx.showProgressDialog
import ks.bs.mysoccerting.scene.base.FragmentBaseViewModel

class AccountFragment : Fragment() {

    val viewModel = createVm()

    private fun createVm() = AccountViewModel(object:AccountViewModel.Contract{
        override fun createDialogForProgress(request: FragmentBaseViewModel.ProgressBarOwner.Request): Dialog {
            return showProgressDialog(activity, request.disposable, request.functionOnCancel)
        }
    })

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding = DataBindingUtil.inflate<AccountFragmentBinding>(
            inflater, R.layout.account_fragment, container, false
        )
        binding.setVariable(BR.viewModel, viewModel)
        return binding.root

    }

    override fun onDetach() {
        viewModel.onDetach()
        super.onDetach()
    }

    companion object {
        fun newInstance() = AccountFragment()
    }
}

