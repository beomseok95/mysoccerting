package ks.bs.mysoccerting.scene.club.detail.setting

import android.view.View
import androidx.databinding.ObservableBoolean
import ks.bs.mysoccerting.firebase.CrashlyticsHelper
import ks.bs.mysoccerting.firebase.FirebaseUrls
import ks.bs.mysoccerting.firebase.RxRealTimeDB
import ks.bs.mysoccerting.model.ModelAlaram
import ks.bs.mysoccerting.model.ModelClub
import ks.bs.mysoccerting.rx.rxresult.putObject
import ks.bs.mysoccerting.rx.rxresult.startActivity
import ks.bs.mysoccerting.rx.rxresult.toIntent
import ks.bs.mysoccerting.scene.club.supervisor.MercenarySignUpHelper
import ks.bs.mysoccerting.scene.login.LoginManager
import ks.bs.mysoccerting.scene.profile.ProfileHomeActivity
import ks.bs.mysoccerting.util.convertStandardAge

class MercenaryManagementItemVm(val model: ModelClub.ModelMember) {

    val profileImageUrl get() = model.memberImageUrl
    val nickName get() = "닉네임 : ${model.nickName}"

    val ageVisibility = ObservableBoolean(model.age != null)
    val ageString = if (model.age?.isNotEmpty() == true && model.age != "null") model.age else "0"
    val age get() = "나이대 : ${ageString?.toInt()?.convertStandardAge()}"
    val positionList
        get() =
            if (model.position != null)
                listOf(model.position)
            else
                emptyList()

    val positionVisibility = ObservableBoolean(model.position?.isNotEmpty() ?: false)

    fun showProfile(view: View) {
        ProfileHomeActivity::class.java
            .toIntent()
            .putObject(model.uid)
            .startActivity(view.context)
            .subscribe()
    }

    fun acceptBtnClkEvent(view: View) {
        val path = "${FirebaseUrls.alram}/${LoginManager.user?.uid}"
        RxRealTimeDB.toSingle(path) { mutableListOf<ModelAlaram>() }
            .toObservable()
            .flatMapIterable { it }
            .filter { it.senderUid == model.uid }
            .take(1)
            .subscribe({ model ->
                MercenarySignUpHelper.accept(view, model)

            }, { e ->
                CrashlyticsHelper.logException(e)
            })
            .let { }
    }

    fun rejectBtnClkEvent(view: View) {
        val path = "${FirebaseUrls.alram}/${LoginManager.user?.uid}"
        RxRealTimeDB.toSingle(path) { mutableListOf<ModelAlaram>() }
            .toObservable()
            .flatMapIterable { it }
            .filter { it.senderUid == model.uid }
            .take(1)
            .subscribe({ modelAlaram ->
                MercenarySignUpHelper.reject(view, modelAlaram)
            }, { e ->
                CrashlyticsHelper.logException(e)
            })
            .let { }
    }

    fun showBottomSheet(view: View) {
        val activity = view.context as MercenaryManagementActivity
        MercenaryBottomSheetDialog(model)
            .show(activity.supportFragmentManager, "")
    }

}