package ks.bs.mysoccerting.scene.club.detail.chat

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import kotlinx.android.synthetic.main.fragment_club_chat.*
import ks.bs.mysoccerting.BR
import ks.bs.mysoccerting.R
import ks.bs.mysoccerting.databinding.FragmentClubChatBinding
import ks.bs.mysoccerting.rx.rxresult.getObject
import ks.bs.mysoccerting.rx.rxresult.putObject
import ks.bs.mysoccerting.rx.showProgressDialog
import ks.bs.mysoccerting.scene.base.FragmentBase
import ks.bs.mysoccerting.scene.base.FragmentBaseViewModel


class ClubChatFragment : FragmentBase() {

    var viewModel = createVm()

    var contentId = ""

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding =
            DataBindingUtil.inflate<FragmentClubChatBinding>(inflater, R.layout.fragment_club_chat, container, false)
        binding.setVariable(BR.viewModel, viewModel)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        viewModel.init(contentId)
    }

    private fun createVm() = ClubChatViewModel(object :
        ClubChatViewModel.Contract {

        override fun createDialogForProgress(request: FragmentBaseViewModel.ProgressBarOwner.Request): Dialog {
            return showProgressDialog(activity, request.disposable, request.functionOnCancel)
        }

        override fun scrollBottom() {
            recyclerView.scrollToPosition(recyclerView.adapter!!.itemCount - 1)
        }
    })

    override fun onAttach(context: Context) {
        super.onAttach(context)

        arguments?.let {
            contentId = it.getObject()
        }
    }

    override fun onDetach() {
        viewModel.onDetach()
        super.onDetach()
    }

    companion object {
        fun newInstace(contentId: String) =
            ClubChatFragment()
                .apply {
                    arguments = Bundle().apply {
                        putObject(contentId)
                    }
                }
    }
}
