package ks.bs.mysoccerting.scene.profile

import android.view.View
import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import androidx.navigation.NavOptions
import androidx.navigation.findNavController
import io.reactivex.rxkotlin.addTo
import kotlinx.android.synthetic.main.activity_main.*
import kr.nextm.lib.TToast
import ks.bs.mysoccerting.NavGraphDirections
import ks.bs.mysoccerting.R
import ks.bs.mysoccerting.customview.dialog.DialogButton
import ks.bs.mysoccerting.customview.dialog.TDialog
import ks.bs.mysoccerting.firebase.CrashlyticsHelper
import ks.bs.mysoccerting.firebase.FirebaseHelper
import ks.bs.mysoccerting.firebase.FirebaseUrls
import ks.bs.mysoccerting.firebase.RxRealTimeDB
import ks.bs.mysoccerting.model.ModelClub
import ks.bs.mysoccerting.model.ModelMoveTo
import ks.bs.mysoccerting.model.type.MovePageLocationType
import ks.bs.mysoccerting.prefs.Prefs
import ks.bs.mysoccerting.rx.rxresult.putObject
import ks.bs.mysoccerting.rx.rxresult.startActivity
import ks.bs.mysoccerting.rx.rxresult.startActivityForResult
import ks.bs.mysoccerting.rx.rxresult.toIntent
import ks.bs.mysoccerting.scene.base.FragmentBaseViewModel
import ks.bs.mysoccerting.scene.club.detail.setting.ClubSettingInfoActivity
import ks.bs.mysoccerting.scene.home.MainActivity
import ks.bs.mysoccerting.scene.login.LoginManager
import ks.bs.mysoccerting.util.TLog
import ks.bs.mysoccerting.util.getString

class ProfileViewModel(val contract: ProfileFragmentContract) : FragmentBaseViewModel(contract) {
    interface ProfileFragmentContract : BaseVmListener {
        fun showToast(message: String)
        fun finish()
    }

    val profileImageUrl = ObservableField<String>()
    val nickName = ObservableField("Guest")
    val logoutBtnVisibility = ObservableBoolean()

    fun profilSettingBtnClkEvent(view: View) {
        LoginManager.noUserSafeAction(view.context, compositeDisposable) {

            ProfileSettingActivity::class.java.toIntent()
                .startActivityForResult<Pair<String?, String?>>(view.context)
                .subscribe({ responsePair ->
                    if (responsePair.first != null)
                        profileImageUrl.set(responsePair.first)
                }, { e ->
                    CrashlyticsHelper.logException(e)
                })
                .addTo(compositeDisposable)
        }
    }

    val email = ObservableField("Guest")
    val joinedMercenaryCount = ObservableField<String>()
    val joinedMercenaryBedgeVisibile = ObservableBoolean(false)

    fun getUserInfo() {
        if (LoginManager.auth.currentUser != null) {
            LoginManager.userSubject
                .subscribe({ response ->
                    nickName.set(response.nickName)
                    profileImageUrl.set(response.profileImageUrl)
                    logoutBtnVisibility.set(true)
                    val mCount = response.mercenaryClubInfo?.count() ?: 0
                    joinedMercenaryCount.set(mCount.toString())
                    joinedMercenaryBedgeVisibile.set(mCount > 0)

                }, { e ->
                    CrashlyticsHelper.logException(e)
                }).addTo(compositeDisposable)
        } else {
            logoutBtnVisibility.set(false)
        }
    }

    fun logout(view: View) {
        TDialog.Builder(view.context, R.string.notice.getString())
            .setMessage("정말 로그아웃 하시겠습니까?\n앱이 종료됩니다.")
            .isColored(true)
            .addButton(DialogButton.CANCEL())
            .addButton(DialogButton.OK())
            .toSingle()
            .filter { it.ok }
            .subscribe({
                TToast.show("로그아웃 성공")
                auth.signOut()
                LoginManager.logout()
                Prefs.save {
                    Prefs.settings.isLogined = false
                    Prefs.settings.loginUid = ""
                }
                contract.finish()
            }, { e -> CrashlyticsHelper.logException(e) })
            .addTo(compositeDisposable)
    }

    fun moveToTermsOfService(view: View) {
        view.findNavController()
            .navigate(R.id.action_profileFragment_to_termsOfServiceFragment)
    }

    fun moveToHelpPage(view: View) {
        view.findNavController()
            .navigate(R.id.action_profileFragment_to_helpFragment)
    }

    fun moveToAccount(view: View) {
        view.findNavController()
            .navigate(R.id.action_profileFragment_to_accountFragment)
    }

    fun moveToProfileHome(view: View) {
        LoginManager.noUserSafeAction(view.context, compositeDisposable) {
            ProfileHomeActivity::class.java
                .toIntent()
                .putObject(auth.currentUser!!.uid)
                .startActivity(view.context)
                .subscribe({
                    TLog.i("move to profile Home Activity")
                }, { e ->
                    CrashlyticsHelper.logException(e)
                })
                .addTo(compositeDisposable)
        }
    }

    fun moveMercenaryManagementPage(view: View) {
        LoginManager.noUserSafeAction(view.context, compositeDisposable) {
            val activity = view.context as MainActivity

            UserMercenaryManagementActivity::class.java
                .toIntent()
                .startActivityForResult<ModelMoveTo>(activity)
                .filter { it.locationcode == MovePageLocationType.CLUB_DETAIL.locationCode }
                .map {
                    activity.bottomNavigation.selectedItemId = R.id.clubFragment
                    backPressed(view)
                    it
                }
                .map { response ->
                    activity.findNavController(R.id.nav_host_fragment)
                        .navigate(
                            NavGraphDirections.actionGlobalClubDetail(response.clubContentId!!),
                            NavOptions.Builder().setPopUpTo(R.id.profileFragment, false)
                                .setLaunchSingleTop(true)
                                .build()
                        )
                }
                .subscribe({ response ->

                }, { e ->
                    CrashlyticsHelper.logException(e)
                })
                .addTo(compositeDisposable)
        }
    }

    fun moveMyClubPage(view: View) {
        LoginManager.noClubSafeAction(view.context, compositeDisposable) {
            RxRealTimeDB.toSingle("${FirebaseUrls.club}/${LoginManager.user?.clubUid!!}") { ModelClub() }
                .flatMapCompletable { modelClub ->
                    ClubSettingInfoActivity::class.java
                        .toIntent()
                        .putObject(modelClub)
                        .startActivity(view.context)
                }
                .subscribe()
        }
    }

    fun moveAlaramSettingPage(view: View) {
        AlramSettingActivity::class.java
            .toIntent()
            .startActivity(view.context)
            .subscribe()
    }

    fun backPressed(view: View) {
        val activity = view.context as MainActivity
        activity.onBackPressed()
    }
}