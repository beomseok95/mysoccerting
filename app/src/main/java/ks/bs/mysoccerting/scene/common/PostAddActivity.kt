package ks.bs.mysoccerting.scene.common

import android.os.Bundle
import androidx.databinding.DataBindingUtil
import io.reactivex.Single
import kotlinx.android.synthetic.main.activity_post_add.*
import kr.nextm.lib.TToast
import ks.bs.mysoccerting.R
import ks.bs.mysoccerting.databinding.ActivityPostAddBinding
import ks.bs.mysoccerting.model.Area
import ks.bs.mysoccerting.rx.rxresult.RxActivityBase
import ks.bs.mysoccerting.scene.filter.AreaDialog
import ks.bs.mysoccerting.scene.filter.CalendarDialog
import ks.bs.mysoccerting.scene.filter.PeopleNumberDialog

class PostAddActivity : RxActivityBase() {
    val viewModel = createVm()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        DataBindingUtil.setContentView<ActivityPostAddBinding>(
            this,
            R.layout.activity_post_add
        ).viewModel = viewModel

        initToolBar()

        viewModel.init(getInput())
    }

    private fun initToolBar() {
        viewModel.title.set("게시글 등록")
    }

    private fun createVm() = PostAddViewModel(object :
        PostAddViewModel.PostAddActivityContract {
        override fun showToast(message: String) {
            TToast.show(message)
        }

        override fun showPeopleNumberDialog(): Single<MutableList<String>> {
            return PeopleNumberDialog()
                .toSingle(this@PostAddActivity)
        }

        override fun showAreaDialog(): Single<Area> {
            return AreaDialog()
                .toSingle(this@PostAddActivity)
        }

        override fun showCalendarDialog(): Single<CalendarDialog.Date> {
            return CalendarDialog()
                .toSingle(this@PostAddActivity)
        }

        override fun backPressEvent() {
            finishWithCancel()
        }

        override fun resetFlexBoxView() {
            plexobx.removeAllViews()
        }

        override fun getContext() = baseContext
    })

    override fun onDestroy() {
        viewModel.onDestroy()
        super.onDestroy()
    }
}
