package ks.bs.mysoccerting.scene.profile

import android.view.View
import androidx.databinding.ObservableField

class TermOfServiceActivityVm {
    val url = ObservableField("file:///android_asset/terms_of_service.html")

    fun backPressed(view: View) {
        val activity = view.context as TermsOfServiceActivity
        activity.onBackPressed()
    }
}