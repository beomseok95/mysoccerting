package ks.bs.mysoccerting.scene.club.detail.board

import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.EditText
import android.widget.TextView
import androidx.databinding.ObservableField
import com.google.firebase.database.GenericTypeIndicator
import io.reactivex.rxkotlin.addTo
import ks.bs.mysoccerting.BR
import ks.bs.mysoccerting.R
import ks.bs.mysoccerting.databinding.FilteredRecyclerViewAdapter
import ks.bs.mysoccerting.databinding.ItemClubBoardBinding
import ks.bs.mysoccerting.firebase.CrashlyticsHelper
import ks.bs.mysoccerting.firebase.FirebaseUrls
import ks.bs.mysoccerting.firebase.RxRealTimeDB
import ks.bs.mysoccerting.model.ModelClubBoard
import ks.bs.mysoccerting.rx.rxresult.startActivity
import ks.bs.mysoccerting.rx.rxresult.toIntent
import ks.bs.mysoccerting.scene.base.FragmentBaseViewModel
import ks.bs.mysoccerting.scene.login.LoginManager
import ks.bs.mysoccerting.util.EditKeyborad
import ks.bs.mysoccerting.util.TLog

class ClubBoardViewModel(val contract: Contract) : FragmentBaseViewModel(contract) {
    interface Contract : BaseVmListener

    val adapter = FilteredRecyclerViewAdapter<ClubBoardItemViewModel, ItemClubBoardBinding>(
        R.layout.item_club_board, BR.viewModel
    )

    fun getContents(contentId: String) {
        val indecatior = object : GenericTypeIndicator<HashMap<String, ModelClubBoard>>() {}
        val path = "${FirebaseUrls.clubBoard}/$contentId"
        RxRealTimeDB.toObservable(path, indecatior) { HashMap() }
            .subscribe({ response ->
                val itemList = mutableListOf<ClubBoardItemViewModel>()
                response.values.forEach { model ->
                    itemList.add(
                        (ClubBoardItemViewModel(
                            model,
                            contract
                        ))
                    )
                }
                adapter.clear()
                adapter.insertAll(itemList)
                adapter.items.sortByDescending { it.model.timestamp }

            }, { CrashlyticsHelper.logException(it) })
            .addTo(compositeDisposable)
    }

    fun movePostWritePage(view: View) {
        LoginManager.clubMemberSafeAction(view.context, compositeDisposable) {
            ClubBoardAddActivity::class.java.toIntent()
                .startActivity(view.context)
                .subscribe()
        }
    }
}