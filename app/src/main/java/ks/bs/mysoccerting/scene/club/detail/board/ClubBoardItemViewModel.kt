package ks.bs.mysoccerting.scene.club.detail.board

import android.view.View
import io.reactivex.disposables.CompositeDisposable
import ks.bs.mysoccerting.model.ModelClubBoard
import ks.bs.mysoccerting.rx.rxresult.putObject
import ks.bs.mysoccerting.rx.rxresult.startActivity
import ks.bs.mysoccerting.rx.rxresult.toIntent
import ks.bs.mysoccerting.util.DateEx

class ClubBoardItemViewModel(val model: ModelClubBoard, val contract: ClubBoardViewModel.Contract) {

    val compositeDisposable = CompositeDisposable()

    fun convertBoardDateString() = DateEx.convertBoardDateString(model.timestamp)

    fun moveClubBoardDetailPage(v: View) {
        DetailBoardActivity::class.java.toIntent()
            .putObject(model)
            .startActivity(v.context!!)
            .subscribe()
    }
}