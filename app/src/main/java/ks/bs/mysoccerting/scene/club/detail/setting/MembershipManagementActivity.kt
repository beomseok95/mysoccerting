package ks.bs.mysoccerting.scene.club.detail.setting

import android.os.Bundle
import androidx.databinding.DataBindingUtil
import kotlinx.android.synthetic.main.activity_membership_management.toolbar_title
import ks.bs.mysoccerting.BR
import ks.bs.mysoccerting.R
import ks.bs.mysoccerting.databinding.ActivityMembershipManagementBinding
import ks.bs.mysoccerting.scene.base.ActivityBase

class MembershipManagementActivity : ActivityBase() {

    val viewModel = createVm()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        DataBindingUtil.setContentView<ActivityMembershipManagementBinding>(
            this,
            R.layout.activity_membership_management
        )
            .setVariable(BR.viewModel, viewModel)

        toolbar_title.text = "클럽가입신청관리"

        viewModel.loadAffiliateList()
    }

    fun createVm() = MembershipManagementVm()

    override fun onDestroy() {
        viewModel.clearDisposables()
        super.onDestroy()
    }
}
