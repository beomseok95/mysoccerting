package ks.bs.mysoccerting.scene.match

import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.EditText
import android.widget.TextView
import androidx.databinding.ObservableField
import androidx.navigation.findNavController
import io.reactivex.Single
import io.reactivex.rxkotlin.addTo
import ks.bs.mysoccerting.BR
import ks.bs.mysoccerting.R
import ks.bs.mysoccerting.databinding.FilteredRecyclerViewAdapter
import ks.bs.mysoccerting.databinding.ItemClubBinding
import ks.bs.mysoccerting.firebase.CrashlyticsHelper
import ks.bs.mysoccerting.firebase.FirebaseHelper
import ks.bs.mysoccerting.firebase.FirebaseUrls
import ks.bs.mysoccerting.firebase.RxRealTimeDB
import ks.bs.mysoccerting.model.Area
import ks.bs.mysoccerting.model.ModelClub
import ks.bs.mysoccerting.model.ModelClubMatch
import ks.bs.mysoccerting.rx.withProgressBallAnimation
import ks.bs.mysoccerting.scene.base.FragmentBaseViewModel
import ks.bs.mysoccerting.scene.filter.CalendarDialog
import ks.bs.mysoccerting.scene.login.LoginManager
import ks.bs.mysoccerting.support.DontCare
import ks.bs.mysoccerting.util.EditKeyborad
import ks.bs.mysoccerting.util.TLog
import java.util.*


class MatchingViewModel(private val contract: Contract) : FragmentBaseViewModel(contract), EditKeyborad {

    interface Contract : BaseVmListener {
        fun showToast(message: String)
        fun showPeopleNumberDialog(): Single<MutableList<String>>
        fun showAreaDialog(): Single<Area>
        fun showCalendarDialog(): Single<CalendarDialog.Date>
        fun moveItemDetailPage(item: ModelClubMatch): Single<Boolean>
        fun movePostWirteActivity(): Single<Any>
        fun changeClubTabSelect()
        fun resetFlexBoxView()

    }

    var isFiltered = ObservableField(false)

    var cloudInfoViewTextList = ObservableField<MutableList<String>>(mutableListOf())
    var peopleNumberFilterList = mutableListOf<String>()
    var dateFilterList = mutableListOf<String>()
    var cityFilterList = mutableListOf<String>()

    override var searchText = ObservableField<String>("")

    override val editTextActionListener =
        TextView.OnEditorActionListener { v, actionId, _ -> initEditAction(actionId, v) }


    private fun initEditAction(actionId: Int, v: TextView?): Boolean {
        when (actionId) {
            EditorInfo.IME_ACTION_SEARCH -> {
                TLog.d("actionSearch KeyBorad Hide")
                isFiltered.set(true)
                changeFilter()
                hideKeyBorad(v)
            }

            else -> {
                TLog.d("Nomal KeyBorad Hide")
                changeFilter()
                hideKeyBorad(v)
                return false
            }
        }
        return true
    }

    var adapter = FilteredRecyclerViewAdapter<MatchClubVm, ItemClubBinding>(
        R.layout.item_club,
        BR.clubMatch
    )


    fun getContents() {
        FirebaseHelper.matchClubSubject
            .map { it.values.toList() }
            .subscribe({ response ->
                adapter.clear()
                response.forEach {
                    adapter.insert(MatchClubVm(it, contract))
                }
                adapter.items.sortByDescending { it.timestamp }
            }, { TLog.e(it) }).addTo(compositeDisposable)
    }


    fun showAreaDialog() {
        contract.showAreaDialog()
            .subscribe({
                val city = it.selectedCity?.trim()
                val town = it.selectedTown?.trim()

                if (town == null && city == null) return@subscribe

                if (town == null) {
                    cityFilterList = mutableListOf("$city")
                    contract.resetFlexBoxView()
                    cloudInfoViewTextList.set((peopleNumberFilterList + dateFilterList + cityFilterList).toMutableList())
                    changeFilter()
                    isFiltered.set(true)
                    return@subscribe
                }

                cityFilterList.map { index ->
                    if (index.startsWith("$city")) {
                        cityFilterList.remove("$city")
                        contract.resetFlexBoxView()
                        cityFilterList.add("$city $town")
                        cloudInfoViewTextList.set((peopleNumberFilterList + dateFilterList + cityFilterList).toMutableList())
                        changeFilter()
                        isFiltered.set(true)
                        return@subscribe
                    }
                }

                cityFilterList = mutableListOf("$city $town")
                contract.resetFlexBoxView()
                cloudInfoViewTextList.set((peopleNumberFilterList + dateFilterList + cityFilterList).toMutableList())
                changeFilter()
                isFiltered.set(true)

            }, { e ->
                CrashlyticsHelper.logException(e)
            }).let { addDisposable(it) }
    }


    fun showCalendarDialog() {
        contract.showCalendarDialog()
            .subscribe({ date ->

                val firstDate = date.firstDate
                val lastDate = date.lastDate

                if (firstDate == null)
                    return@subscribe

                dateFilterList =
                    if (lastDate == null) {
                        mutableListOf("$firstDate")
                    } else {
                        mutableListOf("$firstDate-$lastDate")
                    }

                contract.resetFlexBoxView()
                cloudInfoViewTextList.set((peopleNumberFilterList + cityFilterList + dateFilterList).toMutableList())
                changeFilter()
                isFiltered.set(true)

            }, { e ->
                CrashlyticsHelper.logException(e)
            }).let { addDisposable(it) }
    }

    private fun sortPeopleNumberList(numberList: MutableList<String>) {
        numberList.sortWith(Comparator { pos1, pos2 ->
            pos1.substring(0, 2).trim().toInt()
                .compareTo(pos2.substring(0, 2).trim().toInt())
        })
    }

    fun showPeopleNumberDialog() {
        contract.showPeopleNumberDialog().subscribe({ numberList ->
            if (numberList == null) return@subscribe

            sortPeopleNumberList(numberList)

            if (numberList.isEmpty()) return@subscribe


            peopleNumberFilterList = mutableListOf()

            if (numberList[0] == "모든인원") {
                peopleNumberFilterList =
                    mutableListOf(
                        "5 vs 5",
                        "6 vs 6",
                        "7 vs 7",
                        "8 vs 8",
                        "9 vs 9 ",
                        "10 vs 10",
                        "11 vs 11"
                    )
            } else {
                numberList.forEach { number ->
                    if (!peopleNumberFilterList.contains(number)) {
                        peopleNumberFilterList.add(number)
                    }
                }

            }

            contract.resetFlexBoxView()
            cloudInfoViewTextList.set((cityFilterList + dateFilterList + peopleNumberFilterList).toMutableList())
            changeFilter()
            isFiltered.set(true)
        }, { e ->
            CrashlyticsHelper.logException(e)
        }).let { addDisposable(it) }
    }

    fun movePostWritePage(view: View) {
        LoginManager.needClubAssistantGrade(view.context, compositeDisposable) {
            contract.movePostWirteActivity()
                .subscribe({

                }, { e ->
                    CrashlyticsHelper.logException(e)
                })
                .addTo(compositeDisposable)
        }
    }

    private fun changeFilter() {
        cityFilterList.forEach { it.toLowerCase() }
        dateFilterList.forEach { it.toLowerCase() }
        peopleNumberFilterList.forEach { it.toLowerCase() }

        if (searchText.get()!!.isNotEmpty())
            adapter.changeFilter {
                cityFilterList.contains(it.matcingArea.toLowerCase()) ||
                        cityFilterList.contains(it.matcingArea.split(" ")[0]) ||
                        (dateFilterList.isNotEmpty() && it.hopeDate.toLowerCase().contains(dateFilterList.joinToString())) ||
                        it.matchingPeopleNumbewr?.contains(peopleNumberFilterList.joinToString())?:false||
                        it.matcingArea.contains(searchText.get().toString()) ||
                        it.clubName!!.contains(searchText.get().toString()) ||
                        it.content.contains(searchText.get().toString()) ||
                        it.hopeDate.contains(searchText.get().toString()) ||
                        it.hopeDate.contains(searchText.get().toString())
            }
        else
            adapter.changeFilter {
                cityFilterList.contains(it.matcingArea.toLowerCase()) ||
                        cityFilterList.contains(it.matcingArea.split(" ")[0]) ||
                        (dateFilterList.isNotEmpty() && it.hopeDate.toLowerCase().contains(dateFilterList.joinToString())) ||
                        it.matchingPeopleNumbewr?.contains(peopleNumberFilterList.joinToString())?:false
            }
    }


    fun filterRefresh() {
        searchText.set("")

        isFiltered.set(false)
        adapter.resetFilter()
        contract.resetFlexBoxView()
        adapter.items.sortByDescending { it.timestamp }

        peopleNumberFilterList = mutableListOf()
        dateFilterList = mutableListOf()
        cityFilterList = mutableListOf()
    }


    fun onSearch(editText: EditText) {
        changeFilter()
        if (editText.text.isNotEmpty())
            isFiltered.set(true)
        hideKeyBorad(editText)
    }
}