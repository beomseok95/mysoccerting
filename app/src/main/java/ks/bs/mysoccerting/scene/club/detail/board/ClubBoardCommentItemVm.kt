package ks.bs.mysoccerting.scene.club.detail.board

import android.view.View
import com.google.firebase.auth.FirebaseAuth
import io.reactivex.Completable
import io.reactivex.Single
import ks.bs.mysoccerting.customview.dialog.DialogButton
import ks.bs.mysoccerting.firebase.CrashlyticsHelper
import ks.bs.mysoccerting.firebase.FirebaseUrls
import ks.bs.mysoccerting.firebase.RxRealTimeDB
import ks.bs.mysoccerting.model.ModelComment
import ks.bs.mysoccerting.scene.club.detail.ClubDetailFragment
import ks.bs.mysoccerting.scene.login.LoginManager
import ks.bs.mysoccerting.support.DontCare
import ks.bs.mysoccerting.util.DateEx

class ClubBoardCommentItemVm(val model: ModelComment, val contract: DetailBoardViewModel.Contract) {
    val timeStamp get() = DateEx.convertDateString(model.timestamp)

    fun amendBtnClkEvent(view: View, commentModel: ClubBoardCommentItemVm) {
        contract.moveAmendActivity()
            .flatMap { ammendCommentContent ->
                commentAmend(
                    FirebaseUrls.clubBoard,
                    ammendCommentContent,
                    commentModel.model
                )
            }
            .subscribe({
                contract.showToast("댓글이 수정되었습니다.")
            }, { e ->
                CrashlyticsHelper.logException(e)
            })
            .let { }
    }

    private fun commentAmend(
        path: String,
        amendCommentContent: String,
        commentModel: ModelComment
    ): Single<DontCare> {
        require(ClubDetailFragment.CLUB_UID.isNotEmpty()) { "CLUB_UID need to not null"}

        val ammendPath =
            "$path/${ClubDetailFragment.CLUB_UID}/${commentModel.postConetntId}/comments/${commentModel.contentId}"
        return RxRealTimeDB.toSingle(ammendPath) { ModelComment() }
            .map { content ->
                content.writerEmail = FirebaseAuth.getInstance()?.currentUser!!.email//댓글을 입력한 유저아이디를 입력
                content.content = amendCommentContent
                content.writerUid = LoginManager.user?.uid
                content.timestamp = commentModel.timestamp

                content
            }
            .flatMap { ammendedComment ->
                RxRealTimeDB.push(ammendPath, ammendedComment)
            }
    }

    fun deleteBtnClkEvent(view: View, commentModel: ClubBoardCommentItemVm) {
        contract.showCommentSubmitDialog()
            .setMessage("댓글을\n삭제하시겠습니까 ?")
            .isColored(true)
            .addButton(DialogButton("취소"))
            .addButton(DialogButton("확인") {
                deleteOkEvent(commentModel.model)
            }).show()
    }

    private fun deleteOkEvent(comment: ModelComment) {
        val topCollectionPath = FirebaseUrls.clubBoard

        deleteComment(topCollectionPath = topCollectionPath, commentDocumentPath = comment.contentId!!)
            .andThen(matchingClubCommentCountSetting(-1))
            .map { contract.reloadCommentCount(it) }
            .subscribe({
                contract.showToast("댓글이 삭제되었습니다.")
            }, { e -> CrashlyticsHelper.logException(e) })
            .let { }
    }

    private fun deleteComment(topCollectionPath: String, commentDocumentPath: String): Completable {
        require(ClubDetailFragment.CLUB_UID.isNotEmpty()) { "CLUB_UID need to not null"}

        return RxRealTimeDB.remove("$topCollectionPath/${ClubDetailFragment.CLUB_UID}/${model.postConetntId}/${FirebaseUrls.comment}/$commentDocumentPath")
    }

    private fun matchingClubCommentCountSetting(num: Int): Single<Int> {
        require(ClubDetailFragment.CLUB_UID.isNotEmpty()) { "CLUB_UID need to not null"}

        val path =
            "${FirebaseUrls.clubBoard}/${ClubDetailFragment.CLUB_UID}/${model.postConetntId}/commentcount"
        return RxRealTimeDB.counter(path, num)

    }
}