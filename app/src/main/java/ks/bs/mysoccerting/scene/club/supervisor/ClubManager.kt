package ks.bs.mysoccerting.scene.club.supervisor

import android.view.View
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.toObservable
import io.reactivex.rxkotlin.zipWith
import ks.bs.mysoccerting.customview.dialog.DialogButton
import ks.bs.mysoccerting.customview.dialog.DialogObservable
import ks.bs.mysoccerting.customview.dialog.TDialog
import ks.bs.mysoccerting.customview.dialog.TextContentParam
import ks.bs.mysoccerting.exception.ErrorCode
import ks.bs.mysoccerting.exception.ErrorCodeException
import ks.bs.mysoccerting.firebase.CrashlyticsHelper
import ks.bs.mysoccerting.firebase.FirebaseUrls
import ks.bs.mysoccerting.firebase.RxRealTimeDB
import ks.bs.mysoccerting.model.*
import ks.bs.mysoccerting.model.time.LocalTime
import ks.bs.mysoccerting.model.type.PostType
import ks.bs.mysoccerting.model.type.TimelineType
import ks.bs.mysoccerting.scene.login.LoginManager
import ks.bs.mysoccerting.support.DontCare
import ks.bs.mysoccerting.util.TLog

object ClubManager {

    fun createTimeLinePost(
        documentId: String,
        type: TimelineType,
        model: ModelAlaram
    ): ModelClubTimeline {
        return ModelClubTimeline(
            nickName = model.senderNickName!!,
            uid = model.senderUid,
            contentUid = documentId,
            memberImageUrl = model.senderImageUrl,
            timestamp = LocalTime().time,
            type = type
        )
    }

    fun isClubGradeCheck() =
        LoginManager.user?.clubAssistantList?.any { it == LoginManager.user?.uid } ?: false || LoginManager.user?.uid == LoginManager.user?.clubMasterUid

    fun isClubMemberCheck(response: ModelUser) =
        response.clubUid == LoginManager.user?.clubUid

    fun maxCountCheck(response: ModelUser) =
        response.mercenaryClubInfo?.count() ?: 0 >= 4

    fun duplicateMercenaryInClubCheck(user: ModelUser?) =
        user?.mercenaryClubInfo?.any { it.contentId == LoginManager.user?.clubUid } ?: false

    fun duplicateMercenaryInClubCheckWithMaster(user: ModelUser?, masterUid: String) =
        user?.mercenaryClubInfo?.any { it.masterUid == masterUid } ?: false

    fun areadyRegisteredMercenary(
        view: View,
        response: Pair<ModelUser, ModelClub>,
        userModel: ModelUser,
        clubModel: ModelClub,
        alaramModel: ModelAlaram
    ): Single<DontCare> {
        return TDialog.Builder(view.context)
            .setMessage("${response.first.nickName}님은 이미 용병으로 등록되어 있습니다.")
            .isColored(true)
            .addButton(DialogButton.OK())
            .toSingle()
            .flatMap {
                val path = "${FirebaseUrls.alram}/${alaramModel.reciverUid}"
                RxRealTimeDB.toSingle(path) { mutableListOf<ModelAlaram>() }
                    .map { response ->

                        val removedModelList = response
                        response.removeAll { it.reciverUid == alaramModel.reciverUid && it.timeStamp == alaramModel.timeStamp }
                        removedModelList
                    }
                    .flatMap { model -> RxRealTimeDB.push(path, model) }
            }
            .map {
                clubModel.mercenaryApplicationList?.removeAll { it.uid == userModel.uid }

                clubModel
            }
            .flatMap { model ->
                RxRealTimeDB.push("${FirebaseUrls.club}/${model.contentId}", model)
            }
    }

    fun areadyMax(
        view: View,
        response: Pair<ModelUser, ModelClub>,
        userModel: ModelUser,
        clubModel: ModelClub,
        alaramModel: ModelAlaram
    ): Single<DontCare> {
        return TDialog.Builder(view.context)
            .setMessage("${response.first.nickName}님의 용병등록횟수가\n이미 최대입니다.")
            .isColored(true)
            .addButton(DialogButton.OK())
            .toSingle()
            .flatMap {
                RxRealTimeDB.push("${FirebaseUrls.users}/${alaramModel.senderUid}", userModel)
                    .flatMap {
                        val path = "${FirebaseUrls.alram}/${alaramModel.reciverUid}"
                        RxRealTimeDB.toSingle(path) { mutableListOf<ModelAlaram>() }
                            .map { response ->
                                val removedModelList = response
                                removedModelList.removeAll { it.reciverUid == alaramModel.reciverUid && it.timeStamp == alaramModel.timeStamp }
                                removedModelList
                            }
                            .flatMap { model -> RxRealTimeDB.push(path, model) }
                    }
            }
            .map {
                clubModel.mercenaryApplicationList?.removeAll { it.uid == userModel.uid }

                clubModel
            }
            .flatMap { model ->
                RxRealTimeDB.push("${FirebaseUrls.club}/${model.contentId}", model)
            }
    }

    fun showDialog(
        view: View,
        title: String,
        message: String,
        cancelable: Boolean = true,
        cancelBtnText: String = "취소",
        okBtnText: String = "확인"
    ): Single<DialogObservable.Result<TextContentParam>> {
        return if (cancelable)
            TDialog.Builder(view.context, title)
                .setMessage(message)
                .isColored(true)
                .addButton(DialogButton(cancelBtnText, false))
                .addButton(DialogButton(okBtnText))
                .toSingle()
        else
            TDialog.Builder(view.context, title)
                .setMessage(message)
                .isColored(true)
                .addButton(DialogButton(okBtnText))
                .toSingle()

    }
}

fun areadyClubExist() = LoginManager.user?.clubUid?.isNotEmpty() == true

fun duplicateMecenaryCheckWithCurrentUser(club: ModelClub) =
    club.mercenaryMember?.none { it.uid == LoginManager.user?.uid } == true


fun <T> Single<T>.noExistClubCheckToUserUid(userUid: String): Single<T> {
    return this.flatMap { input ->
        RxRealTimeDB.toSingle("${FirebaseUrls.users}/$userUid") { ModelUser() }
            .flatMap { RxRealTimeDB.toSingle("${FirebaseUrls.club}/${it.clubUid}") { ModelClub() } }
            .checklub()
            .flatMap { Single.just(input) }
    }
}

fun <T> Single<T>.noExistClubCheck(clubUid: String): Single<T> {
    return this.flatMap { input ->
        RxRealTimeDB.toSingle("${FirebaseUrls.club}/$clubUid") { ModelClub() }
            .checklub()
            .flatMap { Single.just(input) }
    }
}

fun Single<DontCare>.removeClubNames(clubUid: String): Single<DontCare> {
    return this.flatMap {
        RxRealTimeDB.remove("${FirebaseUrls.clubNames}/$clubUid")
            .andThen(Single.just(DontCare))
    }
}


fun Single<DontCare>.removeBoard(userUid: String): Single<DontCare> {
    return this.flatMap {
        RxRealTimeDB.toSingle("${PostType.MY_POST.path}/$userUid/${PostType.CLUB_BOARD.type}") { mutableMapOf<String, ModelMyPost>() }
            .map { it.keys.toList() }
            .toObservable()
            .flatMapIterable { it }
            .flatMapCompletable { RxRealTimeDB.remove("${PostType.CLUB_BOARD.path}/${LoginManager.user?.clubUid}/$it") }
            .andThen(RxRealTimeDB.remove("${PostType.MY_POST.path}/$userUid/${PostType.CLUB_BOARD.type}"))
            .andThen(Single.just(DontCare))
    }
}


fun Single<DontCare>.removeClubSearch(userUid: String): Single<DontCare> {
    return this.flatMap {
        RxRealTimeDB.toSingle("${PostType.MY_POST.path}/$userUid/${PostType.CLUB_SEARCH.type}") { mutableMapOf<String, ModelMyPost>() }
            .map { it.keys.toList() }
            .toObservable()
            .flatMapIterable { it }
            .flatMapCompletable {
                RxRealTimeDB.remove("${PostType.CLUB_SEARCH.path}/$it")
                    .andThen(RxRealTimeDB.remove("${PostType.MY_POST.path}/$userUid/${PostType.CLUB_SEARCH.type}"))
            }
            .andThen(Single.just(DontCare))
    }
}

fun Single<DontCare>.removeClubMatch(userUid: String): Single<DontCare> {
    return this.flatMap {
        RxRealTimeDB.toSingle("${PostType.MY_POST.path}/$userUid/${PostType.CLUB_MATCH.type}") { mutableMapOf<String, ModelMyPost>() }
            .map { it.keys.toList() }
            .toObservable()
            .flatMapIterable { it }
            .flatMapCompletable {
                RxRealTimeDB.remove("${PostType.CLUB_MATCH.path}/$it")
                    .andThen(RxRealTimeDB.remove("${PostType.MY_POST.path}/$userUid/${PostType.CLUB_MATCH.type}"))
            }
            .andThen(Single.just(DontCare))

    }
}

fun Single<DontCare>.updateClubBoard(userModel: ModelUser): Single<DontCare> {
    return this.flatMap {
        RxRealTimeDB.toSingle("${PostType.CLUB_BOARD.path}/${LoginManager.user?.clubUid}") { mutableMapOf<String, ModelClubBoard>() }
            .flatMap { boardList ->
                boardList.filter { it.value.uid == LoginManager.user?.uid }
                    .values.forEach {
                    it.profileImageUrl = userModel.profileImageUrl
                    it.nickName = userModel.nickName!!
                }

                RxRealTimeDB.push("${PostType.CLUB_BOARD.path}/${LoginManager.user?.clubUid}", boardList)
            }
    }
}

fun Single<ModelClub>.checklub(): Single<ModelClub> {
    return this.map {
        if (it.contentId.isEmpty() || it.clubName.isEmpty() || it.createAt.isEmpty())
            throw ErrorCodeException(ErrorCode.NO_EXIST_CLUB)
        else
            it
    }
        .flatMap {
            Single.just(it)
        }
}

fun <T> Observable<T>.updateClubMemberInfo(): Observable<DontCare> {
    return this.flatMap {
        RxRealTimeDB.toSingle("${FirebaseUrls.club}/${LoginManager.user?.clubUid}") { ModelClub() }
            .toObservable()
    }
        .map {
            val changeData = ModelClub.ClubInfoChangeData(
                club = it.clubName,
                clubUid = it.contentId,
                clubJonined = true,
                clubImageUrl = it.clubMarkImageUrl,
                clubMasterUid = it.masterUid,
                clubIntro = it.clubIntro,
                activityAge = it.activityAge,
                activityArea = it.activityArea,
                assitantList = it.assistantList
            )

            changeData to it.clubMembers

        }
        .flatMap { pair ->
            pair.second.map { it.uid }
                .toList()
                .toObservable()
                .flatMap { memberUid ->
                    RxRealTimeDB.toSingle("${FirebaseUrls.users}/$memberUid") { ModelUser() }
                        .map { response ->
                            response.apply {
                                uid = response.uid
                                nickName = response.nickName
                                age = response.age
                                email = response.email
                                area = response.area
                                sex = response.sex
                                profileImageUrl = response.profileImageUrl
                                position = response.position
                                club = pair.first.club
                                clubJonined = pair.first.clubJonined
                                clubImageUrl = pair.first.clubImageUrl
                                clubUid = pair.first.clubUid
                                clubMasterUid = pair.first.clubMasterUid
                                clubAssistantList = pair.first.assitantList
                                mercenaryClubInfo = response.mercenaryClubInfo
                            }
                        }
                        .flatMap { RxRealTimeDB.push("${FirebaseUrls.users}/$memberUid", it) }
                        .toObservable()
                }
        }
        .flatMap { Observable.just(DontCare) }
}


fun <T> Single<T>.updateClubMemberInfo(): Single<DontCare> {
    return this.flatMap { RxRealTimeDB.toSingle("${FirebaseUrls.club}/${LoginManager.user?.clubUid}") { ModelClub() } }
        .map {
            val changeData = ModelClub.ClubInfoChangeData(
                club = it.clubName,
                clubUid = it.contentId,
                clubJonined = true,
                clubImageUrl = it.clubMarkImageUrl,
                clubMasterUid = it.masterUid,
                clubIntro = it.clubIntro,
                activityAge = it.activityAge,
                activityArea = it.activityArea,
                assitantList = it.assistantList
            )

            changeData to it.clubMembers

        }
        .flatMap { pair ->
            pair.second
                .map { it.uid }
                .toList()
                .forEach { memberUid -> updateMemberInfo(memberUid, pair) }

            Single.just(DontCare)
        }
}

fun <T> Single<T>.firedUserSecession(userUid: String, isMercenary: Boolean, clubUID: String): Single<DontCare> {
    if (isMercenary) {
        return this.flatMap {
            RxRealTimeDB.toSingle("${FirebaseUrls.users}/$userUid") { ModelUser() }
                .map { response ->
                    response.mercenaryClubInfo?.removeAll { it.contentId == clubUID }
                    response
                }
                .flatMap { RxRealTimeDB.push("${FirebaseUrls.users}/$userUid", it) }
        }
    } else
        return this.flatMap {
            RxRealTimeDB.toSingle("${FirebaseUrls.users}/$userUid") { ModelUser() }
                .map { response ->
                    response.apply {
                        uid = response.uid
                        nickName = response.nickName
                        age = response.age
                        email = response.email
                        area = response.area
                        sex = response.sex
                        profileImageUrl = response.profileImageUrl
                        position = response.position
                        club = null
                        clubJonined = false
                        clubImageUrl = null
                        clubUid = null
                        clubMasterUid = null
                        clubAssistantList = null
                        mercenaryClubInfo = response.mercenaryClubInfo
                    }
                }
                .flatMap { RxRealTimeDB.push("${FirebaseUrls.users}/$userUid", it) }
        }
}

private fun updateMemberInfo(
    memberUid: String,
    pair: Pair<ModelClub.ClubInfoChangeData, MutableList<ModelClub.ModelMember>>
) {
    RxRealTimeDB.toSingle("${FirebaseUrls.users}/$memberUid") { ModelUser() }
        .map { response ->
            response.apply {
                uid = response.uid
                nickName = response.nickName
                age = response.age
                email = response.email
                area = response.area
                sex = response.sex
                profileImageUrl = response.profileImageUrl
                position = response.position
                club = pair.first.club
                clubJonined = pair.first.clubJonined
                clubImageUrl = pair.first.clubImageUrl
                clubUid = pair.first.clubUid
                clubMasterUid = pair.first.clubMasterUid
                clubAssistantList = pair.first.assitantList
                mercenaryClubInfo = response.mercenaryClubInfo
            }
        }
        .flatMap { RxRealTimeDB.push("${FirebaseUrls.users}/$memberUid", it) }
        .subscribe()
}