package ks.bs.mysoccerting.scene.club.detail.chat

import android.view.View
import androidx.databinding.ObservableField
import com.google.firebase.database.GenericTypeIndicator
import io.reactivex.Observable
import io.reactivex.rxkotlin.addTo
import ks.bs.mysoccerting.BR
import ks.bs.mysoccerting.R
import ks.bs.mysoccerting.databinding.BaseRecyclerViewAdapter
import ks.bs.mysoccerting.databinding.ItemChatBinding
import ks.bs.mysoccerting.firebase.CrashlyticsHelper
import ks.bs.mysoccerting.firebase.FirebaseUrls
import ks.bs.mysoccerting.firebase.RxRealTimeDB
import ks.bs.mysoccerting.model.ModelChat
import ks.bs.mysoccerting.model.time.LocalTime
import ks.bs.mysoccerting.scene.base.ActivityBaseViewModel
import ks.bs.mysoccerting.scene.base.FragmentBaseViewModel
import ks.bs.mysoccerting.scene.club.supervisor.TimeManager
import ks.bs.mysoccerting.scene.club.supervisor.getFormattDate
import ks.bs.mysoccerting.scene.login.LoginManager

class MercenaryChatVm(val contract: Contract) : ActivityBaseViewModel() {
    interface Contract : FragmentBaseViewModel.BaseVmListener {
        fun scrollBottom()
    }

    val adapter = BaseRecyclerViewAdapter<ChatItemVm, ItemChatBinding>(
        R.layout.item_chat,
        BR.viewModel
    )

    val chatText = ObservableField("")
    var contentId: String = ""

    fun init(contentId: String) {
        this.contentId = contentId

        val path = "${FirebaseUrls.clubChat}/$contentId"
        val indecator = object : GenericTypeIndicator<HashMap<String, ModelChat>>() {}

        RxRealTimeDB.toObservable(path, indecator) { HashMap() }
            .overTimeChatDelete(contentId)
            .map { it.values }
            .subscribe({
                adapter.clear()
                it.forEach { chatModel -> adapter.insert(ChatItemVm(chatModel, false)) }
                adapter.items.sortBy { it.timeStamp }
                contract.scrollBottom()

            }, { e ->
                CrashlyticsHelper.logException(e)
            })
            .addTo(compositeDisposable)
    }

    private fun Observable<HashMap<String, ModelChat>>.overTimeChatDelete(contentId: String): Observable<HashMap<String, ModelChat>> {
        return this.flatMap {
            it.forEach { chat ->
                if (TimeManager.judgeTimeOutSevenDays(chat.value.timeStamp.getFormattDate()))
                    RxRealTimeDB.remove("${FirebaseUrls.clubChat}/$contentId/${chat.key}")
                        .subscribe()
            }

            this
        }
    }

    fun chatSend(view: View) {
        if (chatText.get()!!.isEmpty())
            return

        val contentId = RxRealTimeDB.generateRandomKey("chat")

        val chatModel = ModelChat(
            uid = LoginManager.user?.uid!!,
            content = chatText.get()!!,
            nickName = LoginManager.user?.nickName!!,
            profileImageUrl = LoginManager.user?.profileImageUrl,
            timeStamp = LocalTime().time,
            clubName = LoginManager.user?.club
        )

        RxRealTimeDB.push("${FirebaseUrls.clubChat}/${this.contentId}/$contentId", chatModel)
            .subscribe({
                chatText.set("")
            }, { e ->
                CrashlyticsHelper.logException(e)
            })
            .addTo(compositeDisposable)
    }

    fun backPressed(view: View) {
        val activity = view.context as MercenaryChatActivity
        activity.onBackPressed()
    }
}