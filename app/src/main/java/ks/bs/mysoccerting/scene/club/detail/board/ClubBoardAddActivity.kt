package ks.bs.mysoccerting.scene.club.detail.board

import android.os.Bundle
import androidx.databinding.DataBindingUtil
import kotlinx.android.synthetic.main.activity_club_board_add.*
import kr.nextm.lib.TToast
import ks.bs.mysoccerting.R
import ks.bs.mysoccerting.databinding.ActivityClubBoardAddBinding
import ks.bs.mysoccerting.rx.rxresult.RxActivityBase

class ClubBoardAddActivity : RxActivityBase() {
    val viewModel = createVm()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        DataBindingUtil.setContentView<ActivityClubBoardAddBinding>(
            this,
            R.layout.activity_club_board_add
        ).viewModel = viewModel

        initToolBar()
    }

    private fun initToolBar() {
        toolbar_title.text = "게시글 등록"
    }

    private fun createVm() = ClubBoardAddViewModel()

    override fun onDestroy() {
        viewModel.onDestroy()
        super.onDestroy()
    }
}
