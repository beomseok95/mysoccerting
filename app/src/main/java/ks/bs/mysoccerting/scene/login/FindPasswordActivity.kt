package ks.bs.mysoccerting.scene.login

import android.os.Bundle
import androidx.databinding.DataBindingUtil
import kotlinx.android.synthetic.main.activity_find_password.*
import kr.nextm.lib.TToast
import kr.nextm.lib.getColor
import ks.bs.mysoccerting.R
import ks.bs.mysoccerting.databinding.ActivityFindPasswordBinding
import ks.bs.mysoccerting.scene.base.ActivityBase

class FindPasswordActivity : ActivityBase(), FindPasswordViewModel.FindPasswordActivityContract {

    val model = FindPasswordViewModel(this)

    override fun onCreate(savedInstanceState: Bundle?) {
        isLoginActivity = true
        super.onCreate(savedInstanceState)
        val binding = DataBindingUtil.setContentView<ActivityFindPasswordBinding>(this, R.layout.activity_find_password)
        binding.findPasswordViewModel = model

        model.init()

        toolbar_title.text= "비밀번호 재설정"

    }

    override fun showToast(message: String) {
        TToast.show(message)
    }

    override fun onDestroy() {
        model.onDestroy()
        super.onDestroy()
    }
}
