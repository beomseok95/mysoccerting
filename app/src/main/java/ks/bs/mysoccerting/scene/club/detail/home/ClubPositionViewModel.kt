package ks.bs.mysoccerting.scene.club.detail.home

import android.content.ClipData
import android.content.ClipDescription
import android.os.Build
import android.view.*
import android.widget.FrameLayout
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.databinding.ObservableField
import androidx.recyclerview.widget.RecyclerView
import io.reactivex.Maybe
import io.reactivex.Single
import io.reactivex.rxkotlin.addTo
import kr.nextm.lib.TToast
import ks.bs.mysoccerting.BR
import ks.bs.mysoccerting.R
import ks.bs.mysoccerting.customview.dialog.DialogButton
import ks.bs.mysoccerting.customview.dialog.TDialog
import ks.bs.mysoccerting.databinding.BaseRecyclerViewAdapter
import ks.bs.mysoccerting.databinding.ItemClubPositionBinding
import ks.bs.mysoccerting.exception.ErrorCode
import ks.bs.mysoccerting.firebase.CrashlyticsHelper
import ks.bs.mysoccerting.firebase.FirebaseUrls
import ks.bs.mysoccerting.firebase.RxRealTimeDB
import ks.bs.mysoccerting.model.ModelClub
import ks.bs.mysoccerting.rx.withProgressBallAnimation
import ks.bs.mysoccerting.scene.base.ActivityBaseViewModel
import ks.bs.mysoccerting.scene.login.LoginManager
import ks.bs.mysoccerting.support.DontCare
import ks.bs.mysoccerting.util.TLog
import ks.bs.mysoccerting.util.getString


class ClubPositionViewModel(private val contract: ClubPositionContract) : ActivityBaseViewModel() {
    interface ClubPositionContract {
        fun backPressEvent()
        fun onSave(positionList: HashMap<String, String>)
    }

    var modelClub = ModelClub()
    var item = mutableListOf<ModelClub.ModelMember>()
    var tempItem = mutableListOf<ModelClub.ModelMember>()
    var positionList = hashMapOf<String, String>()
    var memberVisibility = ObservableField(false)
    val havePermission = ObservableField(false)

    val adapter = BaseRecyclerViewAdapter<ModelClub.ModelMember, ItemClubPositionBinding>(
        R.layout.item_club_position, BR.clubMember,
        this, BR.listener
    )

    val paramFrame = FrameLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT)
    var tag = ""
    var title = ObservableField<String>()
    lateinit var currentLayout: ViewParent
    var containerId = 0
    var memberListId = 0
    var x: Float = 0f
    var y: Float = 0f
    var moveCount = 0
    lateinit var player: ConstraintLayout


    fun init() {
        havePermission.set(LoginManager.user?.uid == modelClub.masterUid)
        if (modelClub.clubPositionList != null) {
            positionList = modelClub.clubPositionList!!
        }
        getContents()
        paramFrame.gravity = Gravity.CENTER
    }

    fun backPressBtnClkEvent() {
        contract.backPressEvent()
    }

    private fun getContents() {
        item.clear()
        for (member in modelClub.clubMembers) {
            item.add(member)
        }

        if (modelClub.mercenaryMember != null) {
            for (mercenary in modelClub.mercenaryMember!!)
                item.add(mercenary)
        }
        TLog.d(positionList)

        positionList.values.forEach { values ->
            tempItem.add(item.find { it.nickName == values }!!)
            item.removeAll { it.nickName == values }
        }

        adapter.replaceAll(item)
        adapter.notifyDataSetChanged()
    }

    val rootDragListener = View.OnDragListener { v, event ->
        if (event.action == DragEvent.ACTION_DRAG_ENTERED) {
            player.visibility = View.VISIBLE
        }
        return@OnDragListener true
    }

    val touchListener = View.OnTouchListener { v, event ->
        if (!havePermission.get()!! || !memberVisibility.get()!!)
            return@OnTouchListener true
        player = v as ConstraintLayout
        currentLayout = v.parent

        if (event.action == MotionEvent.ACTION_DOWN) {
            x = event.x
            y = event.y
        } else if (event.action == MotionEvent.ACTION_MOVE) {
            if (currentLayout is RecyclerView) {
                if (moveCount == 0) {
                    moveCount++
                    return@OnTouchListener true
                }

                moveCount = 0

                val diffX = Math.abs(x - event.x)
                val diffY = Math.abs(y - event.y)
                TLog.d("move ${event.x} ${event.y} $diffX $diffY ${diffY / diffX}")
                if (diffY / diffX < 0.5 || (diffX == 0f && diffY == 0f))
                    return@OnTouchListener true
            }
            tag = v.tag.toString()
            val item = ClipData.Item(v.tag as CharSequence)
            val mimeTypes = arrayOf(ClipDescription.MIMETYPE_TEXT_PLAIN)
            val data = ClipData(tag, mimeTypes, item)
            val builder = View.DragShadowBuilder(v)

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                v.startDragAndDrop(data, builder, null, 0)
            } else {
                v.startDrag(data, builder, null, 0)
            }

            v.visibility = View.INVISIBLE
            paramFrame.height = v.height
            paramFrame.width = v.width
        }
        return@OnTouchListener true
    }

    val dragListener = View.OnDragListener { onDragLayout, event ->
        val draggingPlayer = player
        when (event.action) {
            DragEvent.ACTION_DROP -> {
                val previousLayout = draggingPlayer.parent as ViewGroup

                if (!positionList.containsKey(onDragLayout.tag)) {
                    when {
                        onDragLayout.id == memberListId -> {
                            /** 포지션 -> 맴버 리스트 */
                            if (currentLayout !is RecyclerView) {
                                previousLayout.removeView(draggingPlayer)

                                positionList.remove(previousLayout.tag.toString())
                                item.add(tempItem.find { it.nickName == tag }!!)
                                tempItem.remove(tempItem.find { it.nickName == tag }!!)

                                item.sortBy { it.nickName }
                                adapter.replaceAll(item)
                                adapter.notifyDataSetChanged()
                            }
                        }
                        currentLayout is RecyclerView -> {
                            /** 맴버 리스트 -> 포지션 */
                            if (positionList.size >= 11) {
                                TToast.show("최대 11명을 넘을수 없습니다")
                                draggingPlayer.visibility = View.VISIBLE
                                return@OnDragListener true
                            }
                            if (onDragLayout is FrameLayout) {

                                previousLayout.removeView(draggingPlayer)

                                positionList[onDragLayout.tag.toString()] = draggingPlayer.tag.toString()

                                onDragLayout.addView(draggingPlayer, paramFrame)

                                tempItem.add(item.find { it.nickName == tag }!!)
                                item.remove(item.find { it.nickName == tag }!!)

                                item.sortBy { it.nickName }
                                adapter.replaceAll(item)
                                adapter.notifyDataSetChanged()
                            }
                        }
                        onDragLayout.id != containerId -> {
                            previousLayout.removeView(draggingPlayer)
                            (onDragLayout as FrameLayout).addView(draggingPlayer, paramFrame)
                            positionList.remove(previousLayout.tag.toString())
                            positionList[onDragLayout.tag.toString()] = draggingPlayer.tag.toString()
                        }
                    }
                    TLog.d(positionList)
                }
                draggingPlayer.visibility = View.VISIBLE
            }
        }
        true
    }

    fun onSave(view: View) {
        requireNotNull(modelClub.contentId) { ErrorCode.FAIL_NO_EXIST_MY_CLUB_CONTENT_ID.message }
        TDialog.Builder(view.context, R.string.notice.getString())
            .setMessage("포지션을 저장 하시겠습니까?")
            .isColored(true)
            .addButton(DialogButton.CANCEL())
            .addButton(DialogButton.OK())
            .toSingle()
            .filter { it.ok }
            .flatMapSingle {
                val path = "${FirebaseUrls.club}/${modelClub.contentId}"
                modelClub.clubPositionList = positionList
                RxRealTimeDB.push(path, modelClub)
                    .withProgressBallAnimation(view.context)
            }
            .subscribe({
                contract.backPressEvent()
            }, { e ->
                if(e is NoSuchElementException)
                    return@subscribe

                CrashlyticsHelper.logException(e)
            })
            .addTo(compositeDisposable)

    }

    fun onEdit() {
        memberVisibility.set(!memberVisibility.get()!!)
    }

}