package ks.bs.mysoccerting.scene.alarm

import android.view.View
import ks.bs.mysoccerting.firebase.FirebaseUrls
import ks.bs.mysoccerting.firebase.RxRealTimeDB
import ks.bs.mysoccerting.model.*
import ks.bs.mysoccerting.model.type.AlarmType
import ks.bs.mysoccerting.model.type.MovePageLocationType
import ks.bs.mysoccerting.rx.rxresult.putObject
import ks.bs.mysoccerting.rx.rxresult.startActivity
import ks.bs.mysoccerting.rx.rxresult.toIntent
import ks.bs.mysoccerting.scene.club.detail.board.DetailBoardActivity
import ks.bs.mysoccerting.scene.club.detail.chat.ChatActivity
import ks.bs.mysoccerting.scene.club.detail.home.MatchUpActivity
import ks.bs.mysoccerting.scene.club.detail.setting.MatchingApplicationManagementActivity
import ks.bs.mysoccerting.scene.club.detail.setting.MembershipManagementActivity
import ks.bs.mysoccerting.scene.club.detail.setting.MercenaryManagementActivity
import ks.bs.mysoccerting.scene.club.supervisor.ClubSignUpHelper
import ks.bs.mysoccerting.scene.club.supervisor.MatchHelper
import ks.bs.mysoccerting.scene.club.supervisor.MercenaryInvitationHelper
import ks.bs.mysoccerting.scene.club.supervisor.MercenarySignUpHelper
import ks.bs.mysoccerting.scene.common.DetailItemActivity
import ks.bs.mysoccerting.scene.common.DetailItemViewModel
import ks.bs.mysoccerting.scene.login.LoginManager
import ks.bs.mysoccerting.scene.profile.ProfileHomeActivity
import ks.bs.mysoccerting.util.DateEx

class AlarmItemViewModel(val model: ModelAlaram, val contract: AlarmViewModel.Contract) {
    val timeAgo get() = DateEx.convertRecentDate(model.timeStamp)
    val timeStamp get() = model.timeStamp
    val senderImageUrl get() = model.senderImageUrl
    val message get() = model.message

    val dividerText get() = model.divider?.text
    val isDividerShowing
        get() = if (model.divider == null)
            false
        else model.divider?.isDividerShowing

    fun showBottomSheet(view: View) {
        val activity = view.context as AlarmActivity
        AlarmBottomSheetDialog(model).show(activity.supportFragmentManager, "")
    }

    fun showProfile(view: View) {
        ProfileHomeActivity::class.java
            .toIntent()
            .putObject(model.senderUid)
            .startActivity(view.context)
            .subscribe()
    }

    fun movePost(view: View) {
        when (model.alaramTypeCode) {
            AlarmType.COMMENT.code -> {
                if (model.commentCallType != null) {
                    DetailItemActivity.CALL_LOCATION = model.commentCallType!!

                    RxRealTimeDB.toSingle(model.postPullPath) { getPostModel() }
                        .flatMapCompletable { model ->
                            DetailItemActivity::class.java
                                .toIntent()
                                .putObject(model)
                                .startActivity(view.context)
                        }
                } else {
                    RxRealTimeDB.toSingle(model.postPullPath) { ModelClubBoard() }
                        .flatMapCompletable { model ->
                            DetailBoardActivity::class.java
                                .toIntent()
                                .putObject(model)
                                .startActivity(view.context)
                        }
                }
                    .subscribe()
            }
            AlarmType.CLUB_SIGNUP.code -> {
                MembershipManagementActivity::class.java
                    .toIntent()
                    .startActivity(view.context)
                    .subscribe()
            }

            AlarmType.CLUB_SIGNUP_COMPLETE.code -> {
                contract.finishPage(
                    ModelMoveTo(
                        locationcode = MovePageLocationType.CLUB_DETAIL.locationCode,
                        clubPullPath = model.postPullPath
                    )
                )
            }
            AlarmType.CLUB_MERCENARY_SIGNUP_REQUEST.code -> {
                MercenaryManagementActivity::class.java
                    .toIntent()
                    .startActivity(view.context)
                    .subscribe()
            }

            AlarmType.CLUB_MERCENARY_SIGNUP_COMPLETE.code -> {
                contract.finishPage(
                    ModelMoveTo(
                        locationcode = MovePageLocationType.PROFILE.locationCode
                    )
                )
            }
            AlarmType.MATCHING_COMPLETE.code -> {
                requireNotNull(LoginManager.user?.clubUid) 
                MatchUpActivity::class.java
                    .toIntent()
                    .putObject(LoginManager.user?.clubUid!!)
                    .startActivity(view.context)
                    .subscribe()
            }
        }
    }

    private fun getPostModel() = when (model.commentCallType) {
        DetailItemViewModel.MATCHING_CLUB_TYPE -> ModelClubMatch()
        DetailItemViewModel.SEARTCH_MERCENARY_TYPE -> ModelMercenarySearch()
        else -> ModelClubSearch()
    }

    fun acceptRejectBtnVisibility() =
        model.alaramTypeCode == AlarmType.CLUB_SIGNUP.code || model.alaramTypeCode == AlarmType.CLUB_MERCENARY_SIGNUP_REQUEST.code
                || model.alaramTypeCode == AlarmType.MERCENARY_INVITATION_REQUEST.code

    fun matchingInfoBtnVisibility() = model.alaramTypeCode == AlarmType.MATCHING_APPLY.code

    fun showMatchingInfoApplyList(view: View) {
        MatchingApplicationManagementActivity::class.java
            .toIntent()
            .startActivity(view.context)
            .subscribe()
    }

    fun acceptBtnClkEvent(view: View) {
        when (model.alaramTypeCode) {
            AlarmType.CLUB_SIGNUP.code -> ClubSignUpHelper.clubSignUpProcess(view, model)
            AlarmType.CLUB_MERCENARY_SIGNUP_REQUEST.code -> MercenarySignUpHelper.accept(view, model)
            AlarmType.MERCENARY_INVITATION_REQUEST.code -> MercenaryInvitationHelper.mercenaryInvitationProccess(
                view,
                model
            )
        }
    }


    fun rejectBtnClkEvent(view: View) {
        when (model.alaramTypeCode) {
            AlarmType.CLUB_SIGNUP.code -> ClubSignUpHelper.reject(view, model)
            AlarmType.CLUB_MERCENARY_SIGNUP_REQUEST.code -> MercenarySignUpHelper.reject(view, model)
            AlarmType.MERCENARY_INVITATION_REQUEST.code -> MercenaryInvitationHelper.reject(view, model)
        }
    }
}