package ks.bs.mysoccerting.scene.filter

import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.math.MathUtils
import androidx.databinding.DataBindingUtil
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.fragment_people_number_dialog.*
import kr.nextm.lib.getDrawable
import ks.bs.mysoccerting.R
import ks.bs.mysoccerting.customview.dialog.RxDialogFragment
import ks.bs.mysoccerting.databinding.FragmentPeopleNumberDialogBinding
import ks.bs.mysoccerting.util.dpToPixels

class PeopleNumberDialog : RxDialogFragment<MutableList<String>>() {

    val viewModel = creatVm()

    val compositeDisposable = CompositeDisposable()

    override fun onCreateCustomView(inflater: LayoutInflater, container: ViewGroup): View {

        return DataBindingUtil.inflate<FragmentPeopleNumberDialogBinding>(
            inflater,
            R.layout.fragment_people_number_dialog,
            container, false
        )
            .let {
                it.viewModel = viewModel
                it.root
            }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel.init()
        initDialogSize()
        initHeaderColor()

    }

    private fun creatVm(): PeopleNumberVm {
        return PeopleNumberVm(object : PeopleNumberVm.Contract {
            override fun finishDialog() {
                finishWithResponse(mutableListOf())
            }

            override fun peopleNumberSelectComplete(numberList: MutableList<String>) {
                finishWithResponse(numberList)
            }
        })
    }

    private fun initDialogSize() {
        layout_dialog_root.addOnLayoutChangeListener(object : View.OnLayoutChangeListener {
            override fun onLayoutChange(
                v: View,
                left: Int,
                top: Int,
                right: Int,
                bottom: Int,
                oldLeft: Int,
                oldTop: Int,
                oldRight: Int,
                oldBottom: Int
            ) {
                v.removeOnLayoutChangeListener(this)
                val headerHeight = 40.dpToPixels()
                val buttonsHeight = 80.dpToPixels()
                val sum = headerHeight + buttonsHeight

                val width = 450.dpToPixels()
                val height = MathUtils.clamp(
                    bottom - top,
                    400.dpToPixels() + sum,
                    500.dpToPixels() + sum
                )

                val displayMetrics = context.resources.displayMetrics
                v.layoutParams.width = minOf(width, displayMetrics.widthPixels)
                v.layoutParams.height = minOf(height, displayMetrics.heightPixels)
                dialog?.window?.setLayout(v.layoutParams.width, v.layoutParams.height)
            }
        })
    }

    private fun initHeaderColor() {
        layout_dialog_header.background = R.drawable.bg_setting_dialog_header.getDrawable()
        tv_dialog_title.setTextColor(Color.WHITE)
    }

    override fun onDestroy() {
        viewModel.clearDisposables()
        compositeDisposable.clear()
        super.onDestroy()
    }
}