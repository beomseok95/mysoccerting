package ks.bs.mysoccerting.scene.club.detail.board

import android.view.View
import androidx.databinding.ObservableField
import io.reactivex.Single
import io.reactivex.rxkotlin.addTo
import kr.nextm.lib.TToast
import ks.bs.mysoccerting.firebase.CrashlyticsHelper
import ks.bs.mysoccerting.firebase.FirebaseUrls
import ks.bs.mysoccerting.firebase.RxRealTimeDB
import ks.bs.mysoccerting.model.ModelClubBoard
import ks.bs.mysoccerting.model.ModelClubTimeline
import ks.bs.mysoccerting.model.ModelMyPost
import ks.bs.mysoccerting.model.time.LocalTime
import ks.bs.mysoccerting.model.type.PostType
import ks.bs.mysoccerting.model.type.TimelineType
import ks.bs.mysoccerting.scene.base.ActivityBaseViewModel
import ks.bs.mysoccerting.scene.club.detail.ClubDetailFragment
import ks.bs.mysoccerting.scene.login.LoginManager
import ks.bs.mysoccerting.support.DontCare
import ks.bs.mysoccerting.util.TLog

class ClubBoardAddViewModel : ActivityBaseViewModel() {

    var title = ObservableField<String>()
    var postTitle = ObservableField<String>()
    var content = ObservableField<String>()

    fun backPressed(view: View) {
        val activity = view.context as ClubBoardAddActivity
        activity.onBackPressed()
    }

    fun okBtnClkEvent(view:View) {
        if (postTitle.get().isNullOrEmpty() || content.get().isNullOrEmpty()) {
            TToast.show("내용을 입력하세요")
            return
        }
        val title = postTitle.get().toString()
        val content = content.get().toString()
        val postContentUid = RxRealTimeDB.generateRandomKey("post")
        val boardPostPath = "${FirebaseUrls.clubBoard}/${LoginManager.user?.clubUid}/$postContentUid"
        val model = ModelClubBoard(
            title = title,
            profileImageUrl = LoginManager.user?.profileImageUrl,
            content = content,
            nickName = LoginManager.user?.nickName!!,
            timestamp = System.currentTimeMillis(),
            contentId = postContentUid,
            uid = LoginManager.user?.uid!!,
            commentcount = 0,
            clubUid = LoginManager.user?.clubUid!!
        )
        RxRealTimeDB.push(boardPostPath, model)
            .pushMyPost(model)
            .flatMap {
                require(ClubDetailFragment.CLUB_UID.isNotEmpty()) { "CLUB_UID need to not null" }

                val contentId = RxRealTimeDB.generateRandomKey("timeline")
                val path = "${FirebaseUrls.club}/${ClubDetailFragment.CLUB_UID}/${FirebaseUrls.timeLine}/$contentId"
                RxRealTimeDB.push(
                    path, createTimeLinePost(
                        content = content,
                        documentId = contentId,
                        type = TimelineType.POST,
                        contentPath = boardPostPath
                    )
                )
            }
            .subscribe({
                backPressed(view = view)
                TLog.d("게시글 작성 성공")
            }, { e ->
                CrashlyticsHelper.logException(e)
            }).addTo(compositeDisposable)
    }

    private fun Single<DontCare>.pushMyPost(model:ModelClubBoard): Single<DontCare> {
        return this.flatMap {
            val requestModel = ModelMyPost(
                contentId = model.contentId,
                content = model.content,
                type = PostType.CLUB_BOARD,
                mPostPath = "${PostType.MY_POST.path}/${LoginManager.user?.uid}/${PostType.CLUB_BOARD.type}/${model.contentId}",
                timestamp = model.timestamp
            )
            RxRealTimeDB.push(requestModel.mPostPath,requestModel)
        }
            .flatMap { this }

    }

    private fun createTimeLinePost(
        content: String,
        documentId: String,
        type: TimelineType,
        contentPath: String
    ): ModelClubTimeline {
        return ModelClubTimeline(
            nickName = LoginManager.user?.nickName!!,
            uid = LoginManager.user?.uid!!,
            contentUid = documentId,
            memberImageUrl = LoginManager.user?.profileImageUrl,
            timestamp = LocalTime().time,
            content = content,
            type = type,
            contentPath = contentPath
        )
    }
}
