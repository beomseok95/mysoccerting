package ks.bs.mysoccerting.scene.login

import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.os.Bundle
import android.view.View
import android.view.WindowManager
import androidx.databinding.DataBindingUtil
import com.facebook.CallbackManager
import com.facebook.login.LoginManager
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.firebase.auth.FirebaseUser
import com.kakao.auth.AuthType
import com.kakao.auth.Session
import kr.nextm.lib.TToast
import ks.bs.mysoccerting.R
import ks.bs.mysoccerting.databinding.ActivityLoginBinding
import ks.bs.mysoccerting.scene.base.ActivityBase
import ks.bs.mysoccerting.scene.home.MainActivity
import org.jetbrains.anko.startActivity


class LoginActivity : ActivityBase(), LoginViewModel.LoginActivityContract {

    private val googleSignInClient: GoogleSignInClient by lazy {
        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestIdToken(getString(R.string.default_web_client_id))
            .requestEmail()
            .build()
        GoogleSignIn.getClient(this, gso)
    }

    private val callbackManager: CallbackManager by lazy {
        CallbackManager.Factory.create()
    }

    val model = LoginViewModel(this, callbackManager)

    override fun onCreate(savedInstanceState: Bundle?) {
        isLoginActivity = true
        model.init()
        super.onCreate(savedInstanceState)
        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN)
        val databinding = DataBindingUtil.setContentView<ActivityLoginBinding>(this, R.layout.activity_login)
        databinding.loginViewModel = model
    }


    override fun facebookLogin() {
        LoginManager.getInstance().logInWithReadPermissions(
            this,
            listOf("public_profile", "email")
        )
    }

    override fun googleLogin() {
        val signInIntent = googleSignInClient.signInIntent
        startActivityForResult(signInIntent, GOOGLE_LOGIN_CODE)
    }

    override fun kakaoLogin() {
        if (!model.isConnected()) {
            showToast("인터넷 연결을 확인해주세요")
            return
        }
        val session = Session.getCurrentSession()
        session.addCallback(model.callback)
        session.open(AuthType.KAKAO_LOGIN_ALL, this@LoginActivity)
    }

    override fun lookMain() {
        startActivity<MainActivity>()
    }

    override fun emailLogin() {
        startActivity<EmailLoginActivity>()
    }

    override fun signUpEmail() {
        startActivity<SignUpEmailActivity>()
    }

    override fun moveMainPage(user: FirebaseUser?) {
        if (user != null) {
//            TToast.show("${R.string.signin_complete} \n 로그인정보 ${user.email}")
            model.progressVisibility.set(View.GONE)
            startActivity<MainActivity>()
        }
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        callbackManager.onActivityResult(requestCode, resultCode, data) //Facebook SDk로 값 넘겨주기
        model.onActivityResult(requestCode, resultCode, data)
    }

    override fun showToast(message: String) {
        TToast.show(message)
    }

    override fun onStart() {
        super.onStart()

        moveMainPage(auth.currentUser)//자동로그인설정
    }

    override fun getConnectivityManager(): ConnectivityManager {
        return getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
    }

    override fun onDestroy() {
        model.onDestroy()
        super.onDestroy()
    }

    companion object {
        const val GOOGLE_LOGIN_CODE = 9001 //GoogleLogin ,Intent Requset ID
    }
}
