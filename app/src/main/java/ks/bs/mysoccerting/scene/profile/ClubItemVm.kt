package ks.bs.mysoccerting.scene.profile

import android.view.View
import ks.bs.mysoccerting.R
import ks.bs.mysoccerting.customview.dialog.DialogButton
import ks.bs.mysoccerting.customview.dialog.TDialog
import ks.bs.mysoccerting.firebase.FirebaseUrls
import ks.bs.mysoccerting.firebase.RxRealTimeDB
import ks.bs.mysoccerting.model.ModelClub
import ks.bs.mysoccerting.model.ModelClubTimeline
import ks.bs.mysoccerting.model.ModelUser
import ks.bs.mysoccerting.model.time.LocalTime
import ks.bs.mysoccerting.model.type.TimelineType
import ks.bs.mysoccerting.rx.withProgressBallAnimation
import ks.bs.mysoccerting.scene.login.LoginManager
import ks.bs.mysoccerting.util.TLog
import ks.bs.mysoccerting.util.getString

class ClubItemVm(val model: ModelUser.ModelMercenaryClub, val contract: UserMercenaryManagementVm.Contract) {
    val clubName get() = model.clubName
    val clubMarkImageUrl get() = model.clubMarkImageUrl


    fun moveMercenaryClubPage() {
        contract.finishPageWithResponse(model.contentId)
    }

    fun unRegisterMercenary(view: View) {
        val mercenaryClubInfoPath = "${FirebaseUrls.users}/${LoginManager.user?.uid}/mercenaryClubInfo"
        val clubPath = "${FirebaseUrls.club}/${model.contentId}"
        TDialog.Builder(view.context, R.string.notice.getString())
            .setMessage("정말로 용병을 탈퇴하시겠습니까???")
            .isColored(true)
            .addButton(DialogButton.CANCEL())
            .addButton(DialogButton.OK())
            .toSingle()
            .filter { it.ok }
            .flatMapSingle { RxRealTimeDB.toSingle(mercenaryClubInfoPath) { mutableListOf<ModelUser.ModelMercenaryClub>() } }
            .flatMap {
                it.removeAll { it.contentId == model.contentId }
                RxRealTimeDB.push(mercenaryClubInfoPath, it)
            }
            .flatMap { RxRealTimeDB.toSingle(clubPath) { ModelClub() } }
            .flatMap {
                it.mercenaryMember?.removeAll { it.uid == LoginManager.user?.uid }
                it.mercenaryNumber = it.mercenaryNumber - 1
                val timelineContentId = RxRealTimeDB.generateRandomKey("timeline")
                it.timeline?.put(
                    timelineContentId, ModelClubTimeline(
                        nickName = LoginManager.user?.nickName!!,
                        uid = LoginManager.user?.uid!!,
                        contentUid = timelineContentId,
                        memberImageUrl = LoginManager.user?.profileImageUrl,
                        content = "${LoginManager.user?.nickName} 님이 용병을 탈퇴하였습니다. ",
                        timestamp = LocalTime().time,
                        type = TimelineType.MERCENARY_RETIREMENT
                    )
                )
                RxRealTimeDB.push(clubPath, it)
            }
            .flatMap {
                TDialog.Builder(view.context, R.string.notice.getString())
                    .setMessage("${model.clubName} 클럽의\n용병을 탈퇴하였습니다.")
                    .addButton(DialogButton.OK())
                    .isColored(true)
                    .toSingle()
            }
            .subscribe({
                TLog.d("unRegisterMercenary success")
            }, { e ->
                TLog.e(e)
            })
            .let { }
    }

}