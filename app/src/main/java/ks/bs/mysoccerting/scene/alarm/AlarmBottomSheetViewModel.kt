package ks.bs.mysoccerting.scene.alarm

import android.view.View
import androidx.databinding.ObservableField
import io.reactivex.rxkotlin.addTo
import kr.nextm.lib.TToast
import ks.bs.mysoccerting.firebase.CrashlyticsHelper
import ks.bs.mysoccerting.firebase.FirebaseUrls
import ks.bs.mysoccerting.firebase.RxRealTimeDB
import ks.bs.mysoccerting.model.ModelAlaram
import ks.bs.mysoccerting.rx.withProgressBallAnimation
import ks.bs.mysoccerting.scene.base.FragmentBaseViewModel
import ks.bs.mysoccerting.util.TLog

class AlarmBottomSheetViewModel(val contract: Contract) : FragmentBaseViewModel(contract) {

    interface Contract : BaseVmListener {
        fun dismissBottomSheet()
    }

    val alramMessage = ObservableField<String>()
    val alaramSenderImageUrl = ObservableField<String>()

    var modelAlaram = ModelAlaram()


    fun initContent(modelAlaram: ModelAlaram) {
        this.modelAlaram = modelAlaram
        alramMessage.set(modelAlaram.message)
        alaramSenderImageUrl.set(modelAlaram.senderImageUrl)
    }

    fun deleteAlarm(view: View) {
        val path = "${FirebaseUrls.alram}/${modelAlaram.reciverUid}"
        RxRealTimeDB.toSingle(path) { mutableListOf<ModelAlaram>() }
            .map { response ->
                response.removeAll { it.reciverUid == modelAlaram.reciverUid && it.timeStamp == modelAlaram.timeStamp }
                response
            }
            .flatMap { model -> RxRealTimeDB.push(path, model) }
            .withProgressBallAnimation(view.context)
            .subscribe({
                contract.dismissBottomSheet()
                TToast.show("알람메시지 삭제를\n성공했습니다.")
                TLog.d("delete Alaram success")
            }, { e ->
                contract.dismissBottomSheet()
                TToast.show("알람메시지 삭제를\n실패했습니다.")
                CrashlyticsHelper.logException(e)
            })
            .addTo(compositeDisposable)
    }
}