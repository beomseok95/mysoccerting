package ks.bs.mysoccerting.scene.club.detail.setting

import android.view.View
import io.reactivex.rxkotlin.addTo
import ks.bs.mysoccerting.BR
import ks.bs.mysoccerting.R
import ks.bs.mysoccerting.databinding.BaseRecyclerViewAdapter
import ks.bs.mysoccerting.databinding.ItemMembershipManagementBinding
import ks.bs.mysoccerting.firebase.CrashlyticsHelper
import ks.bs.mysoccerting.firebase.FirebaseUrls
import ks.bs.mysoccerting.firebase.RxRealTimeDB
import ks.bs.mysoccerting.model.ModelUser
import ks.bs.mysoccerting.scene.base.ActivityBaseViewModel
import ks.bs.mysoccerting.scene.login.LoginManager

class MembershipManagementVm : ActivityBaseViewModel() {

    val adapter =
        BaseRecyclerViewAdapter<MembershipManagementItemVm, ItemMembershipManagementBinding>(
            R.layout.item_membership_management,
            BR.viewModel
        )

    fun loadAffiliateList() {
        val path = "${FirebaseUrls.club}/${LoginManager.user?.clubUid}/membershipApplicationList"

        RxRealTimeDB.toObservable(path) { mutableListOf<ModelUser>() }
            .subscribe({
                adapter.clear()
                it.forEach { model -> adapter.insert(
                    MembershipManagementItemVm(
                        model
                    )
                ) }

            }, { e ->
                CrashlyticsHelper.logException(e)
            })
            .addTo(compositeDisposable)
    }


    fun backPressed(view: View) {
        val activity = view.context as MembershipManagementActivity
        activity.onBackPressed()
    }

}