package ks.bs.mysoccerting.scene.club.detail.dialog

import android.app.Dialog
import android.content.Context
import android.content.DialogInterface
import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.fragment_area_dialog.*
import kotlinx.android.synthetic.main.fragment_area_dialog.btn_dialog_close
import kotlinx.android.synthetic.main.fragment_area_dialog.layout_dialog_header
import kotlinx.android.synthetic.main.fragment_area_dialog.layout_dialog_root
import kotlinx.android.synthetic.main.fragment_area_dialog.tv_dialog_title
import kotlinx.android.synthetic.main.fragment_position_dialog.*
import kr.nextm.lib.TToast
import kr.nextm.lib.getDrawable
import ks.bs.mysoccerting.R
import ks.bs.mysoccerting.customview.dialog.RxDialogFragment
import ks.bs.mysoccerting.model.type.ClubGradeType
import ks.bs.mysoccerting.rx.showProgressDialog
import ks.bs.mysoccerting.scene.base.FragmentBaseViewModel
import ks.bs.mysoccerting.util.dpToPixels

class MemberInfoDialog(val contentId:String,val count:Int,val isInfoDialog: Boolean): RxDialogFragment<ClubGradeType.ModelGradeChange>() {

    val viewModel = createVm()

    val compositeDisposable = CompositeDisposable()

    override fun onCreateCustomView(inflater: LayoutInflater, container: ViewGroup): View {
        return DataBindingUtil.inflate<ks.bs.mysoccerting.databinding.FragmentMemberInfoDialogBinding>(
            inflater,
            R.layout.fragment_member_info_dialog,
            container, false
        )
            .let {
                it.viewModel = viewModel
                it.root
            }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel.getContent(parseObject(),isInfoDialog, contentId,count)
        initDialogSize()
        initHeaderColor()
    }

    private fun createVm(): MemberInfoVm{

        return MemberInfoVm(object : MemberInfoVm.Contract {
            override fun onBackPressed() {
                finish()
            }

            override fun getContext(): Context {
                return context
            }

            override fun showToast(message: String) {
                TToast.show(message)
            }

            override fun createDialogForProgress(request: FragmentBaseViewModel.ProgressBarOwner.Request): Dialog {
                return showProgressDialog(context, request.disposable, request.functionOnCancel)

            }

            override fun fire(isMercenary: Boolean, count: Int) {
                finishWithResponse(ClubGradeType.ModelGradeChange(isMercenary=isMercenary,count = count,nickName = viewModel.model.nickName))
            }

            override fun delegate(count: Int) {
                val model =ClubGradeType.ModelGradeChange(
                    gradeType = viewModel.model.gradeType,
                    nickName = viewModel.model.nickName,
                    isDelegated = true)
                finishWithResponse(model)
            }
        })
    }

    private fun initDialogSize() {
        layout_dialog_root.addOnLayoutChangeListener(object : View.OnLayoutChangeListener {
            override fun onLayoutChange(
                v: View,
                left: Int,
                top: Int,
                right: Int,
                bottom: Int,
                oldLeft: Int,
                oldTop: Int,
                oldRight: Int,
                oldBottom: Int
            ) {
                v.removeOnLayoutChangeListener(this)

                val width = 450.dpToPixels()
                val height = viewModel.getHeight()


                val displayMetrics = context.resources.displayMetrics
                v.layoutParams.width = minOf(width, displayMetrics.widthPixels)
                v.layoutParams.height = minOf(height, displayMetrics.heightPixels)
                dialog?.window?.setLayout(v.layoutParams.width, v.layoutParams.height)
            }
        })
    }

    private fun initHeaderColor() {
        layout_dialog_header.background = R.drawable.bg_setting_dialog_header.getDrawable()
        tv_dialog_title.setTextColor(Color.WHITE)
        btn_dialog_close.setColorFilter(Color.WHITE)
    }

    override fun onCancel(dialog: DialogInterface) {
        finish()
    }

    fun finish(){
        viewModel.save(dialog?.context)

        finishWithResponse(ClubGradeType.ModelGradeChange(gradeType = viewModel.model.gradeType))
    }

    override fun onDestroy() {
        viewModel.clearDisposables()
        compositeDisposable.clear()
        super.onDestroy()
    }
}