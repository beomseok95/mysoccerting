package ks.bs.mysoccerting.scene.common

import androidx.databinding.Observable
import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import android.text.TextUtils
import android.view.View
import ks.bs.mysoccerting.R
import ks.bs.mysoccerting.scene.base.ActivityBaseViewModel
import ks.bs.mysoccerting.util.getString

class CommentAmendViewModel(private val contract: CommentAdmendActivityContract) : ActivityBaseViewModel() {

    interface CommentAdmendActivityContract {
        fun showToast(message: String)
        fun sendResult(commentConetent: String)
    }

    var amendComment = ObservableField<String>()
    var isValid = ObservableBoolean(false)
    var buttonColor = ObservableBoolean(false)

    fun init() {
        amendComment.addOnPropertyChangedCallback(object : Observable.OnPropertyChangedCallback() {
            override fun onPropertyChanged(sender: Observable?, propertyId: Int) {
                validation()
            }
        })
    }

    fun validation() {
        if (!TextUtils.isEmpty(amendComment.get())) {
            isValid.set(true)
            buttonColor.set(true)
            return
        }
        isValid.set(false)
        buttonColor.set(false)
    }

    fun okBtnEvent() {
        val comment = amendComment.get().toString()
        if (!comment.isNullOrEmpty()) {
            contract.sendResult(comment)
            return
        }
        contract.showToast(R.string.commet_content_hint.getString())
    }

    fun backPressed(view: View) {
        val activity = view.context as CommentAmendActivity
        activity.onBackPressed()
    }
}