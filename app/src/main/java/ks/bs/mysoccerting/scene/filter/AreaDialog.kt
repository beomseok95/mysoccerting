package ks.bs.mysoccerting.scene.filter

import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.math.MathUtils
import androidx.databinding.DataBindingUtil
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.fragment_area_dialog.*
import kr.nextm.lib.TToast
import kr.nextm.lib.getDrawable
import ks.bs.mysoccerting.R
import ks.bs.mysoccerting.customview.dialog.RxDialogFragment
import ks.bs.mysoccerting.model.Area
import ks.bs.mysoccerting.rx.showProgressDialog
import ks.bs.mysoccerting.scene.base.FragmentBaseViewModel
import ks.bs.mysoccerting.util.AppInstance
import ks.bs.mysoccerting.util.TLog
import ks.bs.mysoccerting.util.dpToPixels

class AreaDialog : RxDialogFragment<Area>() {

    val viewModel = creatVm()

    val compositeDisposable = CompositeDisposable()

    override fun onCreateCustomView(inflater: LayoutInflater, container: ViewGroup): View {

        return DataBindingUtil.inflate<ks.bs.mysoccerting.databinding.FragmentAreaDialogBinding>(
            inflater,
            R.layout.fragment_area_dialog,
            container, false
        )
            .let {
                it.viewModel = viewModel
                it.root
            }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        initDialogSize()
        initHeaderColor()
        viewModel.setCityViewToLayout()
    }

    private fun creatVm(): AreaVm {

        return AreaVm(object : AreaVm.Contract {
            override fun resetCityGridLayout() {
                cityGridLayout.removeAllViews()
            }

            override fun onBackPressed() {
                if (viewModel.cityVisibility.get()) {
                    dismiss()
                    return
                }
                viewModel.cityVisibility.set(true)
                viewModel.townVisibility.set(false)
                viewModel.completeBtnVisibility.set(true)
                townGridLayout.removeAllViews()
            }

            override fun selectComplete(selectedCity: String?, selectedTown: String?) {
                finishWithResponse(Area(selectedCity = selectedCity, selectedTown = selectedTown))
            }

            override fun getContext(): Context {
                return context
            }

            override fun showToast(message: String) {
                TToast.show(message)
            }

            override fun createDialogForProgress(request: FragmentBaseViewModel.ProgressBarOwner.Request): Dialog {
                return showProgressDialog(context, request.disposable, request.functionOnCancel)

            }
        })
    }

    private fun initDialogSize() {
        layout_dialog_root.addOnLayoutChangeListener(object : View.OnLayoutChangeListener {
            override fun onLayoutChange(
                v: View,
                left: Int,
                top: Int,
                right: Int,
                bottom: Int,
                oldLeft: Int,
                oldTop: Int,
                oldRight: Int,
                oldBottom: Int
            ) {
                v.removeOnLayoutChangeListener(this)
                val headerHeight = 40.dpToPixels()
                val buttonsHeight = 80.dpToPixels()
                val sum = headerHeight + buttonsHeight

                val width = 450.dpToPixels()
                val height = MathUtils.clamp(
                    bottom - top,
                    400.dpToPixels() + sum,
                    500.dpToPixels() + sum
                )
                TLog.d("${400.dpToPixels()} ${AppInstance.get().resources.displayMetrics}")
                val displayMetrics = context.resources.displayMetrics
                v.layoutParams.width = minOf(width, displayMetrics.widthPixels)
                v.layoutParams.height = minOf(height, displayMetrics.heightPixels)
                dialog?.window?.setLayout(v.layoutParams.width, v.layoutParams.height)
            }
        })
    }

    private fun initHeaderColor() {
        layout_dialog_header.background = R.drawable.bg_setting_dialog_header.getDrawable()
        tv_dialog_title.setTextColor(Color.WHITE)
        btn_dialog_close.setColorFilter(Color.WHITE)
    }

    override fun onDestroy() {
        viewModel.clearDisposables()
        compositeDisposable.clear()
        super.onDestroy()
    }
}