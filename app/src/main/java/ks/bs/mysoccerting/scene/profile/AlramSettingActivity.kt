package ks.bs.mysoccerting.scene.profile

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import kotlinx.android.synthetic.main.activity_privacy_policy.*
import ks.bs.mysoccerting.BR
import ks.bs.mysoccerting.R
import ks.bs.mysoccerting.databinding.ActivityAlramSettingBinding
import ks.bs.mysoccerting.databinding.ActivityPrivacyPolicyBinding

class AlramSettingActivity : AppCompatActivity() {

    val viewModel = createVm()

    private fun createVm() = AlaramSettingVm()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        DataBindingUtil.setContentView<ActivityAlramSettingBinding>(this, R.layout.activity_alram_setting)
            .setVariable(BR.vm, viewModel)

        toolbar_title.text = "알람설정"
    }
}
