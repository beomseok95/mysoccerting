package ks.bs.mysoccerting.scene.club.detail.chat

import ks.bs.mysoccerting.model.ModelChat
import ks.bs.mysoccerting.scene.login.LoginManager
import ks.bs.mysoccerting.util.DateEx
import ks.bs.mysoccerting.util.wrapWith

class ChatItemVm(val model: ModelChat, private val isMatchUp: Boolean) {

    val content get() = model.content
    val nickname get() = if (isMatchUp) "${model.nickName} ${model.clubName?.wrapWith("[", "]")}" else model.nickName
    val profileImageUrl get() = model.profileImageUrl
    val timeStamp get() = model.timeStamp
    val isSelf get() = model.uid == LoginManager.user?.uid

    val chatTime get() = DateEx.convertRecentDate(model.timeStamp)
}