package ks.bs.mysoccerting.scene.club.detail.setting


import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import ks.bs.mysoccerting.BR
import ks.bs.mysoccerting.R
import ks.bs.mysoccerting.customview.dialog.TDialog
import ks.bs.mysoccerting.rx.rxresult.getObject
import ks.bs.mysoccerting.rx.rxresult.putObject
import ks.bs.mysoccerting.rx.showProgressDialog
import ks.bs.mysoccerting.scene.base.FragmentBase
import ks.bs.mysoccerting.scene.base.FragmentBaseViewModel
import ks.bs.mysoccerting.scene.club.clubtour.ClubDetailViewPagerInterface


class ClubSettingFragment : FragmentBase() {

    var viewModel = createVm()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            viewModel.contentId = it.getObject()
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val binding =
            DataBindingUtil.inflate<ViewDataBinding>(inflater, R.layout.fragment_club_setting, container, false)

        binding.setVariable(BR.viewModel, viewModel)

        viewModel.init()

        return binding.root
    }

    private fun createVm() = ClubSettingViewModel(object :
        ClubSettingViewModel.Contract {
        override fun showDialog(title: String): TDialog.Builder {
            requireNotNull(activity?.baseContext)
            return TDialog.Builder(activity?.baseContext!!, title)
        }

        override fun finish() {
            activity?.finish()
        }

        override fun moveClubBase() {
            listener?.moveClubBase()
        }

        override fun createDialogForProgress(request: FragmentBaseViewModel.ProgressBarOwner.Request): Dialog {
            return showProgressDialog(activity, request.disposable, request.functionOnCancel)
        }

        override fun moveBrowse() {
            listener?.moveBrowse()
        }
    })

    override fun onDetach() {
        viewModel.onDetach()
        super.onDetach()
    }


    companion object {

        var listener: ClubDetailViewPagerInterface? = null

        fun newInstace(contentId: String, viewpagerListener: ClubDetailViewPagerInterface) =
            ClubSettingFragment()
                .apply {

                    listener = viewpagerListener

                    arguments = Bundle().apply {
                        putObject(contentId)
                    }
                }

    }

}
