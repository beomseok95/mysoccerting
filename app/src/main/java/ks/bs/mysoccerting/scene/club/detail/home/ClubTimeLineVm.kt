package ks.bs.mysoccerting.scene.club.detail.home

import android.view.View
import ks.bs.mysoccerting.firebase.RxRealTimeDB
import ks.bs.mysoccerting.model.ModelClubBoard
import ks.bs.mysoccerting.model.ModelClubTimeline
import ks.bs.mysoccerting.model.type.TimelineType
import ks.bs.mysoccerting.rx.rxresult.putObject
import ks.bs.mysoccerting.rx.rxresult.startActivity
import ks.bs.mysoccerting.rx.rxresult.toIntent
import ks.bs.mysoccerting.scene.club.detail.board.DetailBoardActivity
import ks.bs.mysoccerting.scene.profile.ProfileHomeActivity
import ks.bs.mysoccerting.util.DateEx

class ClubTimeLineVm(val model: ModelClubTimeline, val contract: ClubDetailHomeViewModel.Contract) {
    val writeTime get() = DateEx.convertRecentDate(model.timestamp!!)
    val summary get() = "${model.nickName} ${model.type.postText}"

    fun showProfile(view: View) {
        ProfileHomeActivity::class.java
            .toIntent()
            .putObject(model.uid)
            .startActivity(view.context)
            .subscribe()
    }


    fun movePost(view: View) {
        if (model.type == TimelineType.POST)
            RxRealTimeDB.toSingle(model.contentPath!!) { ModelClubBoard() }
                .flatMapCompletable { model ->
                    DetailBoardActivity::class.java
                        .toIntent()
                        .putObject(model)
                        .startActivity(view.context)
                }
                .subscribe()
    }
}