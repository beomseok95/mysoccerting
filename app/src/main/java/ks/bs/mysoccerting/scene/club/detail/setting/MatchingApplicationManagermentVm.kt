package ks.bs.mysoccerting.scene.club.detail.setting

import android.view.View
import ks.bs.mysoccerting.BR
import ks.bs.mysoccerting.R
import ks.bs.mysoccerting.databinding.BaseRecyclerViewAdapter
import ks.bs.mysoccerting.databinding.ItemMatchingApplyManagementBinding
import ks.bs.mysoccerting.firebase.CrashlyticsHelper
import ks.bs.mysoccerting.firebase.FirebaseUrls
import ks.bs.mysoccerting.firebase.RxRealTimeDB
import ks.bs.mysoccerting.model.ModelClub
import ks.bs.mysoccerting.scene.base.ActivityBaseViewModel
import ks.bs.mysoccerting.scene.login.LoginManager
import ks.bs.mysoccerting.util.TLog

class MatchingApplicationManagermentVm : ActivityBaseViewModel() {

    fun backPressed(view: View) {
        val activity = view.context as MatchingApplicationManagementActivity
        activity.onBackPressed()
    }

    val adapter =
        BaseRecyclerViewAdapter<MatchingApplyManagementItemVm, ItemMatchingApplyManagementBinding>(
            R.layout.item_matching_apply_management,
            BR.viewModel
        )


    fun init() {
        val path = "${FirebaseUrls.club}/${LoginManager.user?.clubUid}/matchApplicationList"
        RxRealTimeDB.toObservable(path) { mutableListOf(ModelClub.MatchApplicationInfo()) }
            .subscribe({
                adapter.clear()

                it?.forEach {
                    if (!it.senderClubUid.isNullOrEmpty() && !it.reciverUid.isNullOrEmpty() && !it.senderUid.isNullOrEmpty())
                        adapter.insert(MatchingApplyManagementItemVm(it))
                }
                adapter.items.sortByDescending { it.timeStamp }
            }, { e ->
                CrashlyticsHelper.logException(e)
            })
    }
}