package ks.bs.mysoccerting.scene.profile

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import kotlinx.android.synthetic.main.activity_privacy_policy.*
import ks.bs.mysoccerting.BR
import ks.bs.mysoccerting.R
import ks.bs.mysoccerting.databinding.ActivityPrivacyPolicyBinding

class PrivacyPolicyActivity : AppCompatActivity() {

    val viewModel = createVm()

    private fun createVm() = PrivacyPolicyVm()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        DataBindingUtil.setContentView<ActivityPrivacyPolicyBinding>(this, R.layout.activity_privacy_policy)
            .setVariable(BR.vm, viewModel)

        toolbar_title.text ="개인정보처리방침"
    }
}
