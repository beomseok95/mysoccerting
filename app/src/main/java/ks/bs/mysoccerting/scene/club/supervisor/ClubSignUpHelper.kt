package ks.bs.mysoccerting.scene.club.supervisor

import android.view.View
import io.reactivex.Single
import io.reactivex.rxkotlin.zipWith
import ks.bs.mysoccerting.R
import ks.bs.mysoccerting.customview.dialog.DialogButton
import ks.bs.mysoccerting.customview.dialog.DialogObservable
import ks.bs.mysoccerting.customview.dialog.TDialog
import ks.bs.mysoccerting.customview.dialog.TextContentParam
import ks.bs.mysoccerting.exception.ErrorCode
import ks.bs.mysoccerting.fcm.FcmHelper
import ks.bs.mysoccerting.firebase.CrashlyticsHelper
import ks.bs.mysoccerting.firebase.FirebaseUrls
import ks.bs.mysoccerting.firebase.RxRealTimeDB
import ks.bs.mysoccerting.model.ModelAlaram
import ks.bs.mysoccerting.model.ModelClub
import ks.bs.mysoccerting.model.ModelUser
import ks.bs.mysoccerting.model.time.LocalTime
import ks.bs.mysoccerting.model.type.AlarmType
import ks.bs.mysoccerting.model.type.ClubGradeType
import ks.bs.mysoccerting.model.type.TimelineType
import ks.bs.mysoccerting.rx.withProgressBallAnimation
import ks.bs.mysoccerting.rx.withProgressBallDialog
import ks.bs.mysoccerting.scene.club.detail.ClubDetailFragment
import ks.bs.mysoccerting.scene.login.LoginManager
import ks.bs.mysoccerting.support.DontCare
import ks.bs.mysoccerting.util.TLog
import ks.bs.mysoccerting.util.getString
import ks.bs.mysoccerting.util.wrapWith

object ClubSignUpHelper {

    fun clubSignUpProcess(
        view: View, modelAlaram: ModelAlaram
    ) {
        requireNotNull(modelAlaram.senderUid) { "ClubSignUpHelper.clubSignUpProcess senderUid need to Not null" }
        requireNotNull(modelAlaram.reciverUid) { "ClubSignUpHelper.clubSignUpProcess reciverUid need to Not null" }

        var clubModel: ModelClub
        var userModel: ModelUser
        RxRealTimeDB.toSingle("${FirebaseUrls.users}/${modelAlaram.senderUid}") { ModelUser() }
            .zipWith(RxRealTimeDB.toSingle("${FirebaseUrls.club}/${LoginManager.user?.clubUid}") { ModelClub() })
            .flatMap { response ->

                require(response.second.contentId.isNotEmpty()) { "ClubSignUpHelper.clubSignUpProcess club info need Not null!!" }
                require(
                    response.first.uid?.isNotEmpty() ?: false
                ) { "ClubSignUpHelper.clubSignUpProcess user info need Not null!!" }
                clubModel = response.second
                userModel = response.first

                when {
                    response.first.clubUid != null ->
                        TDialog.Builder(view.context)
                            .setMessage("이미 클럽에 가입되어 있습니다.")
                            .isColored(true)
                            .addButton(DialogButton.OK())
                            .toSingle()
                            .flatMap { removeAlaram(modelAlaram) }
                            .removeMemberList(clubModel, userModel)


                    mercenaryCheck(response) ->
                        TDialog.Builder(view.context)
                            .setMessage("이미 클럽에 용병으로 등록되어있습니다.")
                            .isColored(true)
                            .addButton(DialogButton.OK())
                            .toSingle()
                            .flatMap { removeAlaram(modelAlaram) }
                            .removeMemberList(clubModel, userModel)

                    else -> {
                        clubModel = response.second
                        userModel =
                            response.first.apply {
                                clubUid = response.second.contentId
                                clubJonined = true
                                clubImageUrl = response.second.clubMarkImageUrl
                                club = response.second.clubName
                                clubMasterUid = response.second.masterUid
                            }

                        RxRealTimeDB.push("${FirebaseUrls.users}/${modelAlaram.senderUid}", userModel)
                            .flatMap { removeAlaram(modelAlaram) }
                            .map {
                                clubModel.membershipApplicationList?.removeAll { it.uid == userModel.uid }

                                clubModel.clubMemberNumber = clubModel.clubMemberNumber + 1
                                clubModel.clubMembers.add(
                                    ModelClub.ModelMember(
                                        nickName = userModel.nickName ?: "",
                                        uid = userModel.uid!!,
                                        memberImageUrl = userModel.profileImageUrl ?: "",
                                        position = userModel.position?.joinToString("  "),
                                        age = userModel.age.toString(),
                                        gradeType = ClubGradeType.PLAYER.name,
                                        mercenary = false
                                    )
                                )
                                clubModel
                            }
                            .flatMap { model ->
                                RxRealTimeDB.push("${FirebaseUrls.club}/${model.contentId}", model)
                            }
                            .flatMap {
                                RxRealTimeDB.toSingle("${FirebaseUrls.alram}/${modelAlaram.senderUid}") { mutableListOf<ModelAlaram>() }
                            }
                            .flatMap { alaramList ->
                                val alarmData = ModelAlaram(
                                    senderEmail = LoginManager.user?.email,
                                    senderImageUrl = clubModel.clubMarkImageUrl,
                                    timeStamp = LocalTime().time,
                                    senderUid = LoginManager.user?.uid!!,
                                    senderNickName = LoginManager.user?.nickName,
                                    reciverUid = modelAlaram.senderUid,
                                    alaramTypeCode = AlarmType.CLUB_SIGNUP_COMPLETE.code,
                                    message = "${clubModel.clubName.wrapWith(
                                        "[",
                                        "]"
                                    )} ${AlarmType.CLUB_SIGNUP_COMPLETE.text}",
                                    postPullPath = "${FirebaseUrls.club}/${clubModel.contentId}"
                                )
                                alaramList.add(alarmData)

                                RxRealTimeDB.push("${FirebaseUrls.alram}/${modelAlaram.senderUid}", alaramList)
                            }
                            .flatMap {
                                FcmHelper.sendMessage(
                                    destinationUid = modelAlaram.senderUid,
                                    message = "${clubModel.clubName.wrapWith(
                                        "[",
                                        "]"
                                    )} ${AlarmType.CLUB_SIGNUP_COMPLETE.text}"
                                )
                            }
                            .flatMap {
                                val contentId = RxRealTimeDB.generateRandomKey("timeline")
                                val timelinePath =
                                    "${FirebaseUrls.club}/${LoginManager.user?.clubUid}/${FirebaseUrls.timeLine}/$contentId"

                                RxRealTimeDB.push(
                                    timelinePath, ClubManager.createTimeLinePost(
                                        documentId = contentId,
                                        type = TimelineType.APPROVE_SIGN_UP,
                                        model = modelAlaram
                                    )
                                )
                            }
                            .flatMap {
                                TDialog.Builder(view.context)
                                    .setMessage("클럽가입이 수락되었습니다.")
                                    .isColored(true)
                                    .addButton(DialogButton.OK())
                                    .toSingle()
                            }
                    }
                }
            }
            .withProgressBallDialog(view.context)
            .subscribe({
                TLog.d("user info update \nalaram and club membership list remove complete")
            }, { e ->
                CrashlyticsHelper.logException(e)
            })
            .let { }
    }

    private fun removeAlaram(modelAlaram: ModelAlaram): Single<DontCare> {
        val path = "${FirebaseUrls.alram}/${modelAlaram.reciverUid}"
        return RxRealTimeDB.toSingle(path) { mutableListOf<ModelAlaram>() }
            .map { response ->
                response.removeAll { it.reciverUid == modelAlaram.reciverUid && it.timeStamp == modelAlaram.timeStamp }
                response
            }
            .flatMap { model -> RxRealTimeDB.push(path, model) }
    }

    private fun mercenaryCheck(response: Pair<ModelUser, ModelClub>) =
        response.first.mercenaryClubInfo?.any { it.contentId == response.second.contentId } == true

    fun reject(view: View, modelAlaram: ModelAlaram) {
        requireNotNull(modelAlaram.senderUid) { "ClubSignUpHelper.reject senderUid need to Not null" }
        requireNotNull(modelAlaram.reciverUid) { "ClubSignUpHelper.reject reciverUid need to Not null" }

        val path = "${FirebaseUrls.alram}/${modelAlaram.reciverUid}"
        RxRealTimeDB.toSingle(path) { mutableListOf<ModelAlaram>() }
            .map { response ->
                response.removeAll { it.reciverUid == modelAlaram.reciverUid && it.timeStamp == modelAlaram.timeStamp }
                response
            }
            .flatMap { model -> RxRealTimeDB.push(path, model) }
            .flatMap {
                RxRealTimeDB.toSingle("${FirebaseUrls.alram}/${modelAlaram.senderUid}") { mutableListOf<ModelAlaram>() }
                    .zipWith(RxRealTimeDB.toSingle("${FirebaseUrls.club}/${LoginManager.user?.clubUid}") { ModelClub() })
            }
            .flatMap { response ->
                val alarmData = ModelAlaram(
                    senderEmail = LoginManager.user?.email,
                    senderImageUrl = response.second.clubMarkImageUrl,
                    timeStamp = LocalTime().time,
                    senderUid = LoginManager.user?.uid!!,
                    senderNickName = LoginManager.user?.nickName,
                    reciverUid = modelAlaram.senderUid,
                    alaramTypeCode = AlarmType.CLUB_SIGNUP_REJECT.code,
                    message = "${response.second.clubName.wrapWith(
                        "[",
                        "]"
                    )} ${AlarmType.CLUB_SIGNUP_REJECT.text}"
                )

                response.first.add(alarmData)

                response.second.membershipApplicationList?.removeAll { it.uid == modelAlaram.senderUid }

                RxRealTimeDB.push("${FirebaseUrls.club}/${response.second.contentId}", response.second)
                    .flatMap {
                        RxRealTimeDB.push("${FirebaseUrls.alram}/${modelAlaram.senderUid}", response.first)
                    }
                    .flatMap {
                        FcmHelper.sendMessage(
                            destinationUid = modelAlaram.senderUid,
                            message = "${response.second.clubName.wrapWith(
                                "[",
                                "]"
                            )} ${AlarmType.CLUB_SIGNUP_REJECT.text}"
                        )
                    }
            }
            .withProgressBallAnimation(view.context)
            .flatMap {
                TDialog.Builder(view.context)
                    .setMessage("클럽가입이 거절되었습니다.")
                    .isColored(true)
                    .addButton(DialogButton.OK())
                    .toSingle()
            }
            .subscribe({
                TLog.d("reject Success !!")
            }, { e ->
                CrashlyticsHelper.logException(e)
            })
            .let { }
    }
}

fun Single<DontCare>.removeMemberList(clubModel: ModelClub, userModel: ModelUser): Single<DontCare> {
    return this.map {
        clubModel.membershipApplicationList?.removeAll { it.uid == userModel.uid }
        clubModel
    }
        .flatMap { model -> RxRealTimeDB.push("${FirebaseUrls.club}/${model.contentId}", model) }
}

fun Single<ModelClub>.applyForClubProccess(
    view: View,
    modelClub: ModelClub
): Single<DialogObservable.Result<TextContentParam>> {
    return this.flatMap { club ->
        val model: ModelClub

        when {
            areadyClubExist() ->
                TDialog.Builder(view.context, R.string.notice.getString())
                    .setMessage("이미 다른 클럽에 가입되어 있습니다.")
                    .isColored(true)
                    .addButton(DialogButton.OK())
                    .toSingle()

            areadyAppied(club) ->
                TDialog.Builder(view.context, R.string.notice.getString())
                    .setMessage("이미 클럽에 가입신청 하였습니다.")
                    .isColored(true)
                    .addButton(DialogButton.OK())
                    .toSingle()

            duplicateMecenaryCheckWithCurrentUser(club) -> {
                model = club
                require(model.contentId.isNotEmpty()) { ErrorCode.NO_EXIST_CLUB }
                requireNotNull(LoginManager.user) { ErrorCode.NO_CURRENT_LOGIN_USER }
                requireNotNull(LoginManager.user?.uid) { ErrorCode.NO_CURRENT_LOGIN_USER }
                require(ClubDetailFragment.CLUB_UID.isNotEmpty()) { ErrorCode.NO_EXIST_CLUB }

                modelClub.membershipApplicationList?.add(LoginManager.user!!)

                RxRealTimeDB.push("${FirebaseUrls.club}/${ClubDetailFragment.CLUB_UID}", model)
                    .flatMap {
                        RxRealTimeDB.toSingle("${FirebaseUrls.alram}/${model.masterUid}") { mutableListOf<ModelAlaram>() }
                            .zipWith(Single.just(model))
                    }
                    .flatMap { response ->
                        val alarmData = ModelAlaram(
                            senderEmail = LoginManager.user?.email,
                            senderImageUrl = LoginManager.user?.profileImageUrl,
                            timeStamp = LocalTime().time,
                            senderUid = LoginManager.user?.uid!!,
                            senderNickName = LoginManager.user?.nickName,
                            reciverUid = response.second.masterUid,
                            alaramTypeCode = AlarmType.CLUB_SIGNUP.code,
                            message = "${LoginManager.user?.nickName} ${AlarmType.CLUB_SIGNUP.text}"
                        )

                        response.first.add(alarmData)

                        if (response.second.membershipApplicationList == null)
                            response.second.membershipApplicationList = mutableListOf(LoginManager.user!!)
                        else
                            response.second.membershipApplicationList?.add(LoginManager.user!!)

                        RxRealTimeDB.push("${FirebaseUrls.alram}/${response.second.masterUid}", response.first)
                            .flatMap {
                                RxRealTimeDB.push(
                                    "${FirebaseUrls.club}/${response.second.contentId}/membershipApplicationList",
                                    response.second.membershipApplicationList
                                )
                            }
                            .flatMap { Single.just(response.second) }
                    }
                    .flatMap { response ->

                        FcmHelper.sendMessage(
                            destinationUid = response.masterUid,
                            message = "${LoginManager.user?.nickName} ${AlarmType.CLUB_SIGNUP.text}"
                        )
                    }
                    .flatMap {
                        TDialog.Builder(view.context, R.string.notice.getString())
                            .setMessage("클럽가입신청에 성공했습니다.")
                            .isColored(true)
                            .addButton(DialogButton.OK())
                            .toSingle()
                    }
            }

            else ->
                TDialog.Builder(view.context, R.string.notice.getString())
                    .setMessage("용병에 가입되어있으면 신청할 수 없습니다.")
                    .isColored(true)
                    .addButton(DialogButton.OK())
                    .toSingle()
        }
    }
}

private fun areadyAppied(club: ModelClub): Boolean {
    return club.membershipApplicationList?.any { it.uid == LoginManager.user?.uid } ?: false
}
