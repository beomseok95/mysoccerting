package ks.bs.mysoccerting.scene.club.detail.setting

import android.app.Activity
import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import ks.bs.mysoccerting.BR
import ks.bs.mysoccerting.R
import ks.bs.mysoccerting.model.ModelUser
import ks.bs.mysoccerting.rx.showProgressDialog
import ks.bs.mysoccerting.scene.base.FragmentBaseViewModel

class ClubBottomSheetDialog(val model: ModelUser) : BottomSheetDialogFragment() {

    val viewModel = createViewModel()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val binding = DataBindingUtil.inflate<ks.bs.mysoccerting.databinding.FragmentClubBottomSheetDialogBinding>(
            inflater,
            R.layout.fragment_club_bottom_sheet_dialog,
            container,
            false
        )
        binding.setVariable(BR.viewModel, viewModel)

        return binding.root

    }

    private fun createViewModel() = ClubBottomSheetViewModel(object :
        ClubBottomSheetViewModel.Contract {
        override fun dismissBottomSheet() {
            dismiss()
        }

        override fun createDialogForProgress(request: FragmentBaseViewModel.ProgressBarOwner.Request): Dialog {
            return showProgressDialog(activity, request.disposable, request.functionOnCancel)
        }
    })


    override fun onAttach(activity: Activity) {
        super.onAttach(activity)
        viewModel.initContent(model)
    }

    override fun onDetach() {
        viewModel.clearDisposables()
        super.onDetach()
    }
}