package ks.bs.mysoccerting.scene.club.detail.setting

import android.view.View
import androidx.databinding.ObservableBoolean
import io.reactivex.rxkotlin.zipWith
import ks.bs.mysoccerting.customview.dialog.DialogButton
import ks.bs.mysoccerting.customview.dialog.TDialog
import ks.bs.mysoccerting.fcm.FcmHelper
import ks.bs.mysoccerting.firebase.CrashlyticsHelper
import ks.bs.mysoccerting.firebase.FirebaseUrls
import ks.bs.mysoccerting.firebase.RxRealTimeDB
import ks.bs.mysoccerting.model.ModelAlaram
import ks.bs.mysoccerting.model.ModelClub
import ks.bs.mysoccerting.model.ModelClubTimeline
import ks.bs.mysoccerting.model.ModelUser
import ks.bs.mysoccerting.model.time.LocalTime
import ks.bs.mysoccerting.model.type.AlarmType
import ks.bs.mysoccerting.model.type.TimelineType
import ks.bs.mysoccerting.rx.rxresult.putObject
import ks.bs.mysoccerting.rx.rxresult.startActivity
import ks.bs.mysoccerting.rx.rxresult.toIntent
import ks.bs.mysoccerting.rx.withProgressBallDialog
import ks.bs.mysoccerting.scene.club.supervisor.ClubSignUpHelper
import ks.bs.mysoccerting.scene.login.LoginManager
import ks.bs.mysoccerting.scene.profile.ProfileHomeActivity
import ks.bs.mysoccerting.util.TLog
import ks.bs.mysoccerting.util.convertStandardAge
import ks.bs.mysoccerting.util.wrapWith

class MembershipManagementItemVm(val model: ModelUser) {

    val profileImageUrl get() = model.profileImageUrl
    val nickName get() = "닉네임 : ${model.nickName}"

    val ageVisibility = ObservableBoolean(model.age != null)
    val age get() = "나이대 : ${model.age?.convertStandardAge()}"
    val positionList get() = model.position ?: emptyList()
    val positionVisibility = ObservableBoolean(model.position?.isNotEmpty() ?: false)

    fun showProfile(view: View) {
        ProfileHomeActivity::class.java
            .toIntent()
            .putObject(model.uid!!)
            .startActivity(view.context)
            .subscribe()
    }

    fun acceptBtnClkEvent(view: View) {
        val path = "${FirebaseUrls.alram}/${LoginManager.user?.clubMasterUid}"
        RxRealTimeDB.toSingle(path) { mutableListOf<ModelAlaram>() }
            .toObservable()
            .flatMapIterable { it }
            .filter { it.senderUid == model.uid }
            .take(1)
            .subscribe({ modelAlaram ->
                ClubSignUpHelper.clubSignUpProcess(view, modelAlaram)
            }, { e ->
                CrashlyticsHelper.logException(e)
            })
            .let { }
    }

    fun rejectBtnClkEvent(view: View) {
        val path = "${FirebaseUrls.alram}/${LoginManager.user?.clubMasterUid}"
        RxRealTimeDB.toSingle(path) { mutableListOf<ModelAlaram>() }
            .toObservable()
            .flatMapIterable { it }
            .filter { it.senderUid == model.uid }
            .take(1)
            .subscribe({ modelAlaram ->
                ClubSignUpHelper.reject(view, modelAlaram)
            }, { e ->
                CrashlyticsHelper.logException(e)
            })
            .let { }
    }

    fun showBottomSheet(view: View) {
        val activity = view.context as MembershipManagementActivity
        ClubBottomSheetDialog(model)
            .show(activity.supportFragmentManager, "")
    }

}