package ks.bs.mysoccerting.scene.login

import android.content.Intent
import androidx.databinding.DataBindingUtil
import android.os.Bundle
import com.google.firebase.auth.FirebaseUser
import kotlinx.android.synthetic.main.activity_email_login.*
import kr.nextm.lib.TToast
import ks.bs.mysoccerting.R
import ks.bs.mysoccerting.databinding.ActivityEmailLoginBinding
import ks.bs.mysoccerting.scene.base.ActivityBase
import ks.bs.mysoccerting.scene.home.MainActivity
import org.jetbrains.anko.startActivity

class EmailLoginActivity : ActivityBase(), EmailLoginViewModel.EmailLoginActivityContract {
    val model = EmailLoginViewModel(this)
    override fun onCreate(savedInstanceState: Bundle?) {
        isLoginActivity = true
        super.onCreate(savedInstanceState)
        val binding = DataBindingUtil.setContentView<ActivityEmailLoginBinding>(this, R.layout.activity_email_login)
        binding.emailLoginViewModel = model
        model.init()

        toolbar_title.text = "이메일 로그인"
    }


    override fun showToast(message: String) {
        TToast.show(message)
    }

    override fun moveFindPasswordPage() {
        startActivity<FindPasswordActivity>()
    }

    override fun moveSignUpPage() {
        startActivity<SignUpEmailActivity>()
    }

    override fun moveMainPage(user: FirebaseUser?) {
        if (user != null) {
//            TToast.show("${getString(R.string.signin_complete)} \n 로그인정보 ${user.email}")
            val intent = Intent(this, MainActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
            startActivity(intent)

            finish()
        }
    }

    override fun onDestroy() {
        model.onDestroy()
        super.onDestroy()
    }
}
