package ks.bs.mysoccerting.scene.club.detail.home

import android.content.Context
import android.graphics.Bitmap
import android.os.Bundle
import android.view.LayoutInflater
import android.widget.FrameLayout
import androidx.databinding.DataBindingUtil
import com.bumptech.glide.Glide
import com.bumptech.glide.request.target.SimpleTarget
import com.bumptech.glide.request.transition.Transition
import kotlinx.android.synthetic.main.activity_club_position.*
import kotlinx.android.synthetic.main.item_club_position.view.*
import ks.bs.mysoccerting.R
import ks.bs.mysoccerting.databinding.ActivityClubPositionBinding
import ks.bs.mysoccerting.model.ModelClub
import ks.bs.mysoccerting.rx.rxresult.RxActivityBase
import ks.bs.mysoccerting.rx.rxresult.getObject


class ClubPositionActivity : RxActivityBase() {

    private val viewModel = createVm()

    private fun createVm() = ClubPositionViewModel(object :
        ClubPositionViewModel.ClubPositionContract {
        override fun backPressEvent() {
            finishWithCancel()
        }

        override fun onSave(positionList: HashMap<String, String>) {
            finishWithResponse(positionList)
        }


    })

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.modelClub = intent.getObject()!!
        viewModel.init()
        DataBindingUtil.setContentView<ActivityClubPositionBinding>(this, R.layout.activity_club_position)
            .let {
                it.viewModel = viewModel
            }
        viewModel.containerId = container.id
        viewModel.memberListId = memberList.id

        setPositionList()
    }

    fun setPositionList() {
        val layoutInflater = getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        viewModel.modelClub.clubPositionList?.forEach { position ->
            val layout = container.findViewWithTag(position.key) as FrameLayout
            var view = layoutInflater.inflate(R.layout.item_club_position, layout, true) as FrameLayout
            val memberList = mutableListOf<ModelClub.ModelMember>()
            memberList.addAll(viewModel.modelClub.clubMembers)
            if (viewModel.modelClub.mercenaryMember != null) {
                memberList.addAll(viewModel.modelClub.mercenaryMember!!)
            }
            val member = memberList.find { member ->
                member.nickName == position.value
            }
            val constraint = view.getChildAt(1)
            constraint.memberName.text = member?.nickName
            if (!member?.memberImageUrl.isNullOrEmpty()) {
                Glide.with(applicationContext).asBitmap().load(member?.memberImageUrl)
                    .into(object : SimpleTarget<Bitmap>() {
                        override fun onResourceReady(resource: Bitmap, transition: Transition<in Bitmap>?) {
                            constraint.circleImageView.setImageBitmap(resource)
                        }
                    })
            } else {
                constraint.circleImageView.setBackgroundResource(R.drawable.ic_club_mark_default)
            }
            constraint.tag = member?.nickName
            constraint.setOnTouchListener(viewModel.touchListener)
        }

    }

    override fun onDestroy() {
        viewModel.onDestroy()
        super.onDestroy()
    }

    override fun onBackPressed() {
        finishWithResponse("")
    }
}