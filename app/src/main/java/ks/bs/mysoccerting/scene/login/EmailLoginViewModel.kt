package ks.bs.mysoccerting.scene.login

import androidx.databinding.Observable
import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import androidx.databinding.ObservableInt
import android.text.TextUtils
import android.view.View
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import ks.bs.mysoccerting.exception.ErrorCode
import ks.bs.mysoccerting.scene.base.ActivityBaseViewModel
import ks.bs.mysoccerting.util.TLog

class EmailLoginViewModel(private val contract: EmailLoginActivityContract) :
    ActivityBaseViewModel() {

    interface EmailLoginActivityContract {
        fun moveMainPage(user: FirebaseUser?)
        fun showToast(message: String)
        fun moveFindPasswordPage()
        fun moveSignUpPage()
    }

    var email = ObservableField<String>()
    var password = ObservableField<String>()
    var isValid = ObservableBoolean(false)
    var buttonColor = ObservableBoolean(false)
    var progressVisibility = ObservableInt(View.GONE)

    fun init() {
        email.addOnPropertyChangedCallback(object : Observable.OnPropertyChangedCallback() {
            override fun onPropertyChanged(sender: Observable?, propertyId: Int) {
                validation()
            }
        })
        password.addOnPropertyChangedCallback(object : Observable.OnPropertyChangedCallback() {
            override fun onPropertyChanged(sender: Observable?, propertyId: Int) {
                validation()
            }
        })
    }

    fun validation() {
        val isValideEmail = !TextUtils.isEmpty(email.get())
        val isValidePassword = !TextUtils.isEmpty(password.get())
        if (isValideEmail && isValidePassword) {
            isValid.set(true)
            buttonColor.set(true)
            return
        }
        isValid.set(false)
        buttonColor.set(false)
    }

    fun loginButtonClickEvent() {
        progressVisibility.set(View.VISIBLE)
        auth.signInWithEmailAndPassword(email.get().toString(), password.get().toString())
            .addOnCompleteListener {
                if (it.isSuccessful) {
                    progressVisibility.set(View.GONE)
                    contract.moveMainPage(auth.currentUser)//로그인 성공 및 다음 페이지 호출
                } else {
                    val e = it.exception!!.message.toString()
                    TLog.e(e)
                    when (e) {
                        ErrorCode.INVALID_OR_NO_PASSWORD.code -> contract.showToast(ErrorCode.INVALID_OR_NO_PASSWORD.message)
                        ErrorCode.NO_USER_EMAIL.code -> contract.showToast(ErrorCode.NO_USER_EMAIL.message)
                        ErrorCode.EMAIL_FORMAT_WRONG.code -> contract.showToast(ErrorCode.EMAIL_FORMAT_WRONG.message)
                        else -> contract.showToast(e)
                    }
                    progressVisibility.set(View.GONE)
                }
            }
    }

    fun findPasswordClickEvent() {
        contract.moveFindPasswordPage()
    }

    fun moveSignUpPageClickEvent() {
        contract.moveSignUpPage()
    }

    fun backPressed(view: View) {
        val activity = view.context as EmailLoginActivity
        activity.onBackPressed()
    }
}