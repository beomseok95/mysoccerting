package ks.bs.mysoccerting.scene.search


import io.reactivex.rxkotlin.addTo
import ks.bs.mysoccerting.firebase.CrashlyticsHelper
import ks.bs.mysoccerting.model.ModelClubSearch
import ks.bs.mysoccerting.util.TLog

class SearchClubViewModel(val model: ModelClubSearch, val contract: SearchViewModel.SearchFragmentContract) {

    val clubImageUrl get() = model.clubImageUrl
    val clubName get() = model.clubName ?: ""
    val hopeDate get() = "용병 구인 희망일 : ${model.hopeDate }"
    val content get() = model.content
    val uid get() = model.uid
    val commentcount get() = model.commentcount
    val hopeArea get() = model.hopeArea
    val contentId get() = model.contentId
    val timestamp get() = model.timestamp
    val position get() = model.position
    val positionText get() = "구인 포지션"
    val age = ""

    fun moveItemDetailPageClub() {
        contract.moveItemDetailPageClub(model)
            .subscribe({}, { CrashlyticsHelper.logException(it) })
            .addTo(contract.getDisposable())
    }
}