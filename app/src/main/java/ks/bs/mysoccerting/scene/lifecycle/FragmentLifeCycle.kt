package ks.bs.mysoccerting.scene.lifecycle

interface FragmentLifeCycle {
    fun onDetach()
}