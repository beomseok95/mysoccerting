package ks.bs.mysoccerting.scene.club.detail.home

import android.os.Bundle
import kotlinx.android.synthetic.main.activity_match_up.*
import ks.bs.mysoccerting.R
import ks.bs.mysoccerting.databinding.ActivityMatchUpBinding
import ks.bs.mysoccerting.scene.base.BindingActivity

class MatchUpActivity : BindingActivity<ActivityMatchUpBinding>() {

    override fun layoutRes(): Int = R.layout.activity_match_up

    override val viewModel = createVm()

    fun createVm() = MatchUpVm()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        toolbar_title.text = "매칭리스트"

        viewModel.init(getInput())
    }

    override fun onDestroy() {
        viewModel.clearDisposables()
        super.onDestroy()
    }
}
