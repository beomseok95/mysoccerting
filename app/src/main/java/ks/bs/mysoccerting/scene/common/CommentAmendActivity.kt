package ks.bs.mysoccerting.scene.common

import androidx.databinding.DataBindingUtil
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_comment_amend.*
import kotlinx.android.synthetic.main.activity_comment_amend.view.*
import kr.nextm.lib.TToast
import ks.bs.mysoccerting.BR
import ks.bs.mysoccerting.R
import ks.bs.mysoccerting.databinding.ActivityCommentAmendBinding
import ks.bs.mysoccerting.rx.rxresult.RxActivityBase

class CommentAmendActivity : RxActivityBase(),
    CommentAmendViewModel.CommentAdmendActivityContract {

    val viewModel = CommentAmendViewModel(this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding = DataBindingUtil.setContentView<ActivityCommentAmendBinding>(this, R.layout.activity_comment_amend)

        viewModel.init()

        toolbar_title.text = "댓글 수정"

        binding.setVariable(BR.viewModel, viewModel)

    }

    override fun sendResult(commentConetent: String) {
        finishWithResponse(commentConetent)
    }

    override fun showToast(message: String) {
        TToast.show(message)
    }

    override fun onDestroy() {
        viewModel.onDestroy()
        super.onDestroy()
    }
}
