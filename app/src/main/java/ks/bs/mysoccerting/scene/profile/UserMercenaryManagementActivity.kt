package ks.bs.mysoccerting.scene.profile

import android.os.Bundle
import androidx.databinding.DataBindingUtil
import kotlinx.android.synthetic.main.activity_user_mercenary_management.*
import ks.bs.mysoccerting.BR
import ks.bs.mysoccerting.R
import ks.bs.mysoccerting.databinding.ActivityUserMercenaryManagementBinding
import ks.bs.mysoccerting.model.ModelMoveTo
import ks.bs.mysoccerting.model.type.MovePageLocationType
import ks.bs.mysoccerting.rx.rxresult.RxActivityBase

class UserMercenaryManagementActivity : RxActivityBase() {

    val viewModel = createVm()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        DataBindingUtil.setContentView<ActivityUserMercenaryManagementBinding>(
            this,
            R.layout.activity_user_mercenary_management
        )
            .setVariable(BR.viewModel, viewModel)

        toolbar_title.text = "용병 참여중인 클럽"

        viewModel.init()
    }

    fun createVm() = UserMercenaryManagementVm(object : UserMercenaryManagementVm.Contract {
        override fun finishPageWithResponse(contentId: String) {
            finishWithResponse(
                ModelMoveTo(
                    locationcode = MovePageLocationType.CLUB_DETAIL.locationCode,
                    clubContentId = contentId
                )
            )
        }
    })
}
