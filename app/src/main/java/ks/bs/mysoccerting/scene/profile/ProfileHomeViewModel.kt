package ks.bs.mysoccerting.scene.profile

import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import io.reactivex.rxkotlin.addTo
import ks.bs.mysoccerting.firebase.FirebaseUrls
import ks.bs.mysoccerting.firebase.RxRealTimeDB
import ks.bs.mysoccerting.model.ModelUser
import ks.bs.mysoccerting.model.type.ChipType
import ks.bs.mysoccerting.rx.toJson
import ks.bs.mysoccerting.scene.base.ActivityBaseViewModel
import ks.bs.mysoccerting.util.TLog
import ks.bs.mysoccerting.util.convertStandardAge

class ProfileHomeViewModel(val contract: ProfileHomeContract) : ActivityBaseViewModel() {
    interface ProfileHomeContract {
        fun backpressed()
        fun addChips(tag: ChipType, text: String)
    }

    val nickName = ObservableField("닉네임")
    val profileImageUrl = ObservableField<String>()
    val clubName = ObservableField<String>()
    val clubImageUrl = ObservableField<String>()
    val isClubJoined = ObservableBoolean(false)
    val position = ObservableField<String>()
    val isPositionEnabled = ObservableBoolean(true)
    val area = ObservableField<String>()
    val isAreaEnabled = ObservableBoolean(true)
    val age = ObservableField<String>()
    val isAgeEnabled = ObservableBoolean(true)


    fun init(uid: String) {
        val path = "${FirebaseUrls.users}/$uid"
        RxRealTimeDB.toSingle(path) { ModelUser() }
            .subscribe({ response ->
                TLog.d(response.toJson())
                nickName.set(response.nickName)
                profileImageUrl.set(response.profileImageUrl)

                clubName.set(response.club)
                clubImageUrl.set(response.clubImageUrl)
                isClubJoined.set(response.clubJonined)

                response.position?.forEach { contract.addChips(ChipType.POSITION, it) }
                isPositionEnabled.set(response.position != null && response.position!!.isNotEmpty())

                if (response.area != null)
                    contract.addChips(ChipType.AREA, response.area!!)
                else
                    isAreaEnabled.set(false)

                if (response.age != null)
                    contract.addChips(ChipType.AGE, response.age.toString())
                else
                    isAgeEnabled.set(false)

            }, { e ->
                TLog.e(e)
            })
            .addTo(compositeDisposable)
    }

    fun onBackPressed() {
        contract.backpressed()
    }
}