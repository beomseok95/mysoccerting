package ks.bs.mysoccerting.scene.club.detail.home

import ks.bs.mysoccerting.model.ModelClub

class ClubMercenaryItemVm(val model: ModelClub.ModelMember) {
    val mercenaryImageUrl get() = model.memberImageUrl
}