package ks.bs.mysoccerting.scene.club.detail.setting

import androidx.databinding.ObservableField
import ks.bs.mysoccerting.BR
import ks.bs.mysoccerting.R
import ks.bs.mysoccerting.databinding.BaseRecyclerViewAdapter
import ks.bs.mysoccerting.databinding.ItemClubMemberListBinding
import ks.bs.mysoccerting.model.ModelClub
import ks.bs.mysoccerting.model.type.ClubGradeType
import ks.bs.mysoccerting.model.type.ClubGradeType.Companion.toGrade
import ks.bs.mysoccerting.scene.base.ActivityBaseViewModel
import ks.bs.mysoccerting.scene.club.detail.home.ClubPeopleItemVm
import ks.bs.mysoccerting.scene.club.detail.setting.ClubMembersInfoActivity.Companion.DELEGATED
import ks.bs.mysoccerting.util.TLog

class ClubMembersInfoViewModel(private val contract: Contract) : ActivityBaseViewModel(), ClubPeopleItemVm.Contract {
    interface Contract {
        fun backPressEvent()
        fun delegated(msg : String)
    }

    override fun delegated(isDelegated: Boolean, nickName: String) {
        if (isDelegated) {
            memberAdapter.items[0].model.gradeType = ClubGradeType.ASSISTANT.name
            memberAdapter.items.forEach {
                if (it.nickName == nickName)
                    it.model.gradeType= ClubGradeType.COACH.name
            }
            contract.delegated(DELEGATED)
        }
    }

    override fun refreshAdapter() {
        memberAdapter.items.sortByDescending {
            ClubGradeType.valueOf(toGrade(imageDrawable = it.gradeImage.get())).grade
        }
        memberAdapter.notifyDataSetChanged()
        mercenaryAdapter.notifyDataSetChanged()
    }

    override fun removeItem(isMercenary: Boolean, count: Int,nickName: String?) {

        if (isMercenary)
            mercenaryAdapter.remove(mercenaryAdapter.items[count])
        else
            memberAdapter.items.forEach { clubPeopleItemVm ->
                if(clubPeopleItemVm.model.nickName==nickName)
                    memberAdapter.remove(clubPeopleItemVm)
            }


    }

    var modelClub = ModelClub()
    var showMercenaryList = ObservableField<Boolean>()
    var item = mutableListOf<ModelClub.ModelMember>()

    val memberAdapter = BaseRecyclerViewAdapter<ClubPeopleItemVm, ItemClubMemberListBinding>(
        R.layout.item_club_member_list, BR.viewModel
    )
    val mercenaryAdapter = BaseRecyclerViewAdapter<ClubPeopleItemVm, ItemClubMemberListBinding>(
        R.layout.item_club_member_list, BR.viewModel
    )
    var title = ObservableField<String>()

    fun backPressBtnClkEvent() {
        contract.backPressEvent()
    }

    fun getContents(modelClub: ModelClub) {
        for (i in modelClub.clubMembers.indices) {
            memberAdapter.insert(ClubPeopleItemVm(modelClub.clubMembers[i], modelClub.contentId, i, true, this))
        }

        if (modelClub.mercenaryMember != null) {
            for (i in modelClub.mercenaryMember.indices) {
                TLog.d(modelClub.mercenaryMember[i].mercenary)
                mercenaryAdapter.insert(
                    ClubPeopleItemVm(
                        modelClub.mercenaryMember[i],
                        modelClub.contentId,
                        i,
                        true,
                        this
                    )
                )
            }
            showMercenaryList.set(true)
        }

        memberAdapter.items.sortByDescending {
            ClubGradeType.valueOf(toGrade(imageDrawable = it.gradeImage.get())).grade
        }
    }
}