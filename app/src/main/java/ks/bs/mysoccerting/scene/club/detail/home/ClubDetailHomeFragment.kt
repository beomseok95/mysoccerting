package ks.bs.mysoccerting.scene.club.detail.home


import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import ks.bs.mysoccerting.BR
import ks.bs.mysoccerting.R
import ks.bs.mysoccerting.rx.rxresult.getObject
import ks.bs.mysoccerting.rx.rxresult.putObject
import ks.bs.mysoccerting.rx.showProgressDialog
import ks.bs.mysoccerting.scene.base.FragmentBase
import ks.bs.mysoccerting.scene.base.FragmentBaseViewModel


class ClubDetailHomeFragment : FragmentBase() {

    var viewModel = createVm()

    var contentId = ""

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding =
            DataBindingUtil.inflate<ViewDataBinding>(
                inflater,
                R.layout.fragment_club_detail_home,
                container,
                false
            )
        binding.setVariable(BR.viewModel, viewModel)
        return binding.root
    }

    private fun createVm() = ClubDetailHomeViewModel(object :
        ClubDetailHomeViewModel.Contract {
        override fun createDialogForProgress(request: FragmentBaseViewModel.ProgressBarOwner.Request): Dialog {
            return showProgressDialog(activity, request.disposable, request.functionOnCancel)
        }
    })

    override fun onAttach(context: Context) {
        super.onAttach(context)

        arguments?.let {
            contentId = it.getObject()
        }

        viewModel.getContents(contentId)
    }

    override fun onDetach() {
        viewModel.onDetach()
        super.onDetach()
    }

    override fun onResume() {
        super.onResume()
        viewModel.checkUserInfo()
    }

    companion object {
        fun newInstace(contentId: String) =
            ClubDetailHomeFragment()
                .apply {
                    arguments = Bundle().apply {
                        putObject(contentId)
                    }
                }
    }
}
