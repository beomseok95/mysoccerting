package ks.bs.mysoccerting.scene.common

import android.content.Context
import android.text.TextUtils
import android.view.View
import androidx.databinding.Observable
import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import io.reactivex.Single
import io.reactivex.rxkotlin.addTo
import ks.bs.mysoccerting.firebase.CrashlyticsHelper
import ks.bs.mysoccerting.firebase.FirebaseUrls
import ks.bs.mysoccerting.firebase.RxRealTimeDB
import ks.bs.mysoccerting.model.*
import ks.bs.mysoccerting.model.time.LocalTime
import ks.bs.mysoccerting.model.type.PostType
import ks.bs.mysoccerting.scene.base.ActivityBaseViewModel
import ks.bs.mysoccerting.scene.filter.CalendarDialog
import ks.bs.mysoccerting.scene.filter.PositionDialog
import ks.bs.mysoccerting.scene.login.LoginManager
import ks.bs.mysoccerting.scene.match.MatchingFragment
import ks.bs.mysoccerting.scene.search.SearchFragment
import ks.bs.mysoccerting.util.TLog
import ks.bs.mysoccerting.util.convertStandardAge
import java.util.*
import kotlin.collections.HashMap

class PostAddViewModel(val contract: PostAddActivityContract) : ActivityBaseViewModel() {
    interface PostAddActivityContract {
        fun showToast(message: String)
        fun showPeopleNumberDialog(): Single<MutableList<String>>
        fun showAreaDialog(): Single<Area>
        fun showCalendarDialog(): Single<CalendarDialog.Date>
        fun backPressEvent()
        fun resetFlexBoxView()
        fun getContext(): Context

    }

    var positionList = HashMap<String, Boolean>()
    var title = ObservableField<String>()
    var content = ObservableField<String>()

    var cloudInfoViewTextList = ObservableField<MutableList<String>>(mutableListOf())
    var peopleNumberFilterList = mutableListOf<String>()
    var dateFilterList = mutableListOf<String>()
    var cityFilterList = mutableListOf<String>()
    var positionFilter = mutableListOf<String>()

    var isEmptyContent = true

    var positionVisibility = ObservableBoolean(true)
    var peopleNumberVisibility = ObservableBoolean(true)


    var calledPage: String = ""

    fun init(calledPage: String) {

        this.calledPage = calledPage

        when (calledPage) {
            MatchingFragment.CALL_TAG_MATCHING -> {
                positionVisibility.set(false)
                peopleNumberVisibility.set(true)
            }
            else -> {
                positionVisibility.set(true)
                peopleNumberVisibility.set(false)
            }
        }

        content.addOnPropertyChangedCallback(object : Observable.OnPropertyChangedCallback() {
            override fun onPropertyChanged(sender: Observable?, propertyId: Int) {
                isEmptyContent = TextUtils.isEmpty(content.get())
            }
        })
    }


    fun showAreaDialog() {
        contract.showAreaDialog()
            .subscribe({
                val city = it.selectedCity?.trim()
                val town = it.selectedTown?.trim()

                if (town == null && city == null) return@subscribe

                if (town == null) {
                    cityFilterList = mutableListOf("$city")
                    contract.resetFlexBoxView()
                    cloudInfoViewTextList.set((peopleNumberFilterList + dateFilterList + positionFilter + cityFilterList).toMutableList())
                    return@subscribe
                }

                cityFilterList.map { index ->
                    if (index.startsWith("$city")) {
                        cityFilterList.remove("$city")
                        contract.resetFlexBoxView()
                        cityFilterList.add("$city $town")
                        cloudInfoViewTextList.set((peopleNumberFilterList + dateFilterList + positionFilter + cityFilterList).toMutableList())
                        return@subscribe
                    }
                }

                cityFilterList = mutableListOf("$city $town")
                contract.resetFlexBoxView()
                cloudInfoViewTextList.set((peopleNumberFilterList + dateFilterList + positionFilter + cityFilterList).toMutableList())


            }, { e ->
                CrashlyticsHelper.logException(e)
            }).let { addDisposable(it) }
    }


    fun showCalendarDialog() {
        contract.showCalendarDialog()
            .subscribe({ date ->

                val firstDate = date.firstDate
                val lastDate = date.lastDate

                if (firstDate == null)
                    return@subscribe

                dateFilterList =
                    if (lastDate == null) {
                        mutableListOf("$firstDate")
                    } else {
                        mutableListOf("$firstDate-$lastDate")
                    }

                contract.resetFlexBoxView()
                cloudInfoViewTextList.set((peopleNumberFilterList + positionFilter + cityFilterList + dateFilterList).toMutableList())


            }, { e ->
                CrashlyticsHelper.logException(e)
            }).let { addDisposable(it) }
    }

    fun showPositionDialog(view: View) {
        PositionDialog()
            .toSingle(view.context)
            .subscribe({ result ->
                positionFilter = mutableListOf(result)
                contract.resetFlexBoxView()
                cloudInfoViewTextList.set((peopleNumberFilterList + cityFilterList + dateFilterList + positionFilter).toMutableList())
            }, { e ->
                CrashlyticsHelper.logException(e)
            })
            .addTo(compositeDisposable)
    }

    fun showPeopleNumberDialog() {
        contract.showPeopleNumberDialog().subscribe({ numberList ->
            if (numberList == null) return@subscribe

            sortPeopleNumberList(numberList)

            if (numberList.isEmpty()) return@subscribe


            peopleNumberFilterList = mutableListOf()

            if (numberList[0] == "모든인원") {
                peopleNumberFilterList =
                    mutableListOf(
                        "5 vs 5",
                        "6 vs 6",
                        "7 vs 7",
                        "8 vs 8",
                        "9 vs 9 ",
                        "10 vs 10",
                        "11 vs 11"
                    )
            } else {
                numberList.forEach { number ->
                    if (!peopleNumberFilterList.contains(number)) {
                        peopleNumberFilterList.add(number)
                    }
                }

            }

            contract.resetFlexBoxView()
            cloudInfoViewTextList.set((cityFilterList + dateFilterList + positionFilter + peopleNumberFilterList).toMutableList())
        }, { e ->
            CrashlyticsHelper.logException(e)
        }).let { addDisposable(it) }
    }

    private fun sortPeopleNumberList(numberList: MutableList<String>) {
        numberList.sortWith(Comparator { pos1, pos2 ->
            pos1.substring(0, 2).trim().toInt()
                .compareTo(pos2.substring(0, 2).trim().toInt())
        })
    }


    fun resetBtnClickEvent() {
        contract.resetFlexBoxView()
        content.set("")
        peopleNumberFilterList = mutableListOf()
        dateFilterList = mutableListOf()
        cityFilterList = mutableListOf()
        positionFilter = mutableListOf()

    }

    fun backPressBtnClkEvent() {
        contract.backPressEvent()
    }


    fun invalidateCheck(): Boolean {
        return when (calledPage) {
            MatchingFragment.CALL_TAG_MATCHING -> {
                peopleNumberFilterList.isEmpty() || cityFilterList.isEmpty() || dateFilterList.isEmpty() || isEmptyContent
            }
            else -> {
                positionFilter.isEmpty() || cityFilterList.isEmpty() || dateFilterList.isEmpty() || isEmptyContent
            }
        }
    }

    fun okBtnClkEvent() {
        if (invalidateCheck()) {
            contract.showToast("모든 내용을 입력해주세요.")

            return
        }

        val date: String = dateFilterList.joinToString("").trim()

        val dateTimeStamp = getSelectedDayToTimeStamp(date)

        val position = if (positionFilter.isNotEmpty()) positionFilter[0] else null

        val content = content.get().toString()

        var path = ""
        val document = RxRealTimeDB.generateRandomKey("post")

        val userInfoPath = "${FirebaseUrls.users}/${auth.currentUser?.uid}"

        RxRealTimeDB.toSingle(userInfoPath) { ModelUser() }
            .flatMap { data ->

                val requestModel =
                    when (calledPage) {
                        MatchingFragment.CALL_TAG_MATCHING -> {
                            path = FirebaseUrls.clubMatch

                            ModelClubMatch(
                                clubImageUrl = data.clubImageUrl,
                                clubName = data.club,
                                content = content,
                                timestamp = System.currentTimeMillis(),
                                uid = auth.currentUser!!.uid,
                                matcingArea = cityFilterList.joinToString(),
                                hopeDate = date,
                                matchingPeopleNumbewr = peopleNumberFilterList.joinToString(),
                                contentId = document,
                                clubUid = LoginManager.user?.clubUid!!,
                                matchHopeDateTimeStamp = dateTimeStamp
                            )
                        }
                        SearchFragment.CALL_TAG_SEARCH_MERCENARY -> {
                            path = FirebaseUrls.mercenarySeach

                            ModelMercenarySearch(
                                profileImageUrl = data.profileImageUrl,
                                nickName = data.nickName,
                                age = data.age?.convertStandardAge() ?: (LoginManager.user?.age?.convertStandardAge()
                                    ?: 10.convertStandardAge()),
                                hopeDate = date,
                                content = content,
                                timestamp = System.currentTimeMillis(),
                                position = position,
                                uid = auth.currentUser!!.uid,
                                hopeArea = cityFilterList.joinToString(),
                                contentId = document
                            )
                        }
                        else -> {
                            path = FirebaseUrls.clubSeach

                            ModelClubSearch(
                                clubImageUrl = data.clubImageUrl,
                                clubName = LoginManager.user?.club,
                                hopeDate = date,
                                content = content,
                                position = position,
                                timestamp = System.currentTimeMillis(),
                                uid = auth.currentUser?.uid ?: "",
                                hopeArea = cityFilterList.joinToString(),
                                contentId = document,
                                clubMasterUid = LoginManager?.user?.clubMasterUid!!
                            )
                        }
                    }

                Single.just(requestModel)
            }
            .pushMyPost()
            .flatMap { data ->
                val pushPath = "$path/$document"
                RxRealTimeDB.push(pushPath, data)
            }
            .subscribe({
                contract.showToast("게시글 작성완료!")
                contract.backPressEvent()
                TLog.d("게시글 작성 성공")
            }, { e ->
                contract.showToast("게시글작성이 실패했습니다.")
                TLog.e("게시글 작성 실패")
                CrashlyticsHelper.logException(e)

            }).addTo(compositeDisposable)
    }

    private fun Single<Any>.pushMyPost(): Single<Any> {
        return this.flatMap {
            val requestModel = when (calledPage) {
                MatchingFragment.CALL_TAG_MATCHING -> {
                    val model = it as ModelClubMatch
                    ModelMyPost(
                        content = model.content,
                        contentId= model.contentId,
                        type = PostType.CLUB_MATCH,
                        mPostPath = "${PostType.MY_POST.path}/${LoginManager.user?.uid}/${PostType.CLUB_MATCH.type}/${model.contentId}",
                        timestamp = model.timestamp
                    )
                }
                SearchFragment.CALL_TAG_SEARCH_MERCENARY -> {
                    val model = it as ModelMercenarySearch
                    ModelMyPost(
                        content = model.content,
                        contentId= model.contentId,
                        type = PostType.MERCENARY_SEARCH,
                        mPostPath = "${PostType.MY_POST.path}/${LoginManager.user?.uid}/${PostType.MERCENARY_SEARCH.type}/${model.contentId}",
                        timestamp = model.timestamp
                    )
                }
                else -> {
                    val model = it as ModelClubSearch
                    ModelMyPost(
                        content = model.content,
                        contentId= model.contentId,
                        type = PostType.CLUB_SEARCH,
                        mPostPath = "${PostType.MY_POST.path}/${LoginManager.user?.uid}/${PostType.CLUB_SEARCH.type}/${model.contentId}",
                        timestamp = model.timestamp
                    )

                }

            }
            RxRealTimeDB.push(requestModel.mPostPath,requestModel)
        }
            .flatMap { this }

    }

    private fun getSelectedDayToTimeStamp(date: String): Long {
        val beforeYear = LocalTime().extractDate()
        val year = beforeYear.split("-")[0].takeLast(2)
        var month = date.split("월")[0]
        var day = date.split("월")[1].split("일")[0]

        if (month.toInt() < 10)
            month = "0$month"

        if (day.toInt() < 10)
            day = "0$day"

        return LocalTime("$year-$month-$day 12:00:00").time
    }

}
