package ks.bs.mysoccerting.scene.profile

import android.os.Bundle
import androidx.databinding.DataBindingUtil
import com.google.android.material.chip.Chip
import kotlinx.android.synthetic.main.activity_profile_home.*
import kr.nextm.lib.getDrawable
import ks.bs.mysoccerting.BR
import ks.bs.mysoccerting.R
import ks.bs.mysoccerting.databinding.ActivityProfileHomeBinding
import ks.bs.mysoccerting.model.type.ChipType
import ks.bs.mysoccerting.model.type.PositionType
import ks.bs.mysoccerting.rx.rxresult.RxActivityBase
import ks.bs.mysoccerting.util.convertStandardAge
import ks.bs.mysoccerting.util.getAgeType

class ProfileHomeActivity : RxActivityBase() {
    val viewModel = createVm()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        DataBindingUtil.setContentView<ActivityProfileHomeBinding>(
            this,
            R.layout.activity_profile_home
        ).setVariable(BR.viewModel, viewModel)

        viewModel.init(getInput())

    }

    private fun createVm() = ProfileHomeViewModel(object : ProfileHomeViewModel.ProfileHomeContract {
        override fun backpressed() {
            finish()
        }

        override fun addChips(tag: ChipType, text: String) {
            val chip = Chip(this@ProfileHomeActivity)
            chip.text = text
            when (tag) {
                ChipType.POSITION -> {
                    chip.chipIcon = PositionType.valueOf(text).iconDrawable
                    chip.setTextAppearanceResource(R.style.ChipTextStyle)
                    chip.setChipBackgroundColorResource(PositionType.valueOf(text).bgColor)
                    positionChips.addView(chip)
                }
                ChipType.AGE -> {
                    val age = text.toInt()
                    chip.text = age.convertStandardAge()
                    chip.chipIcon = age.getAgeType().icon
                    chip.setTextAppearanceResource(R.style.ChipTextStyle)
                    chip.setChipBackgroundColorResource(R.color.brown)
                    ageChips.addView(chip)
                }
                else -> {
                    chip.chipIcon = R.drawable.ic_location.getDrawable()
                    chip.setTextAppearanceResource(R.style.ChipTextStyle)
                    chip.setChipBackgroundColorResource(R.color.coral)
                    areaChips.addView(chip)
                }
            }
        }

    })

    override fun onDestroy() {
        viewModel.onDestroy()
        super.onDestroy()
    }
}
