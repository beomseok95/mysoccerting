package ks.bs.mysoccerting.scene.search

import io.reactivex.rxkotlin.addTo
import ks.bs.mysoccerting.firebase.CrashlyticsHelper
import ks.bs.mysoccerting.model.ModelMercenarySearch
import ks.bs.mysoccerting.util.TLog

class SearchMercenaryViewModel(val model: ModelMercenarySearch, val contract: SearchViewModel.SearchFragmentContract) {

    val profileImageUrl get() = model.profileImageUrl
    val userId get() = model.nickName ?: ""
    val age get() = model.age
    val hopeDate get() = "용병 참가 희망일 : ${model.hopeDate}"
    val content get() = model.content
    val uid get() = model.uid
    val commentcount get() = model.commentcount
    val hopeArea get() = model.hopeArea
    val contentId get() = model.contentId
    val timestamp get() = model.timestamp
    val position get() = model.position
    val positionText get() = "주 포지션"

    fun moveItemDetailPageMercenary() {
        contract.moveItemDetailPageMercenary(model)
            .subscribe({}, { CrashlyticsHelper.logException(it) })
            .addTo(contract.getDisposable())
    }
}