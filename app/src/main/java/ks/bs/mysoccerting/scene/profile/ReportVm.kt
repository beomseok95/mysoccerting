package ks.bs.mysoccerting.scene.profile

import android.view.View
import androidx.databinding.ObservableField
import io.reactivex.rxkotlin.addTo
import kr.nextm.lib.TToast
import ks.bs.mysoccerting.firebase.CrashlyticsHelper
import ks.bs.mysoccerting.firebase.FirebaseUrls
import ks.bs.mysoccerting.firebase.RxRealTimeDB
import ks.bs.mysoccerting.model.ModelReport
import ks.bs.mysoccerting.model.time.LocalTime
import ks.bs.mysoccerting.model.time.Pattern
import ks.bs.mysoccerting.scene.base.ActivityBaseViewModel
import ks.bs.mysoccerting.scene.login.LoginManager

class ReportVm : ActivityBaseViewModel() {

    var title = ObservableField<String>()
    var postTitle = ObservableField<String>()
    var content = ObservableField<String>()

    var type = ""

    fun backPressed(view: View) {
        val activity = view.context as ReportActivity
        activity.onBackPressed()
    }

    fun okBtnClkEvent(view: View) {
        if (postTitle.get().isNullOrEmpty() || content.get().isNullOrEmpty()) {
            TToast.show("내용을 입력하세요")
            return
        }
        val title = postTitle.get().toString()
        val content = content.get().toString()
        val prePath = if (type == HelpViewModel.SUGGESTIONS) FirebaseUrls.suggestions else FirebaseUrls.report
        val contentId = RxRealTimeDB.generateRandomKey("service")
        val path = "$prePath/$contentId"

        val model = ModelReport(
            title = title,
            content = content,
            date = LocalTime().formatString(Pattern.toStringDefault),
            uid = LoginManager.user?.uid,
            contentId = contentId
        )

        RxRealTimeDB.push(path, model)
            .subscribe({
                if (type == HelpViewModel.SUGGESTIONS)
                    TToast.show("건의사항 작성을 성공하였습니다.\n빠른시일내에 반영할수있도록 하겠습니다.감사합니다")
                else
                    TToast.show("버그신고 작성을 완료하였습니다.\n빠른시일내에 반영할수있도록 하겠습니다.감사합니다")

                backPressed(view = view)
            }, { e ->
                CrashlyticsHelper.logException(e)
            })
            .addTo(compositeDisposable)
    }
}