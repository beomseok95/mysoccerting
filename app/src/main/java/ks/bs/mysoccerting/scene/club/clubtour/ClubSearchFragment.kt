package ks.bs.mysoccerting.scene.club.clubtour


import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import io.reactivex.Single
import ks.bs.mysoccerting.BR
import ks.bs.mysoccerting.R
import ks.bs.mysoccerting.model.Area
import ks.bs.mysoccerting.rx.showProgressDialog
import ks.bs.mysoccerting.scene.base.FragmentBase
import ks.bs.mysoccerting.scene.base.FragmentBaseViewModel
import ks.bs.mysoccerting.scene.filter.AreaDialog


class ClubSearchFragment : FragmentBase() {
    var viewModel = createVm()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding =
            DataBindingUtil.inflate<ViewDataBinding>(inflater, R.layout.fragment_club_search, container, false)
        binding.setVariable(BR.viewModel, viewModel)
        return binding.root
    }

    private fun createVm() = ClubSearchViewModel(object :
        ClubSearchViewModel.ClubSearchFragmentContract {
        override fun showAreaDialog(): Single<Area> {
            requireNotNull(context) { "context need not null!!" }
            return AreaDialog()
                .toSingle(context!!)
        }

        override fun createDialogForProgress(request: FragmentBaseViewModel.ProgressBarOwner.Request): Dialog {
            return showProgressDialog(activity, request.disposable, request.functionOnCancel)
        }
    })

    override fun onAttach(context: Context) {
        super.onAttach(context)
        viewModel.getContents()
    }

    override fun onDetach() {
        viewModel.onDetach()
        super.onDetach()
    }


}
