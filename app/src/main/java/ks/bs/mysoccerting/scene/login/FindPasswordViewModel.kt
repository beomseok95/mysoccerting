package ks.bs.mysoccerting.scene.login

import androidx.databinding.Observable
import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import android.text.TextUtils
import android.view.View
import com.google.firebase.auth.FirebaseAuth
import ks.bs.mysoccerting.R
import ks.bs.mysoccerting.exception.ErrorCode
import ks.bs.mysoccerting.scene.base.ActivityBaseViewModel
import ks.bs.mysoccerting.util.TLog
import ks.bs.mysoccerting.util.ValidateCheck
import ks.bs.mysoccerting.util.getString

class FindPasswordViewModel(private val contract: FindPasswordActivityContract) :
    ActivityBaseViewModel() {

    interface FindPasswordActivityContract {
        fun showToast(message: String)
    }

    var email = ObservableField<String>()
    var isValid = ObservableBoolean(false)
    var buttonColor = ObservableBoolean(false)

    fun init() {
        email.addOnPropertyChangedCallback(object : Observable.OnPropertyChangedCallback() {
            override fun onPropertyChanged(sender: Observable?, propertyId: Int) {
                validation()
            }
        })
    }

    fun validation() {
        val isValideEmail = !TextUtils.isEmpty(email.get())
        if (isValideEmail) {
            isValid.set(true)
            buttonColor.set(true)
            return
        }
        isValid.set(false)
        buttonColor.set(false)
    }

    fun sendChangePwEmailEvent() {
        val inputEmail = email.get().toString()
        if (ValidateCheck.validateEmail(inputEmail)) {
            resetPassword(inputEmail)
            return
        }
        contract.showToast(R.string.nonValidateEmail.getString())
    }


    private fun resetPassword(email: String) {
        auth.sendPasswordResetEmail(email)
            .addOnCompleteListener {
                when {
                    it.isSuccessful -> contract.showToast(R.string.emailSendComplete.getString())
                    it.isCanceled -> contract.showToast(R.string.emailSendCancled.getString())
                }
            }.addOnFailureListener {
                val e = it.message.toString()
                TLog.e(e)
                when (e) {
                    ErrorCode.NO_USER_EMAIL.code -> contract.showToast(ErrorCode.NO_USER_EMAIL.message)
                }
            }
    }

    fun backPressed(view: View) {
        val activity = view.context as FindPasswordActivity
        activity.onBackPressed()
    }

}

