package ks.bs.mysoccerting.scene.club.clubtour

import android.view.View
import androidx.navigation.findNavController
import ks.bs.mysoccerting.model.ModelClub
import ks.bs.mysoccerting.util.convertStandardAge

class ClubSearchItemVm(val model: ModelClub) {
    val clubMemberNumber get() = model.clubMemberNumber
    val create_at get() = model.createAt
    val clubMarkImageUrl get() = model.clubMarkImageUrl
    val clubName get() = model.clubName
    val activityAge get() = model.activityAge
    val activityArea get() = "활동지역 : ${model.activityArea}"
    val clubIntro get() = "클럽소개 : ${model.clubIntro}"

    fun activityAge() = model.activityAge.convertStandardAge()

    fun clubMemberCount() = model.clubMembers.count().toString()
    fun moveClubDetailPage(view: View) {
        val action =
            ClubSearchFragmentDirections.actionClubSearchFragmentToClubDetailFragment(
                model.contentId
            )
        view.findNavController().navigate(action)
    }

}