package ks.bs.mysoccerting.scene.club.detail.home

import android.graphics.drawable.Drawable
import android.view.View
import androidx.databinding.ObservableField
import ks.bs.mysoccerting.model.ModelClub
import ks.bs.mysoccerting.model.type.ClubGradeType
import ks.bs.mysoccerting.scene.club.detail.dialog.MemberInfoDialog
import ks.bs.mysoccerting.util.TLog

class ClubPeopleItemVm(
    val model: ModelClub.ModelMember,
    val contentId: String,
    val count: Int,
    val isInfoDialog: Boolean = false,
    val contract: Contract? = null
) {
    interface Contract {
        fun refreshAdapter()
        fun removeItem(isMercenary: Boolean, count: Int,nickName: String?)
        fun delegated(isDelegated: Boolean, nickName: String)
    }

    val memberImageUrl get() = model.memberImageUrl
    val nickName get() = model.nickName
    val position get() = model.position
    val gradeImage: ObservableField<Drawable> get() = ObservableField(getGradeDrawable())

    private fun getGradeDrawable(): Drawable {
        return ClubGradeType.valueOf(model.gradeType).imageDrawable
    }

    fun showMemberDetailInfo(view: View) {
        MemberInfoDialog(contentId, count, isInfoDialog)
            .putObject(model)
            .toSingle(view.context)
            .map { model ->
                if (contract == null)
                    return@map

                TLog.d(model)
                when {
                    model.isMercenary != null && model.nickName != null -> {
                        contract.removeItem(model.isMercenary,count, model.nickName)}
                    model.isDelegated == true && model.nickName != null -> {
                        contract.delegated(model.isDelegated, model.nickName)}
                    model.gradeType != null -> {

                        this.model.gradeType = model.gradeType
                        gradeImage.set(getGradeDrawable())
                        /**
                         * contract?.refreshAdapter() 로 바꾸지 말것 에러 뜸*
                         */
                        /**
                         * contract?.refreshAdapter() 로 바꾸지 말것 에러 뜸*
                         */
                    }
                }

                contract.refreshAdapter()

            }.subscribe()
    }
}