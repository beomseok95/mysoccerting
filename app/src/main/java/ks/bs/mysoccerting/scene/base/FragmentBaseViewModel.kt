package ks.bs.mysoccerting.scene.base

import android.app.Dialog
import androidx.databinding.BaseObservable
import com.google.firebase.auth.FirebaseAuth
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import ks.bs.mysoccerting.rx.DisposeOnDestroy
import ks.bs.mysoccerting.rx.networkThread
import ks.bs.mysoccerting.scene.lifecycle.FragmentLifeCycle

open class FragmentBaseViewModel(open val listener: BaseVmListener) : BaseObservable(), DisposeOnDestroy,
    FragmentLifeCycle {
    val compositeDisposable = CompositeDisposable()
    var auth: FirebaseAuth = FirebaseAuth.getInstance()

    fun createDialogForProgress(request: ProgressBarOwner.Request) =
        listener.createDialogForProgress(request)

    fun <T> Observable<T>.withProgressBallAnimation(text: String = ""): Observable<T> {
        var dialog: Dialog? = null

        return networkThread()
            .doOnSubscribe {
                AndroidSchedulers.mainThread().scheduleDirect {
                    dialog = createDialogForProgress(
                        ProgressBarOwner.Request(
                            text,
                            it
                        )
                    )
                }
            }
            .doFinally {
                AndroidSchedulers.mainThread().scheduleDirect {
                    if (dialog?.isShowing == true) {
                        dialog?.dismiss()
                    }
                }
            }
    }

    fun <T> Single<T>.withProgressBallAnimation(text: String = ""): Single<T> {

        var dialog: Dialog? = null

        return networkThread()
            .doOnSubscribe {
                AndroidSchedulers.mainThread().scheduleDirect {
                    dialog = createDialogForProgress(
                        ProgressBarOwner.Request(
                            text,
                            it
                        )
                    )
                }
            }
            .doFinally {
                AndroidSchedulers.mainThread().scheduleDirect {
                    if (dialog?.isShowing == true) {
                        dialog?.dismiss()
                    }
                }
            }
    }

    fun Completable.withProgressBallAnimation(text: String = ""): Completable {
        var dialog: Dialog? = null

        return networkThread()
            .doOnSubscribe {
                AndroidSchedulers.mainThread().scheduleDirect {
                    dialog = createDialogForProgress(
                        ProgressBarOwner.Request(
                            text,
                            it
                        )
                    )
                }
            }
            .doFinally {
                AndroidSchedulers.mainThread().scheduleDirect {
                    if (dialog?.isShowing == true) {
                        dialog?.dismiss()
                    }
                }
            }
    }

    override fun onDetach() {
        clearDisposables()
    }

    override fun clearDisposables() {
        compositeDisposable.clear()
    }

    override fun addDisposable(disposable: Disposable): Boolean {
        return compositeDisposable.add(disposable)
    }


    interface ProgressBarOwner {

        class Request(val text: String, val disposable: Disposable, val functionOnCancel: (() -> Unit)? = null)

        fun createDialogForProgress(request: Request): Dialog

    }

    interface BaseVmListener : ProgressBarOwner
}