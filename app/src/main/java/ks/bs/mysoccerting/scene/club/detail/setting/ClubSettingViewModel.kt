package ks.bs.mysoccerting.scene.club.detail.setting

import android.view.View
import androidx.databinding.ObservableBoolean
import io.reactivex.Single
import io.reactivex.rxkotlin.addTo
import kr.nextm.lib.TToast
import ks.bs.mysoccerting.R
import ks.bs.mysoccerting.customview.dialog.DialogButton
import ks.bs.mysoccerting.customview.dialog.TDialog
import ks.bs.mysoccerting.exception.ErrorCode
import ks.bs.mysoccerting.exception.ErrorCodeException
import ks.bs.mysoccerting.firebase.CrashlyticsHelper
import ks.bs.mysoccerting.firebase.FirebaseUrls
import ks.bs.mysoccerting.firebase.RxRealTimeDB
import ks.bs.mysoccerting.model.ModelClub
import ks.bs.mysoccerting.model.ModelClubTimeline
import ks.bs.mysoccerting.model.time.LocalTime
import ks.bs.mysoccerting.model.type.TimelineType
import ks.bs.mysoccerting.rx.rxresult.putObject
import ks.bs.mysoccerting.rx.rxresult.startActivity
import ks.bs.mysoccerting.rx.rxresult.startActivityForResult
import ks.bs.mysoccerting.rx.rxresult.toIntent
import ks.bs.mysoccerting.rx.withProgressBallAnimation
import ks.bs.mysoccerting.rx.withProgressBallDialog
import ks.bs.mysoccerting.scene.base.FragmentBaseViewModel
import ks.bs.mysoccerting.scene.club.detail.ClubDetailFragment
import ks.bs.mysoccerting.scene.club.detail.board.ClubBoardAddActivity
import ks.bs.mysoccerting.scene.club.detail.setting.ClubMembersInfoActivity.Companion.DELEGATED
import ks.bs.mysoccerting.scene.club.supervisor.*
import ks.bs.mysoccerting.scene.login.LoginManager
import ks.bs.mysoccerting.support.DontCare
import ks.bs.mysoccerting.util.TLog
import ks.bs.mysoccerting.util.getString

class ClubSettingViewModel(val contract: Contract) : FragmentBaseViewModel(contract) {
    interface Contract : BaseVmListener {
        fun showDialog(title: String): TDialog.Builder
        fun moveBrowse()
        fun moveClubBase()
        fun finish()
    }

    var contentId = ""
    val clubSecessionTabVisibility = ObservableBoolean(true)
    val isMaster = ObservableBoolean(false)
    val hasGrade = ObservableBoolean(false)


    var modelClub = ModelClub()

    fun init() {
        require(ClubDetailFragment.CLUB_UID.isNotEmpty()) { "CLUB_UID need to not null" }

        if (LoginManager.isLogin())
            RxRealTimeDB.toObservable("${FirebaseUrls.club}/${ClubDetailFragment.CLUB_UID}") { ModelClub() }
                .subscribe({ club ->

                    modelClub = club

                    if (club.contentId != LoginManager.user?.clubUid)
                        clubSecessionTabVisibility.set(false)
                    else
                        clubSecessionTabVisibility.set(true)

                    club.assistantList?.forEach {
                        if (it == LoginManager.user?.uid)
                            hasGrade.set(true)
                    }
                    if (club.masterUid == LoginManager.user?.uid) {
                        hasGrade.set(true)
                        isMaster.set(true)
                    }

                }, { e ->
                    CrashlyticsHelper.logException(e)
                })
                .addTo(compositeDisposable)
        else {
            clubSecessionTabVisibility.set(false)
            hasGrade.set(false)
        }

    }

    fun movePostWritePage(view: View) {
        LoginManager.noUserSafeAction(view.context, compositeDisposable) {
            ClubBoardAddActivity::class.java.toIntent()
                .startActivity(view.context)
                .subscribe()
        }
    }

    fun withdraw(view: View) {
        requireNotNull(modelClub.masterUid) { ErrorCode.FAIL_NO_MASTER_UID.message }
        requireNotNull(LoginManager.user?.clubUid) { ErrorCode.FAIL_NO_EXIST_MY_CLUB_CONTENT_ID.message }

        val clubUid = LoginManager.user?.clubUid

        if (modelClub.masterUid == LoginManager.user?.uid) {
            if (modelClub.clubMembers.count() == 1)
                TDialog.Builder(view.context, R.string.notice.getString())
                    .setMessage("[주의!!] 클럽과, 클럽에서 작성한\n모든게시물들은 삭제됩니다.\n정말로 클럽을 삭제하시겠습니까?")
                    .addButton(DialogButton.CANCEL())
                    .addButton(DialogButton.OK())
                    .toSingle()
                    .flatMap {
                        if (it.ok)
                            RxRealTimeDB.toSingle("${FirebaseUrls.club}/${LoginManager.user?.clubUid}") { ModelClub() }
                                .map { checkMatchClubExist(it) }
                                .flatMap {
                                    RxRealTimeDB.remove("${FirebaseUrls.club}/${LoginManager.user?.clubUid}")
                                        .andThen(RxRealTimeDB.remove("${FirebaseUrls.clubChat}/${LoginManager.user?.clubUid}"))
                                        .andThen(
                                            RxRealTimeDB.push(
                                                "${FirebaseUrls.users}/${LoginManager.user?.uid}",
                                                LoginManager.clubSecession()
                                            )
                                        )
                                        .flatMap {
                                            Single.just(DontCare)
                                                .removeBoard(LoginManager.user?.uid!!)
                                                .removeClubSearch(LoginManager.user?.uid!!)
                                                .removeClubMatch(LoginManager.user?.uid!!)
                                                .removeClubNames(clubUid!!)
                                        }
                                }
                                .map {
                                    TToast.show("클럽을 삭제하였습니다.")
                                    contract.moveClubBase()
                                }
                        else
                            Single.just(DontCare)
                    }
                    .subscribe({
                        TLog.d("클럽 삭제 성공")
                    }, { e ->
                        if (e is ErrorCodeException) {
                            TDialog.show(view.context, R.string.notice.getString(), e.message)
                            return@subscribe
                        }

                        CrashlyticsHelper.logException(e)
                    })
            else
                TDialog.Builder(view.context, R.string.notice.getString())
                    .setMessage("클럽원이 2명이상이면\n클럽탈퇴가 불가능합니다.")
                    .isColored(true)
                    .addButton(DialogButton.OK())
                    .show()

            return
        }


        TDialog.Builder(view.context, R.string.notice.getString())
            .setMessage("정말 클럽을 탈퇴 하시겠습니까?")
            .isColored(true)
            .addButton(DialogButton.CANCEL())
            .addButton(DialogButton.OK())
            .toSingle()
            .filter { it.ok }
            .flatMapSingle {
                RxRealTimeDB.toSingle("${FirebaseUrls.club}/${LoginManager.user?.clubUid}") { ModelClub() }
                    .map { modelClub ->
                        modelClub.membershipApplicationList?.removeAll { it.uid == LoginManager.user?.uid }
                        modelClub.assistantList?.removeAll { it == LoginManager.user?.uid }
                        modelClub.clubMemberNumber = modelClub.clubMemberNumber - 1
                        var pos = ""
                        modelClub.clubPositionList?.forEach {
                            if (it.value == LoginManager.user?.nickName)
                                pos = it.key
                        }
                        if (pos.isNotEmpty())
                            modelClub.clubPositionList?.remove(pos)
                        modelClub.clubMembers.removeAll { it.uid == LoginManager.user?.uid }
                        modelClub
                    }
                    .flatMap { model -> RxRealTimeDB.push("${FirebaseUrls.club}/${model.contentId}", model) }
                    .removeBoard(LoginManager.user?.uid!!)
                    .removeClubSearch(LoginManager.user?.uid!!)
                    .removeClubMatch(LoginManager.user?.uid!!)
                    .flatMap {
                        val contentId = RxRealTimeDB.generateRandomKey("timeline")
                        val timelinePath =
                            "${FirebaseUrls.club}/${LoginManager.user?.clubUid}/${FirebaseUrls.timeLine}/$contentId"

                        RxRealTimeDB.push(
                            timelinePath, createTimeLinePost(
                                documentId = contentId,
                                type = TimelineType.CLUB_RETIREMENT
                            )
                        )
                    }
                    .updateClubMemberInfo()
                    .flatMap {
                        val userModel = LoginManager.clubSecession()
                        RxRealTimeDB.push("${FirebaseUrls.users}/${LoginManager.user?.uid}", userModel)
                    }
                    .withProgressBallAnimation(view.context)
            }
            .map { contract.moveClubBase() }
            .subscribe({
                TToast.show("클럽탈퇴 성공")
                TLog.d("firedUserSecession complete ")
            }, { e ->
                if (e is NoSuchElementException)
                    return@subscribe

                CrashlyticsHelper.logException(e)
            })
            .addTo(compositeDisposable)

    }

    private fun checkMatchClubExist(it: ModelClub): ModelClub {
        if (it.matchUpList?.isNotEmpty() == true)
            throw ErrorCodeException(ErrorCode.CLUB_MATCH_EXIST)
        return it
    }

    private fun createTimeLinePost(
        documentId: String,
        type: TimelineType
    ): ModelClubTimeline {
        return ModelClubTimeline(
            nickName = LoginManager.user?.nickName!!,
            uid = LoginManager.user?.uid!!,
            contentUid = documentId,
            memberImageUrl = LoginManager.user?.profileImageUrl,
            timestamp = LocalTime().time,
            type = type
        )
    }

    fun membershipManagement(view: View) {
        LoginManager.noUserSafeAction(view.context, compositeDisposable) {
            require(ClubDetailFragment.CLUB_UID.isNotEmpty()) { "CLUB_UID need to not null" }

            RxRealTimeDB.toSingle("${FirebaseUrls.club}/${ClubDetailFragment.CLUB_UID}") { ModelClub() }
                .withProgressBallDialog(view.context)
                .map { club -> club.masterUid }
                .flatMap { clubMaterUid ->
                    if (hasGrade.get())
                        MembershipManagementActivity::class.java
                            .toIntent()
                            .startActivity(view.context)
                            .andThen(Single.just(DontCare))
                    else
                        TDialog.Builder(view.context, R.string.notice.getString())
                            .setMessage("코치 이상부터\n관리할 수 있습니다.")
                            .isColored(true)
                            .addButton(DialogButton.OK())
                            .toSingle()
                }
                .subscribe({
                    TLog.d("membershipManagement call")
                }, { e ->
                    CrashlyticsHelper.logException(e)
                })
                .addTo(compositeDisposable)
        }
    }

    fun mercenarySignUpManagement(view: View) {
        LoginManager.noUserSafeAction(view.context, compositeDisposable) {
            require(ClubDetailFragment.CLUB_UID.isNotEmpty()) { "CLUB_UID need to not null" }

            RxRealTimeDB.toSingle("${FirebaseUrls.club}/${ClubDetailFragment.CLUB_UID}") { ModelClub() }
                .withProgressBallDialog(view.context)
                .map { club -> club.masterUid }
                .flatMap { clubMaterUid ->
                    if (hasGrade.get())
                        MercenaryManagementActivity::class.java
                            .toIntent()
                            .startActivity(view.context)
                            .andThen(Single.just(DontCare))
                    else
                        TDialog.Builder(view.context, R.string.notice.getString())
                            .setMessage("코치 이상부터\n관리할 수 있습니다.")
                            .isColored(true)
                            .addButton(DialogButton.OK())
                            .toSingle()
                }
                .subscribe({
                    TLog.d("membershipManagement call")
                }, { e ->
                    CrashlyticsHelper.logException(e)
                })
                .addTo(compositeDisposable)

        }
    }

    fun matchingManagement(view: View) {
        LoginManager.noUserSafeAction(view.context, compositeDisposable) {
            require(ClubDetailFragment.CLUB_UID.isNotEmpty()) { "CLUB_UID need to not null" }

            RxRealTimeDB.toSingle("${FirebaseUrls.club}/${ClubDetailFragment.CLUB_UID}") { ModelClub() }
                .withProgressBallDialog(view.context)
                .map { club -> club.masterUid }
                .flatMap { clubMaterUid ->
                    if (hasGrade.get())
                        MatchingApplicationManagementActivity::class.java
                            .toIntent()
                            .startActivity(view.context)
                            .andThen(Single.just(DontCare))
                    else
                        TDialog.Builder(view.context, R.string.notice.getString())
                            .setMessage("코치 이상부터\n관리할 수 있습니다.")
                            .isColored(true)
                            .addButton(DialogButton.OK())
                            .toSingle()
                }
                .subscribe({
                    TLog.d("membershipManagement call")
                }, { e ->
                    CrashlyticsHelper.logException(e)
                })
                .addTo(compositeDisposable)

        }
    }


    fun moveToClubBrowse() {
        contract.moveBrowse()
    }

    fun moveToClubInfo(view: View) {
        ClubSettingInfoActivity::class.java
            .toIntent()
            .putObject(modelClub)
            .startActivity(view.context)
            .subscribe()
    }

    fun moveToClubMembersInfo(view: View) {
        ClubMembersInfoActivity::class.java
            .toIntent()
            .putObject(modelClub)
            .startActivityForResult<String>(view.context)
            .subscribe({ msg ->
                if (msg == DELEGATED) {
                    contract.moveClubBase()
                }
            }, { e ->
                CrashlyticsHelper.logException(e)
            }).addTo(compositeDisposable)
    }
}