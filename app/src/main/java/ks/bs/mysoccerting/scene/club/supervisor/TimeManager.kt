package ks.bs.mysoccerting.scene.club.supervisor

import ks.bs.mysoccerting.model.time.LocalTime
import ks.bs.mysoccerting.model.time.Pattern

object TimeManager {

    fun judgeTimeOutSevenDays(date: String): Boolean {
        val sixDays = 86_400_000 * 7
        val inputTime = LocalTime(date).time

        val curTime = System.currentTimeMillis()

        val standardTime =
            if (LocalTime(inputTime).extractDate() <= LocalTime(curTime).extractDate() && LocalTime(inputTime).extractTime().substring(
                    0,
                    2
                ).toInt() < 6
            )
                LocalTime(LocalTime("${LocalTime(inputTime).extractDate()} 06:00:00").time).time
            else
                LocalTime(LocalTime("${LocalTime(inputTime).extractDate()} 06:00:00").time + sixDays).time

        return curTime > standardTime
    }

    fun judgeTimeOutOneDay(date: String): Boolean {
        val sixDays = 86_400_000
        val inputTime = LocalTime(date).time

        val curTime = System.currentTimeMillis()

        val standardTime =
            if (LocalTime(inputTime).extractDate() <= LocalTime(curTime).extractDate() && LocalTime(inputTime).extractTime().substring(
                    0,
                    2
                ).toInt() < 6
            )
                LocalTime(LocalTime("${LocalTime(inputTime).extractDate()} 06:00:00").time).time
            else
                LocalTime(LocalTime("${LocalTime(inputTime).extractDate()} 06:00:00").time + sixDays).time

        return curTime > standardTime
    }
}

fun Long.getFormattDate() = LocalTime(this).formatString(Pattern.toStringDefault)
