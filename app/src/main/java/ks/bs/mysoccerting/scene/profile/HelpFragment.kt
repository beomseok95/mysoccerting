package ks.bs.mysoccerting.scene.profile

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import ks.bs.mysoccerting.BR
import ks.bs.mysoccerting.R
import ks.bs.mysoccerting.databinding.HelpFragmentBinding
import ks.bs.mysoccerting.databinding.TermsOfServiceFragmentBinding

class HelpFragment : Fragment() {

    val viewModel = createVm()

    private fun createVm() = HelpViewModel()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding = DataBindingUtil.inflate<HelpFragmentBinding>(
            inflater, R.layout.help_fragment, container, false
        )
        binding.setVariable(BR.viewModel, viewModel)
        return binding.root

    }

    override fun onDetach() {
//        viewModel.onDetach()
        super.onDetach()
    }

    companion object {
        fun newInstance() = HelpFragment()
    }
}
