package ks.bs.mysoccerting.scene.login

import androidx.databinding.Observable
import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import androidx.databinding.ObservableInt
import android.text.TextUtils
import android.view.View
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import ks.bs.mysoccerting.R
import ks.bs.mysoccerting.exception.ErrorCode
import ks.bs.mysoccerting.scene.base.ActivityBaseViewModel
import ks.bs.mysoccerting.util.TLog
import ks.bs.mysoccerting.util.ValidateCheck
import ks.bs.mysoccerting.util.getString

class SignUpEmailViewModel(private val contract: SIgnUpActivityContract) :
    ActivityBaseViewModel() {

    interface SIgnUpActivityContract {
        fun showToast(message: String)
        fun moveMainPage(user: FirebaseUser?)
    }

    var email = ObservableField<String>()
    var password = ObservableField<String>()
    var passwordRepeat = ObservableField<String>()
    var buttonColor = ObservableBoolean(false)
    var isValid = ObservableBoolean(false)
    val progressVisibility = ObservableInt(View.GONE)

    fun init() {
        email.addOnPropertyChangedCallback(object : Observable.OnPropertyChangedCallback() {
            override fun onPropertyChanged(sender: androidx.databinding.Observable?, propertyId: Int) {
                validation()
            }
        })
        password.addOnPropertyChangedCallback(object : Observable.OnPropertyChangedCallback() {
            override fun onPropertyChanged(sender: Observable?, propertyId: Int) {
                validation()
            }
        })
        passwordRepeat.addOnPropertyChangedCallback(object : Observable.OnPropertyChangedCallback() {
            override fun onPropertyChanged(sender: Observable?, propertyId: Int) {
                validation()
            }
        })
    }

    fun validation() {
        val isValidEmail = !TextUtils.isEmpty(email.get())
        val isValidPassword = !TextUtils.isEmpty(password.get())
        val isValidPasswordRepeat = !TextUtils.isEmpty(passwordRepeat.get())
        if (isValidEmail && isValidPassword && isValidPasswordRepeat) {
            isValid.set(true)
            buttonColor.set(true)
            return
        }
        isValid.set(false)
        buttonColor.set(false)

    }

    fun createLoginEmail() {
        val inputEmail = email.get().toString()
        val inputPw = password.get().toString()
        val inputPwRepeat = passwordRepeat.get().toString()
        when {
            !ValidateCheck.validateEmail(inputEmail) -> contract.showToast(R.string.nonValidateEmail.getString())
            inputPw != inputPwRepeat -> contract.showToast(R.string.noAccordPW.getString())
            !ValidateCheck.validatePassword(inputPw) || !ValidateCheck.validatePassword(inputPwRepeat) -> contract.showToast(
                R.string.passwordVaildateText.getString()
            )
            else -> {
                progressVisibility.set(View.VISIBLE)
                auth.createUserWithEmailAndPassword(inputEmail, inputPw)
                    .addOnCompleteListener {
                        when {
                            it.isSuccessful -> {
                                contract.showToast(R.string.signUpSuccess.getString())
                                contract.moveMainPage(auth.currentUser)
                                progressVisibility.set(View.GONE)
                            }
                            it.exception?.message.isNullOrEmpty() -> {
                                TLog.e(it.exception!!.message.toString())

                                contract.showToast(R.string.signUpError.getString())
                                progressVisibility.set(View.GONE)
                            }
                            else -> {
                                val e = it.exception!!.message.toString()
                                TLog.e(e)
                                when (e) {
                                    ErrorCode.ALREADY_ACOUNT.code -> contract.showToast(ErrorCode.ALREADY_ACOUNT.message)//ERROR_EMAIL_ALREADY_IN_USE
                                    else -> contract.showToast(e)
                                }
                                progressVisibility.set(View.GONE)
                            }
                        }
                    }
            }
        }
    }

    fun backPressed(view: View) {
        val activity = view.context as SignUpEmailActivity
        activity.onBackPressed()
    }
}



