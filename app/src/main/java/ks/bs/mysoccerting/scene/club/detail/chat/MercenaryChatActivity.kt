package ks.bs.mysoccerting.scene.club.detail.chat

import android.app.Dialog
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_chat.*
import ks.bs.mysoccerting.R
import ks.bs.mysoccerting.databinding.ActivityChatBinding
import ks.bs.mysoccerting.databinding.ActivityMercenaryChatBinding
import ks.bs.mysoccerting.rx.showProgressDialog
import ks.bs.mysoccerting.scene.base.BindingActivity
import ks.bs.mysoccerting.scene.base.FragmentBaseViewModel

class MercenaryChatActivity : BindingActivity<ActivityMercenaryChatBinding>() {

    override fun layoutRes(): Int = R.layout.activity_mercenary_chat

    override val viewModel = createVm()

    fun createVm() = MercenaryChatVm(object : MercenaryChatVm.Contract {
        override fun scrollBottom() {
            recyclerView.scrollToPosition(recyclerView.adapter!!.itemCount - 1)
        }

        override fun createDialogForProgress(request: FragmentBaseViewModel.ProgressBarOwner.Request): Dialog {
            return showProgressDialog(this@MercenaryChatActivity, request.disposable, request.functionOnCancel)
        }
    })

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        toolbar_title.text = "클럽채팅방"

        viewModel.init(getInput())
    }

    override fun onDestroy() {
        viewModel.clearDisposables()
        super.onDestroy()
    }
}
