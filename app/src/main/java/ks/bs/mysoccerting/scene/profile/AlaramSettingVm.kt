package ks.bs.mysoccerting.scene.profile

import android.view.View
import android.widget.CompoundButton
import androidx.databinding.ObservableBoolean
import ks.bs.mysoccerting.prefs.Prefs

class AlaramSettingVm {

    val checked = ObservableBoolean(Prefs.settings.fcmEnabled)
    val switchListener = CompoundButton.OnCheckedChangeListener { compoundButton, b ->
        checked.set(b)
        Prefs.save { Prefs.settings.fcmEnabled = b }
    }

    fun backPressed(view: View) {
        val activity = view.context as AlramSettingActivity
        activity.onBackPressed()
    }
}