package ks.bs.mysoccerting.scene.home

import android.view.View
import androidx.navigation.NavOptions
import androidx.navigation.findNavController
import io.reactivex.rxkotlin.addTo
import kotlinx.android.synthetic.main.activity_main.*
import ks.bs.mysoccerting.NavGraphDirections
import ks.bs.mysoccerting.R
import ks.bs.mysoccerting.firebase.CrashlyticsHelper
import ks.bs.mysoccerting.firebase.RxRealTimeDB
import ks.bs.mysoccerting.model.ModelClub
import ks.bs.mysoccerting.model.ModelMoveTo
import ks.bs.mysoccerting.model.type.MovePageLocationType
import ks.bs.mysoccerting.rx.rxresult.getActivity
import ks.bs.mysoccerting.rx.rxresult.startActivityForResult
import ks.bs.mysoccerting.rx.rxresult.toIntent
import ks.bs.mysoccerting.rx.withProgressBallAnimation
import ks.bs.mysoccerting.scene.alarm.AlarmActivity
import ks.bs.mysoccerting.scene.base.ActivityBaseViewModel
import ks.bs.mysoccerting.scene.login.LoginManager
import ks.bs.mysoccerting.util.TLog

class MainViewModel : ActivityBaseViewModel() {

    fun userInfoInit() {
        if (LoginManager.auth.currentUser != null)
            LoginManager.init(compositeDisposable)
    }

    fun backPressed(view: View) {
        val activity = view.context as MainActivity
        activity.onBackPressed()
    }


    fun moveNotificationPage(view: View) {
        val activity = view.context as MainActivity
        AlarmActivity::class.java
            .toIntent()
            .startActivityForResult<ModelMoveTo>(view.context.getActivity())
            .subscribe({ result ->
                when (result.locationcode) {
                    MovePageLocationType.CLUB_DETAIL.locationCode ->
                        RxRealTimeDB.toSingle(result.clubPullPath!!) { ModelClub() }
                            .map { club ->

                                activity.findNavController(R.id.nav_host_fragment)
                                    .navigate(
                                        NavGraphDirections.actionGlobalClubDetail(club.contentId),
                                        NavOptions.Builder().setPopUpTo(R.id.matchingFragment, false)
                                            .setLaunchSingleTop(true)
                                            .build()
                                    )
                            }
                            .withProgressBallAnimation(view.context)
                            .subscribe({
                                backPressed(view)
                                activity.bottomNavigation.selectedItemId = R.id.clubFragment
                            }, { e ->
                                TLog.e(e)
                            })

                    MovePageLocationType.PROFILE.locationCode -> {
                        activity.findNavController(R.id.nav_host_fragment)
                            .navigate(NavGraphDirections.actionGlobalProfile())
                    }
                    else -> {
                        //noting
                    }
                }


            }, { e ->
                CrashlyticsHelper.logException(e)
            })
            .addTo(compositeDisposable)
    }
}