package ks.bs.mysoccerting.scene.match

import android.app.Dialog
import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.math.MathUtils
import androidx.databinding.DataBindingUtil
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.fragment_matching_info_check_dialog.*
import kr.nextm.lib.getDrawable
import ks.bs.mysoccerting.R
import ks.bs.mysoccerting.customview.dialog.RxDialogFragment
import ks.bs.mysoccerting.databinding.FragmentMatchingInfoCheckDialogBinding
import ks.bs.mysoccerting.model.ModelClubMatch
import ks.bs.mysoccerting.rx.showProgressDialog
import ks.bs.mysoccerting.scene.base.FragmentBaseViewModel
import ks.bs.mysoccerting.support.DontCare
import ks.bs.mysoccerting.util.dpToPixels

class MatchingInfoCheckDialog : RxDialogFragment<Boolean>() {

    val viewModel = creatVm()

    val compositeDisposable = CompositeDisposable()

    override fun onCreateCustomView(inflater: LayoutInflater, container: ViewGroup): View {

        return DataBindingUtil.inflate<FragmentMatchingInfoCheckDialogBinding>(
            inflater,
            R.layout.fragment_matching_info_check_dialog,
            container,
            false
        )
            .let {
                it.viewModel = viewModel
                it.root
            }
    }

    private fun creatVm() = MatchingInfoCheckVm(object : MatchingInfoCheckVm.Contract {
        override fun createDialogForProgress(request: FragmentBaseViewModel.ProgressBarOwner.Request): Dialog {
            return showProgressDialog(context, request.disposable, request.functionOnCancel)
        }

        override fun onBackPressed() {
            dismiss()
        }

        override fun response(result: Boolean) {
            finishWithResponse(result)
        }
    })


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        initDialogSize()
        initHeaderColor()
        viewModel.init(parseObject())
    }


    private fun initDialogSize() {
        layout_dialog_root.addOnLayoutChangeListener(object : View.OnLayoutChangeListener {
            override fun onLayoutChange(
                v: View,
                left: Int,
                top: Int,
                right: Int,
                bottom: Int,
                oldLeft: Int,
                oldTop: Int,
                oldRight: Int,
                oldBottom: Int
            ) {
                v.removeOnLayoutChangeListener(this)
                val headerHeight = 40.dpToPixels()
                val buttonsHeight = 80.dpToPixels()
                val sum = headerHeight + buttonsHeight

                val width = 450.dpToPixels()
                val height = MathUtils.clamp(
                    bottom - top,
                    400.dpToPixels() + sum,
                    500.dpToPixels() + sum
                )

                val displayMetrics = context.resources.displayMetrics
                v.layoutParams.width = minOf(width, displayMetrics.widthPixels)
                v.layoutParams.height = minOf(height, displayMetrics.heightPixels)
                dialog?.window?.setLayout(v.layoutParams.width, v.layoutParams.height)
            }
        })
    }

    private fun initHeaderColor() {
        layout_dialog_header.background = R.drawable.bg_setting_dialog_header.getDrawable()
        tv_dialog_title.setTextColor(Color.WHITE)
        btn_dialog_close.setColorFilter(Color.WHITE)
    }

    override fun onDestroy() {
        viewModel.clearDisposables()
        compositeDisposable.clear()
        super.onDestroy()
    }
}