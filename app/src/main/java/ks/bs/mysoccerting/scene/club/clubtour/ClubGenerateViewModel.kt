package ks.bs.mysoccerting.scene.club.clubtour

import android.content.pm.PackageManager
import android.graphics.drawable.Drawable
import android.net.Uri
import android.view.View
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.core.net.toUri
import androidx.databinding.ObservableField
import io.reactivex.Single
import io.reactivex.rxkotlin.addTo
import kr.nextm.lib.TToast
import ks.bs.mysoccerting.R
import ks.bs.mysoccerting.customview.dialog.DialogButton
import ks.bs.mysoccerting.customview.dialog.TDialog
import ks.bs.mysoccerting.exception.ErrorCode
import ks.bs.mysoccerting.exception.ErrorCodeException
import ks.bs.mysoccerting.firebase.CrashlyticsHelper
import ks.bs.mysoccerting.firebase.FirebaseUrls
import ks.bs.mysoccerting.firebase.RxRealTimeDB
import ks.bs.mysoccerting.firebase.RxStorage
import ks.bs.mysoccerting.model.Area
import ks.bs.mysoccerting.model.ModelClub
import ks.bs.mysoccerting.model.ModelEditData
import ks.bs.mysoccerting.model.type.ClubGradeType
import ks.bs.mysoccerting.model.type.EditType
import ks.bs.mysoccerting.rx.rxresult.putObject
import ks.bs.mysoccerting.rx.rxresult.startActivityForResult
import ks.bs.mysoccerting.rx.rxresult.toIntent
import ks.bs.mysoccerting.rx.withProgressBallAnimation
import ks.bs.mysoccerting.scene.base.FragmentBaseViewModel
import ks.bs.mysoccerting.scene.filter.AreaDialog
import ks.bs.mysoccerting.scene.login.LoginManager
import ks.bs.mysoccerting.scene.profile.EditProfileInfoActivity
import ks.bs.mysoccerting.scene.profile.TransparentAlbumActivity
import ks.bs.mysoccerting.util.TLog
import ks.bs.mysoccerting.util.getString
import ks.bs.mysoccerting.util.isAgeAll

class ClubGenerateViewModel(val contract: ClubGenerateFragmentContract) : FragmentBaseViewModel(contract) {
    interface ClubGenerateFragmentContract : BaseVmListener {
        fun showAreaDialog(): Single<Area>
        fun getDrawableFromUri(uri: Uri): Drawable
        fun checkExternalStoragePermission()
        fun finishSuccess(contentId: String)
        fun bringToFront()
    }

    val clubName = ObservableField<String>()
    val activityAge = ObservableField<String>()
    val intro = ObservableField<String>()
    val area = ObservableField<String>()
    val clubMarkImageUrl = ObservableField<String>()

    fun showAreaDialog(view: View) {
        AreaDialog()
            .toSingle(view.context)
            .subscribe({
                val city = it.selectedCity
                val town = it.selectedTown

                if (town == null && city == null) {
                    return@subscribe
                }
                if (town == null) {
                    area.set("$city")
                    return@subscribe
                }

                area.set("$city $town")
            }, {
                CrashlyticsHelper.logException(it)
                TLog.e(it.message.toString())
            }).let { addDisposable(it) }
    }

    fun changeProfile(view: View) {
        if (ContextCompat.checkSelfPermission(
                view.context,
                android.Manifest.permission.READ_EXTERNAL_STORAGE
            ) == PackageManager.PERMISSION_GRANTED
        ) {
            TransparentAlbumActivity::class.java
                .toIntent()
                .startActivityForResult<String>(view.context)
                .subscribe({ response ->
                    TLog.d(response)
                    clubMarkImageUrl.set(response)

                }, { e ->
                    CrashlyticsHelper.logException(e)
                })
                .addTo(compositeDisposable)
        } else {
            TToast.show("권한없음")
            contract.checkExternalStoragePermission()
        }
    }

    fun moveEditPage(view: View, child: TextView, isAge: Boolean) {
        EditProfileInfoActivity::class.java
            .toIntent()
            .putObject(
                ModelEditData(
                    tag = view.tag.toString(),
                    data = child.text.toString(),
                    hint = EditType.valueOf(view.tag.toString()).hint,
                    isAge = isAge
                )
            )
            .startActivityForResult<String>(view.context)
            .subscribe({ result ->
                resultAdd(view.tag.toString(), result)
            }, {

            })
            .addTo(compositeDisposable)
    }

    private fun resultAdd(tag: String, result: String) {
        when (tag) {
            EditType.CLUB_NAME.tag -> clubName.set(result)
            EditType.ACTIVITY_AGE.tag -> activityAge.set(result)
            EditType.INTRO.tag -> intro.set(result)
        }
    }

    fun ok(view: View) {
        if (clubName.get().isNullOrEmpty() || activityAge.get().isNullOrEmpty() || area.get().isNullOrEmpty() || intro.get().isNullOrEmpty()) {
            TToast.show("모든 정보를 입력해주세요")
            return
        }

        val document = RxRealTimeDB.generateRandomKey("club")
        val path = "${FirebaseUrls.club}/$document"

        val data = ModelClub(
            clubMarkImageUrl = clubMarkImageUrl.get().toString(),
            clubName = clubName.get().toString(),
            clubIntro = intro.get().toString(),
            activityAge = isAgeAll(activityAge.get()),
            activityArea = area.get().toString(),
            createAt = System.currentTimeMillis().toString(),
            masterUid = auth.currentUser?.uid!!,
            contentId = document,
            clubMembers = mutableListOf(
                ModelClub.ModelMember(
                    nickName = LoginManager.user?.nickName!!,
                    uid = LoginManager.user?.uid!!,
                    memberImageUrl = LoginManager.user?.profileImageUrl,
                    position = LoginManager.user?.position?.joinToString(" "),
                    age = LoginManager.user?.age.toString(),
                    gradeType = ClubGradeType.COACH.name,
                    mercenary = false
                )
            )
        )

        val profileObservable =
            if (clubMarkImageUrl.get() != null)
                RxStorage.push("clubProfile", clubMarkImageUrl.get()!!.toUri())
                    .withProgressBallAnimation(view.context)
            else
                Single.just("")

        TDialog.Builder(view.context, "알림")
            .setMessage("한번 생성된 클럽 이름은 변경할 수 없습니다\n클럽을 생성하시겠습니까?")
            .addButton(DialogButton.OK()).isColored(true)
            .toSingle()
            .filter { it.ok }
            .flatMapSingle { RxRealTimeDB.toSingle(FirebaseUrls.clubNames) { HashMap<String, String>() } }
            .flatMap { clubNames ->
                if (clubNames.values.any { it == clubName.get().toString() })
                    throw ErrorCodeException(ErrorCode.CLUB_NAME_DUPLICATE)
                else {
                    clubNames[document] = clubName.get().toString()
                    RxRealTimeDB.push(FirebaseUrls.clubNames, clubNames)
                }
            }
            .flatMap { profileObservable }
            .flatMap {
                val profileImageUrl =
                    if (it.isEmpty())
                        clubMarkImageUrl.get()
                    else
                        it

                data.clubMarkImageUrl = profileImageUrl

                Single.just(data)
            }
            .flatMap { clubData -> RxRealTimeDB.push(path, clubData) }
            .map { LoginManager.saveLocalUserClubData(data) }
            .flatMap { userInfo -> RxRealTimeDB.push("${FirebaseUrls.users}/${userInfo.uid}", userInfo) }
            .subscribe({
                TLog.d("User Info change success!!")
                TToast.show("클럽 생성을 성공하였습니다.")
                contract.finishSuccess(data.contentId)
            }, { e ->
                if (e is NoSuchElementException)
                    return@subscribe

                if (e is ErrorCodeException) {
                    TDialog.Builder(view.context, R.string.notice.getString())
                        .setMessage(e.message)
                        .isColored(true)
                        .addButton(DialogButton.CANCEL())
                        .addButton(DialogButton.OK())
                        .show()

                    return@subscribe
                }

                TToast.show("클럽생성이 실패하였습니다.\n$e")
                CrashlyticsHelper.logException(e)
            })
            .addTo(compositeDisposable)
    }
}