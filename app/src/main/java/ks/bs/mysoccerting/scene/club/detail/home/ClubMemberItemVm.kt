package ks.bs.mysoccerting.scene.club.detail.home

import ks.bs.mysoccerting.model.ModelClub

class ClubMemberItemVm(val model: ModelClub.ModelMember) {
    val memberImageUrl get() = model.memberImageUrl
}