package ks.bs.mysoccerting.scene.alarm

import android.os.Bundle
import androidx.databinding.DataBindingUtil
import kotlinx.android.synthetic.main.activity_alarm.*
import ks.bs.mysoccerting.BR
import ks.bs.mysoccerting.R
import ks.bs.mysoccerting.databinding.ActivityAlarmBinding
import ks.bs.mysoccerting.model.ModelMoveTo
import ks.bs.mysoccerting.rx.rxresult.RxActivityBase

class AlarmActivity : RxActivityBase() {

    val viewModel = createViewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        DataBindingUtil.setContentView<ActivityAlarmBinding>(
            this,
            R.layout.activity_alarm
        )
            .setVariable(BR.viewModel, viewModel)

        toolbar_title.text = "알람"

        viewModel.loadAlarmData()
    }

    private fun createViewModel() = AlarmViewModel(object : AlarmViewModel.Contract {
        override fun finishPage(moveLocation: ModelMoveTo) {
            finishWithResponse(moveLocation)
        }
    })
}
