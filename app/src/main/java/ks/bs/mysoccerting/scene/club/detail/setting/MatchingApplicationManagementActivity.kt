package ks.bs.mysoccerting.scene.club.detail.setting

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import kotlinx.android.synthetic.main.activity_matching_application_management.*
import ks.bs.mysoccerting.BR
import ks.bs.mysoccerting.R
import ks.bs.mysoccerting.databinding.ActivityMatchingApplicationManagementBinding

class MatchingApplicationManagementActivity : AppCompatActivity() {

    val vm = createVm()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        DataBindingUtil.setContentView<ActivityMatchingApplicationManagementBinding>(
            this,
            R.layout.activity_matching_application_management
        )
            .setVariable(BR.viewModel, vm)

        vm.init()

        toolbar_title.text ="매칭신청관리"
    }


    fun createVm() = MatchingApplicationManagermentVm()
}
