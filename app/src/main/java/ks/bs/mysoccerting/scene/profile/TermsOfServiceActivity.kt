package ks.bs.mysoccerting.scene.profile

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import kotlinx.android.synthetic.main.activity_terms_of_service.*
import ks.bs.mysoccerting.BR
import ks.bs.mysoccerting.R
import ks.bs.mysoccerting.databinding.ActivityTermsOfServiceBinding

class TermsOfServiceActivity : AppCompatActivity() {

    val viewModel = createVm()

    private fun createVm() = TermOfServiceActivityVm()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        DataBindingUtil.setContentView<ActivityTermsOfServiceBinding>(this, R.layout.activity_terms_of_service)
            .setVariable(BR.vm, viewModel)

        toolbar_title.text="이용약관"
    }
}
