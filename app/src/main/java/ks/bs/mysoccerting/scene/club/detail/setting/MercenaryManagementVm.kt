package ks.bs.mysoccerting.scene.club.detail.setting

import android.view.View
import io.reactivex.rxkotlin.addTo
import ks.bs.mysoccerting.BR
import ks.bs.mysoccerting.R
import ks.bs.mysoccerting.databinding.BaseRecyclerViewAdapter
import ks.bs.mysoccerting.databinding.ItemMercenaryManagementBinding
import ks.bs.mysoccerting.firebase.CrashlyticsHelper
import ks.bs.mysoccerting.firebase.FirebaseUrls
import ks.bs.mysoccerting.firebase.RxRealTimeDB
import ks.bs.mysoccerting.model.ModelClub
import ks.bs.mysoccerting.scene.base.ActivityBaseViewModel
import ks.bs.mysoccerting.scene.login.LoginManager

class MercenaryManagementVm : ActivityBaseViewModel() {

    val adapter =
        BaseRecyclerViewAdapter<MercenaryManagementItemVm, ItemMercenaryManagementBinding>(
            R.layout.item_mercenary_management,
            BR.viewModel
        )

    fun loadAffiliateList() {
        val path = "${FirebaseUrls.club}/${LoginManager.user?.clubUid}/mercenaryApplicationList"

        RxRealTimeDB.toObservable(path) { mutableListOf<ModelClub.ModelMember>() }
            .subscribe({
                adapter.clear()
                it.forEach { model -> adapter.insert(
                    MercenaryManagementItemVm(
                        model
                    )
                ) }

            }, { e ->
                CrashlyticsHelper.logException(e)
            })
            .addTo(compositeDisposable)
    }


    fun backPressed(view: View) {
        val activity = view.context as MercenaryManagementActivity
        activity.onBackPressed()
    }

}