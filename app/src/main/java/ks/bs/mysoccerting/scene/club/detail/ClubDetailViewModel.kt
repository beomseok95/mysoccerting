package ks.bs.mysoccerting.scene.club.detail

import androidx.databinding.ObservableField
import ks.bs.mysoccerting.scene.base.FragmentBaseViewModel

class ClubDetailViewModel(val contract: ClubDetailFragmentContract) : FragmentBaseViewModel(contract) {
    var contentId = String()
    var isClubMember = ObservableField<Boolean>()

    interface ClubDetailFragmentContract : BaseVmListener
}