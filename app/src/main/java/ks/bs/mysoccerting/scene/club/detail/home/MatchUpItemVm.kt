package ks.bs.mysoccerting.scene.club.detail.home

import android.view.View
import ks.bs.mysoccerting.model.ModelClub
import ks.bs.mysoccerting.rx.rxresult.putObject
import ks.bs.mysoccerting.rx.rxresult.startActivity
import ks.bs.mysoccerting.rx.rxresult.toIntent
import ks.bs.mysoccerting.scene.club.detail.chat.ChatActivity

class MatchUpItemVm(val model: ModelClub.MatchInfo) {
    val clubMarkImageUrl1 get() = model.clubMarkImageUrl1
    val clubMarkImageUrl2 get() = model.clubMarkImageUrl2
    val clubName1 get() = model.clubName1
    val clubName2 get() = model.clubName2
    val hopeDate get() = "매칭일 : ${model.hopeDate}"
    val hopeDateTimeStamp get() = model.hopeDateTimeStamp
    val peopleNumber get() = "매칭인원 : ${model.peopleNumber}"
    val area get() = "매칭지역 : ${model.area}"

    fun moveToChatPage(view: View) {
        ChatActivity::class.java
            .toIntent()
            .putObject(model.contentId)
            .startActivity(view.context)
            .subscribe()
    }
}