package ks.bs.mysoccerting.scene.base

import android.Manifest
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.Settings
import android.util.DisplayMetrics
import androidx.appcompat.app.AppCompatActivity
import android.view.MenuItem
import androidx.core.app.ActivityCompat
import com.google.firebase.auth.FirebaseAuth
import io.reactivex.Single
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.rxkotlin.addTo
import ks.bs.mysoccerting.BuildConfig
import ks.bs.mysoccerting.firebase.CrashlyticsHelper
import ks.bs.mysoccerting.rx.DisposeOnDestroy
import ks.bs.mysoccerting.rx.caller.ActivityCaller
import ks.bs.mysoccerting.rx.mainThread
import ks.bs.mysoccerting.support.DontCare
import ks.bs.mysoccerting.support.cheat.FloatingCheatService
import ks.bs.mysoccerting.util.NetworkStatus
import java.util.concurrent.TimeUnit
import kotlin.math.min


open class ActivityBase : AppCompatActivity(), DisposeOnDestroy {
    private val ACTION_MANAGE_OVERLAY_PERMISSION_REQUEST_CODE = 5469

    var compositeDisposable = CompositeDisposable()
    var auth: FirebaseAuth = FirebaseAuth.getInstance()

    var isLoginActivity = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (!isLoginActivity) {
            Single.just(DontCare)
                .delay(2,TimeUnit.SECONDS)
                .mainThread()
                .subscribe({
                    checkPermission()
                    startOverlayWindowService(this)
                },{ e ->
                    CrashlyticsHelper.logException(e)
                })
                .addTo(compositeDisposable)
        }
        fixDp()
    }

    fun checkPermission() {
        ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE), 1)
    }

    override fun onResume() {
        super.onResume()
        NetworkStatus.checkAndAlert(this)
    }

    override fun onDestroy() {
        clearDisposables()
        super.onDestroy()
    }

    override fun addDisposable(disposable: Disposable): Boolean {
        return compositeDisposable.add(disposable)
    }

    override fun clearDisposables() {
        compositeDisposable.clear()
    }


    open fun finishWithException(t: Throwable) {
        val intent = Intent()
        intent.putExtra(ActivityCaller.RESULT_EXTRA_ERROR, t)
        setResult(RESULT_CANCELED, intent)
        finish()
    }

    private fun startOverlayWindowService(context: Context) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M
            && !Settings.canDrawOverlays(context)
        )
            this.permissionOverlay()

    }

    fun setToolbar(
        toolbar: androidx.appcompat.widget.Toolbar,
        title: String,
        isBackBtnDisplay: Boolean = true,
        textColorWhite: Boolean = false
    ) {
        if (textColorWhite)
            toolbar.setTitleTextColor(Color.WHITE)
        else
            toolbar.setTitleTextColor(Color.GRAY)

        setSupportActionBar(toolbar)
        supportActionBar?.title = title
        supportActionBar?.elevation = 5f
        supportActionBar?.setDisplayHomeAsUpEnabled(isBackBtnDisplay)
    }

    fun changeToolbarTitle(title: String) {
        supportActionBar?.title = title
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            android.R.id.home -> {
                finish()
                return true
            }

        }
        return super.onOptionsItemSelected(item) //Equal finish()
    }

    private fun permissionOverlay() {
        val intent = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            Intent(
                Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
                Uri.parse("package:$packageName")
            )
        } else {
            TODO("VERSION.SDK_INT < M")
        }
        startActivityForResult(intent, ACTION_MANAGE_OVERLAY_PERMISSION_REQUEST_CODE)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == ACTION_MANAGE_OVERLAY_PERMISSION_REQUEST_CODE && Settings.canDrawOverlays(this)) {
            // You have permission
            // 오버레이 설정창 이동 후 이벤트 처리합니다.
            startFloatingCheat()
        }
    }

    private fun startFloatingCheat() {
        if (BuildConfig.FLAVOR_TYPE)
            startService(Intent(this, FloatingCheatService::class.java))
    }


    private fun fixDp() {
        val metrics = DisplayMetrics()
        windowManager.defaultDisplay.getMetrics(metrics)

        val baseSizeDp = 560

        val r1 = baseSizeDp / 160f

        val sizeDp = min(metrics.widthPixels, metrics.heightPixels)
        val r2 = sizeDp / metrics.densityDpi.toFloat()
//        if (Math.abs(r1 - r2) > 1f) {
        val properDpi = (sizeDp / r1).toInt()
        val properDensity = properDpi / 160f
        metrics.densityDpi = properDpi
        metrics.density = properDensity
        metrics.scaledDensity = properDensity
        resources.displayMetrics.setTo(metrics)
//        }
    }
}