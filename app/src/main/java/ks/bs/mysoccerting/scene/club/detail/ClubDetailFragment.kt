package ks.bs.mysoccerting.scene.club.detail


import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import io.reactivex.rxkotlin.addTo
import kotlinx.android.synthetic.main.fragment_club_detail.*
import ks.bs.mysoccerting.BR
import ks.bs.mysoccerting.R
import ks.bs.mysoccerting.adapter.ClubDetailViewPagerAdapter
import ks.bs.mysoccerting.customview.ClubDetailTab
import ks.bs.mysoccerting.firebase.CrashlyticsHelper
import ks.bs.mysoccerting.rx.showProgressDialog
import ks.bs.mysoccerting.scene.base.FragmentBase
import ks.bs.mysoccerting.scene.base.FragmentBaseViewModel
import ks.bs.mysoccerting.scene.club.clubtour.ClubDetailViewPagerInterface
import ks.bs.mysoccerting.scene.login.LoginManager


class ClubDetailFragment : FragmentBase() {
    val args: ClubDetailFragmentArgs by navArgs()
    var viewModel = createVm()


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding =
            DataBindingUtil.inflate<ViewDataBinding>(inflater, R.layout.fragment_club_detail, container, false)
        binding.setVariable(BR.viewModel, viewModel)
        return binding.root
    }


    private fun createVm() = ClubDetailViewModel(object :
        ClubDetailViewModel.ClubDetailFragmentContract {

        override fun createDialogForProgress(request: FragmentBaseViewModel.ProgressBarOwner.Request): Dialog {
            return showProgressDialog(activity, request.disposable, request.functionOnCancel)
        }
    })

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        setPager()
        setTab()
    }

    private fun setPager() {
        require(CLUB_UID.isNotEmpty()) { "CLUB_UID need to not null" }

        val pagerAdapter = ClubDetailViewPagerAdapter(
            activity!!.supportFragmentManager,
            CLUB_UID,
            object : ClubDetailViewPagerInterface {
                override fun moveClubBase() {
                    findNavController().navigate(R.id.action_clubDetailFragment_to_clubFragment)
                }

                override fun moveBrowse() {
                    findNavController().navigate(R.id.action_clubDetailFragment_to_clubSearchFragment)
                }
            }
        )

        viewPager.adapter = pagerAdapter
        viewPager.offscreenPageLimit = 4
    }

    private fun setTab() {
        tablayout.setupWithViewPager(viewPager)
        tablayout.removeAllTabs()

        tablayout.addTab(
            tablayout.newTab().setCustomView(
                ClubDetailTab(
                    tablayout.context,
                    bgRes = R.drawable.bg_club_detail_tab1
                )
            )
        )
        tablayout.addTab(
            tablayout.newTab().setCustomView(
                ClubDetailTab(
                    tablayout.context,
                    bgRes = R.drawable.bg_club_detail_tab2
                )
            )
        )
        tablayout.addTab(
            tablayout.newTab().setCustomView(
                ClubDetailTab(
                    tablayout.context,
                    bgRes = R.drawable.bg_club_detail_tab3
                )
            )
        )
        tablayout.addTab(
            tablayout.newTab().setCustomView(
                ClubDetailTab(
                    tablayout.context,
                    bgRes = R.drawable.bg_club_detail_tab4
                )
            )
        )
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        require(args.clubItem.isNotEmpty()) { "args.clubItem need to not null" }

        viewModel.contentId = args.clubItem
        CLUB_UID = args.clubItem

        checkJoinedClub()


    }

    private fun checkJoinedClub() {
        LoginManager.userSubject
            .map {
                if (LoginManager.user?.clubUid == CLUB_UID)
                    viewModel.isClubMember.set(true)
                else
                    viewModel.isClubMember.set(false)
            }
            .subscribe({}, { e ->
                CrashlyticsHelper.logException(e)
            })
            .addTo(compositeDisposable)
    }

    override fun onDetach() {
        viewModel.onDetach()
        super.onDetach()
    }

    companion object {
        var CLUB_UID = ""
    }
}
