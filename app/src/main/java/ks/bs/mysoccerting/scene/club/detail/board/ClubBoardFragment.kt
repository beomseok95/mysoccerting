package ks.bs.mysoccerting.scene.club.detail.board


import android.app.Dialog
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import ks.bs.mysoccerting.BR
import ks.bs.mysoccerting.R
import ks.bs.mysoccerting.rx.rxresult.getObject
import ks.bs.mysoccerting.rx.rxresult.putObject
import ks.bs.mysoccerting.rx.showProgressDialog
import ks.bs.mysoccerting.scene.base.FragmentBase
import ks.bs.mysoccerting.scene.base.FragmentBaseViewModel


class ClubBoardFragment : FragmentBase() {
    var viewModel = createVm()

    var contentId = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            contentId = it.getObject()
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val binding = DataBindingUtil.inflate<ViewDataBinding>(inflater, R.layout.fragment_club_board, container, false)
            .apply { setVariable(BR.viewModel, viewModel) }


        viewModel.getContents(contentId)
        return binding.root
    }

    private fun createVm() = ClubBoardViewModel(object :
        ClubBoardViewModel.Contract {

        override fun createDialogForProgress(request: FragmentBaseViewModel.ProgressBarOwner.Request): Dialog {
            return showProgressDialog(activity, request.disposable, request.functionOnCancel)
        }
    })


    override fun onDetach() {
        viewModel.onDetach()
        super.onDetach()
    }

    companion object {
        fun newInstace(contentId: String) =
            ClubBoardFragment()
                .apply {
                    arguments = Bundle().apply {
                        putObject(contentId)
                    }
                }
    }
}
