package ks.bs.mysoccerting.scene.match


import android.app.Dialog
import android.content.Context
import android.content.Intent
import androidx.databinding.DataBindingUtil
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import io.reactivex.Single
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_post_add.*
import kr.nextm.lib.TToast
import ks.bs.mysoccerting.BR
import ks.bs.mysoccerting.R
import ks.bs.mysoccerting.databinding.FragmentMatchingBinding
import ks.bs.mysoccerting.model.Area
import ks.bs.mysoccerting.model.ModelClubMatch
import ks.bs.mysoccerting.rx.rxresult.putObject
import ks.bs.mysoccerting.rx.rxresult.startActivityForResult
import ks.bs.mysoccerting.rx.showProgressDialog
import ks.bs.mysoccerting.scene.base.FragmentBase
import ks.bs.mysoccerting.scene.base.FragmentBaseViewModel
import ks.bs.mysoccerting.scene.common.DetailItemActivity
import ks.bs.mysoccerting.scene.common.PostAddActivity
import ks.bs.mysoccerting.scene.filter.AreaDialog
import ks.bs.mysoccerting.scene.filter.CalendarDialog
import ks.bs.mysoccerting.scene.filter.PeopleNumberDialog
import ks.bs.mysoccerting.scene.home.MainActivity


class MatchingFragment : FragmentBase() {
    var viewModel = createVm()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val binding =
            DataBindingUtil.inflate<FragmentMatchingBinding>(inflater, R.layout.fragment_matching, container, false)
        binding.setVariable(BR.matchingViewModel, viewModel)


        return binding.root
    }

    fun createVm() = MatchingViewModel(object :
        MatchingViewModel.Contract {
        override fun showToast(message: String) {
            TToast.show(message)
        }

        override fun showAreaDialog(): Single<Area> {
            requireNotNull(context) { "context need not null!!" }
            return AreaDialog()
                .toSingle(context!!)
        }

        override fun showCalendarDialog(): Single<CalendarDialog.Date> {
            requireNotNull(context) { "context need to Not Null!!" }
            return CalendarDialog()
                .toSingle(context!!)
        }

        override fun showPeopleNumberDialog(): Single<MutableList<String>> {
            requireNotNull(context) { "Context need to  Not null!!" }
            return PeopleNumberDialog()
                .toSingle(context!!)
        }

        override fun moveItemDetailPage(item: ModelClubMatch): Single<Boolean> {
            DetailItemActivity.CALL_LOCATION =
                CALL_TAG_MATCHING
            return Intent(activity, DetailItemActivity::class.java)
                .putObject(item)
                .startActivityForResult(context!!)
        }

        override fun movePostWirteActivity(): Single<Any> {
            return Intent(activity, PostAddActivity::class.java)
                .putObject(CALL_TAG_MATCHING)
                .startActivityForResult(context!!)
        }

        override fun changeClubTabSelect() {
            val activity = activity as MainActivity
            activity.bottomNavigation.selectedItemId = R.id.clubFragment
        }

        override fun createDialogForProgress(request: FragmentBaseViewModel.ProgressBarOwner.Request): Dialog {
            return showProgressDialog(activity, request.disposable, request.functionOnCancel)
        }

        override fun resetFlexBoxView() {
            plexobx.removeAllViews()
        }
    })

    override fun onAttach(context: Context) {
        super.onAttach(context)
        viewModel.getContents()
    }

    override fun onDetach() {
        viewModel.onDetach()
        super.onDetach()
    }


    companion object {
        const val CALL_TAG_MATCHING = "Matching"
    }
}
