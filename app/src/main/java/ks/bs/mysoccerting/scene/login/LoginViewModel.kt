package ks.bs.mysoccerting.scene.login

import android.content.Intent
import androidx.databinding.ObservableInt
import android.net.ConnectivityManager
import android.view.View
import com.facebook.AccessToken
import com.facebook.CallbackManager
import com.facebook.FacebookCallback
import com.facebook.FacebookException
import com.facebook.login.LoginManager
import com.facebook.login.LoginResult
import com.google.android.gms.auth.api.Auth
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.common.api.ApiException
import com.google.firebase.auth.FacebookAuthProvider

import com.google.firebase.auth.FirebaseUser
import com.google.firebase.auth.GoogleAuthProvider
import com.kakao.auth.ISessionCallback
import com.kakao.auth.Session
import com.kakao.network.ErrorResult
import com.kakao.usermgmt.UserManagement
import com.kakao.usermgmt.callback.MeResponseCallback
import com.kakao.usermgmt.response.model.UserProfile
import com.kakao.util.exception.KakaoException
import com.kakao.util.helper.log.Logger
import kr.nextm.lib.TToast
import ks.bs.mysoccerting.exception.ErrorCode
import ks.bs.mysoccerting.firebase.CrashlyticsHelper
import ks.bs.mysoccerting.scene.base.ActivityBaseViewModel
import ks.bs.mysoccerting.scene.login.LoginActivity.Companion.GOOGLE_LOGIN_CODE
import ks.bs.mysoccerting.util.TLog


class LoginViewModel(
    private val contract: LoginActivityContract,
    private val callbackManager: CallbackManager
) : ActivityBaseViewModel() {

    interface LoginActivityContract {
        fun facebookLogin()
        fun googleLogin()
        fun kakaoLogin()
        fun emailLogin()
        fun lookMain()
        fun signUpEmail()
        fun moveMainPage(user: FirebaseUser?)
        fun showToast(message: String)
        fun getConnectivityManager(): ConnectivityManager
    }

    val progressVisibility = ObservableInt(View.GONE)

    var callback: SessionCallback? = null

    fun facebookLoginBtnClick() {
        progressVisibility.set(View.VISIBLE)
        contract.facebookLogin()
        LoginManager.getInstance().registerCallback(callbackManager, object : FacebookCallback<LoginResult> {
            override fun onSuccess(result: LoginResult?) {
                handleFacebookAccessToken(result?.accessToken)
            }

            override fun onCancel() {
                progressVisibility.set(View.GONE)
            }

            override fun onError(error: FacebookException?) {
                progressVisibility.set(View.GONE)
                TLog.e(error?.message.toString())
            }
        })
    }

    private fun handleFacebookAccessToken(token: AccessToken?) { //Facebook 토큰을 넘겨주는 줌
        if (token != null) {
            val credential = FacebookAuthProvider.getCredential(token.token)
            auth.signInWithCredential(credential)
                .addOnCompleteListener {
                    if (it.isSuccessful) {
                        contract.moveMainPage(auth.currentUser)
                        progressVisibility.set(View.GONE)
                    }
                }.addOnFailureListener {
                    val e = it.message.toString()
                    TLog.e(it)
                    when (e) {
                        ErrorCode.SAME_EMAIL.code -> contract.showToast(ErrorCode.SAME_EMAIL.message)//FirebaseAuthUserCollisionException
                        else -> contract.showToast(e)
                    }
                    progressVisibility.set(View.GONE)
                }
        }
    }


    fun googleLoginBtnClick() {
        progressVisibility.set(View.VISIBLE)
        contract.googleLogin()
    }

    fun kakaoLoginBtnClick() {
        contract.kakaoLogin()
    }

    fun emailLoginBtnClick() {
        contract.emailLogin()
    }

    fun lookMainBtnClick() {
        contract.lookMain()

    }

    fun signUpBtnClick() {
        contract.signUpEmail()
    }

    fun isConnected(): Boolean {
        val netInfo = contract.getConnectivityManager().activeNetworkInfo
        return netInfo != null && netInfo.isConnectedOrConnecting
    }


    private fun requestMe() {
        UserManagement.getInstance().requestMe(object : MeResponseCallback() {
            override fun onFailure(errorResult: ErrorResult?) {
                TLog.d(errorResult!!.toString())
            }

            override fun onSessionClosed(errorResult: ErrorResult) {
                TLog.d(errorResult.toString())
            }

            override fun onSuccess(userProfile: UserProfile) {
                TLog.d(userProfile.toString())
                TLog.d(userProfile.uuid)
                TLog.d(userProfile.id.toString())
            }

            override fun onNotSignedUp() {
                TLog.d("onNotSignedUp")
            }
        })
    }

    inner class SessionCallback : ISessionCallback {
        override fun onSessionOpened() {
            //access token을 성공적으로 발급 받아 valid access token을 가지고 있는 상태. 일반적으로 로그인 후의 다음 activity로 이동한다.
            if (Session.getCurrentSession().isOpened) { // 한 번더 세션을 체크해주었습니다.
                requestMe()
            }
        }

        override fun onSessionOpenFailed(exception: KakaoException?) {
            if (exception != null) {
                Logger.e(exception)
            }
        }
    }


    fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == GOOGLE_LOGIN_CODE) { //구글에서 승인된 정보를 가지고 오기
            try {
                val result = GoogleSignIn.getSignedInAccountFromIntent(data)
                val account = result.getResult(ApiException::class.java)
                firebaseAuthWithGoogle(account!!)
            } catch (e: ApiException) {
                TToast.show("구글로그인에 실패하였습니다.")
                CrashlyticsHelper.logException(e)
                progressVisibility.set(View.GONE)
            }
        }
    }

    private fun firebaseAuthWithGoogle(account: GoogleSignInAccount) {
        val credential = GoogleAuthProvider.getCredential(account.idToken, null)
        auth.signInWithCredential(credential)
            .addOnCompleteListener {
                if (it.isSuccessful) {
                    contract.moveMainPage(auth.currentUser)
                }
            }.addOnFailureListener {
                progressVisibility.set(View.GONE)
                TLog.e(it.message.toString())
            }
    }


    fun init() {
        callback = SessionCallback()
        Session.getCurrentSession().addCallback(callback)
        if (Session.getCurrentSession().isOpened) { // 한 번더 세션을 체크해주었습니다.
            requestMe()
            contract.lookMain()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        Session.getCurrentSession().removeCallback(callback)
    }
}