package ks.bs.mysoccerting.scene.login

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import io.reactivex.Single
import io.reactivex.rxkotlin.addTo
import ks.bs.mysoccerting.R
import ks.bs.mysoccerting.firebase.CrashlyticsHelper
import ks.bs.mysoccerting.scene.base.ActivityBase
import ks.bs.mysoccerting.support.DontCare
import java.util.concurrent.TimeUnit

class SplashActivity : ActivityBase() {

    override fun onCreate(savedInstanceState: Bundle?) {
        isLoginActivity = true
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        Single.just(DontCare)
            .delay(2, TimeUnit.SECONDS)
            .subscribe({
                startActivity(Intent(this@SplashActivity, LoginActivity::class.java))
                finish()
            }, { e ->
                CrashlyticsHelper.logException(e)
            })
            .addTo(compositeDisposable)
    }
}
