package ks.bs.mysoccerting.scene.club.detail.setting

import android.graphics.drawable.Drawable
import android.net.Uri
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import kotlinx.android.synthetic.main.activity_profile_setting.*
import ks.bs.mysoccerting.R
import ks.bs.mysoccerting.databinding.ActivityClubSettingInfoBinding
import ks.bs.mysoccerting.rx.rxresult.RxActivityBase
import ks.bs.mysoccerting.rx.rxresult.getObject
import ks.bs.mysoccerting.util.toDrawable

class ClubSettingInfoActivity : RxActivityBase() {
    val viewModel = createVm()

    private fun createVm() = ClubSettingInfoViewModel(object :
        ClubSettingInfoViewModel.Contract {
        override fun getDrawableFromUri(uri: Uri): Drawable {
            return uri.toDrawable(this@ClubSettingInfoActivity)
        }

        override fun checkExternalStoragePermission() {
            checkPermission()
        }
    })

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        DataBindingUtil.setContentView<ActivityClubSettingInfoBinding>(
            this,
            R.layout.activity_club_setting_info
        ).viewModel = viewModel

        viewModel.getContent(intent.getObject())

        initToolbar()

    }

    private fun initToolbar() {
        toolbar_title.text = "클럽 정보"
    }
}