package ks.bs.mysoccerting.scene.club.supervisor

import android.view.View
import io.reactivex.Single
import io.reactivex.rxkotlin.zipWith
import ks.bs.mysoccerting.R
import ks.bs.mysoccerting.customview.dialog.DialogButton
import ks.bs.mysoccerting.customview.dialog.DialogObservable
import ks.bs.mysoccerting.customview.dialog.TDialog
import ks.bs.mysoccerting.customview.dialog.TextContentParam
import ks.bs.mysoccerting.exception.ErrorCode
import ks.bs.mysoccerting.exception.toDialogMessage
import ks.bs.mysoccerting.fcm.FcmHelper
import ks.bs.mysoccerting.firebase.CrashlyticsHelper
import ks.bs.mysoccerting.firebase.FirebaseUrls
import ks.bs.mysoccerting.firebase.RxRealTimeDB
import ks.bs.mysoccerting.model.ModelAlaram
import ks.bs.mysoccerting.model.ModelClub
import ks.bs.mysoccerting.model.ModelUser
import ks.bs.mysoccerting.model.time.LocalTime
import ks.bs.mysoccerting.model.type.AlarmType
import ks.bs.mysoccerting.model.type.ClubGradeType
import ks.bs.mysoccerting.model.type.TimelineType
import ks.bs.mysoccerting.rx.withProgressBallAnimation
import ks.bs.mysoccerting.rx.withProgressBallDialog
import ks.bs.mysoccerting.scene.login.LoginManager
import ks.bs.mysoccerting.util.TLog
import ks.bs.mysoccerting.util.getString
import ks.bs.mysoccerting.util.wrapWith

object MercenarySignUpHelper {

    fun accept(view: View, modelAlaram: ModelAlaram) {
        var clubModel: ModelClub
        var userModel: ModelUser
        RxRealTimeDB.toSingle("${FirebaseUrls.users}/${modelAlaram.senderUid}") { ModelUser() }
            .zipWith(RxRealTimeDB.toSingle("${FirebaseUrls.club}/${LoginManager.user?.clubUid}") { ModelClub() })
            .flatMap { response ->
                userModel = response.first
                clubModel = response.second

                require(clubModel.contentId.isNotEmpty()) { ErrorCode.FAIL_NO_EXIST_MY_CLUB_CONTENT_ID.message }
                require(userModel.uid?.isNotEmpty() ?: false) { ErrorCode.FAIL_NO_OPPONENET_USER_INFO.message }

                when {

                    ClubManager.duplicateMercenaryInClubCheck(response.first) ->
                        ClubManager.areadyRegisteredMercenary(view, response, userModel, clubModel, modelAlaram)

                    ClubManager.maxCountCheck(response.first) ->
                        ClubManager.areadyMax(
                            view,
                            response,
                            userModel,
                            clubModel,
                            modelAlaram
                        )

                    else -> mercenarySignUpAcceptProccess(clubModel, userModel, view, modelAlaram)
                }
            }
            .withProgressBallDialog(view.context)
            .subscribe({
                TLog.d("user info update \nalaram and club mercenary list remove complete")
            }, { e ->
                TDialog.show(view.context, e.toDialogMessage())
                CrashlyticsHelper.logException(e)
            })
            .let { }
    }

    private fun checkAreadApplied(mercenaryApplicationList: MutableList<ModelClub.ModelMember>?) =
        mercenaryApplicationList?.any { it.uid == LoginManager.user?.uid } ?: false


    private fun mercenarySignUpAcceptProccess(
        clubModel: ModelClub,
        userModel: ModelUser,
        view: View,
        modelAlaram: ModelAlaram
    ): Single<DialogObservable.Result<TextContentParam>> {
        requireNotNull(modelAlaram.senderUid) { ErrorCode.FAIL_NO_SENDER_UID.message }
        requireNotNull(modelAlaram.reciverUid) { ErrorCode.FAIL_NO_RECIVER_UID.message }
        require(clubModel.contentId.isNotEmpty()) { ErrorCode.FAIL_NO_EXIST_MY_CLUB_CONTENT_ID.message }
        require(
            userModel.uid?.isNotEmpty() ?: false
        ) { ErrorCode.FAIL_NO_SENDER_UID.message }

        val mercenaryModel = ModelUser.ModelMercenaryClub(
            clubMarkImageUrl = clubModel.clubMarkImageUrl,
            clubName = clubModel.clubName,
            masterUid = clubModel.masterUid,
            contentId = clubModel.contentId
        )

        if (userModel.mercenaryClubInfo == null)
            userModel.mercenaryClubInfo = mutableListOf(mercenaryModel)
        else
            userModel.mercenaryClubInfo?.also { it.add(mercenaryModel) }


        return RxRealTimeDB.push("${FirebaseUrls.users}/${modelAlaram.senderUid}", userModel)
            .flatMap {
                val path = "${FirebaseUrls.alram}/${modelAlaram.reciverUid}"
                RxRealTimeDB.toSingle(path) { mutableListOf<ModelAlaram>() }
                    .map { response ->

                        response.removeAll { it.reciverUid == modelAlaram.reciverUid && it.timeStamp == modelAlaram.timeStamp }
                        response
                    }
                    .flatMap { model -> RxRealTimeDB.push(path, model) }
            }
            .map {
                clubModel.mercenaryApplicationList?.removeAll { it.uid == userModel.uid }

                clubModel.mercenaryNumber = clubModel.mercenaryNumber + 1
                clubModel.mercenaryMember?.add(
                    ModelClub.ModelMember(
                        nickName = userModel.nickName ?: "",
                        uid = userModel.uid!!,
                        memberImageUrl = userModel.profileImageUrl ?: "",
                        position = userModel.position?.joinToString("  ") ?: "",
                        age = userModel.age?.toString(),
                        gradeType = ClubGradeType.MERCENARY.name,
                        mercenary = true
                    )
                )
                clubModel
            }
            .flatMap { model ->
                RxRealTimeDB.push("${FirebaseUrls.club}/${model.contentId}", model)
            }
            .flatMap {
                RxRealTimeDB.toSingle("${FirebaseUrls.alram}/${modelAlaram.senderUid}") { mutableListOf<ModelAlaram>() }
            }
            .flatMap { alaramList ->

                val alarmData = ModelAlaram(
                    senderEmail = LoginManager.user?.email,
                    senderImageUrl = clubModel.clubMarkImageUrl,
                    timeStamp = LocalTime().time,
                    senderUid = LoginManager.user?.uid!!,
                    senderNickName = LoginManager.user?.nickName,
                    reciverUid = modelAlaram.senderUid,
                    alaramTypeCode = AlarmType.CLUB_MERCENARY_SIGNUP_COMPLETE.code,
                    message = "${clubModel.clubName.wrapWith(
                        "[",
                        "]"
                    )} ${AlarmType.CLUB_MERCENARY_SIGNUP_COMPLETE.text}",
                    postPullPath = "${FirebaseUrls.club}/${clubModel.contentId}"
                )

                alaramList.add(alarmData)

                RxRealTimeDB.push("${FirebaseUrls.alram}/${modelAlaram.senderUid}", alaramList)
            }
            .flatMap {
                FcmHelper.sendMessage(
                    destinationUid = modelAlaram.senderUid,
                    message = "${clubModel.clubName.wrapWith(
                        "[",
                        "]"
                    )} ${AlarmType.CLUB_MERCENARY_SIGNUP_COMPLETE.text}"
                )
            }
            .flatMap {
                val contentId = RxRealTimeDB.generateRandomKey("timeline")
                val timelinePath =
                    "${FirebaseUrls.club}/${LoginManager.user?.clubUid}/${FirebaseUrls.timeLine}/$contentId"

                RxRealTimeDB.push(
                    timelinePath, ClubManager.createTimeLinePost(
                        documentId = contentId,
                        type = TimelineType.MERCENARY_SIGN_UP,
                        model = modelAlaram
                    )
                )
            }
            .flatMap {
                TDialog.Builder(view.context)
                    .setMessage("용병신청이 수락되었습니다.")
                    .isColored(true)
                    .addButton(DialogButton.OK())
                    .toSingle()
            }
    }

    fun request(
        view: View,
        writerUid: String,
        clubMasterUid: String,
        name: String,
        mercenaryApplicationList: MutableList<ModelClub.ModelMember>?
    ): Single<DialogObservable.Result<TextContentParam>> {
        return when {
            checkAreadApplied(mercenaryApplicationList) -> {
                TDialog.Builder(view.context, R.string.notice.getString())
                    .setMessage("이미 용병가입신청이 되어있습니다.")
                    .isColored(true)
                    .addButton(DialogButton.CANCEL())
                    .toSingle()
            }

            ClubManager.maxCountCheck(LoginManager.user!!) ->
                ClubManager.showDialog(
                    view,
                    title = R.string.notice.getString(),
                    message = "신청할수있는 최대 용병 수를 초과하였습니다.",
                    cancelable = false
                )
            ClubManager.duplicateMercenaryInClubCheckWithMaster(LoginManager.user, masterUid = clubMasterUid) ->
                ClubManager.showDialog(
                    view,
                    title = R.string.notice.getString(),
                    message = "이미 이 클럽에 용병으로 등록되어 있습니다",
                    cancelable = false
                )

            else -> putInfoMercenaryRequest(
                view,
                writerUid,
                name
            )

        }
    }

    private fun putInfoMercenaryRequest(
        view: View,
        writerUid: String,
        name: String
    ): Single<DialogObservable.Result<TextContentParam>> {

        return RxRealTimeDB.toSingle("${FirebaseUrls.alram}/$writerUid") { mutableListOf<ModelAlaram>() }
            .noExistClubCheckToUserUid(writerUid)
            .flatMap { responseList ->
                val alarmData = ModelAlaram(
                    senderEmail = LoginManager.user?.email,
                    senderImageUrl = LoginManager.user?.profileImageUrl,
                    timeStamp = LocalTime().time,
                    senderUid = LoginManager.user?.uid!!,
                    senderNickName = LoginManager.user?.nickName,
                    reciverUid = writerUid,
                    alaramTypeCode = AlarmType.CLUB_MERCENARY_SIGNUP_REQUEST.code,
                    message = "${LoginManager.user?.nickName} ${AlarmType.CLUB_MERCENARY_SIGNUP_REQUEST.text}"
                )

                responseList.add(alarmData)

                RxRealTimeDB.push("${FirebaseUrls.alram}/$writerUid", responseList)
            }
            .flatMap {
                FcmHelper.sendMessage(
                    destinationUid = writerUid,
                    message = "${LoginManager.user?.nickName} ${AlarmType.CLUB_MERCENARY_SIGNUP_REQUEST.text}"
                )
            }
            .flatMap {
                RxRealTimeDB.toSingle("${FirebaseUrls.users}/$writerUid") { ModelUser() }
            }
            .flatMap { master ->

                require(!master.clubUid.isNullOrEmpty()) { ErrorCode.NO_EXIST_CLUB.message }

                RxRealTimeDB.toSingle("${FirebaseUrls.club}/${master.clubUid}") { ModelClub() }
                    .flatMap { modelClub ->
                        modelClub.mercenaryApplicationList?.add(
                            ModelClub.ModelMember(
                                nickName = LoginManager.user?.nickName!!,
                                uid = LoginManager.user?.uid!!,
                                memberImageUrl = LoginManager.user?.profileImageUrl,
                                position =
                                if (LoginManager.user?.position != null)
                                    LoginManager.user?.position!![0]
                                else
                                    null,
                                age = LoginManager.user?.age.toString()
                            )
                        )
                        RxRealTimeDB.push(
                            "${FirebaseUrls.club}/${master.clubUid}", modelClub
                        )
                    }
            }
            .flatMap {
                TDialog.Builder(view.context, "알림")
                    .setMessage("$name 클럽에 용병등록신청이\n완료되었습니다. ")
                    .isColored(true)
                    .addButton(DialogButton.OK())
                    .toSingle()
            }
            .withProgressBallAnimation(view.context)
    }

    fun reject(view: View, modelAlaram: ModelAlaram) {
        requireNotNull(modelAlaram.senderUid) { ErrorCode.FAIL_NO_SENDER_UID.message }
        requireNotNull(modelAlaram.reciverUid) { ErrorCode.FAIL_NO_RECIVER_UID.message }

        val path = "${FirebaseUrls.alram}/${modelAlaram.reciverUid}"
        RxRealTimeDB.toSingle(path) { mutableListOf<ModelAlaram>() }
            .map { response ->
                response.removeAll { it.reciverUid == modelAlaram.reciverUid && it.timeStamp == modelAlaram.timeStamp }
                response
            }
            .flatMap { model -> RxRealTimeDB.push(path, model) }
            .flatMap {
                RxRealTimeDB.toSingle("${FirebaseUrls.alram}/${modelAlaram.senderUid}") { mutableListOf<ModelAlaram>() }
                    .zipWith(RxRealTimeDB.toSingle("${FirebaseUrls.club}/${LoginManager.user?.clubUid}") { ModelClub() })
            }
            .flatMap { response ->

                require(response.second.contentId.isNotEmpty()) { ErrorCode.NO_EXIST_CLUB.message }

                val alarmData = ModelAlaram(
                    senderEmail = LoginManager.user?.email,
                    senderImageUrl = response.second.clubMarkImageUrl,
                    timeStamp = LocalTime().time,
                    senderUid = LoginManager.user?.uid!!,
                    senderNickName = LoginManager.user?.nickName,
                    reciverUid = modelAlaram.senderUid,
                    alaramTypeCode = AlarmType.CLUB_MERCENARY_SIGNUP_REJECT.code,
                    message = "${response.second.clubName.wrapWith(
                        "[",
                        "]"
                    )} ${AlarmType.CLUB_MERCENARY_SIGNUP_REJECT.text}"
                )

                response.first.add(alarmData)

                response.second.mercenaryApplicationList?.removeAll { it.uid == modelAlaram.senderUid }

                RxRealTimeDB.push("${FirebaseUrls.club}/${response.second.contentId}", response.second)
                    .flatMap {
                        RxRealTimeDB.push("${FirebaseUrls.alram}/${modelAlaram.senderUid}", response.first)
                    }
                    .flatMap {
                        FcmHelper.sendMessage(
                            destinationUid = modelAlaram.senderUid,
                            message = "${response.second.clubName.wrapWith(
                                "[",
                                "]"
                            )} ${AlarmType.CLUB_MERCENARY_SIGNUP_REJECT.text}"
                        )
                    }
            }
            .withProgressBallAnimation(view.context)
            .flatMap {
                TDialog.Builder(view.context)
                    .setMessage("용병신청을 거절하였습니다.")
                    .isColored(true)
                    .addButton(DialogButton.OK())
                    .toSingle()
            }
            .subscribe({
                TLog.d("reject Success !!")
            }, { e ->
                TDialog.show(view.context, e.toDialogMessage())
                CrashlyticsHelper.logException(e)
            })
            .let { }
    }

}