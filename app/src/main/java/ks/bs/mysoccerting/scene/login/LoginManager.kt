package ks.bs.mysoccerting.scene.login

import android.content.Context
import com.google.firebase.auth.FirebaseAuth
import io.reactivex.Single
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.subjects.BehaviorSubject
import kr.nextm.lib.TLog
import ks.bs.mysoccerting.customview.dialog.DialogButton
import ks.bs.mysoccerting.customview.dialog.TDialog
import ks.bs.mysoccerting.exception.ErrorCode
import ks.bs.mysoccerting.exception.ErrorCodeException
import ks.bs.mysoccerting.firebase.CrashlyticsHelper
import ks.bs.mysoccerting.firebase.FirebaseUrls
import ks.bs.mysoccerting.firebase.RxRealTimeDB
import ks.bs.mysoccerting.model.ModelClub
import ks.bs.mysoccerting.model.ModelUser
import ks.bs.mysoccerting.model.type.DialogNoticeType
import ks.bs.mysoccerting.rx.rxresult.startActivity
import ks.bs.mysoccerting.rx.rxresult.toIntent
import ks.bs.mysoccerting.scene.club.detail.ClubDetailFragment

object LoginManager {
    val auth: FirebaseAuth = FirebaseAuth.getInstance()

    var user: ModelUser? = null

    var userSubject = BehaviorSubject.create<ModelUser>()


    fun isLogin() = user != null

    fun init(compositeDisposable: CompositeDisposable) {
        setUserSnapshot()
        renewalingUserInfoSnapshot(compositeDisposable)
    }

    private fun renewalingUserInfoSnapshot(disposable: CompositeDisposable) {
        userSubject.subscribe({ userInfo ->
            TLog.d("userInfoChange $userInfo")
            user = userInfo
        }, { e ->
            CrashlyticsHelper.logException(e)
        })
            .addTo(disposable)
    }

    private fun setUserSnapshot() {
        val path = "${FirebaseUrls.users}/${auth.currentUser!!.uid}"
        RxRealTimeDB.toObservable(path) { ModelUser() }
            .doOnError { e ->
                CrashlyticsHelper.logException(e)
            }
            .subscribe(userSubject)
    }

    fun getCurrentUser(): Single<ModelUser> {
        val path = "${FirebaseUrls.users}/${auth.currentUser!!.uid}"
        return RxRealTimeDB.toSingle(path) { ModelUser() }

    }

    fun getUserInfo(): Single<ModelUser> {
        return Single.create { emitter ->
            if (user != null)
                emitter.onSuccess(user!!)
            else
                emitter.onError(ErrorCodeException(ErrorCode.NO_CURRENT_LOGIN_USER))
        }

    }

    fun noUserSafeAction(context: Context, disposable: CompositeDisposable, function: () -> Unit) {
        if (auth.currentUser != null)
            function()
        else
            showDialog(context, disposable, DialogNoticeType.LOGIN)
    }


    fun saveUserInfo(model: ModelUser) {
        user = model
    }

    fun noClubSafeAction(context: Context, disposable: CompositeDisposable, function: () -> Unit) {
        if (auth.currentUser != null) {
            require(user != null) { "LoginManger user need not null!! noClubSafeAction" }
            if (user?.clubUid != null) {
                function()
            } else
                showDialog(context, disposable, DialogNoticeType.NEEDCLUB)

        } else
            showDialog(context, disposable, DialogNoticeType.LOGIN)
    }

    fun clubMemberSafeAction(context: Context, disposable: CompositeDisposable, function: () -> Unit) {
        if (auth.currentUser != null) {
            require(user != null) { "LoginManger user need not null!! clubMemberSafeAction" }
            if (user?.clubUid != null) {
                if (user?.clubUid == ClubDetailFragment.CLUB_UID)
                    function()
                else
                    showDialog(context, disposable, DialogNoticeType.NEEDCLUB)
            } else
                showDialog(context, disposable, DialogNoticeType.NEEDCLUB)
        } else
            showDialog(context, disposable, DialogNoticeType.LOGIN)
    }

    fun needLoginToNoCLub(context: Context, disposable: CompositeDisposable, function: () -> Unit) {
        if (auth.currentUser != null) {
            require(user != null) { "LoginManger user need not null!! needLoginToNoCLub" }
            if (user?.clubUid == null) {
                function()

            } else
                showDialog(context, disposable, DialogNoticeType.AREADY_CLUB)
        } else
            showDialog(context, disposable, DialogNoticeType.LOGIN)
    }

    fun needClubAssistantGrade(context: Context, disposable: CompositeDisposable, function: () -> Unit) {
        if (auth.currentUser != null) {
            require(user != null) { "LoginManger user need not null!! needClubAssistantGrade" }
            if (user?.clubUid != null) {
                if (user?.clubAssistantList?.find { it == user?.uid }?.isNotEmpty() == true || user?.clubMasterUid == user?.uid) {
                    function()
                } else
                    showDialog(context, disposable, DialogNoticeType.NEED_GRADE)
            } else
                showDialog(context, disposable, DialogNoticeType.NEEDCLUB)
        } else
            showDialog(context, disposable, DialogNoticeType.LOGIN)
    }

    fun needClubAssistantGradeInvitation(context: Context, disposable: CompositeDisposable, function: () -> Unit) {
        if (auth.currentUser != null) {
            require(user != null) { "LoginManger user need not null!! needClubAssistantGradeInvitation" }
            if (user?.clubUid != null) {
                if (user?.clubAssistantList?.find { it == user?.uid }?.isNotEmpty() == true || user?.clubMasterUid == user?.uid) {
                    function()
                } else
                    showDialog(context, disposable, DialogNoticeType.NEED_GRADE_INVITATION)
            } else
                showDialog(context, disposable, DialogNoticeType.NEEDCLUB)
        } else
            showDialog(context, disposable, DialogNoticeType.LOGIN)
    }

    private fun showDialog(context: Context, disposable: CompositeDisposable, dnt: DialogNoticeType) {
        when (dnt) {
            DialogNoticeType.LOGIN ->
                showNeedDialog(
                    title = "알림",
                    message = "로그인이 필요합니다.\n로그인하시겠습니까?",
                    context = context,
                    disposable = disposable
                ) { moveToLoginActivity(context) }
            DialogNoticeType.NEEDCLUB ->
                showNeedDialog(
                    title = "알림",
                    message = "클럽가입이 필요합니다.\n클럽을 가입해주세요",
                    context = context,
                    disposable = disposable,
                    cancelable = false
                ) {}
            DialogNoticeType.NEED_GRADE -> {
                showNeedDialog(
                    title = "알림",
                    message = "클럽등급 코치 이상만\n작성할 수 있습니다.",
                    context = context,
                    disposable = disposable,
                    cancelable = false
                ) {}
            }
            DialogNoticeType.NEED_GRADE_INVITATION -> {
                showNeedDialog(
                    title = "알림",
                    message = "클럽등급 코치 이상만\n초대할 수 있습니다.",
                    context = context,
                    disposable = disposable,
                    cancelable = false
                ) {}
            }
            else -> showNeedDialog(
                title = "알림",
                message = "이미 클럽에 가입되어 있습니다",
                context = context,
                disposable = disposable,
                cancelable = false
            ) { }
        }

    }

    private fun showNeedDialog(
        title: String,
        message: String,
        context: Context,
        disposable: CompositeDisposable,
        cancelable: Boolean = true,
        afterAction: () -> Unit
    ) {

        if (cancelable)
            TDialog.Builder(context, title)
                .setMessage(message)
                .isColored(true)
                .addButton(DialogButton.CANCEL())
                .addButton(DialogButton.OK())
                .toSingle()
                .filter { it.ok }
                .subscribe({
                    afterAction()
                }, { e ->
                    CrashlyticsHelper.logException(e)
                })
                .addTo(disposable)
        else
            TDialog.Builder(context, title)
                .setMessage(message)
                .addButton(DialogButton.OK())
                .isColored(true)
                .toSingle()
                .filter { it.ok }
                .subscribe({
                    afterAction()
                }, { e ->
                    CrashlyticsHelper.logException(e)
                })
                .addTo(disposable)
    }

    private fun moveToLoginActivity(context: Context) {
        LoginActivity::class.java.toIntent().startActivity(context).subscribe()
    }


    fun clubSecession(): ModelUser {

        return user!!.apply {
            club = null
            clubJonined = false
            clubImageUrl = null
            clubUid = null
            clubMasterUid = null
            clubAssistantList = null
        }
    }

    fun saveLocalUserClubData(data: ModelClub): ModelUser {
        user?.clubUid = data.contentId
        user?.club = data.clubName
        user?.clubJonined = true
        user?.clubImageUrl = data.clubMarkImageUrl
        user?.clubMasterUid = data.masterUid
        return user!!
    }

    fun logout() {
        user = null
    }
}