package ks.bs.mysoccerting.scene.filter

import androidx.databinding.ObservableArrayMap
import android.view.View
import android.widget.Button
import ks.bs.mysoccerting.R
import ks.bs.mysoccerting.model.ModelPosition
import ks.bs.mysoccerting.scene.base.ActivityBaseViewModel

class PositionDialogViewModel(private val contract: PositionDialogActivityContract) : ActivityBaseViewModel() {
    interface PositionDialogActivityContract {
        fun finishDialog()
        fun positionSelectComplete(posList : ObservableArrayMap<String,Boolean>)
    }

    var posList = ModelPosition.getPosList()


    fun okButtonClickEvent() {
        contract.positionSelectComplete(posList)
    }

    fun dialogCloseButtonClickEvent() {
        contract.finishDialog()
    }

    fun positionClick(v: View) {
        posList["direction"] = false
        when (v.id) {
            R.id.AFW -> {
                ModelPosition.pressLine(posList,v.tag.toString())
            }
            R.id.AAM -> {
                ModelPosition.pressLine(posList,v.tag.toString())
            }
            R.id.ACM -> {
                ModelPosition.pressLine(posList,v.tag.toString())
            }
            R.id.ADM -> {
                ModelPosition.pressLine(posList,v.tag.toString())
            }
            R.id.ADF -> {
                ModelPosition.pressLine(posList,v.tag.toString())
            }
            R.id.ALL -> {
                posList["direction"] = true
                posList["ALL"] = !posList["ALL"]!!
            }
            R.id.GK -> {
                posList["GK"] = !posList["GK"]!!
            }
            else -> {
                posList[(v as Button).text.toString()] = !posList[v.text.toString()]!!
                ModelPosition.isPressedLine(posList,v.tag.toString())
            }
        }
        if (posList["direction"]!!) {
            ModelPosition.pressAll(posList)
        } else {
            ModelPosition.isPressedAll(posList)
        }
    }

}
