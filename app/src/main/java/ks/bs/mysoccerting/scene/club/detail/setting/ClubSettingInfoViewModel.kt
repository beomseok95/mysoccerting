package ks.bs.mysoccerting.scene.club.detail.setting

import android.content.pm.PackageManager
import android.graphics.drawable.Drawable
import android.net.Uri
import android.view.View
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.core.net.toUri
import androidx.databinding.ObservableField
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.toObservable
import io.reactivex.rxkotlin.zipWith
import kr.nextm.lib.TToast
import ks.bs.mysoccerting.exception.ErrorCode
import ks.bs.mysoccerting.firebase.CrashlyticsHelper
import ks.bs.mysoccerting.firebase.FirebaseUrls
import ks.bs.mysoccerting.firebase.RxRealTimeDB
import ks.bs.mysoccerting.firebase.RxStorage
import ks.bs.mysoccerting.model.ModelClub
import ks.bs.mysoccerting.model.ModelEditData
import ks.bs.mysoccerting.model.ModelUser
import ks.bs.mysoccerting.model.type.EditType
import ks.bs.mysoccerting.rx.rxresult.getActivity
import ks.bs.mysoccerting.rx.rxresult.putObject
import ks.bs.mysoccerting.rx.rxresult.startActivityForResult
import ks.bs.mysoccerting.rx.rxresult.toIntent
import ks.bs.mysoccerting.rx.withProgressBallAnimation
import ks.bs.mysoccerting.scene.base.ActivityBaseViewModel
import ks.bs.mysoccerting.scene.club.supervisor.updateClubMemberInfo
import ks.bs.mysoccerting.scene.filter.AreaDialog
import ks.bs.mysoccerting.scene.login.LoginManager
import ks.bs.mysoccerting.scene.profile.EditProfileInfoActivity
import ks.bs.mysoccerting.scene.profile.TransparentAlbumActivity
import ks.bs.mysoccerting.support.DontCare
import ks.bs.mysoccerting.util.TLog
import ks.bs.mysoccerting.util.isAgeAll

class ClubSettingInfoViewModel(val contract: Contract) : ActivityBaseViewModel() {
    interface Contract {
        fun getDrawableFromUri(uri: Uri): Drawable
        fun checkExternalStoragePermission()
    }


    val clubName = ObservableField<String>()
    val activityAge = ObservableField<String>()
    val activityArea = ObservableField<String>()
    val clubIntro = ObservableField<String>()
    val clubMarkImageUrl = ObservableField<String>()

    var selectClubMarkUri: String? = null
    var selectClubBackgroundImageUri: String? = null

    var isClickedEdit = ObservableField(false)
    var isEditable = ObservableField(false)
    var modelClub = ModelClub()

    fun getContent(model: ModelClub) {
        modelClub = model
        clubIntro.set(modelClub.clubIntro)
        clubName.set(modelClub.clubName)
        activityAge.set(isAgeAll(modelClub.activityAge))
        activityArea.set(modelClub.activityArea)
        clubMarkImageUrl.set(modelClub.clubMarkImageUrl)

        if (LoginManager.auth.currentUser != null && modelClub.masterUid == LoginManager.auth.uid)
            isClickedEdit.set(true)
    }

    fun backPressed(view: View) {
        val activity = view.getActivity() as ClubSettingInfoActivity
        activity.onBackPressed()
    }

    fun moveEditPage(view: View, child: TextView, isAge: Boolean = false) {
        if (isEditable.get()!!)
            EditProfileInfoActivity::class.java
                .toIntent()
                .putObject(
                    ModelEditData(
                        tag = view.tag.toString(),
                        data = child.text.toString(),
                        hint = EditType.valueOf(view.tag.toString()).hint,
                        isAge = isAge
                    )
                )
                .startActivityForResult<String>(view.context)
                .subscribe({ result ->
                    resultAdd(view.tag.toString(), result)
                }, {
                    CrashlyticsHelper.logException(it)
                })
                .addTo(compositeDisposable)
    }

    private fun resultAdd(tag: String, result: String) {
        when (tag) {
            EditType.ACTIVITY_AGE.tag -> activityAge.set(result)
            EditType.INTRO.tag -> clubIntro.set(result)
        }
    }

    fun showAreaDialog(view: View) {
        if (isEditable.get()!!)
            AreaDialog()
                .toSingle(view.context)
                .subscribe({
                    val city = it.selectedCity
                    val town = it.selectedTown

                    if (town == null && city == null) {
                        return@subscribe
                    }
                    if (town == null) {
                        activityArea.set("$city")
                        return@subscribe
                    }

                    activityArea.set("$city $town")
                }, {
                    CrashlyticsHelper.logException(it)
                    TLog.e(it.message.toString())
                }).let { addDisposable(it) }
    }

    fun onChangeClubMark(view: View) {
        if (ContextCompat.checkSelfPermission(
                view.context,
                android.Manifest.permission.READ_EXTERNAL_STORAGE
            ) == PackageManager.PERMISSION_GRANTED
        ) {
            TransparentAlbumActivity::class.java
                .toIntent()
                .startActivityForResult<String>(view.context)
                .subscribe({ response ->
                    selectClubMarkUri = response
                    clubMarkImageUrl.set(response)
                }, { e ->
                    CrashlyticsHelper.logException(e)
                })
                .addTo(compositeDisposable)
        } else {
            TToast.show("권한없음")
            contract.checkExternalStoragePermission()
        }
    }

    fun onEdit() {
        isClickedEdit.set(!isClickedEdit.get()!!)
        isEditable.set(!isEditable.get()!!)
        selectClubMarkUri = null
        selectClubBackgroundImageUri = null
    }

    fun onSave(view: View) {
        requireNotNull(modelClub.contentId) { ErrorCode.FAIL_NO_EXIST_MY_CLUB_CONTENT_ID.message }

        isClickedEdit.set(!isClickedEdit.get()!!)
        isEditable.set(!isEditable.get()!!)

        if (activityAge.get().isNullOrEmpty() || activityArea.get().isNullOrEmpty() || clubIntro.get().isNullOrEmpty()) {
            TToast.show("모든 정보를 입력해주세요")
            return
        }

        val path = "${FirebaseUrls.club}/${modelClub.contentId}"

        modelClub.clubName = clubName.get().toString()
        modelClub.clubIntro = clubIntro.get().toString()
        modelClub.activityAge = isAgeAll(activityAge.get())
        modelClub.activityArea = activityArea.get().toString()
        modelClub.clubMarkImageUrl = clubMarkImageUrl.get().toString()

        val clubMarkObservable =
            if (selectClubMarkUri != null)
                RxStorage.push("clubProfile", clubMarkImageUrl.get()!!.toUri())
                    .toObservable()
                    .withProgressBallAnimation(view.context)
            else
                Observable.just("")



        clubMarkObservable
            .flatMapSingle {
                val profileImageUrl =
                    if (it.isEmpty())
                        clubMarkImageUrl.get()
                    else
                        it

                modelClub.clubMarkImageUrl = profileImageUrl

                Single.just(modelClub)
            }
            .flatMapSingle { clubData -> RxRealTimeDB.push(path, clubData) }
            .updateClubMemberInfo()
            .subscribe({
                TLog.d("club info change success")
            }, { e ->
                TToast.show("클럽 정보 저장을 실패하였습니다.\n$e")
                CrashlyticsHelper.logException(e)
            })
            .addTo(compositeDisposable)
    }
}