package ks.bs.mysoccerting.scene.filter

import android.content.Intent
import androidx.databinding.DataBindingUtil
import androidx.databinding.ObservableArrayMap
import android.os.Bundle
import androidx.core.math.MathUtils
import androidx.appcompat.app.AppCompatActivity
import android.view.View
import android.view.Window
import kotlinx.android.synthetic.main.activity_position_dialog.*
import ks.bs.mysoccerting.BR
import ks.bs.mysoccerting.R
import ks.bs.mysoccerting.databinding.ActivityPositionDialogBinding
import ks.bs.mysoccerting.model.ModelPosition
import ks.bs.mysoccerting.rx.rxresult.RxActivityBase
import ks.bs.mysoccerting.rx.rxresult.putObject
import ks.bs.mysoccerting.util.dpToPixels

class PositionDialogActivity : RxActivityBase(), PositionDialogViewModel.PositionDialogActivityContract {
    data class PositionList(
        var posList: HashMap<String, Boolean> = hashMapOf()
    )

    val viewModel = PositionDialogViewModel(this)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        val binding =
            DataBindingUtil.setContentView<ActivityPositionDialogBinding>(this, R.layout.activity_position_dialog)
        binding.setVariable(BR.positionViewModel, viewModel)
        initDialogSize()

        if (!getInput<HashMap<String, Boolean>>().isNullOrEmpty()) {
            viewModel.posList = ModelPosition.hashToObservable(getInput())
        }
        setResult(RESULT_OK, Intent().putObject(PositionList()))
    }

    override fun finishDialog() {
        finishWithResponse(PositionList(hashMapOf()))
    }

    override fun positionSelectComplete(posList: ObservableArrayMap<String, Boolean>) {
        val hashList = ModelPosition.observableToHash(posList)
        finishWithResponse(PositionList(hashList))
    }

    private fun initDialogSize() {
        layout_dialog_root.addOnLayoutChangeListener(object : View.OnLayoutChangeListener {
            override fun onLayoutChange(
                v: View,
                left: Int,
                top: Int,
                right: Int,
                bottom: Int,
                oldLeft: Int,
                oldTop: Int,
                oldRight: Int,
                oldBottom: Int
            ) {
                v.removeOnLayoutChangeListener(this)
                val headerHeight = 40.dpToPixels()
                val buttonHeight = 40.dpToPixels()

                val sum = headerHeight + buttonHeight

                val width = 400.dpToPixels()
                val height = MathUtils.clamp(
                    bottom - top,
                    500.dpToPixels() + sum,
                    700.dpToPixels() + sum
                )

                val displayMetrics = baseContext.resources.displayMetrics
                v.layoutParams.width = minOf(width, displayMetrics.widthPixels)
                v.layoutParams.height = minOf(height, displayMetrics.heightPixels)
                window.setLayout(v.layoutParams.width, v.layoutParams.height)
            }
        })
    }

    override fun onBackPressed() {
        finishDialog()
        super.onBackPressed()
    }


    override fun onDestroy() {
        viewModel.onDestroy()
        super.onDestroy()
    }


}
