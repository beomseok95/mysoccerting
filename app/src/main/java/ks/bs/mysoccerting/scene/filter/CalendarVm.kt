package ks.bs.mysoccerting.scene.filter

import android.view.View
import com.ptrstovka.calendarview2.CalendarDay
import com.ptrstovka.calendarview2.CalendarView2
import ks.bs.mysoccerting.scene.base.FragmentBaseViewModel
import ks.bs.mysoccerting.util.convertFormat
import ks.bs.mysoccerting.util.transToMonthAndDay


class CalendarVm(override val listener: Contract) : FragmentBaseViewModel(listener) {

    interface Contract : BaseVmListener {
        fun finishDialog()
        fun dateSelectComplete(firstSelectDay: String?, lastSelectDay: String?)
        fun getCalendar(): CalendarView2
    }

    var firstSelectDay: String? = null
    var lastSelectDay: String? = null

    fun onRangeSelectedListener(widget: CalendarView2, dates: MutableList<CalendarDay>) {
        val firstDay = dates[0]
        val lastDay = dates[dates.size - 1]
        firstSelectDay = firstDay.convertFormat().transToMonthAndDay()
        lastSelectDay = lastDay.convertFormat().transToMonthAndDay()
    }

    fun onDateChangedListener(widget: CalendarView2, date: CalendarDay, selected: Boolean) {
        if (!lastSelectDay.isNullOrBlank()) {
            firstSelectDay = null
            lastSelectDay = null
        }
        firstSelectDay = date.convertFormat().transToMonthAndDay()
    }

    fun resetBtnClickEvent() {
        firstSelectDay = null
        lastSelectDay = null
        listener.getCalendar().clearSelection()
    }

    fun dialogCloseButtonClickEvent() {
        listener.finishDialog()
    }

    fun cancel(view: View) {
        listener.finishDialog()
    }

    fun ok(view: View) {
        if (firstSelectDay.isNullOrEmpty() && lastSelectDay.isNullOrEmpty()) {
            listener.dateSelectComplete(null, null)
        } else if (!firstSelectDay.isNullOrEmpty() && lastSelectDay.isNullOrEmpty()) {
            requireNotNull(firstSelectDay) { "FirstSelectDay is Empty" }
            listener.dateSelectComplete(firstSelectDay!!, null)
        } else {
            require(!firstSelectDay.isNullOrEmpty() || !lastSelectDay.isNullOrEmpty()) { "FirstSelectDay And LastSelectDay is Empty" }
            listener.dateSelectComplete(firstSelectDay!!, lastSelectDay)
        }
    }
}