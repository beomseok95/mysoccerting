package ks.bs.mysoccerting.scene.profile

import android.app.Activity
import android.content.Intent
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.storage.FirebaseStorage
import ks.bs.mysoccerting.R
import ks.bs.mysoccerting.rx.rxresult.RxActivityBase
import ks.bs.mysoccerting.rx.toJson
import org.jetbrains.anko.toast

class TransparentAlbumActivity : RxActivityBase() {

    private val PICK_PROFILE_FROM_ALBUM = 10

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)


        val action =
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                Intent.ACTION_OPEN_DOCUMENT
            } else {
                Intent.ACTION_PICK
            }

        val photoPickerIntent = Intent(action)
        photoPickerIntent.type = "image/*"
        this.startActivityForResult(photoPickerIntent, PICK_PROFILE_FROM_ALBUM)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == PICK_PROFILE_FROM_ALBUM && resultCode == Activity.RESULT_OK) {
            val imageUri = data?.data
            finishWithResponse(imageUri.toString())
        } else
            finishWithCancel()
    }
}
