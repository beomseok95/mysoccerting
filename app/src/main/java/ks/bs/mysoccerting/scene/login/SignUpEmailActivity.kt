package ks.bs.mysoccerting.scene.login

import android.content.Intent
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import com.google.firebase.auth.FirebaseUser
import kotlinx.android.synthetic.main.activity_sign_up_email.*
import kr.nextm.lib.TToast
import ks.bs.mysoccerting.R
import ks.bs.mysoccerting.databinding.ActivitySignUpEmailBinding
import ks.bs.mysoccerting.scene.base.ActivityBase
import ks.bs.mysoccerting.scene.home.MainActivity
import org.jetbrains.anko.startActivity

class SignUpEmailActivity : ActivityBase(), SignUpEmailViewModel.SIgnUpActivityContract {

    val model = SignUpEmailViewModel(this)

    override fun onCreate(savedInstanceState: Bundle?) {
        isLoginActivity = true
        super.onCreate(savedInstanceState)
        val binding = DataBindingUtil.setContentView<ActivitySignUpEmailBinding>(this, R.layout.activity_sign_up_email)
        binding.signUpViewModel = model
        model.init()
        toolbar_title.text ="회원가입"
    }


    override fun showToast(message: String) {
        TToast.show(message)
    }

    override fun moveMainPage(user: FirebaseUser?) {
        if (user != null) {
//            TToast.show("${getString(R.string.signin_complete)} \n 로그인정보 ${user.email}")

            val intent = Intent(this, MainActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
            startActivity(intent)

            finish()
            return
        }
        TToast.show("사용자정보가 없습니다.")
    }

    override fun onDestroy() {
        model.onDestroy()
        super.onDestroy()
    }
}
