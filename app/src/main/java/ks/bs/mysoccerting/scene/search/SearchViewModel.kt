package ks.bs.mysoccerting.scene.search

import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.EditText
import android.widget.TextView
import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import com.google.android.material.tabs.TabLayout
import io.reactivex.Single
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import ks.bs.mysoccerting.BR
import ks.bs.mysoccerting.R
import ks.bs.mysoccerting.databinding.FilteredRecyclerViewAdapter
import ks.bs.mysoccerting.databinding.ItemSearchClubBinding
import ks.bs.mysoccerting.databinding.ItemSearchMercenaryBinding
import ks.bs.mysoccerting.firebase.CrashlyticsHelper
import ks.bs.mysoccerting.firebase.FirebaseHelper
import ks.bs.mysoccerting.model.Area
import ks.bs.mysoccerting.model.ModelBase
import ks.bs.mysoccerting.model.ModelClubSearch
import ks.bs.mysoccerting.model.ModelMercenarySearch
import ks.bs.mysoccerting.scene.base.FragmentBaseViewModel
import ks.bs.mysoccerting.scene.filter.CalendarDialog
import ks.bs.mysoccerting.scene.filter.PositionDialog
import ks.bs.mysoccerting.scene.login.LoginManager
import ks.bs.mysoccerting.util.EditKeyborad
import ks.bs.mysoccerting.util.TLog
import kotlin.collections.HashMap

class SearchViewModel(private val contract: SearchFragmentContract) : FragmentBaseViewModel(contract), EditKeyborad {

    interface SearchFragmentContract : BaseVmListener {
        fun showToast(message: String)
        fun showPeopleNumberDialog(): Single<MutableList<String>>
        fun showAreaDialog(): Single<Area>
        fun showCalendarDialog(): Single<CalendarDialog.Date>
        fun moveItemDetailPageMercenary(item: ModelMercenarySearch): Single<ModelBase>
        fun moveItemDetailPageClub(item: ModelClubSearch): Single<ModelBase>
        fun resetViewModel()
        fun movePostWirteActivity(currentPage: String): Single<Any>
        fun getDisposable(): CompositeDisposable
        fun resetFlexBoxView()
    }

    var tabIsFirst = true

    var positionList = HashMap<String, Boolean>()

    var cloudInfoViewTextList = ObservableField<MutableList<String>>(mutableListOf())
    var peopleNumberFilterList = mutableListOf<String>()
    var dateFilterList = mutableListOf<String>()
    var cityFilterList = mutableListOf<String>()
    var positionFilterList = mutableListOf<String>()
    val editHint = ObservableField("초대할 용병을 검색해보세요.")
    var adapterVisibility = ObservableBoolean(true)
    var isFiltered = ObservableBoolean(false)

    val mercenaryAdapter = FilteredRecyclerViewAdapter<SearchMercenaryViewModel, ItemSearchMercenaryBinding>(
        R.layout.item_search_mercenary,
        BR.mercenarySearch
    )
    val clubAdapter = FilteredRecyclerViewAdapter<SearchClubViewModel, ItemSearchClubBinding>(
        R.layout.item_search_club,
        BR.clubSearch
    )

    override var searchText = ObservableField("")

    override val editTextActionListener =
        TextView.OnEditorActionListener { v, actionId, event -> initEditAction(actionId, v) }

    private fun initEditAction(actionId: Int, v: TextView?): Boolean {
        when (actionId) {
            EditorInfo.IME_ACTION_SEARCH -> {
                TLog.d("actionSearch KeyBorad Hide")
                isFiltered.set(true)
                changeClubFilter()
                changeMercenaryFilter()
                hideKeyBorad(v)
            }

            else -> {
                TLog.d("Nomal KeyBorad Hide")
                changeClubFilter()
                changeMercenaryFilter()
                hideKeyBorad(v)
                return false
            }
        }
        return true
    }

    val tabLayoutListener = object : TabLayout.OnTabSelectedListener {
        override fun onTabReselected(p0: TabLayout.Tab?) {
        }

        override fun onTabUnselected(p0: TabLayout.Tab?) {
        }

        override fun onTabSelected(tab: TabLayout.Tab?) {
            val pageStatus = tab!!.position == MERCENARY_PAGE
            tabIsFirst = pageStatus
            if (tabIsFirst)
                editHint.set("초대할 용병을 검색해보세요.")
            else
                editHint.set("용병구인중인 클럽을 검색해보세요.")
            adapterVisibility.set(pageStatus)
            filterRefresh()
        }
    }

    private fun getContentMercenary() {
        FirebaseHelper.searchMercenarySubject
            .map { it.values.toList() }
            .subscribe({ response ->

                mercenaryAdapter.clear()

                response.map { responseModel ->
                    mercenaryAdapter.insert(responseModel.let {
                        SearchMercenaryViewModel(
                            it,
                            contract
                        )
                    })
                }

                mercenaryAdapter.items.sortByDescending { it.timestamp }
            }, { CrashlyticsHelper.logException(it) })
            .addTo(compositeDisposable)
    }

    private fun getContentClub() {
        FirebaseHelper.searchClubSubject
            .map { it.values.toList() }
            .subscribe({ response ->
                clubAdapter.clear()

                response.map { responseModel ->
                    clubAdapter.insert(responseModel.let {
                        SearchClubViewModel(
                            it,
                            contract
                        )
                    })
                }

                clubAdapter.items.sortByDescending { it.timestamp }
            }, { CrashlyticsHelper.logException(it) })
            .addTo(compositeDisposable)
    }


    fun showAreaDialog() {
        contract.showAreaDialog()
            .subscribe({
                val city = it.selectedCity?.trim()
                val town = it.selectedTown?.trim()

                if (town == null && city == null) return@subscribe

                if (town == null) {
                    cityFilterList = mutableListOf("$city")
                    contract.resetFlexBoxView()
                    cloudInfoViewTextList.set((peopleNumberFilterList + dateFilterList + positionFilterList + cityFilterList).toMutableList())
                    changeClubFilter()
                    changeMercenaryFilter()
                    isFiltered.set(true)
                    return@subscribe
                }

                cityFilterList.map { index ->
                    if (index.startsWith("$city")) {
                        cityFilterList.remove("$city")
                        contract.resetFlexBoxView()
                        cityFilterList.add("$city $town")
                        cloudInfoViewTextList.set((peopleNumberFilterList + dateFilterList + positionFilterList + cityFilterList).toMutableList())
                        changeClubFilter()
                        changeMercenaryFilter()
                        isFiltered.set(true)
                        return@subscribe
                    }
                }

                cityFilterList = mutableListOf("$city $town")
                contract.resetFlexBoxView()
                cloudInfoViewTextList.set((peopleNumberFilterList + dateFilterList + positionFilterList + cityFilterList).toMutableList())
                changeClubFilter()
                changeMercenaryFilter()
                isFiltered.set(true)

            }, { e ->
                CrashlyticsHelper.logException(e)
            }).let { addDisposable(it) }
    }


    fun showCalendarDialog() {
        contract.showCalendarDialog()
            .subscribe({ date ->

                val firstDate = date.firstDate
                val lastDate = date.lastDate

                if (firstDate == null)
                    return@subscribe

                dateFilterList =
                    if (lastDate == null) {
                        mutableListOf("$firstDate")
                    } else {
                        mutableListOf("$firstDate-$lastDate")
                    }

                contract.resetFlexBoxView()
                cloudInfoViewTextList.set((peopleNumberFilterList + positionFilterList + cityFilterList + dateFilterList).toMutableList())
                changeClubFilter()
                changeMercenaryFilter()
                isFiltered.set(true)

            }, { e ->
                CrashlyticsHelper.logException(e)
            }).let { addDisposable(it) }
    }

    fun showPositionDialog(view: View) {
        PositionDialog()
            .toSingle(view.context)
            .subscribe({ result ->
                positionFilterList = mutableListOf(result)
                contract.resetFlexBoxView()
                cloudInfoViewTextList.set((peopleNumberFilterList + cityFilterList + dateFilterList + positionFilterList).toMutableList())
                changeClubFilter()
                changeMercenaryFilter()
                isFiltered.set(true)
            }, { e ->
                CrashlyticsHelper.logException(e)
            })
            .addTo(compositeDisposable)
    }

    fun movePostWritePage(view: View) {
        if (tabIsFirst)
            LoginManager.noUserSafeAction(view.context, compositeDisposable) {
                contract.movePostWirteActivity(SearchFragment.CALL_TAG_SEARCH_MERCENARY)
                    .subscribe({}, { e -> TLog.e(e) })
                    .addTo(compositeDisposable)
            }
        else
            LoginManager.needClubAssistantGrade(view.context, compositeDisposable) {
                contract.movePostWirteActivity(SearchFragment.CALL_TAG_SEARCH_CLUB)
                    .subscribe({}, { e -> TLog.e(e) })
                    .addTo(compositeDisposable)
            }
    }

    private fun changeClubFilter() {
        cityFilterList.forEach { it.toLowerCase() }
        positionFilterList.forEach { it.toLowerCase() }
        dateFilterList.forEach { it.toLowerCase() }


        if (searchText.get()!!.isNotEmpty())
            clubAdapter.changeFilter {
                cityFilterList.contains(it.hopeArea.toLowerCase()) ||
                        cityFilterList.contains(it.hopeArea.split(" ")[0]) ||
                        positionFilterList.joinToString().toLowerCase().contains(it.position?.toLowerCase().toString()) ||
                        dateFilterList.contains(it.hopeDate.toLowerCase()) ||
                        it.hopeArea.contains(searchText.get().toString()) ||
                        it.clubName.contains(searchText.get().toString()) ||
                        it.content.contains(searchText.get().toString()) ||
                        it.hopeDate.contains(searchText.get().toString()) ||
                        it.age.contains(searchText.get().toString()) ||
                        (dateFilterList.isNotEmpty() && it.hopeDate.toLowerCase().contains(dateFilterList.joinToString()))
            }
        else
            clubAdapter.changeFilter {
                cityFilterList.contains(it.hopeArea.toLowerCase()) ||
                        cityFilterList.contains(it.hopeArea.split(" ")[0]) ||
                        positionFilterList.joinToString().toLowerCase().contains(it.position?.toLowerCase().toString()) ||
                        (dateFilterList.isNotEmpty() && it.hopeDate.toLowerCase().contains(dateFilterList.joinToString()))
            }
    }

    private fun changeMercenaryFilter() {
        cityFilterList.forEach { it.toLowerCase() }
        positionFilterList.forEach { it.toLowerCase() }
        dateFilterList.forEach { it.toLowerCase() }

        if (searchText.get()!!.isNotEmpty())
            mercenaryAdapter.changeFilter {
                cityFilterList.contains(it.hopeArea.toLowerCase()) ||
                        cityFilterList.contains(it.hopeArea.split(" ")[0]) ||
                        positionFilterList.joinToString().toLowerCase().contains(it.position?.toLowerCase().toString()) ||
                        dateFilterList.contains(it.hopeDate.toLowerCase()) ||
                        it.hopeArea.contains(searchText.get().toString()) ||
                        it.userId.contains(searchText.get().toString()) ||
                        it.content.contains(searchText.get().toString()) ||
                        it.hopeDate.contains(searchText.get().toString()) ||
                        it.age.contains(searchText.get().toString()) ||
                        (dateFilterList.isNotEmpty() && it.hopeDate.toLowerCase().contains(dateFilterList.joinToString()))
            }
        else
            mercenaryAdapter.changeFilter {
                cityFilterList.contains(it.hopeArea.toLowerCase()) ||
                        cityFilterList.contains(it.hopeArea.split(" ")[0]) ||
                        positionFilterList.joinToString().toLowerCase().contains(it.position?.toLowerCase().toString()) ||
                        (dateFilterList.isNotEmpty() && it.hopeDate.toLowerCase().contains(dateFilterList.joinToString()))
            }
    }

    fun init() {
        getContentMercenary()
        getContentClub()
    }


    fun filterRefresh() {
        searchText.set("")

        isFiltered.set(false)
        mercenaryAdapter.resetFilter()
        clubAdapter.resetFilter()
        contract.resetFlexBoxView()
        mercenaryAdapter.items.sortByDescending { it.timestamp }
        clubAdapter.items.sortByDescending { it.timestamp }

        peopleNumberFilterList = mutableListOf()
        dateFilterList = mutableListOf()
        cityFilterList = mutableListOf()
        positionFilterList = mutableListOf()
    }

    fun onSearch(editText: EditText) {
        if (tabIsFirst)
            changeMercenaryFilter()
        else
            changeClubFilter()
        if (editText.text.isNotEmpty())
            isFiltered.set(true)
        hideKeyBorad(editText)
    }

    companion object {
        const val MERCENARY_PAGE = 0
        const val CLUB_PAGE = 1
    }
}