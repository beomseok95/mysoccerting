package ks.bs.mysoccerting.scene.common

import android.content.Intent
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import io.reactivex.Single
import kotlinx.android.synthetic.main.activity_item_detail.*
import kr.nextm.lib.TToast
import ks.bs.mysoccerting.BR
import ks.bs.mysoccerting.R
import ks.bs.mysoccerting.customview.dialog.TDialog
import ks.bs.mysoccerting.databinding.ActivityItemDetailBinding
import ks.bs.mysoccerting.rx.rxresult.RxActivityBase
import ks.bs.mysoccerting.rx.rxresult.startActivityForResult
import ks.bs.mysoccerting.scene.match.MatchingFragment
import ks.bs.mysoccerting.scene.search.SearchFragment

class DetailItemActivity : RxActivityBase() {
    private val detailItemViewModel = createVm()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        DataBindingUtil.setContentView<ActivityItemDetailBinding>(this, R.layout.activity_item_detail)
            .setVariable(BR.viewModel, detailItemViewModel)

        toolbar_title.text ="게시글 상세"

        if (CALL_LOCATION == SearchFragment.CALL_TAG_SEARCH_MERCENARY)
            detailItemViewModel.setSearchMercenaryItem(getInput())
        if (CALL_LOCATION == MatchingFragment.CALL_TAG_MATCHING)
            detailItemViewModel.setMatchClubItem(getInput())
        if (CALL_LOCATION == SearchFragment.CALL_TAG_SEARCH_CLUB)
            detailItemViewModel.setSearchClubItem(getInput())
    }

    private fun createVm() = DetailItemViewModel(object : DetailItemViewModel.DetailItemContract {
        override fun moveAmendActivity(): Single<String> {
            return Intent(this@DetailItemActivity, CommentAmendActivity::class.java)
                .startActivityForResult(this@DetailItemActivity)
        }

        override fun finishPage(moveToClubPage: Boolean) {
            finishWithResponse(moveToClubPage)
        }

        override fun showCommentSubmitDialog(): TDialog.Builder {
            return TDialog.Builder(this@DetailItemActivity, "알림")
        }

        override fun showToast(message: String) {
            TToast.show(message)
        }
    })


    override fun onBackPressed() {
        finishWithResponse("")
    }

    companion object {
        var CALL_LOCATION = ""
    }

    override fun onDestroy() {
        detailItemViewModel.onDestroy()
        super.onDestroy()
    }
}