package ks.bs.mysoccerting.scene.club.detail.setting

import android.os.Bundle
import androidx.databinding.DataBindingUtil
import kotlinx.android.synthetic.main.activity_club_members_info.*
import kr.nextm.lib.TToast
import ks.bs.mysoccerting.R
import ks.bs.mysoccerting.databinding.ActivityClubMembersInfoBinding
import ks.bs.mysoccerting.model.ModelClub
import ks.bs.mysoccerting.rx.rxresult.RxActivityBase
import ks.bs.mysoccerting.rx.rxresult.getObject

class ClubMembersInfoActivity : RxActivityBase() {

    private val viewModel = createVm()

    private fun createVm() = ClubMembersInfoViewModel(object :
        ClubMembersInfoViewModel.Contract{
        override fun backPressEvent() {
            finishWithCancel()
        }

        override fun delegated(msg: String) {
            finishWithResponse(msg)
        }
    })

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        DataBindingUtil.setContentView<ActivityClubMembersInfoBinding>(this, R.layout.activity_club_members_info)
            .viewModel=viewModel


        toolbar_title.text= "클럽원 관리"
        viewModel.getContents(
            modelClub = intent.getObject()
        )
    }

    override fun onDestroy() {
        viewModel.clearDisposables()
        super.onDestroy()
    }

    override fun onBackPressed() {
        finishWithResponse("")
    }

    companion object{
        const val DELEGATED = "delegate"
    }
}