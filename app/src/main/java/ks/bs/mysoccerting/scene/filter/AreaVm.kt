package ks.bs.mysoccerting.scene.filter

import android.content.Context
import android.graphics.Color
import android.view.View
import androidx.databinding.ObservableArrayMap
import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import androidx.databinding.ObservableMap
import io.reactivex.Observable
import jxl.Sheet
import jxl.Workbook
import kotlinx.android.synthetic.main.view_select_area.view.*
import ks.bs.mysoccerting.R
import ks.bs.mysoccerting.customview.SelectAreaView
import ks.bs.mysoccerting.customview.setContent
import ks.bs.mysoccerting.rx.networkThread
import ks.bs.mysoccerting.scene.base.FragmentBaseViewModel


class AreaVm(override val listener: Contract) : FragmentBaseViewModel(listener) {

    interface Contract : BaseVmListener {
        fun selectComplete(selectedCity: String?, selectedTown: String?)
        fun onBackPressed()
        fun getContext(): Context
        fun showToast(message: String)
        fun resetCityGridLayout()
    }

    val isSelectedCityAreaView = ObservableArrayMap<SelectAreaView, Boolean>()
    val isSelectTownAreaView = ObservableArrayMap<SelectAreaView, Boolean>()
    val selectCityAreaView = ObservableField<List<SelectAreaView>>()
    val selectTownAreaView = ObservableField<List<SelectAreaView>>()
    val cityVisibility = ObservableBoolean(true)
    val townVisibility = ObservableBoolean(false)
    val completeBtnVisibility = ObservableBoolean(true)
    val cancelText = ObservableField("취소")

    private var selectedCity: String? = null
    private var selectedCode: String? = null
    private var selectedTown: String? = null

    var cityList = mutableMapOf<String, String>()


    fun setCityViewToLayout() {
        listener.resetCityGridLayout()
        loadCell(0, 1)
            .subscribe { cities ->
                cityList = cities
                addCityView(cities)
            }.let { addDisposable(it) }
    }

    private fun loadCell(columnCode: Int, columnName: Int): Observable<MutableMap<String, String>> {
        var workbook: Workbook
        var sheet: Sheet
        return Observable.create { emitter ->
            val inputStream = listener.getContext().resources.assets.open("area_list.xls")
            workbook = Workbook.getWorkbook(inputStream)
            sheet = workbook.getSheet(0)
            val rowStart = 2
            val rowEnd = 3483
            val loadedList = mutableMapOf<String, String>()
            for (row in rowStart..rowEnd) {
                val code = sheet.getCell(columnCode, row).contents
                val name = sheet.getCell(columnName, row).contents
                loadedList[code] = name
            }
            emitter.onNext(loadedList)
            workbook.close()
            emitter.onComplete()
        }
    }

    private fun addCityView(cities: MutableMap<String, String>) {
        val cityList = mutableListOf<SelectAreaView>()
        for ((cityCode, cityName) in cities) {
            cityList.add(SelectAreaView(listener.getContext()).setContent(cityName) {
                if (!isSelectedCityAreaView[it]!!) {
                    selectedCode = cityCode
                    selectedCity = cityName
                    for (i in 0 until isSelectedCityAreaView.size) {
                        isSelectedCityAreaView.setValueAt(i, false)
                    }

                    isSelectedCityAreaView[it] = !isSelectedCityAreaView[it]!!
                    setSelectView(it, isSelectedCityAreaView[it]!!)
                } else {
                    if (selectedCode == cityCode) {
                        selectedCode = ""
                        selectedCity = ""
                    }
                    isSelectedCityAreaView[it] = !isSelectedCityAreaView[it]!!
                    setSelectView(it, isSelectedCityAreaView[it]!!)
                }
            }.also {
                isSelectedCityAreaView[it] = false
            })
        }
        isSelectedCityAreaView.addOnMapChangedCallback(listListener)
        selectCityAreaView.set(cityList)

    }

    private fun setSelectView(view: SelectAreaView, isSelected: Boolean) {
        if (isSelected) {
            view.imageView.setBackgroundResource(R.drawable.bg_filter_selected)
            view.imageView.setTextColor(Color.WHITE)
        } else {
            view.imageView.setBackgroundResource(R.drawable.bg_filter_default)
            view.imageView.setTextColor(view.resources.getColor(R.color.colorPrimary))
        }
    }

    fun nextBtnClickEvent() {
        if (selectedCode.isNullOrEmpty()) {
            listener.showToast("도시를 선택해 주세요")
            return
        }

        val filteringTownList = mutableListOf<SelectAreaView>()
        loadCell(2, 3)
            .networkThread()
            .subscribe {
                completeBtnVisibility.set(false)
                cityVisibility.set(false)
                townVisibility.set(true)

                for ((townCode, townName) in it) {
                    if (townCode.startsWith(selectedCode!!)) {
                        setCityViewToLayout()
                        filteringTownList.add(SelectAreaView(listener.getContext())
                            .setContent(townName) { select ->
                                if (!isSelectTownAreaView[select]!!) {
                                    selectedTown = townName

                                    isSelectTownAreaView.forEach {
                                        setSelectView(it.key, false)
                                    }

                                    for (i in 0 until isSelectTownAreaView.size) {
                                        isSelectTownAreaView.setValueAt(i, false)
                                    }

                                    isSelectTownAreaView[select] = !isSelectTownAreaView[select]!!
                                    setSelectView(select, isSelectTownAreaView[select]!!)
                                } else {
                                    selectedTown = null
                                    isSelectTownAreaView[select] = !isSelectTownAreaView[select]!!
                                    setSelectView(select, isSelectTownAreaView[select]!!)
                                }
                            }
                            .also { townView ->
                                isSelectTownAreaView[townView] = false
                            })
                        selectTownAreaView.set(filteringTownList)
                        isSelectedCityAreaView.addOnMapChangedCallback(listListener)
                    }
                }

                cancelText.set("이전")

            }.let { addDisposable(it) }
    }

    private val listListener =
        object : ObservableMap.OnMapChangedCallback<ObservableMap<SelectAreaView, Boolean>, SelectAreaView, Boolean>() {
            override fun onMapChanged(sender: ObservableMap<SelectAreaView, Boolean>?, key: SelectAreaView?) {
                sender?.forEach {
                    setSelectView(it.key, it.value)
                }
            }
        }


    fun cancel(view: View) {
        listener.onBackPressed()
        cancelText.set("취소")
        selectedCode = ""
        selectedCity = null
        selectedTown = null
    }

    fun ok(view: View) {
        listener.selectComplete(selectedCity, selectedTown)
    }
}