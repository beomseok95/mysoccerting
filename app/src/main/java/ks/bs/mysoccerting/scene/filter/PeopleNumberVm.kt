package ks.bs.mysoccerting.scene.filter

import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.databinding.ObservableArrayMap
import androidx.databinding.ObservableField
import androidx.databinding.ObservableMap
import ks.bs.mysoccerting.BR
import ks.bs.mysoccerting.R
import ks.bs.mysoccerting.databinding.BaseRecyclerViewAdapter
import ks.bs.mysoccerting.databinding.ItemPeopleNumberBinding
import ks.bs.mysoccerting.model.ModelPeopleNumber
import ks.bs.mysoccerting.scene.base.ActivityBaseViewModel
import ks.bs.mysoccerting.util.App
import java.util.Comparator
import kotlin.collections.ArrayList
import kotlin.collections.set


class PeopleNumberVm(private val contract: Contract) : ActivityBaseViewModel() {
    interface Contract {
        fun peopleNumberSelectComplete(numberList: MutableList<String>)
        fun finishDialog()
    }

    private var currentPosition = 0
    private var spinnerId = 0

    lateinit var spinnerList: ArrayList<ArrayAdapter<String>>
    lateinit var currentNumberList: ObservableArrayMap<String, Boolean>
    val adapter = BaseRecyclerViewAdapter<ModelPeopleNumber, ItemPeopleNumberBinding>(
        R.layout.item_people_number,
        BR.peopleNumber,
        this,
        BR.viewModel
    )

    fun okButtonClickEvent() {
        contract.peopleNumberSelectComplete(currentNumberList
            .filter { it.value == false }
            .map { it.key }
            .toMutableList())
    }

    fun dialogCloseButtonClickEvent() {
        contract.finishDialog()
    }

    fun addGameNumber() {
        spinnerList.add(
            ArrayAdapter(
                App.getApplicationContext(),
                R.layout.custom_simple_dropdown_item_1line,
                currentNumberList
                    .filter { it.value }
                    .map { it.key }
                    .toMutableList()
            )
        )
        adapter.insert(
            ModelPeopleNumber(spinnerId,
                ObservableField(0),
                ObservableField(sortList(currentNumberList
                    .filter { it.value }
                    .map { it.key }
                    .toMutableList())[0]),
                currentNumberList
                    .filter { it.value }
                    .map { it.key }
                    .toMutableList(),
                spinnerList[spinnerId++]
            )
        )
        adapter.notifyDataSetChanged()
    }

    fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long, item: ModelPeopleNumber) {
        currentPosition = position
        currentNumberList[item.peopleNumber.get()] = true
        item.pos.set(position)
        item.peopleNumber.set(item.numberList[position])
        currentNumberList[item.peopleNumber.get()] = false
    }

    fun onSelectAll() {
        contract.peopleNumberSelectComplete(mutableListOf("모든 인원"))
    }

    fun init() {
        spinnerList = ArrayList()
        currentNumberList = ObservableArrayMap()
        spinnerId = 0
        val numberList = mutableListOf("5 vs 5", "6 vs 6", "7 vs 7", "8 vs 8", "9 vs 9", "10 vs 10", "11 vs 11")
        spinnerList.add(
            ArrayAdapter(
                App.getApplicationContext(),
                R.layout.custom_simple_dropdown_item_1line,
                numberList
            )
        )

        adapter.items.add(
            ModelPeopleNumber(
                spinnerId, ObservableField(6), ObservableField("11 vs 11")
                , numberList.toMutableList(), spinnerList[spinnerId++]
            )
        )
        mutableListOf("5 vs 5", "6 vs 6", "7 vs 7", "8 vs 8", "9 vs 9", "10 vs 10", "11 vs 11").forEach {
            currentNumberList[it] = true
        }
        currentNumberList.addOnMapChangedCallback(listListener)
        adapter.notifyDataSetChanged()
    }

    private val listListener =
        object : ObservableMap.OnMapChangedCallback<ObservableMap<String, Boolean>, String, Boolean>() {
            override fun onMapChanged(sender: ObservableMap<String, Boolean>?, key: String?) {
                adapter.items.indices.forEach { idx ->
                    val item = adapter.items[idx]
                    item.numberList.clear()
                    sender?.filter {
                        it.value == true
                    }?.forEach { notSelected ->
                        item.numberList.add(notSelected.key)
                    }
                    if (!item.numberList.contains(item.peopleNumber.get())) {
                        item.numberList.add(item.peopleNumber.get()!!)
                    }

                    item.numberList = sortList(item.numberList)

                    if (item.pos.get() != currentPosition) {
                        item.pos.set(item.numberList.indexOf(item.peopleNumber.get()!!))
                    }
                    spinnerList[idx].clear()
                    spinnerList[idx].addAll(item.numberList)
                    spinnerList[idx].notifyDataSetChanged()
                }
            }
        }

    fun sortList(numberList: MutableList<String>): MutableList<String> {
        numberList.sortWith(Comparator { pos1, pos2 ->
            pos1.substring(0, 2).trim().toInt()
                .compareTo(pos2.substring(0, 2).trim().toInt())
        })
        return numberList
    }


    fun cancel(view: View) {
        contract.finishDialog()
    }

    fun ok(view: View) {
        contract.peopleNumberSelectComplete(currentNumberList.filter { it.value == false }.map { it.key }.toMutableList())
    }
}