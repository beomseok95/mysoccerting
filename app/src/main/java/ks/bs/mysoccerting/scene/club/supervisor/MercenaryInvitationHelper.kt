package ks.bs.mysoccerting.scene.club.supervisor

import android.view.View
import io.reactivex.Single
import io.reactivex.rxkotlin.zipWith
import ks.bs.mysoccerting.R
import ks.bs.mysoccerting.customview.dialog.DialogButton
import ks.bs.mysoccerting.customview.dialog.DialogObservable
import ks.bs.mysoccerting.customview.dialog.TDialog
import ks.bs.mysoccerting.customview.dialog.TextContentParam
import ks.bs.mysoccerting.exception.ErrorCode
import ks.bs.mysoccerting.exception.toDialogMessage
import ks.bs.mysoccerting.fcm.FcmHelper
import ks.bs.mysoccerting.firebase.CrashlyticsHelper
import ks.bs.mysoccerting.firebase.FirebaseUrls
import ks.bs.mysoccerting.firebase.RxRealTimeDB
import ks.bs.mysoccerting.model.ModelAlaram
import ks.bs.mysoccerting.model.ModelClub
import ks.bs.mysoccerting.model.ModelUser
import ks.bs.mysoccerting.model.time.LocalTime
import ks.bs.mysoccerting.model.type.AlarmType
import ks.bs.mysoccerting.model.type.ClubGradeType
import ks.bs.mysoccerting.model.type.TimelineType
import ks.bs.mysoccerting.rx.withProgressBallAnimation
import ks.bs.mysoccerting.rx.withProgressBallDialog
import ks.bs.mysoccerting.scene.login.LoginManager
import ks.bs.mysoccerting.util.TLog
import ks.bs.mysoccerting.util.getString
import ks.bs.mysoccerting.util.wrapWith

object MercenaryInvitationHelper {

    fun mercenaryInvitationProccess(view: View, modelAlaram: ModelAlaram) {

        requireNotNull(modelAlaram.senderUid) { ErrorCode.FAIL_NO_SENDER_UID.message }
        requireNotNull(modelAlaram.reciverUid) { ErrorCode.FAIL_NO_RECIVER_UID.message }

        var clubModel: ModelClub
        var userModel: ModelUser
        RxRealTimeDB.toSingle("${FirebaseUrls.users}/${modelAlaram.reciverUid}") { ModelUser() }
            .zipWith(RxRealTimeDB.toSingle("${FirebaseUrls.club}/${modelAlaram.clubUid}") { ModelClub() })
            .flatMap { response ->
                userModel = response.first
                clubModel = response.second

                require(clubModel.contentId.isNotEmpty()) { ErrorCode.NO_EXIST_CLUB.message }
                require(
                    userModel.uid?.isNotEmpty() ?: false
                ) { ErrorCode.FAIL_NO_USER_INFO_SELF.message }

                when {
                    duplicateMercenaryInClubCheck(response.first, modelAlaram.clubUid) ->
                        ClubManager.areadyRegisteredMercenary(view, response, userModel, clubModel, modelAlaram)

                    ClubManager.maxCountCheck(response.first) ->
                        ClubManager.areadyMax(
                            view,
                            response,
                            userModel,
                            clubModel,
                            modelAlaram
                        )

                    else -> mercenaryInvitationAccept(clubModel, userModel, view, modelAlaram)
                }
            }
            .withProgressBallDialog(view.context)
            .subscribe({
                TLog.d("user info update \nalaram and club mercenary list remove complete")
            }, { e ->
                TDialog.show(view.context, e.toDialogMessage())
                CrashlyticsHelper.logException(e)
            })
            .let { }
    }

    private fun duplicateMercenaryInClubCheck(user: ModelUser?, clubUid: String?) =
        user?.mercenaryClubInfo?.any { it.contentId == clubUid } ?: false

    private fun mercenaryInvitationAccept(
        clubModel: ModelClub,
        userModel: ModelUser,
        view: View,
        modelAlaram: ModelAlaram
    ): Single<DialogObservable.Result<TextContentParam>> {

        require(clubModel.contentId.isNotEmpty()) { ErrorCode.NO_EXIST_CLUB.message }
        require(
            userModel.uid?.isNotEmpty() ?: false
        ) { ErrorCode.FAIL_NO_USER_INFO_SELF.message }
        requireNotNull(modelAlaram.senderUid) { ErrorCode.FAIL_NO_SENDER_UID.message }
        requireNotNull(modelAlaram.reciverUid) { ErrorCode.FAIL_NO_RECIVER_UID.message }

        val mercenaryModel = ModelUser.ModelMercenaryClub(
            clubMarkImageUrl = clubModel.clubMarkImageUrl,
            clubName = clubModel.clubName,
            masterUid = clubModel.masterUid,
            contentId = clubModel.contentId
        )

        if (userModel.mercenaryClubInfo == null)
            userModel.mercenaryClubInfo = mutableListOf(mercenaryModel)
        else
            userModel.mercenaryClubInfo?.also { it.add(mercenaryModel) }


        return RxRealTimeDB.push("${FirebaseUrls.users}/${modelAlaram.reciverUid}", userModel)
            .flatMap {
                val path = "${FirebaseUrls.alram}/${modelAlaram.reciverUid}"
                RxRealTimeDB.toSingle(path) { mutableListOf<ModelAlaram>() }
                    .map { response ->
                        response.removeAll { it.reciverUid == modelAlaram.reciverUid && it.timeStamp == modelAlaram.timeStamp }
                        response
                    }
                    .flatMap { model -> RxRealTimeDB.push(path, model) }
            }
            .map {
                clubModel.mercenaryApplicationList?.removeAll { it.uid == userModel.uid }

                clubModel.mercenaryNumber = clubModel.mercenaryNumber + 1
                clubModel.mercenaryMember?.add(
                    ModelClub.ModelMember(
                        nickName = userModel.nickName ?: "",
                        uid = userModel.uid!!,
                        memberImageUrl = userModel.profileImageUrl ?: "",
                        position = userModel.position?.joinToString("  ") ?: "",
                        age = userModel.age?.toString(),
                        gradeType = ClubGradeType.MERCENARY.name,
                        mercenary = true
                    )
                )
                clubModel
            }
            .flatMap { model -> RxRealTimeDB.push("${FirebaseUrls.club}/${model.contentId}", model) }
            .flatMap {
                val contentId = RxRealTimeDB.generateRandomKey("timeline")
                val timelinePath =
                    "${FirebaseUrls.club}/${modelAlaram.clubUid}/${FirebaseUrls.timeLine}/$contentId"

                RxRealTimeDB.push(
                    timelinePath, ClubManager.createTimeLinePost(
                        documentId = contentId,
                        type = TimelineType.MERCENARY_SIGN_UP,
                        model = modelAlaram
                    )
                )
            }
            .flatMap {
                FcmHelper.sendMessage(
                    destinationUid = modelAlaram.senderUid,
                    message = "${LoginManager.user?.nickName} 님이\n${clubModel.clubName.wrapWith(
                        "[",
                        "]"
                    )} ${AlarmType.MERCENARY_INVITATION_COMPLETE.text}"
                )
            }
            .flatMap { RxRealTimeDB.toSingle("${FirebaseUrls.alram}/${modelAlaram.senderUid}") { mutableListOf<ModelAlaram>() } }
            .flatMap { response ->

                val alarmData = ModelAlaram(
                    senderEmail = LoginManager.user?.email,
                    senderImageUrl = LoginManager.user?.profileImageUrl,
                    timeStamp = LocalTime().time,
                    senderUid = LoginManager.user?.uid!!,
                    senderNickName = LoginManager.user?.nickName,
                    reciverUid = modelAlaram.senderUid,
                    alaramTypeCode = AlarmType.MERCENARY_INVITATION_COMPLETE.code,
                    message = "${LoginManager.user?.nickName} 님이 ${AlarmType.MERCENARY_INVITATION_COMPLETE.text}"
                )

                response.add(alarmData)

                RxRealTimeDB.push("${FirebaseUrls.alram}/${modelAlaram.senderUid}", response)
            }
            .flatMap {
                TDialog.Builder(view.context)
                    .setMessage("${clubModel.clubName} 클럽에 용병으로 참여되었습니다.")
                    .isColored(true)
                    .addButton(DialogButton.OK())
                    .toSingle()
            }
    }

    fun invitation(view: View, postWriterUid: String, name: String): Single<DialogObservable.Result<TextContentParam>> {
        return RxRealTimeDB.toSingle("${FirebaseUrls.users}/$postWriterUid") { ModelUser() }
            .flatMap { user ->
                when {
                    !ClubManager.isClubGradeCheck() ->
                        ClubManager.showDialog(
                            view,
                            title = R.string.notice.getString(),
                            message = "클럽등급 코치 이상만 초대할 수 있습니다.",
                            cancelable = false
                        )

                    ClubManager.isClubMemberCheck(user) ->
                        ClubManager.showDialog(
                            view,
                            title = R.string.notice.getString(),
                            message = "${name}님은 이미 클럽에 가입되어 있습니다.",
                            cancelable = false
                        )

                    ClubManager.maxCountCheck(user) ->
                        ClubManager.showDialog(
                            view,
                            title = R.string.notice.getString(),
                            message = "${name}님은 최대 용병 수를 초과하였습니다.",
                            cancelable = false
                        )

                    ClubManager.duplicateMercenaryInClubCheck(user) ->
                        ClubManager.showDialog(
                            view,
                            title = R.string.notice.getString(),
                            message = "이미 이 클럽에 용병으로 등록되어 있습니다",
                            cancelable = false
                        )

                    else -> mercenaryInvitationRequest(view, postWriterUid, name)
                }
            }
    }

    private fun mercenaryInvitationRequest(
        view: View,
        postWriterUid: String,
        name: String
    ): Single<DialogObservable.Result<TextContentParam>> {

        require(postWriterUid.isNotEmpty()) { ErrorCode.FAIL_NO_POST_WRITER_INFO.message }

        return RxRealTimeDB.toSingle("${FirebaseUrls.alram}/$postWriterUid") { mutableListOf<ModelAlaram>() }
            .flatMap { responseList ->

                require(!LoginManager.user?.clubUid.isNullOrEmpty()) { ErrorCode.FAIL_NO_EXIST_MY_CLUB_CONTENT_ID.message }

                val alarmData = ModelAlaram(
                    senderEmail = LoginManager.user?.email,
                    senderImageUrl = LoginManager.user?.profileImageUrl,
                    timeStamp = LocalTime().time,
                    senderUid = LoginManager.user?.uid!!,
                    senderNickName = LoginManager.user?.nickName,
                    reciverUid = postWriterUid,
                    alaramTypeCode = AlarmType.MERCENARY_INVITATION_REQUEST.code,
                    message = "${LoginManager.user?.nickName} 님이\n${LoginManager.user?.club?.wrapWith(
                        "[",
                        "]"
                    )} ${AlarmType.MERCENARY_INVITATION_REQUEST.text}",
                    clubUid = LoginManager.user?.clubUid
                )

                responseList.add(alarmData)

                RxRealTimeDB.push("${FirebaseUrls.alram}/$postWriterUid", responseList)
            }
            .flatMap {
                FcmHelper.sendMessage(
                    destinationUid = postWriterUid,
                    message = "${LoginManager.user?.nickName} 님이\n${LoginManager.user?.club?.wrapWith(
                        "[",
                        "]"
                    )} ${AlarmType.MERCENARY_INVITATION_REQUEST.text}"
                )
            }
            .flatMap {
                TDialog.Builder(view.context, "알림")
                    .setMessage("$name 님에게\n용병초대메시지를 전송하였습니다.")
                    .isColored(true)
                    .addButton(DialogButton.OK())
                    .toSingle()
            }
    }


    fun reject(view: View, modelAlaram: ModelAlaram) {
        requireNotNull(modelAlaram.senderUid) { ErrorCode.FAIL_NO_SENDER_UID.message }
        requireNotNull(modelAlaram.reciverUid) { ErrorCode.FAIL_NO_RECIVER_UID.message }

        val path = "${FirebaseUrls.alram}/${modelAlaram.reciverUid}"
        RxRealTimeDB.toSingle(path) { mutableListOf<ModelAlaram>() }
            .map { response ->
                response.removeAll { it.reciverUid == modelAlaram.reciverUid && it.timeStamp == modelAlaram.timeStamp }
                response
            }
            .flatMap { model -> RxRealTimeDB.push(path, model) }
            .flatMap {
                FcmHelper.sendMessage(
                    destinationUid = modelAlaram.senderUid,
                    message = "${LoginManager.user?.nickName} 님이 클럽용병초대를 거절하였습니다."
                )
            }
            .flatMap { RxRealTimeDB.toSingle("${FirebaseUrls.alram}/${modelAlaram.senderUid}") { mutableListOf<ModelAlaram>() } }
            .flatMap { response ->

                val alarmData = ModelAlaram(
                    senderEmail = LoginManager.user?.email,
                    senderImageUrl = LoginManager.user?.profileImageUrl,
                    timeStamp = LocalTime().time,
                    senderUid = LoginManager.user?.uid!!,
                    senderNickName = LoginManager.user?.nickName,
                    reciverUid = modelAlaram.senderUid,
                    alaramTypeCode = AlarmType.CLUB_MERCENARY_SIGNUP_REJECT.code,
                    message = "${LoginManager.user?.nickName} 님이 클럽용병초대를 거절하였습니다."
                )

                response.add(alarmData)

                RxRealTimeDB.push("${FirebaseUrls.alram}/${modelAlaram.senderUid}", response)
            }
            .withProgressBallAnimation(view.context)
            .flatMap {
                TDialog.Builder(view.context)
                    .setMessage("용병신청을 거절하였습니다.")
                    .isColored(true)
                    .addButton(DialogButton.OK())
                    .toSingle()
            }
            .subscribe({
                TLog.d("reject Success !!")
            }, { e ->
                TDialog.show(view.context, e.toDialogMessage())
                CrashlyticsHelper.logException(e)
            })
            .let { }
    }
}