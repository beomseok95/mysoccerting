package ks.bs.mysoccerting.scene.filter

import android.app.Dialog
import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.math.MathUtils
import androidx.databinding.DataBindingUtil
import com.ptrstovka.calendarview2.CalendarDay
import com.ptrstovka.calendarview2.CalendarView2
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.fragment_calendar_dialog.calendarView
import kotlinx.android.synthetic.main.fragment_calendar_dialog.layout_dialog_header
import kotlinx.android.synthetic.main.fragment_calendar_dialog.layout_dialog_root
import kotlinx.android.synthetic.main.fragment_calendar_dialog.tv_dialog_title
import kr.nextm.lib.getDrawable
import ks.bs.mysoccerting.R
import ks.bs.mysoccerting.customview.dialog.RxDialogFragment
import ks.bs.mysoccerting.model.time.LocalTime
import ks.bs.mysoccerting.rx.showProgressDialog
import ks.bs.mysoccerting.scene.base.FragmentBaseViewModel
import ks.bs.mysoccerting.util.dpToPixels

class CalendarDialog : RxDialogFragment<CalendarDialog.Date>() {

    data class Date(var firstDate: String? = null, var lastDate: String? = null)


    val viewModel = creatVm()

    val compositeDisposable = CompositeDisposable()

    override fun onCreateCustomView(inflater: LayoutInflater, container: ViewGroup): View {

        return DataBindingUtil.inflate<ks.bs.mysoccerting.databinding.FragmentCalendarDialogBinding>(
            inflater,
            R.layout.fragment_calendar_dialog,
            container, false
        )
            .let {
                it.viewModel = viewModel
                it.root
            }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        initDialogSize()
        initHeaderColor()
        initCalendar()
    }

    private fun initCalendar() {
//        calendarView.selectionMode = CalendarView2.SELECTION_MODE_RANGE
        calendarView.selectionMode = CalendarView2.SELECTION_MODE_SINGLE
        calendarView
            .state()
            .edit()
            .setMinimumDate(LocalTime().date)
            .commit()
    }


    private fun creatVm(): CalendarVm {

        return CalendarVm(object : CalendarVm.Contract {
            override fun dateSelectComplete(firstSelectDay: String?, lastSelectDay: String?) {
                finishWithResponse(Date(firstDate = firstSelectDay, lastDate = lastSelectDay))
            }

            override fun getCalendar(): CalendarView2 {
                return calendarView
            }

            override fun finishDialog() {
                dismiss()
            }

            override fun createDialogForProgress(request: FragmentBaseViewModel.ProgressBarOwner.Request): Dialog {
                return showProgressDialog(context, request.disposable, request.functionOnCancel)

            }
        })
    }

    private fun initDialogSize() {
        layout_dialog_root.addOnLayoutChangeListener(object : View.OnLayoutChangeListener {
            override fun onLayoutChange(
                v: View,
                left: Int,
                top: Int,
                right: Int,
                bottom: Int,
                oldLeft: Int,
                oldTop: Int,
                oldRight: Int,
                oldBottom: Int
            ) {
                v.removeOnLayoutChangeListener(this)
                val headerHeight = 40.dpToPixels()
                val buttonsHeight = 80.dpToPixels()
                val sum = headerHeight + buttonsHeight

                val width = 450.dpToPixels()
                val height = MathUtils.clamp(
                    bottom - top,
                    400.dpToPixels() + sum,
                    500.dpToPixels() + sum
                )

                val displayMetrics = context.resources.displayMetrics
                v.layoutParams.width = minOf(width, displayMetrics.widthPixels)
                v.layoutParams.height = minOf(height, displayMetrics.heightPixels)
                dialog?.window?.setLayout(v.layoutParams.width, v.layoutParams.height)
            }
        })
    }

    private fun initHeaderColor() {
        layout_dialog_header.background = R.drawable.bg_setting_dialog_header.getDrawable()
        tv_dialog_title.setTextColor(Color.WHITE)
    }

    override fun onDestroy() {
        viewModel.clearDisposables()
        compositeDisposable.clear()
        super.onDestroy()
    }
}