package ks.bs.mysoccerting.scene.club.detail.home

import android.view.View
import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import io.reactivex.Observable
import io.reactivex.rxkotlin.addTo
import ks.bs.mysoccerting.BR
import ks.bs.mysoccerting.R
import ks.bs.mysoccerting.customview.dialog.DialogButton
import ks.bs.mysoccerting.customview.dialog.TDialog
import ks.bs.mysoccerting.databinding.BaseRecyclerViewAdapter
import ks.bs.mysoccerting.databinding.ItemClubMemberBinding
import ks.bs.mysoccerting.databinding.ItemClubTimelineBinding
import ks.bs.mysoccerting.firebase.CrashlyticsHelper
import ks.bs.mysoccerting.firebase.FirebaseUrls
import ks.bs.mysoccerting.firebase.RxRealTimeDB
import ks.bs.mysoccerting.model.ModelClub
import ks.bs.mysoccerting.model.ModelClubTimeline
import ks.bs.mysoccerting.rx.rxresult.putObject
import ks.bs.mysoccerting.rx.rxresult.startActivity
import ks.bs.mysoccerting.rx.rxresult.startActivityForResult
import ks.bs.mysoccerting.rx.rxresult.toIntent
import ks.bs.mysoccerting.rx.toJson
import ks.bs.mysoccerting.rx.withProgressBallAnimation
import ks.bs.mysoccerting.scene.base.FragmentBaseViewModel
import ks.bs.mysoccerting.scene.club.detail.*
import ks.bs.mysoccerting.scene.club.detail.chat.MercenaryChatActivity
import ks.bs.mysoccerting.scene.club.supervisor.TimeManager
import ks.bs.mysoccerting.scene.club.supervisor.applyForClubProccess
import ks.bs.mysoccerting.scene.club.supervisor.getFormattDate
import ks.bs.mysoccerting.scene.login.LoginManager
import ks.bs.mysoccerting.util.DateEx
import ks.bs.mysoccerting.util.TLog
import ks.bs.mysoccerting.util.getString

class ClubDetailHomeViewModel(val contract: Contract) : FragmentBaseViewModel(contract) {
    interface Contract : BaseVmListener

    val memberAdapter = BaseRecyclerViewAdapter<ClubMemberItemVm, ItemClubMemberBinding>(
        R.layout.item_club_member, BR.viewModel
    )

    val mercenaryAdapter = BaseRecyclerViewAdapter<ClubMercenaryItemVm, ItemClubMemberBinding>(
        R.layout.item_club_mercenary, BR.viewModel
    )

    val timelineAdapter = BaseRecyclerViewAdapter<ClubTimeLineVm, ItemClubTimelineBinding>(
        R.layout.item_club_timeline, BR.viewModel
    )

    var modelClub = ModelClub()
    val clubMarkUrl = ObservableField<String>()
    val clubName = ObservableField<String>()
    val intro = ObservableField<String>()
    val signUpBtnVisibility = ObservableBoolean(false)
    val isMatchExsit = ObservableBoolean(false)
    val match_D_day = ObservableField("D-1")
    val clubChatBtnVisibility = ObservableBoolean(false)


    var contentId = ""

    fun getContents(contentId: String) {
        this.contentId = contentId

        val path = "${FirebaseUrls.club}/$contentId"
        RxRealTimeDB.toObservable(path) { ModelClub() }
            .overTimelineDelete()
            .overTiemMatchDelete()
            .subscribe({ model ->
                putClubHomePageData(model)

                putMembersData(model)

                putMercenaryData(model)

                getTimelineContents(model.timeline)

                checkUserInfo()

                clubChatBtnVisibility.set(contentId == LoginManager.user?.clubUid || LoginManager.user?.mercenaryClubInfo?.any { it.contentId == this.contentId } ?: false)

                if (model.matchUpList != null) {
                    isMatchExsit.set(true)
                    val matchModel = model.matchUpList.values.toList().sortedBy { it.hopeDateTimeStamp }
                    val dday = DateEx.calculateDday(matchModel[0].hopeDateTimeStamp)
                    match_D_day.set("D-$dday")
                } else
                    isMatchExsit.set(false)

            }, { TLog.e(it) })
            .addTo(compositeDisposable)
    }

    private fun putClubHomePageData(model: ModelClub) {
        modelClub = model
        clubMarkUrl.set(modelClub.clubMarkImageUrl)
        clubName.set(modelClub.clubName)
        intro.set(modelClub.clubIntro)
    }

    private fun putMembersData(model: ModelClub) {
        memberAdapter.clear()

        model.clubMembers.forEach { memberModel ->
            memberAdapter.insert(ClubMemberItemVm(memberModel))
        }
    }

    private fun putMercenaryData(model: ModelClub) {
        mercenaryAdapter.clear()

        model.mercenaryMember?.forEach { modelMercenary ->
            mercenaryAdapter.insert(ClubMercenaryItemVm(modelMercenary))
        }
    }

    private fun getTimelineContents(timeline: MutableMap<String, ModelClubTimeline>?) {
        timelineAdapter.clear()

        timeline?.values?.toList()?.forEach { clubTimelineModel ->
            timelineAdapter.insert(
                ClubTimeLineVm(
                    clubTimelineModel,
                    contract
                )
            )
        }
        timelineAdapter.items.sortByDescending { it.model.timestamp }

    }

    fun checkUserInfo() {
        signUpBtnVisibility.set(LoginManager.user?.clubUid == null)
    }

    fun moveMatchUpPage(view: View) {
        MatchUpActivity::class.java
            .toIntent()
            .putObject(modelClub.contentId)
            .startActivity(view.context)
            .subscribe()
    }

    fun movePositionPage(view: View) {
        ClubPositionActivity::class.java
            .toIntent()
            .putObject(modelClub)
            .startActivity(view.context)
            .subscribe()
    }

    fun moveChatActivity(view: View) {
        MercenaryChatActivity::class.java
            .toIntent()
            .putObject(modelClub.contentId)
            .startActivity(view.context)
            .subscribe()
    }

    fun moveMemberListPage(view: View, type: String) {
        ClubMemberListActivity::class.java
            .toIntent()
            .putExtra("TYPE", type)
            .putObject(modelClub)
            .startActivityForResult<String>(view.context)
            .subscribe({},
                { e -> CrashlyticsHelper.logException(e) })
            .addTo(compositeDisposable)
    }


    fun clubSignUp(view: View) {
        LoginManager.noUserSafeAction(view.context, compositeDisposable) {
            TDialog.Builder(view.context, R.string.notice.getString())
                .setMessage("클럽가입을 신청하시겠습니까?")
                .isColored(true)
                .addButton(DialogButton.CANCEL())
                .addButton(DialogButton.OK())
                .toSingle()
                .filter { it.ok }
                .flatMapSingle {
                    require(ClubDetailFragment.CLUB_UID.isNotEmpty()) { "CLUB_UID need to not null" }

                    RxRealTimeDB.toSingle("${FirebaseUrls.club}/${ClubDetailFragment.CLUB_UID}") { ModelClub() }
                        .applyForClubProccess(view, modelClub)
                        .withProgressBallAnimation(view.context)

                }
                .subscribe({
                    TLog.d("club sign up request success")
                }, { e ->
                    CrashlyticsHelper.logException(e)
                })
                .addTo(compositeDisposable)
        }
    }

    private fun Observable<ModelClub>.overTimelineDelete(): Observable<ModelClub> {
        return this
            .flatMap {
                it.timeline?.forEach {
                    if (TimeManager.judgeTimeOutSevenDays(it.value.timestamp!!.getFormattDate()))
                        RxRealTimeDB.remove("${FirebaseUrls.club}/$contentId/timeline/${it.key}")
                            .subscribe()
                }

                this
            }
    }

    private fun Observable<ModelClub>.overTiemMatchDelete(): Observable<ModelClub> {
        return this
            .flatMap {
                it.matchUpList?.forEach {
                    if (TimeManager.judgeTimeOutOneDay(it.value.hopeDateTimeStamp.getFormattDate()))
                        RxRealTimeDB.remove("${FirebaseUrls.matchChat}/${it.key}")
                            .andThen(RxRealTimeDB.remove("${FirebaseUrls.club}/${it.value.clubUid1}/matchUpList/${it.key}"))
                            .andThen(RxRealTimeDB.remove("${FirebaseUrls.club}/${it.value.clubUid2}/matchUpList/${it.key}"))
                            .subscribe()
                }

                this
            }
    }

    companion object {
        const val MEMBER = "member"
        const val MERCENARY = "mercenary"
    }
}
