package ks.bs.mysoccerting.scene.profile

import android.os.Bundle
import androidx.databinding.DataBindingUtil
import kotlinx.android.synthetic.main.activity_club_board_add.*
import ks.bs.mysoccerting.R
import ks.bs.mysoccerting.databinding.ActivityReportBinding
import ks.bs.mysoccerting.rx.rxresult.RxActivityBase

class ReportActivity : RxActivityBase() {

    val viewModel = createVm()

    private fun createVm() = ReportVm()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        DataBindingUtil.setContentView<ActivityReportBinding>(
            this,
            R.layout.activity_report
        ).vm = viewModel

        viewModel.type = getInput()

        initToolBar()
    }

    private fun initToolBar() {
        val titleText =
            if (getInput<String>() == HelpViewModel.SUGGESTIONS)
                "건의하기"
            else
                "버그신고하기"

        toolbar_title.text = titleText
    }


    override fun onDestroy() {
        viewModel.onDestroy()
        super.onDestroy()
    }
}
