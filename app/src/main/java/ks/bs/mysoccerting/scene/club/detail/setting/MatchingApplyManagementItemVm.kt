package ks.bs.mysoccerting.scene.club.detail.setting

import android.view.View
import ks.bs.mysoccerting.customview.dialog.ImageDialog
import ks.bs.mysoccerting.firebase.FirebaseUrls
import ks.bs.mysoccerting.firebase.RxRealTimeDB
import ks.bs.mysoccerting.model.ModelClub
import ks.bs.mysoccerting.model.time.LocalTime
import ks.bs.mysoccerting.model.time.Pattern
import ks.bs.mysoccerting.rx.rxresult.putObject
import ks.bs.mysoccerting.rx.rxresult.startActivity
import ks.bs.mysoccerting.rx.rxresult.toIntent
import ks.bs.mysoccerting.scene.club.supervisor.MatchHelper
import ks.bs.mysoccerting.util.dpToPixels

class MatchingApplyManagementItemVm(val model: ModelClub.MatchApplicationInfo) {


    val clubMarkUrl get() = model.clubMarkImageUrl
    val clubName get() = "클럽이름 : ${model.clubName}"
    val hopeDate get() = "매칭일 : ${model.hopeDate}"
    val peopleNumber get() = "인원수 : ${model.peopleNumber}"
    val area get() = "지역 : ${model.area}"
    val timeStamp get() = model.timeStamp
    val applyTime get() = "신청시간 : ${LocalTime(model.timeStamp).formatString(Pattern.toStringDefault)}"


    fun showClubInfo(view: View) {
        requireNotNull(model.clubUid) { "MatchingApplyManagementItemVm clubUid need to Not null!!1" }
        RxRealTimeDB.toSingle("${FirebaseUrls.club}/${model.clubUid}") { ModelClub() }
            .flatMapCompletable { modelClub ->
                ClubSettingInfoActivity::class.java
                    .toIntent()
                    .putObject(modelClub)
                    .startActivity(view.context)
            }
            .subscribe()
    }

    fun showClubMarkImage(view: View) {

        if (model.clubMarkImageUrl != null)
            ImageDialog.Builder(view.context, "")
                .setImgUrl(model.clubMarkImageUrl)
                .isSettingDialog(true)
                .setSize(350.dpToPixels(), 350.dpToPixels())
                .create()
                .show()
    }

    fun acceptBtnClkEvent(view: View) {
        MatchHelper.accept(view, modelAlaram = model)
    }

    fun rejectBtnClkEvent(view: View) {
        MatchHelper.reject(view, modelAlaram = model)
    }
}