package ks.bs.mysoccerting.scene.club.clubtour


import android.app.Dialog
import android.graphics.drawable.Drawable
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.navigation.fragment.findNavController
import io.reactivex.Single
import kotlinx.android.synthetic.main.activity_profile_setting.*
import ks.bs.mysoccerting.BR
import ks.bs.mysoccerting.R
import ks.bs.mysoccerting.model.Area
import ks.bs.mysoccerting.rx.showProgressDialog
import ks.bs.mysoccerting.scene.base.FragmentBase
import ks.bs.mysoccerting.scene.base.FragmentBaseViewModel
import ks.bs.mysoccerting.scene.filter.AreaDialog
import ks.bs.mysoccerting.scene.home.MainActivity
import ks.bs.mysoccerting.util.toDrawable


class ClubGenerateFragment : FragmentBase() {
    var viewModel = createVm()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding =
            DataBindingUtil.inflate<ViewDataBinding>(inflater, R.layout.fragment_club_generate, container, false)
        binding.setVariable(BR.viewModel, viewModel)
        return binding.root
    }

    private fun createVm() = ClubGenerateViewModel(object :
        ClubGenerateViewModel.ClubGenerateFragmentContract {
        override fun showAreaDialog(): Single<Area> {
            return AreaDialog()
                .toSingle(context!!)
        }

        override fun getDrawableFromUri(uri: Uri): Drawable {
            return uri.toDrawable(this@ClubGenerateFragment.context!!)
        }

        override fun checkExternalStoragePermission() {
            (activity as MainActivity).checkPermission()
        }

        override fun finishSuccess(contentId: String) {
            val action =
                ClubGenerateFragmentDirections.actionClubGenerateFragmentToClubDetailFragment(
                    contentId
                )
            findNavController().navigate(action)
        }

        override fun bringToFront() {
            circleImageView.bringToFront()
        }

        override fun createDialogForProgress(request: FragmentBaseViewModel.ProgressBarOwner.Request): Dialog {
            return showProgressDialog(activity, request.disposable, request.functionOnCancel)
        }
    })

    override fun onDetach() {
        viewModel.onDetach()
        super.onDetach()
    }

}
