package ks.bs.mysoccerting.scene.base

import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import ks.bs.mysoccerting.rx.DisposeOnDestroy
import ks.bs.mysoccerting.util.NetworkStatus

abstract class
FragmentBase : androidx.fragment.app.Fragment(), DisposeOnDestroy {
    var compositeDisposable = CompositeDisposable()
    override fun addDisposable(disposable: Disposable): Boolean {
        return compositeDisposable.add(disposable)
    }

    override fun onResume() {
        super.onResume()
        NetworkStatus.checkAndAlert(this.context!!)
    }

    override fun clearDisposables() {
        compositeDisposable.clear()
    }

    override fun onDetach() {
        super.onDetach()
        compositeDisposable.clear()
    }

}