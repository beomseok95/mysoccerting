package ks.bs.mysoccerting.scene.club.clubtour

import android.view.View
import androidx.navigation.findNavController
import ks.bs.mysoccerting.R
import ks.bs.mysoccerting.scene.base.FragmentBaseViewModel
import ks.bs.mysoccerting.scene.login.LoginManager

class ClubViewModel(val contract: Contract) : FragmentBaseViewModel(contract) {
    interface Contract:BaseVmListener
    fun onBrowse(view: View) {
            view.findNavController()
                .navigate(R.id.clubSearchFragment)
    }

    fun onGenerate(view: View) {
        LoginManager.needLoginToNoCLub(view.context, compositeDisposable) {
            view.findNavController()
                .navigate(R.id.clubGenerateFragment)
        }

    }
}