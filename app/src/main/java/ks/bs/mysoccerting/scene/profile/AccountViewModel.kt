package ks.bs.mysoccerting.scene.profile

import android.view.View
import androidx.navigation.findNavController
import ks.bs.mysoccerting.R
import ks.bs.mysoccerting.scene.base.FragmentBaseViewModel
import ks.bs.mysoccerting.scene.login.LoginManager
import kotlin.contracts.contract

class AccountViewModel(val contract:Contract) : FragmentBaseViewModel(contract) {
    interface Contract:BaseVmListener

    val mail = auth.currentUser?.email ?: ""

    fun passwordReset(view: View) {
        LoginManager.noUserSafeAction(view.context, compositeDisposable) {
            view.findNavController()
                .navigate(R.id.action_accountFragment_to_findPasswordActivity)
        }
    }
}
