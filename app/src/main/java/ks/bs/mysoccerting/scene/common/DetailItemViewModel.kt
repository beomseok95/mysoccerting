package ks.bs.mysoccerting.scene.common

import android.view.View
import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import androidx.databinding.ViewDataBinding
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.GenericTypeIndicator
import com.google.firebase.firestore.ListenerRegistration
import io.reactivex.Completable
import io.reactivex.Single
import io.reactivex.rxkotlin.addTo
import ks.bs.mysoccerting.BR
import ks.bs.mysoccerting.R
import ks.bs.mysoccerting.customview.dialog.DialogButton
import ks.bs.mysoccerting.customview.dialog.TDialog
import ks.bs.mysoccerting.databinding.BaseRecyclerViewAdapter
import ks.bs.mysoccerting.databinding.ItemMatchClubCommentBinding
import ks.bs.mysoccerting.databinding.ItemSearchMercanaryCommentBinding
import ks.bs.mysoccerting.exception.CanceledByUserException
import ks.bs.mysoccerting.exception.ErrorCode
import ks.bs.mysoccerting.exception.toDialogMessage
import ks.bs.mysoccerting.fcm.FcmHelper
import ks.bs.mysoccerting.firebase.CrashlyticsHelper
import ks.bs.mysoccerting.firebase.FirebaseUrls
import ks.bs.mysoccerting.firebase.RxRealTimeDB
import ks.bs.mysoccerting.model.*
import ks.bs.mysoccerting.model.time.LocalTime
import ks.bs.mysoccerting.model.type.AlarmType
import ks.bs.mysoccerting.scene.base.ActivityBaseViewModel
import ks.bs.mysoccerting.scene.club.supervisor.ClubManager
import ks.bs.mysoccerting.scene.club.supervisor.MatchHelper
import ks.bs.mysoccerting.scene.club.supervisor.MercenaryInvitationHelper
import ks.bs.mysoccerting.scene.club.supervisor.MercenarySignUpHelper
import ks.bs.mysoccerting.scene.login.LoginManager
import ks.bs.mysoccerting.support.DontCare
import ks.bs.mysoccerting.util.DateEx
import ks.bs.mysoccerting.util.TLog

class DetailItemViewModel(private val contract: DetailItemContract) : ActivityBaseViewModel() {

    interface DetailItemContract {
        fun showToast(message: String)
        fun showCommentSubmitDialog(): TDialog.Builder
        fun moveAmendActivity(): Single<String>
        fun finishPage(moveToCLubPage: Boolean)
    }

    var commentSnapshot: ListenerRegistration? = null

    var adapter: BaseRecyclerViewAdapter<Any, *>? = null

    val name = ObservableField<String>()
    val profileimageurl = ObservableField<String>()
    val hopeDate = ObservableField<String>()
    val content = ObservableField<String>()
    val tiemStamp = ObservableField<String>()
    val age = ObservableField<String>()
    val position = ObservableField<String>()
    val positionText = ObservableField<String>()
    val commentCount = ObservableField<String>()
    val ageLayoutVisibility = ObservableBoolean()
    val positionLayoutVisibility = ObservableBoolean()
    val commentVisibility = ObservableBoolean(true)
    val inviteAndMatchingText = ObservableField<String>()
    val inviteAndMatchingBtnVibibility = ObservableBoolean(false)
    val isClub = ObservableBoolean(false)
    var clubMasterUid: String = ""
    var type: String = ""


    lateinit var contentDocument: String

    var postWriterUid = "" //destinationUid

    var commentContent = ObservableField<String>()

    lateinit var matchModel: ModelClubMatch


    private fun <B : ViewDataBinding> initAdapter(resId: Int) {
        adapter = BaseRecyclerViewAdapter<Any, B>(resId, BR.modelComment, this, BR.listener)
    }

    private fun getComment(collectionPath: String, contentDocument: String) {
        val indecatior = object : GenericTypeIndicator<HashMap<String, ModelComment>>() {}
        RxRealTimeDB.toObservable("$collectionPath/$contentDocument/comments", indecatior) { HashMap() }
            .map { response ->
                adapter?.clear()
                response
            }
            .map { it.values.toList() }
            .subscribe({ response ->
                response.forEach {
                    if (LoginManager.user != null)
                        it.isWriter = (LoginManager.user?.uid == it.writerUid)
                }
                adapter?.insertAll(response)
                adapter?.items?.sortBy {
                    val model = it as ModelComment
                    model.timestamp
                }

                adapter?.notifyDataSetChanged()
            }, { e ->
                CrashlyticsHelper.logException(e)
            })
            .addTo(compositeDisposable)
    }

    private fun getPathType(inputType: String) =
        when (inputType) {
            MATCHING_CLUB_TYPE -> FirebaseUrls.clubMatch
            SEARTCH_MERCENARY_TYPE -> FirebaseUrls.mercenarySeach
            else -> FirebaseUrls.clubSeach
        }

    private fun getInputItemTypeFromPath(collectionPath: String) =
        when (collectionPath) {
            FirebaseUrls.clubMatch -> MATCHING_CLUB_TYPE
            FirebaseUrls.mercenarySeach -> SEARTCH_MERCENARY_TYPE
            else -> SEARTCH_CLUB_TYPE

        }

    fun setSearchMercenaryItem(item: ModelMercenarySearch) {
        require(item.contentId.isNotEmpty()) { "comment contentDocument need to not null or empry" }

        type = SEARTCH_MERCENARY_TYPE

        contentDocument = item.contentId
        name.set(item.nickName)
        profileimageurl.set(item.profileImageUrl)
        hopeDate.set("용병 참가 희망일\n${item.hopeDate}")
        content.set(item.content)
        tiemStamp.set(DateEx.convertDateString(item.timestamp))
        age.set(item.age)
        position.set(item.position)
        positionText.set("주 포지션")
        commentCount.set(item.commentcount.toString())
        ageLayoutVisibility.set(true)
        positionLayoutVisibility.set(true)
        inviteAndMatchingText.set("용병으로초대")
        postWriterUid = item.uid
        clubMasterUid = item.clubMasterUid
        isClub.set(false)
        inviteAndMatchingBtnVibibility.set(clubExist() && postWriterUid != LoginManager.user?.uid)
        initAdapter<ItemSearchMercanaryCommentBinding>(R.layout.item_search_mercanary_comment)
        getComment(getPathType(type), contentDocument)
    }

    fun setSearchClubItem(item: ModelClubSearch) {
        require(item.contentId.isNotEmpty()) { "comment contentDocument need to not null or empry" }

        type = SEARTCH_CLUB_TYPE

        contentDocument = item.contentId
        name.set(item.clubName)
        profileimageurl.set(item.clubImageUrl)
        hopeDate.set("용병 구인 희망일\n${item.hopeDate}")
        content.set(item.content)
        tiemStamp.set(DateEx.convertDateString(item.timestamp))
        position.set(item.position)
        positionText.set("구인 포지션")
        commentCount.set(item.commentcount.toString())
        ageLayoutVisibility.set(false)
        positionLayoutVisibility.set(true)
        inviteAndMatchingText.set("용병신청")
        postWriterUid = item.uid
        clubMasterUid = item.clubMasterUid
        inviteAndMatchingBtnVibibility.set(LoginManager.isLogin() && postWriterUid != LoginManager.user?.uid)
        isClub.set(true)
        initAdapter<ItemSearchMercanaryCommentBinding>(R.layout.item_search_club_comment)
        getComment(getPathType(type), contentDocument)
    }

    fun setMatchClubItem(item: ModelClubMatch) {
        require(item.contentId.isNotEmpty()) { "comment contentDocument need to not null or empry" }

        matchModel = item

        type = MATCHING_CLUB_TYPE

        contentDocument = item.contentId
        name.set(item.clubName)
        profileimageurl.set(item.clubImageUrl)
        hopeDate.set("매칭 희망일\n${item.hopeDate}")
        content.set(item.content)
        tiemStamp.set(DateEx.convertDateString(item.timestamp))
        commentCount.set(item.commentcount.toString())
        ageLayoutVisibility.set(false)
        positionLayoutVisibility.set(false)
        inviteAndMatchingText.set("매칭신청")
        postWriterUid = item.uid
        inviteAndMatchingBtnVibibility.set(
            clubExist() && postWriterUid != LoginManager.user?.uid &&
                    LoginManager.user?.clubUid != item.clubUid
        )
        isClub.set(true)
        initAdapter<ItemMatchClubCommentBinding>(R.layout.item_match_club_comment)
        getComment(getPathType(type), contentDocument)
    }

    private fun clubExist(): Boolean {
        if (LoginManager.user == null)
            return false

        return LoginManager.user!!.clubUid != null
    }

    fun onPause() {
        commentSnapshot?.remove()
    }

    fun commentBtnClkEvent() {
        commentVisibility.set(!commentVisibility.get())
    }

    fun inviteAndMatchingBtnClkEvent(view: View) {
        when (type) {
            MATCHING_CLUB_TYPE ->
                MatchHelper.apply(view, matchModel)
                    .subscribe({
                        TLog.d("inviteAndMatchingBtnClkEvent complete")
                    }, { e ->
                        if (e is CanceledByUserException)
                            return@subscribe

                        if (e is NoSuchElementException)
                            return@subscribe

                        TDialog.show(view.context, e.toDialogMessage())
                        CrashlyticsHelper.logException(e)
                    })
                    .addTo(compositeDisposable)

            SEARTCH_CLUB_TYPE -> {
                require(clubMasterUid.isNotEmpty()) { ErrorCode.FAIL_NO_CLUB_MASTER_UID.message }

                ClubManager.showDialog(
                    view,
                    title = "용병등록신청하기",
                    message = "${name.get()} 클럽에 용병등록을\n신청하시겠습니까?",
                    okBtnText = "신청"
                )
                    .flatMap {
                        if (it.ok)
                            RxRealTimeDB.toSingle("${FirebaseUrls.users}/$clubMasterUid") { ModelUser() }
                                .map { it.clubUid }
                                .flatMap { clubUid -> RxRealTimeDB.toSingle("${FirebaseUrls.club}/$clubUid") { ModelClub() } }
                                .flatMap {
                                    MercenarySignUpHelper.request(
                                        view,
                                        postWriterUid,
                                        clubMasterUid,
                                        name.get()!!,
                                        it.mercenaryApplicationList
                                    )
                                }
                        else
                            Single.just(DontCare)
                    }
                    .subscribe({
                        TLog.d("inviteAndMatchingBtnClkEvent complete")
                    }, { e ->
                        if (e is CanceledByUserException)
                            return@subscribe

                        if (e is NoSuchElementException)
                            return@subscribe

                        TDialog.show(view.context, e.toDialogMessage())
                        CrashlyticsHelper.logException(e)
                    })
                    .addTo(compositeDisposable)
            }
            SEARTCH_MERCENARY_TYPE -> {
                LoginManager.needClubAssistantGradeInvitation(view.context, compositeDisposable) {
                    ClubManager.showDialog(
                        view,
                        title = "용병초대하기",
                        message = "${name.get()} 님을 용병으로\n초대하시겠습니까?",
                        okBtnText = "신청"
                    )
                        .flatMap {
                            if (it.ok)
                                MercenaryInvitationHelper.invitation(view, postWriterUid, name.get()!!)
                            else
                                Single.just(DontCare)
                        }
                        .subscribe({
                            TLog.d("inviteAndMatchingBtnClkEvent complete")
                        }, { e ->
                            if (e is CanceledByUserException)
                                return@subscribe

                            if (e is NoSuchElementException)
                                return@subscribe

                            TDialog.show(view.context, e.toDialogMessage())
                            CrashlyticsHelper.logException(e)
                        })
                        .addTo(compositeDisposable)
                }
            }
        }
    }

    fun amendBtnClkEvent(view: View, comment: ModelComment) {
        contract.moveAmendActivity()
            .flatMap { commentAmend(getPathType(type), it, comment) }
            .map { getComment(getPathType(type), contentDocument) }
            .subscribe({
                contract.showToast("댓글이 수정되었습니다.")
            }, {}).let { addDisposable(it) }
    }

    private fun commentAmend(
        path: String,
        amendCommentContent: String,
        model: ModelComment
    ): Single<DontCare> {
        require(!model.contentId.isNullOrEmpty()) { " ModelComment need to not null or empry" }

        val ammendPath = "$path/$contentDocument/comments/${model.contentId}"
        return RxRealTimeDB.toSingle(ammendPath) { ModelComment() }
            .map { content ->
                content.writerEmail = FirebaseAuth.getInstance()?.currentUser!!.email//댓글을 입력한 유저아이디를 입력
                content.content = amendCommentContent
                content.writerUid = LoginManager.user?.uid
                content.timestamp = model.timestamp

                content
            }
            .flatMap { ammendedComment ->
                RxRealTimeDB.push(ammendPath, ammendedComment)
            }
    }

    fun submitBtnClickEvent() {
        if (commentContent.get().isNullOrEmpty()) {
            contract.showToast("댓글을 입력하세요")
            return
        }

        if (FirebaseAuth.getInstance().currentUser == null) {
            contract.showToast("로그인이 필요한\n서비스입니다")
            return
        }

        contract.showCommentSubmitDialog()
            .setMessage("댓글을\n등록하시겠습니까?")
            .isColored(true)
            .addButton(DialogButton("취소"))
            .addButton(DialogButton("확인") {
                commentVisibility.set(true)

                addComment(getPathType(type))

            }).show()

    }

    private fun addComment(collectionPath: String) {
        val document = RxRealTimeDB.generateRandomKey("comment")
        val data = ModelComment(
            writerEmail = LoginManager.user?.email,//댓글을 입력한 유저아이디를 입력
            content = commentContent.get().toString(),
            writerUid = LoginManager.user?.uid,//댓글을 입력한 유저의 UID 입력
            timestamp = System.currentTimeMillis(),
            contentId = document,
            writerNickName = LoginManager.user?.nickName,
            writerProfileUrl = LoginManager.user?.profileImageUrl
        )

        val path = "$collectionPath/$contentDocument/comments/$document"
        RxRealTimeDB.push(path, data)
            .flatMap {
                when (getInputItemTypeFromPath(collectionPath)) {
                    MATCHING_CLUB_TYPE -> matchingClubCommentCountSetting(1)
                    SEARTCH_MERCENARY_TYPE -> searchMercenaryCommentCountSetting(1)
                    else -> searchClubCommentCountSetting(1)
                }
            }
            .flatMap {
                if (LoginManager.user?.uid != postWriterUid)
                    RxRealTimeDB.toSingle("${FirebaseUrls.alram}/$postWriterUid") { mutableListOf<ModelAlaram>() }
                        .flatMap { responseList ->
                            val alarmData = ModelAlaram(
                                senderEmail = LoginManager.user?.email,
                                senderImageUrl = LoginManager.user?.profileImageUrl,
                                timeStamp = LocalTime().time,
                                senderUid = LoginManager.user?.uid!!,
                                senderNickName = LoginManager.user?.nickName,
                                reciverUid = postWriterUid,
                                alaramTypeCode = AlarmType.COMMENT.code,
                                message = "${LoginManager.user?.nickName} ${AlarmType.COMMENT.text}",
                                commentCallType = DetailItemActivity.CALL_LOCATION,
                                postPullPath = "$collectionPath/$contentDocument"
                            )

                            responseList.add(alarmData)

                            RxRealTimeDB.push("${FirebaseUrls.alram}/$postWriterUid", responseList)
                        }
                        .flatMap {
                            FcmHelper.sendMessage(
                                destinationUid = postWriterUid,
                                message = "${LoginManager.user?.nickName} ${AlarmType.COMMENT.text}"
                            )
                        }
                else
                    Single.just(DontCare)
            }
            .subscribe({
                contract.showToast("댓글이 등록되었습니다.")
                commentContent.set("")
            }, { e ->
                CrashlyticsHelper.logException(e)
            }).let { addDisposable(it) }
    }


    fun deleteBtnClkEvent(view: View, comment: ModelComment) {
        contract.showCommentSubmitDialog()
            .setMessage("댓글을\n삭제하시겠습니까 ?")
            .isColored(true)
            .addButton(DialogButton("취소"))
            .addButton(DialogButton("확인") {
                deleteOkEvent(comment)
            }).show()
    }

    private fun deleteOkEvent(comment: ModelComment) {
        val topCollectionPath = getPathType(type)

        deleteComment(topCollectionPath = topCollectionPath, commentDocumentPath = comment.contentId!!)
            .andThen(
                when (getInputItemTypeFromPath(topCollectionPath)) {
                    MATCHING_CLUB_TYPE -> matchingClubCommentCountSetting(-1)
                    SEARTCH_MERCENARY_TYPE -> searchMercenaryCommentCountSetting(-1)
                    else -> searchClubCommentCountSetting(-1)
                }
            )
            .subscribe({
                adapter?.remove(comment)
                contract.showToast("댓글이 삭제되었습니다.")
            }, { e -> CrashlyticsHelper.logException(e) }).addTo(compositeDisposable)
    }

    private fun deleteComment(topCollectionPath: String, commentDocumentPath: String): Completable {
        return RxRealTimeDB.remove("$topCollectionPath/$contentDocument/${FirebaseUrls.comment}/$commentDocumentPath")
    }

    private fun searchMercenaryCommentCountSetting(num: Int): Single<Unit> {
        val path = "${FirebaseUrls.mercenarySeach}/$contentDocument/commentcount"
        return RxRealTimeDB.counter(path, num)
            .map { commentCount.set((commentCount.get()!!.toInt().plus(num)).toString()) }
    }


    private fun matchingClubCommentCountSetting(num: Int): Single<Unit> {
        val path = "${FirebaseUrls.clubMatch}/$contentDocument/commentcount"
        return RxRealTimeDB.counter(path, num)
            .map { commentCount.set((commentCount.get()!!.toInt().plus(num)).toString()) }

    }

    private fun searchClubCommentCountSetting(num: Int): Single<Unit> {

        val path = "${FirebaseUrls.clubSeach}/$contentDocument/commentcount"
        return RxRealTimeDB.counter(path, num)
            .map { commentCount.set((commentCount.get()!!.toInt().plus(num)).toString()) }

    }

    fun backPressed(view: View) {
        val activity = view.context as DetailItemActivity
        activity.onBackPressed()
    }

    companion object {
        const val MATCHING_CLUB_TYPE = "matching_club_type"
        const val SEARTCH_MERCENARY_TYPE = "search_mercenary_type"
        const val SEARTCH_CLUB_TYPE = "search_club_type"

    }
}

