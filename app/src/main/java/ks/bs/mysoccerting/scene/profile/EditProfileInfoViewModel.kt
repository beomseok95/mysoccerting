package ks.bs.mysoccerting.scene.profile

import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.databinding.ObservableField
import kr.nextm.lib.TToast
import ks.bs.mysoccerting.R
import ks.bs.mysoccerting.model.type.EditType
import ks.bs.mysoccerting.util.App
import ks.bs.mysoccerting.util.TLog
import ks.bs.mysoccerting.util.ValidateCheck

class EditProfileInfoViewModel(val contract: EditProfileInfoContract) {
    interface EditProfileInfoContract {
        fun save(data: String)
    }

    var type: EditType? = null
    val hint = ObservableField("")
    val editData = ObservableField("")
    val isAge = ObservableField(false)
    val isSex = ObservableField(false)

    val ageAdapter = ArrayAdapter(
        App.getApplicationContext(),
        R.layout.custom_simple_dropdown_item_1line,
        listOf("10", "20", "30", "40", "50", "ALL")
    )
    val sexAdapter = ArrayAdapter(
        App.getApplicationContext(),
        R.layout.custom_simple_dropdown_item_1line,
        listOf("남자", "여자")
    )

    fun save() {
        if (editData.get().isNullOrEmpty()) {
            TToast.show("내용을 입력해주세요")
            return
        }

        if (type == EditType.ALIAS)
            if (!ValidateCheck.validateNickName(editData.get().toString())) {
                TToast.show("닉네임은 특수문자 사용이 불가능하며 3~10자리만 가능합니다.")
                return
            }

        if (type == EditType.CLUB_NAME)
            if (!ValidateCheck.validateClubName(editData.get().toString())) {
                TToast.show("클럽이름은 특수문자 사용이 불가능하며 3~15자리만 가능합니다.")
                return
            }

        if(type==EditType.INTRO)
            if (!ValidateCheck.validateClubIntro(editData.get().toString())) {
                TToast.show("클럽소개는 10~50자리만 가능합니다.")
                return
            }

        contract.save(editData.get().toString())
    }

    fun backPressed(view: View) {
        val activity = view.context as EditProfileInfoActivity
        activity.onBackPressed()
    }

    fun onAgeItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        editData.set(ageAdapter.getItem(position))
    }

    fun onSexItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        editData.set(sexAdapter.getItem(position))
    }
}