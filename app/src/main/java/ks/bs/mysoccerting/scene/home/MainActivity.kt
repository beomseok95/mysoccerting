package ks.bs.mysoccerting.scene.home

import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.widget.TextView
import androidx.databinding.DataBindingUtil
import androidx.navigation.findNavController
import androidx.navigation.ui.NavigationUI
import com.google.firebase.database.GenericTypeIndicator
import com.google.firebase.messaging.FirebaseMessaging
import io.reactivex.Single
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.zipWith
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.layout_badge.view.*
import ks.bs.mysoccerting.R
import ks.bs.mysoccerting.databinding.ActivityMainBinding
import ks.bs.mysoccerting.fcm.FcmHelper
import ks.bs.mysoccerting.firebase.CrashlyticsHelper
import ks.bs.mysoccerting.firebase.FirebaseHelper
import ks.bs.mysoccerting.firebase.FirebaseUrls
import ks.bs.mysoccerting.firebase.RxRealTimeDB
import ks.bs.mysoccerting.model.ModelAlaram
import ks.bs.mysoccerting.model.ModelClub
import ks.bs.mysoccerting.model.ModelUser
import ks.bs.mysoccerting.model.type.ProfileUrlType
import ks.bs.mysoccerting.prefs.Prefs
import ks.bs.mysoccerting.rx.rxresult.RxActivityBase
import ks.bs.mysoccerting.rx.withProgressBallAnimation
import ks.bs.mysoccerting.scene.login.LoginManager
import ks.bs.mysoccerting.support.DontCare
import ks.bs.mysoccerting.util.TLog
import java.util.*


class MainActivity : RxActivityBase() {

    var viewModel: MainViewModel = MainViewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val binding = DataBindingUtil.setContentView<ActivityMainBinding>(this, R.layout.activity_main)
        binding.viewModel = viewModel

        FirebaseMessaging.getInstance()

        registerUserInfo()

        FirebaseHelper.initialize()

        viewModel.userInfoInit()

        initLayout()
    }

    private fun initLayout() {
        val navController = findNavController(R.id.nav_host_fragment)
//        NavigationUI.setupActionBarWithNavController(this, navController)
        NavigationUI.setupWithNavController(bottomNavigation, navController)

        setSupportActionBar(toolbar)
    }

    private fun registerUserInfo() {
        if (auth.currentUser != null)
            LoginManager.getCurrentUser()
                .ifNoNickNameShouldCreate()
                .updateClubInfo()
                .userInfoRegister()
                .publishUserInfo()
                .registerFcmToken()
                .withProgressBallAnimation(context = this)
                .subscribe({ response ->
                    CrashlyticsHelper.set()

                    Prefs.save {
                        Prefs.settings.isLogined = true
                        Prefs.settings.loginUid = LoginManager.user?.uid!!
                    }

                    setupBadge(notification_view.notification_badge)

                }, { e ->
                    TLog.e(e)
                }).addTo(compositeDisposable)
    }

    private fun Single<ModelUser>.publishUserInfo(): Single<DontCare> {
        return this.flatMap { userInfo ->
            LoginManager.userSubject.onNext(userInfo)
            Single.just(DontCare)
        }
    }

    private fun Single<ModelUser>.updateClubInfo(): Single<ModelUser> {
        return this.flatMap { modelUser ->
            if (modelUser.clubUid != null)
                RxRealTimeDB.toSingle("${FirebaseUrls.club}/${modelUser.clubUid}") { ModelClub() }
                    .flatMap {
                        modelUser.club = it.clubName
                        modelUser.clubMasterUid = it.masterUid
                        modelUser.clubImageUrl = it.clubMarkImageUrl
                        modelUser.clubJonined = true

                        Single.just(modelUser)
                    }
                    .pushAndGet()
            else
                this
        }
    }

    private fun Single<DontCare>.registerFcmToken(): Single<DontCare> {
        return this
            .flatMap { response -> FcmHelper.registerPushToken() }
    }


    private fun Single<ModelUser>.userInfoRegister(): Single<ModelUser> {
        return this
            .flatMap { response ->
                LoginManager.user = response
                Single.just(response)
            }
    }

    private fun Single<ModelUser>.ifNoNickNameShouldCreate(): Single<ModelUser> {
        val path = FirebaseUrls.anonymousUserCount
        val num = Random().nextInt(3)
        return this
            .flatMap { response ->
                if (response.nickName.isNullOrEmpty())
                    RxRealTimeDB.counter(path, 1)
                        .zipWith(this)
                        .flatMap {
                            val model = it.second.apply {
                                this.nickName = "축돌이${it.first}"
                                this.uid = LoginManager.auth.currentUser?.uid
                                this.email = auth.currentUser?.email
                                this.profileImageUrl = ProfileUrlType.values()[num].url
                            }
                            Single.just(model)
                        }
                        .pushAndGet()
                else
                    this
            }
    }

    private fun Single<ModelUser>.pushAndGet(): Single<ModelUser> {
        val path = "${FirebaseUrls.users}/${auth.currentUser?.uid}"

        return this
            .flatMap { RxRealTimeDB.push(path, it) }
            .flatMap { RxRealTimeDB.toSingle(path) { ModelUser() } }
    }

    private fun setupBadge(notificationBadgeTv: TextView) {
        val indecatior = object : GenericTypeIndicator<MutableList<ModelAlaram>>() {}
        RxRealTimeDB.toObservable("${FirebaseUrls.alram}/${LoginManager.user?.uid}", indecatior) { mutableListOf() }
            .map { it.count() }
            .subscribe({ count ->
                if (count == 0)
                    notificationBadgeTv.notification_badge.visibility = View.GONE
                else {
                    notificationBadgeTv.notification_badge.visibility = View.VISIBLE
                    notificationBadgeTv.text = count.toString()
                }
            }, { e ->
                TLog.e(e)
            })
            .addTo(compositeDisposable)
    }


    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == android.R.id.home) {
            onBackPressed()
            return false
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onDestroy() {
        viewModel.onDestroy()
        super.onDestroy()
    }

}
