package ks.bs.mysoccerting.scene.club.detail.dialog

import android.content.Context
import android.graphics.drawable.Drawable
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.TextView
import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import io.reactivex.Single
import io.reactivex.SingleSource
import io.reactivex.rxkotlin.addTo
import ks.bs.mysoccerting.R
import ks.bs.mysoccerting.customview.dialog.DialogButton
import ks.bs.mysoccerting.customview.dialog.TDialog
import ks.bs.mysoccerting.exception.ErrorCode
import ks.bs.mysoccerting.fcm.FcmHelper
import ks.bs.mysoccerting.firebase.CrashlyticsHelper
import ks.bs.mysoccerting.firebase.FirebaseUrls
import ks.bs.mysoccerting.firebase.RxRealTimeDB
import ks.bs.mysoccerting.model.ModelAlaram
import ks.bs.mysoccerting.model.ModelClub
import ks.bs.mysoccerting.model.ModelPush
import ks.bs.mysoccerting.model.time.LocalTime
import ks.bs.mysoccerting.model.type.AlarmType
import ks.bs.mysoccerting.model.type.ClubGradeType
import ks.bs.mysoccerting.model.type.ClubGradeType.Companion.gradeToDrawable
import ks.bs.mysoccerting.model.type.ClubGradeType.Companion.toGrade
import ks.bs.mysoccerting.rx.withProgressBallAnimation
import ks.bs.mysoccerting.scene.base.FragmentBaseViewModel
import ks.bs.mysoccerting.scene.club.supervisor.*
import ks.bs.mysoccerting.scene.login.LoginManager
import ks.bs.mysoccerting.support.DontCare
import ks.bs.mysoccerting.util.App
import ks.bs.mysoccerting.util.TLog
import ks.bs.mysoccerting.util.dpToPixels
import ks.bs.mysoccerting.util.getString
import org.jetbrains.anko.collections.forEachWithIndex


class MemberInfoVm(override val listener: Contract) : FragmentBaseViewModel(listener) {
    interface Contract : BaseVmListener {
        fun onBackPressed()
        fun getContext(): Context
        fun showToast(message: String)
        fun fire(isMercenary: Boolean, count: Int)
        fun delegate(count: Int)
    }


    var defaultPos = 0
    var changedGradeName = ""
    var spinnerVisibility = ObservableBoolean(true)
    val gradeImageDrawable = ObservableField<Drawable>()
    val gradeName = ObservableField<String>()
    val posVisibility = ObservableBoolean(true)
    val btnVisibility = ObservableBoolean(true)
    val isMercenary = ObservableBoolean(false)
    var isMaster = false
    val gradeAdapter = ArrayAdapter(
        App.getApplicationContext(),
        R.layout.custom_simple_dropdown_item_1line,
        listOf("선수", "코치")
    )

    var contentId = ""
    var count = 0
    var isInfoDialog = false
    lateinit var model: ModelClub.ModelMember
    val name = ObservableField<String>()
    val userName = ObservableField<String>()
    val position = ObservableField<String>()
    val memberImageUrl = ObservableField<String>()

    fun getContent(model: ModelClub.ModelMember, isInfoDialog: Boolean, contentId: String, count: Int) {
        this.model = model
        this.contentId = contentId
        this.count = count
        this.isInfoDialog = isInfoDialog
        isMercenary.set(model.mercenary)
        memberImageUrl.set(model.memberImageUrl)
        userName.set(model.nickName)
        position.set(model.position)
        gradeImageDrawable.set(ClubGradeType.valueOf(model.gradeType).imageDrawable)
        gradeName.set(ClubGradeType.valueOf(model.gradeType).gradeName)

        defaultPos = ClubGradeType.valueOf(model.gradeType).grade - 1

        if (LoginManager.user?.clubMasterUid == LoginManager.user?.uid)
            isMaster = true
        if (!isInfoDialog || !isMaster || model.gradeType == ClubGradeType.COACH.name)
            btnVisibility.set(false)
        if (position.get().isNullOrEmpty())
            posVisibility.set(false)
        if (ClubGradeType.valueOf(model.gradeType).grade == ClubGradeType.COACH.grade || !isInfoDialog || !isMaster)
            spinnerVisibility.set(false)

    }

    fun onGradeItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        view as TextView
        view.textSize = 25f
        gradeImageDrawable.set(gradeToDrawable(gradeName = view.text.toString()))
        changedGradeName = view.text.toString()
    }

    fun getHeight(): Int {
        return when {
            model.mercenary && isInfoDialog ->
                230.dpToPixels()
            !model.mercenary && isInfoDialog ->
                260.dpToPixels()
            !model.mercenary ->
                200.dpToPixels()
            model.mercenary ->
                160.dpToPixels()
            else ->
                230.dpToPixels()
        }
    }

    fun fire(view: View) {
        requireNotNull(contentId) { ErrorCode.FAIL_NO_EXIST_MY_CLUB_CONTENT_ID.message }
        requireNotNull(model.uid) { ErrorCode.FAIL_NO_OPPONENET_USER_INFO.message }

        var clubName = ""

        TDialog.Builder(context = view.context, title = R.string.notice.getString())
            .setMessage("정말로 추방 하시겠습니까")
            .isColored(true)
            .addButton(DialogButton.CANCEL())
            .addButton(DialogButton.OK())
            .toSingle()
            .filter { it.ok }
            .flatMapSingle { RxRealTimeDB.toSingle("${FirebaseUrls.club}/$contentId") { ModelClub() } }
            .flatMap { modelClub ->
                if (isMercenary.get()) {
                    modelClub.mercenaryMember?.removeAt(count)
                    modelClub.mercenaryNumber -= 1
                } else {
                    modelClub.clubMembers.removeAt(count)
                    modelClub.clubMemberNumber -= 1
                    modelClub.membershipApplicationList?.removeAll { it.uid == model.uid }
                    var pos = ""
                    modelClub.clubPositionList?.forEach {
                        if (it.value == model.nickName)
                            pos = it.key
                    }
                    if (pos.isNotEmpty())
                        modelClub.clubPositionList?.remove(pos)
                    modelClub.assistantList?.removeAll { it == model.uid }
                }
                clubName = modelClub.clubName
                RxRealTimeDB.push("${FirebaseUrls.club}/$contentId", modelClub)
            }
            .flatMap {
                Single.just(it)
                    .firedUserSecession(model.uid, isMercenary.get(), contentId)
                    .updateClubMemberInfo()
                    .removeBoard(model.uid)
                    .removeClubSearch(model.uid)
                    .removeClubMatch(model.uid)
                    .flatMap { RxRealTimeDB.toSingle("${FirebaseUrls.alram}/${model.uid}") { mutableListOf<ModelAlaram>() } }
                    .flatMap { alaramList ->
                        val gradeName = ClubGradeType.valueOf(model.gradeType).gradeName
                        val alarmData = ModelAlaram(
                            senderEmail = LoginManager.user?.email,
                            senderImageUrl = LoginManager.user?.clubImageUrl,
                            timeStamp = LocalTime().time,
                            senderUid = LoginManager.user?.uid!!,
                            senderNickName = LoginManager.user?.nickName,
                            reciverUid = model.uid,
                            alaramTypeCode = AlarmType.CHANGE_CLUB_GRADE.code,
                            message = "[$clubName($gradeName)] ${AlarmType.CLUB_FIRED.text}"
                        )
                        alaramList.add(alarmData)

                        RxRealTimeDB.push("${FirebaseUrls.alram}/${model.uid}", alaramList)
                    }
                    .flatMap {
                        FcmHelper.sendMessage(
                            destinationUid = model.uid,
                            message = "[$clubName] ${AlarmType.CLUB_FIRED.text}"
                        )
                    }
                    .withProgressBallAnimation(view.context)
            }

            .subscribe({
                listener.fire(isMercenary.get(), count)
            }, {
                if (it is NoSuchFieldException)
                    return@subscribe

                CrashlyticsHelper.logException(it)
            }).addTo(compositeDisposable)

    }


    fun delegate(view: View) {
        requireNotNull(contentId) { ErrorCode.FAIL_NO_EXIST_MY_CLUB_CONTENT_ID.message }

        TDialog.Builder(context = view.context, title = R.string.notice.getString())
            .setMessage("정말로 감독을 위임 하시겠습니까")
            .isColored(true)
            .addButton(DialogButton.CANCEL())
            .addButton(DialogButton.OK())
            .toSingle()
            .filter { it.ok }
            .flatMapSingle { RxRealTimeDB.toSingle("${FirebaseUrls.club}/$contentId") { ModelClub() } }
            .flatMap { modelClub ->
                model.gradeType = ClubGradeType.COACH.name

                modelClub.masterUid = model.uid

                modelClub.clubMembers.forEach {
                    if (it.nickName == model.nickName)
                        it.gradeType =ClubGradeType.COACH.name
                }
                modelClub.clubMembers[0].gradeType = ClubGradeType.ASSISTANT.name
                modelClub.clubMembers.sortByDescending { ClubGradeType.valueOf(it.gradeType).grade }
                TLog.d(modelClub.clubMembers)
                RxRealTimeDB.push("${FirebaseUrls.club}/$contentId/clubMembers", modelClub.clubMembers)
                    .flatMap { RxRealTimeDB.push("${FirebaseUrls.club}/$contentId", modelClub) }
                    .flatMap {
                        updateAssistantListWithDelegate()
                            .updateClubMemberInfo()
                            .withProgressBallAnimation(view.context)
                    }
            }
            .subscribe({
                listener.delegate(count)
            }, {
                if (it is NoSuchFieldException)
                    return@subscribe

                CrashlyticsHelper.logException(it)
            }).addTo(compositeDisposable)


    }

    fun cancel(view: View) {
        listener.onBackPressed()
    }

    fun save(context: Context?) {
        if (gradeName.get() != changedGradeName && spinnerVisibility.get() && !model.mercenary) {
            requireNotNull(contentId) { ErrorCode.FAIL_NO_EXIST_MY_CLUB_CONTENT_ID.message }

            model.gradeType = toGrade(gradeName = changedGradeName)

            gradeChange()
                .flatMap { updateAssistantList() }
                .updateClubMemberInfo()
                .withProgressBallAnimation(context)
                .subscribe({
                }, {
                    CrashlyticsHelper.logException(it)
                })
                .let { }

        }
    }

    private fun gradeChange(): Single<ModelPush.Response> {
        val path = "${FirebaseUrls.club}/$contentId/clubMembers"
        return RxRealTimeDB.toSingle(path) { mutableListOf<ModelClub.ModelMember>() }
            .flatMap { memberList ->
                memberList.forEach { modelMember ->
                    if (modelMember.uid == model.uid)
                        modelMember.gradeType = model.gradeType
                }
                memberList.sortByDescending { ClubGradeType.valueOf(it.gradeType).grade }
                RxRealTimeDB.push(path, memberList)
            }
            .flatMap { RxRealTimeDB.toSingle("${FirebaseUrls.alram}/${model.uid}") { mutableListOf<ModelAlaram>() } }
            .flatMap { alaramList ->
                val alarmData = ModelAlaram(
                    senderEmail = LoginManager.user?.email,
                    senderImageUrl = LoginManager.user?.clubImageUrl,
                    timeStamp = LocalTime().time,
                    senderUid = LoginManager.user?.uid!!,
                    senderNickName = LoginManager.user?.nickName,
                    reciverUid = model.uid,
                    alaramTypeCode = AlarmType.CHANGE_CLUB_GRADE.code,
                    message = "${AlarmType.CHANGE_CLUB_GRADE.text} [${ClubGradeType.valueOf(model.gradeType).gradeName}]"
                )
                alaramList.add(alarmData)

                RxRealTimeDB.push("${FirebaseUrls.alram}/${model.uid}", alaramList)
            }
            .flatMap {
                FcmHelper.sendMessage(
                    destinationUid = model.uid,
                    message = "${AlarmType.CHANGE_CLUB_GRADE.text} [${ClubGradeType.valueOf(model.gradeType).gradeName}]"
                )
            }
    }

    private fun updateAssistantList(): Single<DontCare> {
        return RxRealTimeDB.toSingle("${FirebaseUrls.club}/${LoginManager.user?.clubUid}/assistantList") { mutableListOf<String>() }
            .map { assistantList ->
                if (model.gradeType == ClubGradeType.ASSISTANT.name && !assistantList.contains(model.uid)) {
                    assistantList.add(model.uid)
                    assistantList
                } else {
                    assistantList.remove(model.uid)
                    assistantList
                }
            }
            .flatMap { RxRealTimeDB.push("${FirebaseUrls.club}/${LoginManager.user?.clubUid}/assistantList", it) }
    }

    private fun updateAssistantListWithDelegate(): Single<DontCare> {
        return RxRealTimeDB.toSingle("${FirebaseUrls.club}/${LoginManager.user?.clubUid}/assistantList") { mutableListOf<String>() }
            .map { assistantList ->
                if (!assistantList.contains(LoginManager.user?.uid!!))
                    assistantList.add(LoginManager.user?.uid!!)
                assistantList.remove(model.uid)
                assistantList
            }
            .flatMap { RxRealTimeDB.push("${FirebaseUrls.club}/${LoginManager.user?.clubUid}/assistantList", it) }
    }
}