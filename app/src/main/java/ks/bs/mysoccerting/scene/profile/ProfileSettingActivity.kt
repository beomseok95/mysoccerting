package ks.bs.mysoccerting.scene.profile

import android.graphics.drawable.Drawable
import android.net.Uri
import androidx.databinding.DataBindingUtil
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_alarm.*
import kotlinx.android.synthetic.main.activity_profile_setting.*
import kotlinx.android.synthetic.main.activity_profile_setting.toolbar_title
import ks.bs.mysoccerting.R
import ks.bs.mysoccerting.databinding.ActivityProfileSettingBinding
import ks.bs.mysoccerting.rx.rxresult.RxActivityBase
import ks.bs.mysoccerting.util.toDrawable

class ProfileSettingActivity : RxActivityBase() {
    val viewModel = createVm()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        DataBindingUtil.setContentView<ActivityProfileSettingBinding>(
            this,
            R.layout.activity_profile_setting
        ).viewModel = viewModel

        viewModel.initUserData()

        initToolbar()
    }
    private fun initToolbar() {
        toolbar_title.text = "프로필 수정"
    }

    private fun createVm() = ProfileSettingViewModel(object :
        ProfileSettingViewModel.ProfileSettingContract {
        override fun getDrawableFromUri(uri: Uri): Drawable {
            return uri.toDrawable(this@ProfileSettingActivity)
        }

        override fun checkExternalStoragePermission() {
            checkPermission()
        }

        override fun finishSuccess(images: Pair<String?, String?>) {
            finishWithResponse(images)
        }

        override fun bringToFront() {
            circleImageView.bringToFront()
        }
    })

    override fun onDestroy() {
        viewModel.onDestroy()
        super.onDestroy()
    }
}
