package ks.bs.mysoccerting.scene.base

import androidx.databinding.BaseObservable
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.storage.FirebaseStorage
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import ks.bs.mysoccerting.rx.DisposeOnDestroy
import ks.bs.mysoccerting.scene.lifecycle.ActivityLifeCycle

open class ActivityBaseViewModel : BaseObservable(), DisposeOnDestroy, ActivityLifeCycle {
    val compositeDisposable = CompositeDisposable()
    var auth: FirebaseAuth = FirebaseAuth.getInstance()

    override fun onDestroy() {
        clearDisposables()
    }

    override fun clearDisposables() {
        compositeDisposable.clear()
    }

    override fun addDisposable(disposable: Disposable): Boolean {
        return compositeDisposable.add(disposable)
    }

}