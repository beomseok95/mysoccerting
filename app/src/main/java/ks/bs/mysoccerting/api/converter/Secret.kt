package ks.bs.mysoccerting.api.converter

import android.util.Base64
import java.nio.charset.Charset
import javax.crypto.Cipher
import javax.crypto.spec.SecretKeySpec

/**
 * Created by DoraEmong on 2016-07-11.
 */
object Secret {
    private val secretKey = "soccersecretmoduleclassmethod!!!" // 32bit

    @Throws(Exception::class)
    fun encrypt(message: String): String {

        val encrypted = encrypter().doFinal(message.toByteArray(charset("UTF-8")))
        // String enStr = new String(Base64.encodeBase64URLSafe(encrypted));
        val base64 = String(Base64.encode(encrypted, Base64.URL_SAFE))
        // String safeString = enStr.replace('+','-').replace('/','_');
        return base64
    }

    private fun encrypter(): Cipher {
        val secureKey = SecretKeySpec(secretKey.substring(0, 16).toByteArray(), "AES")
        val cipher = Cipher.getInstance("AES")
        cipher.init(Cipher.ENCRYPT_MODE, secureKey)
        return cipher
    }

    @Throws(Exception::class)
    fun decrypt(message: String): String {

        val byteStr = Base64.decode(message.toByteArray(), Base64.URL_SAFE)
        return String(decrypter().doFinal(byteStr), Charset.forName("UTF-8"))
    }

    private fun decrypter(): Cipher {
        val secureKey = SecretKeySpec(secretKey.substring(0, 16).toByteArray(), "AES")
        val cipher = Cipher.getInstance("AES")
        cipher.init(Cipher.DECRYPT_MODE, secureKey)
        return cipher
    }


//        @Throws(Exception::class)
//        @JvmStatic
//        fun main(args: Array<String>) {
//            val a = Secret.instance!!.encrypt(
//                    "terminal_id=A296911I210000003&terminal_pwd=1&login_mod=W&auto_login=0&key=")
//            println(a)
//            val b = Secret.instance!!.decrypt("1uYE1VlByl_k6yb0_fGmtRprMq3a_5rhDA_OPia5R4E34G81qRzvvbWcsaGoqIlr70REcoTPAlCBbPr7Zn46aTEwOtOfXsoFcBnE9iLATbYU4RH_T58irrjJsNajodi-XTMS8Q0kmFglLpb8hrm2ZEy5Ykr8dErCbhQ9VPSIFnmqiYW06_WpgZBl2FOnx6NE5A5tsKl-o5q_kPUHR2OkteP-DWIY82X8wNqCf5A5OGuveC70nnnq1_HtKKbCQPg-7nqKl3EMRGRrG7WBa53zdq2K4j4Rd-wOuWWMgJK1ZwE")
//            println(b)
//        }
}