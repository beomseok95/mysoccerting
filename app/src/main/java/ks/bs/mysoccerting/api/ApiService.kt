package ks.bs.mysoccerting.api

import ks.bs.mysoccerting.api.service.ApiFcm

object ApiService {

    fun fcm() = RetrofitAdapter.getInstance(Urls.getFcmBaseUrl(), false, false)
        .create(ApiFcm::class.java)

}