package ks.bs.mysoccerting.api.service

import io.reactivex.Single
import ks.bs.mysoccerting.api.Header
import ks.bs.mysoccerting.model.ModelPush
import retrofit2.http.Body
import retrofit2.http.HeaderMap
import retrofit2.http.POST

interface ApiFcm {
    @POST("fcm/send")
    fun pushFcm(
        @HeaderMap hader: Header,
        @Body paramater: ModelPush
    ): Single<ModelPush.Response>
}