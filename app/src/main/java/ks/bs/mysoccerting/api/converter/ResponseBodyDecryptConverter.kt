package ks.bs.mysoccerting.api.converter

/**
 * Created by ian on 2017. 10. 29..
 */

import com.google.gson.Gson
import com.google.gson.JsonObject
import com.google.gson.TypeAdapter
import kr.nextm.lib.TLog
import ks.bs.mysoccerting.BuildConfig
import ks.bs.mysoccerting.util.GsonHelper
import okhttp3.ResponseBody
import retrofit2.Converter
import java.io.IOException
import java.io.StringReader

internal class ResponseBodyDecryptConverter<T>(private val gson: Gson, private val adapter: TypeAdapter<T>) :
    Converter<ResponseBody, T> {

    @Throws(IOException::class)
    override fun convert(value: ResponseBody): T {

        val jsonObject: JsonObject = gson.fromJson(value.string(), JsonObject::class.java)

        val dataEncrypt: String = jsonObject.get("data").toString()

        val dataDecoded = Secret.decrypt(dataEncrypt)

        if (BuildConfig.DEV) {
            printPrettyJson(dataDecoded)
        }

        val reader = StringReader(dataDecoded)

        val jsonReader = gson.newJsonReader(reader)

        try {
            return adapter.read(jsonReader)
        } catch (t: Throwable) {
            TLog.e(t)
            throw t
        } finally {
            value.close()
        }
    }


    private fun printPrettyJson(decoded: String) {
        val jsonObject = gson.fromJson(decoded, JsonObject::class.java)

        TLog.d("### ResponseBodyDecryptConverter -->\n" + GsonHelper.pretty.toJson(jsonObject))
    }

}
