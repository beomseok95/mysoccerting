package ks.bs.mysoccerting.api.converter

/**
 * Created by ian on 2017. 10. 29..
 */

import com.google.gson.Gson
import com.google.gson.JsonObject
import com.google.gson.TypeAdapter
import kr.nextm.lib.TLog
import ks.bs.mysoccerting.BuildConfig
import ks.bs.mysoccerting.util.GsonHelper
import okhttp3.ResponseBody
import retrofit2.Converter
import java.io.IOException
import java.io.StringReader

internal class ResponseBodyConverter<T>(private val gson: Gson, private val adapter: TypeAdapter<T>) : Converter<ResponseBody, T> {

    @Throws(IOException::class)
    override fun convert(value: ResponseBody): T {

        val s = value.string()

        if (BuildConfig.DEV) {
            printPrettyJson(s)
        }

        val reader = StringReader(s)

        val jsonReader = gson.newJsonReader(reader)

        try {
            return adapter.read(jsonReader)
        } finally {
            value.close()
        }
    }

    private fun printPrettyJson(json: String) {
        val jsonObject = gson.fromJson(json, JsonObject::class.java)

        TLog.d("### ResponseBodyConverter -->\n" + GsonHelper.pretty.toJson(jsonObject))
    }

}
