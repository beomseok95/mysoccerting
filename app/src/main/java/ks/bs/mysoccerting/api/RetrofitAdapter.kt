package ks.bs.mysoccerting.api

import ks.bs.mysoccerting.BuildConfig
import ks.bs.mysoccerting.api.converter.EncryptConverterFactory
import ks.bs.mysoccerting.util.GsonHelper
import okhttp3.JavaNetCookieJar
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.net.CookieManager
import java.net.CookiePolicy
import java.net.URI
import java.util.*
import java.util.concurrent.TimeUnit

object RetrofitAdapter {
    private val TIMEOUT: Long = 20

    private val _instances = Hashtable<String, Retrofit>()

    fun getInstance(baseUri: URI, encrypt: Boolean = true, decrypt: Boolean = true): Retrofit {
        return getInstance(baseUri.toString(), encrypt, decrypt)
    }

    @Synchronized
    fun getInstance(baseUrl: String, encrypt: Boolean = true, decrypt: Boolean = true): Retrofit {
        val key = "$baseUrl $encrypt $decrypt"

        if (!_instances.containsKey(key)) {
            val instance = createInstanceWithUrl(baseUrl, encrypt, decrypt)
            _instances[key] = instance
        }

        return _instances[key]!!
    }

    private fun createInstanceWithUrl(baseUrl: String, encrypt: Boolean = true, decrypt: Boolean = true): Retrofit {
        val gson = GsonHelper.gson()

        if (decrypt) {
            require(encrypt, { "decrypt 할 경우, 반드시 encrypt 도 되어야 한다." })
        }

        if (!encrypt) {
            require(!decrypt, { "encrypt 안 할 경우, 반드시 decrypt 도 안해야 한다." })
        }

        val converterFactory =
            if (!encrypt)
                GsonConverterFactory.create(gson)
            else
                EncryptConverterFactory(gson, encrypt, decrypt)

        return Retrofit.Builder()
            .baseUrl(baseUrl)
            .client(createClient())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(converterFactory)
            .build()
    }

    private fun createClient(): OkHttpClient {

        return createHttpClientBuilder()
            .build()
    }

    fun createHttpClientBuilder(): OkHttpClient.Builder {
        return OkHttpClient.Builder()
            .connectTimeout(TIMEOUT, TimeUnit.SECONDS)
            .writeTimeout(TIMEOUT, TimeUnit.SECONDS)
            .readTimeout(TIMEOUT, TimeUnit.SECONDS)
            .cookieJar(JavaNetCookieJar(cookieManager()))
            .addInterceptor(httpLoggingInterceptor())
    }

    private fun cookieManager(): CookieManager {
        val cookieManager = CookieManager()
        cookieManager.setCookiePolicy(CookiePolicy.ACCEPT_ALL)
        return cookieManager
    }

    private fun httpLoggingInterceptor(): HttpLoggingInterceptor {
        val httpLoggingInterceptor = HttpLoggingInterceptor()

        if (BuildConfig.DEV) {
            httpLoggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
        } else {
            httpLoggingInterceptor.level = HttpLoggingInterceptor.Level.NONE
        }
        return httpLoggingInterceptor
    }
}
