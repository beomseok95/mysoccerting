package ks.bs.mysoccerting.api


class Header : HashMap<String, String>() {
    companion object {
        val fcmServerKey =
            "AAAAL7pGoiQ:APA91bEjAe6ou0JRRbafGbE7cDbRiS9YRYVA8yTRAtRQQ2BlK7qN1pB1uHpxGyqUqV80ffitWncWePG5mTrM4c7Pys_sznW-grSRsI9xLHOrAJulvTEt4C9DdSSS5BaNNUg7Zo_s7NNn"

        fun create(): Header {
            val header = Header()

            header["Content-Type"] = "application/json; charset=utf-8"
            header["Authorization"] = "key=$fcmServerKey"
            //header["Content-Type"] = "application/x-www-form-urlencoded;charset=UTF-8"
            return header
        }
    }
}
