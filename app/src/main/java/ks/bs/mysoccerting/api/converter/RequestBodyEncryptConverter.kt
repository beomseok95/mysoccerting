package ks.bs.mysoccerting.api.converter

import com.google.gson.JsonObject
import kr.nextm.lib.TLog
import ks.bs.mysoccerting.model.ModelBase
import ks.bs.mysoccerting.util.GsonHelper
import retrofit2.Converter
import retrofit2.Retrofit
import java.lang.reflect.Type

/**
 * Created by ian on 2018. 2. 20..
 */
class RequestBodyEncryptConverter(val type: Type?, val annotations: Array<Annotation>?, val retrofit: Retrofit?) : Converter<Any, String> {
    override fun convert(value: Any?): String {

        val needEncrypt = (value is ModelBase || value is JsonObject)  /** model base check */

        TLog.v("### RequestBodyEncryptConverter encrypt($needEncrypt) -->\n${GsonHelper.pretty.toJson(value)}")

        if (needEncrypt) {
            return encrypt(value)
        }

        return value.toString()
    }

    private fun encrypt(value: Any?): String {
        val json = GsonHelper.gson().toJson(value)
        val encrypt = Secret.encrypt(json)
        return encrypt
    }

}