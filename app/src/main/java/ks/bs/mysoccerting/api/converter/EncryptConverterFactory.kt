package ks.bs.mysoccerting.api.converter

/**
 * Created by ian on 2017. 10. 29..
 */

import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import okhttp3.ResponseBody
import retrofit2.Converter
import retrofit2.Retrofit
import java.lang.reflect.Type

class EncryptConverterFactory(private val gson: Gson, val encrypt: Boolean, val decrypt: Boolean) : Converter.Factory() {

    override fun stringConverter(type: Type?, annotations: Array<Annotation>?, retrofit: Retrofit?): Converter<*, String>? {

        return if (encrypt)
            RequestBodyEncryptConverter(type, annotations, retrofit)
        else
            RequestBodyConverter(type, annotations, retrofit)
    }

    override fun responseBodyConverter(type: Type, annotations: Array<Annotation>?,
                                       retrofit: Retrofit?): Converter<ResponseBody, *> {
        val adapter = gson.getAdapter(TypeToken.get(type))

        return if (decrypt)
            ResponseBodyDecryptConverter(gson, adapter)
        else
            ResponseBodyConverter(gson, adapter)
    }
}
