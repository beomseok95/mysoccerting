package ks.bs.mysoccerting.api.converter

import kr.nextm.lib.TLog
import ks.bs.mysoccerting.util.GsonHelper
import retrofit2.Converter
import retrofit2.Retrofit
import java.lang.reflect.Type

/**
 * Created by ian on 2018. 2. 20..
 */
class RequestBodyConverter(val type: Type?, val annotations: Array<Annotation>?, val retrofit: Retrofit?) : Converter<Any, String> {
    override fun convert(value: Any?): String {
        TLog.v("### RequestBodyConverter -->\n${GsonHelper.pretty.toJson(value)}")

        return value.toString()
    }
}