package ks.bs.mysoccerting.test

import android.app.Activity
import android.os.Bundle
import io.reactivex.Observable
import ks.bs.mysoccerting.util.TLog
import java.util.concurrent.TimeUnit

class JustOkActivity : Activity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        TLog.d("#JustOkActivity onCreate")
        Observable.timer(1, TimeUnit.SECONDS)
                .subscribe {
                    TLog.d("#RETURN with RESULT_OK")
                    setResult(RESULT_OK)
                    finish()
                }

    }
}
