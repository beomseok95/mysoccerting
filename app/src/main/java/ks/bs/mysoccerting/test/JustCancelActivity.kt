package ks.bs.mysoccerting.test

import android.app.Activity

class JustCancelActivity : Activity() {
    override fun onStart() {
        super.onStart()

        onBackPressed()
    }
}
