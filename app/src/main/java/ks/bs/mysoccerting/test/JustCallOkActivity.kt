package ks.bs.mysoccerting.test

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import io.reactivex.Observable
import ks.bs.mysoccerting.rx.caller.ActivityBundleObservable
import ks.bs.mysoccerting.util.TLog
import java.util.concurrent.TimeUnit

class JustCallOkActivity : Activity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        TLog.d("#JustCallOkActivity onCreate")

        Observable.timer(1, TimeUnit.SECONDS)
                .subscribe {
                    ActivityBundleObservable(null, JustOkActivity::class.java, Bundle())
                            .createObservable()
                            .subscribe {
                                val intent = Intent()
                                intent.putExtras(it)

                                setResult(RESULT_OK, intent)
                                finish()
                            }
                }
    }
}
