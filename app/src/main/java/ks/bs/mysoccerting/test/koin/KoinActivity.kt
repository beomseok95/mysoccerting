package ks.bs.mysoccerting.test.koin

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_koin.*
import ks.bs.mysoccerting.R
import org.koin.android.ext.android.inject

class KoinActivity : AppCompatActivity() {

    val testb: TestB by inject()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_koin)

        textView.text = testb.service.doSometing()

    }


}

