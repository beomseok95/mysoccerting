package ks.bs.mysoccerting.test

import android.app.Activity
import android.os.Bundle
import io.reactivex.Observable
import ks.bs.mysoccerting.rx.caller.ActivityObservable
import ks.bs.mysoccerting.util.TLog
import java.util.concurrent.TimeUnit

class JustCallWithCountActivity : Activity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        TLog.d("#JustCallOkActivity onCreate")

        Observable.timer(1, TimeUnit.SECONDS)
                .subscribe {

                    val inputData = ActivityObservable.getObject<ExtraData>(intent, ExtraData::class.java)!!

                    if (inputData.count - 1 <= 0) {
                        setResult(RESULT_OK, intent)
                        finish()
                        return@subscribe
                    }

                    ActivityObservable<ExtraData>(
                        null,
                        javaClass,
                        ExtraData(inputData.count - 1),
                        inputData.javaClass
                    )
                            .createObservable()
                            .subscribe {
                                setResult(RESULT_OK, intent)
                                finish()
                            }
                }
    }

    data class ExtraData(var count: Int)

}
