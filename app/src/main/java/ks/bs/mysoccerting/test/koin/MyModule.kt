package ks.bs.mysoccerting.test.koin

import org.koin.dsl.bind
import org.koin.dsl.module

val appModul = module {

    single { ServiceImpl() } bind Service::class

    factory { TestA(get()) }
    factory { TestB(get()) }
}


class TestA(val service: Service)
class TestB(val service: ServiceImpl)
