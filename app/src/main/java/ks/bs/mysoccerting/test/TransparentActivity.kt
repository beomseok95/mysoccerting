package ks.bs.mysoccerting.test

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import ks.bs.mysoccerting.R

open class TransparentActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_transparent)
    }
}
