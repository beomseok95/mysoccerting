package ks.bs.mysoccerting.test.koin

import ks.bs.mysoccerting.model.time.LocalTime
import ks.bs.mysoccerting.model.time.Pattern


class ServiceImpl : Service {
    override fun doSometing() = "${LocalTime(System.currentTimeMillis()).formatString(Pattern.toStringDefault)}"
}