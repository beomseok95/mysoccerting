package ks.bs.mysoccerting.databinding

import android.graphics.drawable.Drawable
import android.view.View
import android.view.ViewGroup
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.Button
import android.widget.GridLayout
import android.widget.ImageView
import android.widget.LinearLayout
import androidx.databinding.BindingAdapter
import androidx.databinding.ObservableField
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.SimpleTarget
import com.bumptech.glide.request.transition.Transition
import com.google.android.flexbox.FlexboxLayout
import com.google.android.material.chip.Chip
import com.google.android.material.chip.ChipGroup
import com.google.android.material.tabs.TabLayout
import ks.bs.mysoccerting.R
import ks.bs.mysoccerting.customview.*
import ks.bs.mysoccerting.model.type.PositionType
import ks.bs.mysoccerting.model.type.ProfileType
import ks.bs.mysoccerting.rx.onClickWithAnimation
import ks.bs.mysoccerting.scene.search.SearchViewModel
import ks.bs.mysoccerting.util.TLog
import ks.bs.mysoccerting.util.dpToPixels
import ks.bs.mysoccerting.util.toColor
import ks.bs.mysoccerting.util.toSafeInt
import java.util.*


class BindingAdapter {

    companion object {

        @JvmStatic
        @BindingAdapter("loadUrl")
        fun setWebView(webView: WebView, url: String?) {
            webView.webViewClient = WebViewClient()
            webView.loadUrl(url)
        }

        @JvmStatic
        @BindingAdapter("bind:setChipChild")
        fun setChipChild(parent: ChipGroup, list: List<String>) {
            if (list.isEmpty()) return

            parent.removeAllViews()

            list.forEach { text ->
                val chip = Chip(parent.context)
                chip.text = text

                chip.chipIcon = PositionType.valueOf(text).iconDrawable
                chip.setTextAppearanceResource(R.style.ChipTextStyle)
                chip.setChipBackgroundColorResource(PositionType.valueOf(text).bgColor)
                chip.textSize = 14f
                chip.chipIconSize = 50f
                chip.layoutParams = ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, 20.dpToPixels())
                parent.addView(chip)
            }
        }

        @JvmStatic
        @BindingAdapter("bind:verticalDivider")
        fun recyclerViewVerticalDivider(view: RecyclerView, verticalDivider: Drawable) {
            if (view.itemDecorationCount == 0 || view.getItemDecorationAt(view.itemDecorationCount - 1) !is DividerItemDecoration) {
                val decoration = DividerItemDecoration(view.context, DividerItemDecoration.VERTICAL)
                decoration.setDrawable(verticalDivider)
                view.addItemDecoration(decoration)
            }
        }


        @JvmStatic
        @BindingAdapter("bind:falseGoneVisibility")
        fun falseGoneVisibility(v: View, visible: Boolean) {
            v.visibility = visible.toVisibility(ifFalse = View.GONE)
        }

        @JvmStatic
        @BindingAdapter("bind:setClickable")
        fun setClickable(v: View, visible: Boolean) {
            v.isClickable = visible
        }

        @JvmStatic
        @BindingAdapter("bind:setAgeIconColor")
        fun setAgeIconColor(view: View, age: String?) {
            if (age == null)
                view.setBackgroundResource(20.toColor())
            else
                view.setBackgroundResource(age.split("대")[0].toSafeInt().toColor())
        }

        @JvmStatic
        @BindingAdapter("onClickWithAnimation")
        fun onClickWithAnimation(view: View, onClick: View.OnClickListener) {
            view.onClickWithAnimation { onClick.onClick(view) }
        }

        @JvmStatic
        @BindingAdapter("onClickableWithAnimation", "onClickable", requireAll = false)
        fun onClickableWithAnimation(
            view: View,
            onClick: View.OnClickListener,
            isClickable: ObservableField<Boolean> = ObservableField(false)
        ) {
            view.setOnClickListener { }
            if (isClickable.get()!!) {
                view.onClickWithAnimation { onClick.onClick(view) }
            }
        }

        @JvmStatic
        @BindingAdapter("bind:setCityAreaView")
        fun addCityAreaView(layout: GridLayout, item: MutableList<SelectAreaView>?) {
            item?.forEach {
                layout.addView(it)

            }
        }

        @JvmStatic
        @BindingAdapter("bind:setCloudInfoView")
        fun setCloudInfoView(v: FlexboxLayout, items: MutableList<String>?) {
            items?.forEach {
                TLog.d(it)
                v.addView(CloudInfoView(v.context).setContent(it))
            }
        }

        @JvmStatic
        @BindingAdapter("bind:setTownAreaView")
        fun addTownAreaView(layout: GridLayout, item: MutableList<SelectAreaView>?) {
            item?.forEach {
                layout.addView(it)
            }
        }

        @JvmStatic
        @BindingAdapter("bind:mercenaryPosition")
        fun setPositionLinearLayout(layout: LinearLayout, position: String?) {
            layout.removeAllViews()
            if (position != null)
                layout.addPosition(position)
        }

        @JvmStatic
        @BindingAdapter("itemImageLoad", "isClub")
        fun imageLoad(imageView: ImageView, imageUrl: String?, isClub: Boolean) {

            if (isClub)
                Glide.with(imageView.context)
                    .load(imageUrl)
                    .skipMemoryCache(true)
                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                    .apply(RequestOptions().centerCrop())
                    .placeholder(R.drawable.ic_club_mark_default)
                    .error(R.drawable.ic_club_mark_default)
                    .into(imageView)
            else {
                val num = Random().nextInt(3)
                val randomProfileImage = ProfileType.values()[num].image

                Glide.with(imageView.context)
                    .load(imageUrl)
                    .skipMemoryCache(true)
                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                    .apply(RequestOptions().centerCrop())
                    .placeholder(randomProfileImage)
                    .error(randomProfileImage)
                    .into(imageView)
            }
        }

        @JvmStatic
        @BindingAdapter("bind:backgroundImageLoad")
        fun backgroundLoad(layout: ViewGroup, imageUrl: String?) {
            Glide.with(layout.context)
                .load(imageUrl)
                .skipMemoryCache(true)
                .diskCacheStrategy(DiskCacheStrategy.NONE)
                .error(R.drawable.bg_white)
                .placeholder(R.drawable.background_loading)
                .apply(RequestOptions().centerCrop())
                .into(object : SimpleTarget<Drawable>() { //CustomViewTarget   바꿔야함
                    override fun onResourceReady(resource: Drawable, transition: Transition<in Drawable>?) {
                        layout.background = resource
                    }

                    override fun onLoadFailed(errorDrawable: Drawable?) {
                        super.onLoadFailed(errorDrawable)
//                        layout.backgroundColorResource = R.color.colorWhite
                    }
                })
        }

        @JvmStatic
        @BindingAdapter("bind:setPressed")
        fun setPressed(btn: Button, isPressed: Boolean) {
            btn.isPressed = isPressed
        }

        @JvmStatic
        @BindingAdapter("bind:setTabLayout")
        fun setTabLayout(tabLayout: TabLayout, viewModel: SearchViewModel) {
            tabLayout.addTab(tabLayout.newTab().setCustomView(MercenaryTab(tabLayout.context)))
            tabLayout.addTab(tabLayout.newTab().setCustomView(ClubTab(tabLayout.context)))
            tabLayout.addOnTabSelectedListener(viewModel.tabLayoutListener)
        }
    }
}
