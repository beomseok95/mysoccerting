package ks.bs.mysoccerting.databinding

import androidx.databinding.ObservableField
import androidx.databinding.ViewDataBinding
import androidx.annotation.LayoutRes

open class FilteredObservableRecyclerViewAdapter<ITEM : Any, B : ViewDataBinding>(
        @LayoutRes layout: Int,
        observableItems: ObservableField<List<ITEM>>,
        bindingVariableId: Int? = null,
        val filter: (ITEM) -> Boolean = { true })
    : ObservableRecyclerViewAdapter<ITEM, B>(layout, observableItems, bindingVariableId) {

    private val itemsFiltered get() = items.filter(filter)

    override fun getItemCount(): Int {
        return itemsFiltered.count()
    }

    override fun onBindViewHolder(holder: BaseViewHolder<B>, position: Int) {
        holder.onBindViewHolder(itemsFiltered[position])
    }

}