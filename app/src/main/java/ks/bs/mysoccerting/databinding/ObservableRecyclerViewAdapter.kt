package ks.bs.mysoccerting.databinding

import androidx.databinding.ObservableField
import androidx.databinding.ViewDataBinding
import androidx.annotation.LayoutRes
import androidx.recyclerview.widget.RecyclerView
import android.view.ViewGroup

open class ObservableRecyclerViewAdapter<ITEM : Any, B : ViewDataBinding>(
        @LayoutRes val layout: Int,
        val observableItems: ObservableField<List<ITEM>>,
        val bindingVariableId: Int? = null)
    : RecyclerView.Adapter<BaseViewHolder<B>>() {
    val items get() = observableItems.get()!!

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder<B> {
        return object : BaseViewHolder<B>(
                layout = layout,
                parent = parent,
                bindingVariableId = bindingVariableId
        ) {}
    }

    override fun getItemCount(): Int {
        return items.count()
    }

    override fun onBindViewHolder(holder: BaseViewHolder<B>, position: Int) {
        holder.onBindViewHolder(items[position])
    }

}