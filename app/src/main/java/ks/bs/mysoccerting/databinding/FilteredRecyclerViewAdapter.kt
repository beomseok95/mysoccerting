package ks.bs.mysoccerting.databinding

import androidx.databinding.ViewDataBinding
import androidx.annotation.LayoutRes
import android.view.ViewGroup
import ks.bs.mysoccerting.util.TLog

open class FilteredRecyclerViewAdapter<ITEM : Any, B : ViewDataBinding>(
        @LayoutRes layout: Int,
        bindingVariableId: Int? = null)
    : BaseRecyclerViewAdapter<ITEM, B>(layout, bindingVariableId) {

    var filter: (ITEM) -> Boolean = { true }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder<B> {
        return object : BaseViewHolder<B>(
                layout = layout,
                parent = parent,
                bindingVariableId = bindingVariableId
        ) {}
    }

    override fun getItemCount(): Int {
        return filteredItems().count()
    }

    override fun onBindViewHolder(holder: BaseViewHolder<B>, position: Int) {
        holder.onBindViewHolder(filteredItems()[position])
    }

    private fun filteredItems() = items.filter(filter)

    fun changeFilter(filter: (ITEM) -> Boolean) {
        this.filter = filter
        notifyDataSetChanged()
    }

    fun resetFilter() {
        changeFilter { true }
    }

    fun getTotalItemCount(): Int {
        return super.getItemCount()
    }
}