package ks.bs.mysoccerting.exception


enum class ErrorCode(val code: String, val message: String) {
    SUCCESS("0000", "succes"),
    FAILED("f999", "fail"),
    PREVIOUS_JOB_NOT_FINISHED_YET("m001", "연결 시간이 초과되었습니다."),
    TIMEOUT("m002", "시간이 초과되었습니다"),
    SOCKET_TIMEOUT("m003", "이전 작업이 진행중입니다."),
    USER_CANCEL("m011", "사용자 취소"),
    THREAD_ERROR("m012", "Thread Error"),
    PERMISSION_DENIED("m013", "Permission Denied"),
    CLUB_MATCH_EXIST("club match exist", "다른클럽과 매칭중이여서 탈퇴가 불가능합니다."),
    SAME_EMAIL(
        "An account already exists with the same email address but different sign-in credentials. Sign in using a provider associated with this email address.",
        "같은 이메일로 다른 소셜로그인이 등록되어 있습니다."
    ),
    ALREADY_ACOUNT(
        "The email address is already in use by another account.",
        "이미 같은 이메일로 가입되어 있습니다."
    ),
    INVALID_OR_NO_PASSWORD(
        "The password is invalid or the user does not have a password.",
        "암호가 유효하지 않거나 \n사용자가 암호를 가지고 있지 않습니다."
    ),
    NO_USER_EMAIL(
        "There is no user record corresponding to this identifier. The user may have been deleted.",
        "이메일이 존재하지 않습니다."
    ),
    EMAIL_FORMAT_WRONG(
        "The email address is badly formatted.",
        "이메일주소의 형식이 잘못되었습니다."
    ),
    NO_CURRENT_LOGIN_USER(
        "current login user is null",
        "현재 로그인한 유저가 정보가 없습니다."
    ),
    NO_EXIST_CLUB(
        "club is null",
        "존재하지 않는 클럽입니다."
    ),
    FAIL_NO_EXIST_MY_CLUB_CONTENT_ID(
        "FAIL_NO_EXIST_MY_CLUB_CONTENT_ID",
        "내 클럽 정보가 존재하지않습니다.\n" +
                "요청을 실패하였습니다."
    ),
    FAIL_NO_OPPONENET_USER_INFO(
        "FAIL_NO_OPPONENET_USER_INFO",
        "상대방의 정보가 존재하지 않습니다.\n" +
                "요청을 실패하였습니다."
    ),
    FAIL_NO_SENDER_UID(
        "FAIL_NO_SENDER_UID",
        "보낸이의 정보가 존재하지 않습니다.\n" +
                "요청을 실패하였습니다."
    ),
    FAIL_NO_RECIVER_UID(
        "FAIL_NO_RECIVER_UID",
        "받는이의 정보가 존재하지 않습니다.\n" +
                "요청을 실패하였습니다."
    ),
    FAIL_NO_USER_INFO_SELF(
        "FAIL_NO_USER_INFO",
        "내 정보가 존재하지 않습니다.\n" +
                "요청을 실패하였습니다."
    ),
    FAIL_NO_POST_WRITER_INFO(
        "FAIL_NO_POST_WRITER_INFO",
        "게시글 작성자의 정보가 존재하지 않습니다.\n" +
                "요청을 실패하였습니다."
    ),
    FAIL_NO_CLUB_MASTER_UID(
        "FAIL_NO_CLUB_MASTER_UID",
        "클럽마스터의 정보가존재하지않습니다."),
    FAIL_NO_POST_CONTENT_ID(
        "FAIL_NO_POST_CONTENT_ID",
        "게시글 정보가 존재하지 않습니다.\n" +
                "요청을 실패하였습니다."
    ),
    FAIL_NO_MASTER_UID(
        "FAIL_NO_MASTER_UID",
        "클럽 감독의 정보가 존재하지 않습니다.\n" +
                "요청을 실패하였습니다."
    ),
    CLUB_NAME_DUPLICATE(
        "FAIL_NO_MASTER_UID",
        "이미 존재하는 클럽 이름입니다."
    )
    ;

}