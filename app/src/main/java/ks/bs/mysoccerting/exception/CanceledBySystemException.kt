package ks.bs.mysoccerting.exception

import ks.bs.mysoccerting.R
import ks.bs.mysoccerting.util.getString

open class CanceledBySystemException(message: String = R.string.canceld_by_system.getString()) :
    RuntimeException(message) {

    constructor(throwable: Throwable) : this(throwable.message!!)
}