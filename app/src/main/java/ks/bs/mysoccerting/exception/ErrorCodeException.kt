package ks.bs.mysoccerting.exception

import ks.bs.mysoccerting.rx.ResponseHandleError

open class ErrorCodeException(val code: String, override val message: String) : ErrorLogException("[$code]") {
    constructor(response: ResponseHandleError) : this(response.getErrorCode(), response.getErrorMessage())
    constructor(errorCode: ErrorCode) : this(errorCode.code, errorCode.message)


}