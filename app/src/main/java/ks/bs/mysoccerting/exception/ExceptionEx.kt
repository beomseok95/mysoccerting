package ks.bs.mysoccerting.exception

import android.os.NetworkOnMainThreadException
import io.reactivex.exceptions.CompositeException
import ks.bs.mysoccerting.R
import ks.bs.mysoccerting.rx.toDialogMessageElse
import ks.bs.mysoccerting.util.addDebugSuffix
import ks.bs.mysoccerting.util.getString
import java.net.SocketException
import java.net.SocketTimeoutException
import java.net.UnknownHostException
import java.nio.channels.UnresolvedAddressException
import java.util.concurrent.TimeoutException


fun Throwable.toList(): List<Throwable> {
    if (this is CompositeException) {
        return this.exceptions
    }

    return listOf(this)
}

inline fun <reified T> Throwable.hasTypeOf(): Boolean {
    return toList().any {
        it is T
    }
}

inline fun <reified T> Throwable.getTypeOf(): List<T> {
    return toList().filter {
        it is T
    }
        .map {
            it as T
        }
}

fun Throwable.toDialogMessage(): String {

    return when (this) {
        is CompositeException ->
            toCompositeExceptionMessage()

        is TimeoutException ->
            ErrorCode.TIMEOUT.message.addDebugSuffix(this)
        is SocketTimeoutException ->
            ErrorCode.SOCKET_TIMEOUT.message.addDebugSuffix(this)
        is NetworkOnMainThreadException ->
            ErrorCode.THREAD_ERROR.message.addDebugSuffix(this)

        is SocketException,
        is UnresolvedAddressException,
        is UnknownHostException ->
            R.string.check_network_connection_exception.getString()
                .addDebugSuffix(this)

        else ->
            toDialogMessageElse(this)
    }
}

private fun CompositeException.toCompositeExceptionMessage(): String {
    return if (exceptions.size == 1) {
        exceptions.first().toDialogMessage()
    } else {
        exceptions.joinToString(",\n") { it.toDialogMessage() }
            .let { messages ->
                "[${R.string.composite_error.getString()}]\n$messages"
            }
    }
}

fun toDialogMessageElse(throwable: Throwable): String {
    throwable.message?.let {
        return it
    }

    return throwable.javaClass.simpleName
}