
package ks.bs.mysoccerting.exception

import ks.bs.mysoccerting.R
import ks.bs.mysoccerting.util.getString

open class CanceledByUserException(message: String = R.string.canceled_by_user.getString()) : RuntimeException(message) {

    constructor(throwable: Throwable) : this(throwable.message!!)
}