package ks.bs.mysoccerting.support

import ks.bs.mysoccerting.model.type.YesNo

object DontCare {
    val string = ""
    val int = 0
    val yesNo = YesNo.N
}