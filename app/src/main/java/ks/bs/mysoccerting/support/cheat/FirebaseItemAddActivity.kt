package ks.bs.mysoccerting.support.cheat

import kotlinx.android.synthetic.main.activity_cheat.*
import ks.bs.mysoccerting.fcm.FcmHelper
import ks.bs.mysoccerting.firebase.FirebaseUrls
import ks.bs.mysoccerting.firebase.RxRealTimeDB
import ks.bs.mysoccerting.model.ModelClub
import ks.bs.mysoccerting.model.ModelClubMatch
import ks.bs.mysoccerting.util.TLog
import kotlin.random.Random

class FirebaseItemAddActivity : CheatBaseActivity() {
    data class DummyData(
        val name: String? = null,
        val contentsUid: String
    )

    override fun initializeMenus() {
        rightMenu.addButton("fcm push") {
            FcmHelper.sendMessage("kx1DMafpN1VCE5uXh2RppEYIvjO2", "test", "안녕하세요안녕하세요")
                .subscribe({ response ->
                    println("성공 response-->$response")
                }, { e ->
                    println(
                        e
                    )
                })
        }

        rightMenu.addButton("register fcm toekn") {
            FcmHelper.registerPushToken()
                .subscribe({
                    println("success ")
                }, {
                    println(it)
                })
        }


        rightMenu.addButton("파이어베이스 클럽 둘러보기 추가") {
            val document = RxRealTimeDB.generateRandomKey("club")
            val memberList = mutableListOf<ModelClub.ModelMember>()
            val idx = Random(System.currentTimeMillis()).nextInt(20)
            for (i in 1..idx)
                memberList.add(
                    ModelClub.ModelMember(
                        "축돌이$i",
                        "H4toqck4oQMYscVxCFwJyvcfw6u1",
                        "https://pbs.twimg.com/profile_images/731721244689620992/W7l0sZni_400x400.jpg",
                        "FW"
                    )
                )

            val club = ModelClub(
                masterUid = "H4toqck4oQMYscVxCFwJyvcfw6u1",
                clubIntro = "조기축구회 회원구해요",
                clubMarkImageUrl = "https://pbs.twimg.com/profile_images/731721244689620992/W7l0sZni_400x400.jpg",
                clubName = "중랑조기축구회",
                clubMembers = memberList,
                clubMemberNumber = memberList.size,
                activityAge = Random.nextInt(60) + 1,
                activityArea = "서울특별시 중랑구",
                createAt = System.currentTimeMillis().toString(),
                contentId = document
            )

            val pushPath = "${FirebaseUrls.club}/$document"
            RxRealTimeDB.push(pushPath, club)
                .subscribe({}, { TLog.e(it) })

        }
        rightMenu.addButton("파이어베이스 클럽 매칭 추가") {
            val document = RxRealTimeDB.generateRandomKey("post")

            val clubMatch =
                ModelClubMatch(
                    uid = "H4toqck4oQMYscVxCFwJyvcfw6u1",
                    content = "매칭하실분 구합니다",
                    clubImageUrl = "https://pbs.twimg.com/profile_images/731721244689620992/W7l0sZni_400x400.jpg",
                    hopeDate = "1월1일",
                    timestamp = System.currentTimeMillis(),
                    clubName = "레알마드리드",
                    contentId = document
                )

            val pushPath = "${FirebaseUrls.clubMatch}/$document"
            RxRealTimeDB.push(pushPath, clubMatch)
                .subscribe({}, { TLog.e(it) })

        }
    }
}
