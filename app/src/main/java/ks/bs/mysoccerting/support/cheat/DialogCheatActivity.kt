package ks.bs.mysoccerting.support.cheat

import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_cheat.*
import ks.bs.mysoccerting.customview.dialog.ImageDialog
import ks.bs.mysoccerting.customview.dialog.TDialog
import ks.bs.mysoccerting.customview.dialog.TMultipleSelectionDialog
import ks.bs.mysoccerting.rx.ObservableEx
import ks.bs.mysoccerting.rx.toJson
import ks.bs.mysoccerting.rx.withProgressAnimation
import ks.bs.mysoccerting.scene.filter.AreaDialog
import ks.bs.mysoccerting.scene.filter.CalendarDialog
import ks.bs.mysoccerting.scene.filter.PeopleNumberDialog
import ks.bs.mysoccerting.scene.filter.PositionDialog
import ks.bs.mysoccerting.util.TLog
import ks.bs.mysoccerting.util.dpToPixels

class DialogCheatActivity : CheatBaseActivity() {

    override fun initializeMenus() {
        rightMenu.addButton("new position Dialog") {

            PositionDialog()
                .toSingle(this)
                .subscribe({
                    println(it.toJson())
                }, { e ->
                    TLog.e(e)
                })
        }
        rightMenu.addButton("new people number dialog") {

            PeopleNumberDialog()
                .toSingle(this)
                .subscribe({
                    println(it.toJson())
                }, { e ->
                    TLog.e(e)
                })
        }
        rightMenu.addButton("new calendar Dialog ") {

            CalendarDialog()
                .toSingle(this)
                .subscribe({
                    println(it.toJson())
                }, { e ->
                    TLog.e(e)
                })
        }

        rightMenu.addButton("new area Dialog ") {
            AreaDialog()
                .toSingle(this)
                .subscribe({
                    println(it.toJson())
                }, { e ->
                    TLog.e(e)
                })
        }

        rightMenu.addButton("이미지 다이얼로그 출력") {
            val url = "https://pbs.twimg.com/profile_images/731721244689620992/W7l0sZni_400x400.jpg"
            ImageDialog.Builder(this, "")
                .setImgUrl(url)
                .isSettingDialog(true)
                .setSize(400.dpToPixels(), 300.dpToPixels())
                .create()
                .show()
        }

        rightMenu.addButton("Dialog 프로그레스바") {
            ObservableEx.safeCallable {
                ObservableEx.safeCallable {
                    Thread.sleep(2000)
                }
                    .withProgressAnimation(this) {}
                    .subscribe()
            }
                .subscribeOn(Schedulers.io())
                .subscribe({
                    println(it)
                }, {
                    println(it)
                })
        }
        rightMenu.addButton("T다이어로그 출력") {
            TDialog.show(this, "텍스트다이어로그 출력")
        }
        rightMenu.addButton("T멀티플셀렉트다이어로그 출력") {
            TMultipleSelectionDialog.Builder<String>(this)
                .setTitle("제목")
                .setElements(listOf("1", "2", "3"))
                .setTransform { it }
                .toObservable()

        }
    }
}
