package ks.bs.mysoccerting.support.cheat

import android.app.Activity
import android.os.Bundle
import android.view.KeyEvent
import android.widget.Toast
import com.google.firebase.database.GenericTypeIndicator
import com.kakao.usermgmt.UserManagement
import com.kakao.usermgmt.callback.LogoutResponseCallback
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.zipWith
import kotlinx.android.synthetic.main.activity_cheat.*
import kr.nextm.lib.TToast
import ks.bs.mysoccerting.firebase.CrashlyticsHelper
import ks.bs.mysoccerting.firebase.FirebaseUrls
import ks.bs.mysoccerting.firebase.RxRealTimeDB
import ks.bs.mysoccerting.model.*
import ks.bs.mysoccerting.model.time.LocalTime
import ks.bs.mysoccerting.model.type.ClubGradeType
import ks.bs.mysoccerting.model.type.PostType
import ks.bs.mysoccerting.model.type.TimelineType
import ks.bs.mysoccerting.rx.caller.ActivityBundleObservable
import ks.bs.mysoccerting.rx.caller.ActivityObservable
import ks.bs.mysoccerting.rx.rxresult.startActivity
import ks.bs.mysoccerting.rx.rxresult.toIntent
import ks.bs.mysoccerting.rx.toJson
import ks.bs.mysoccerting.rx.withProgressBallAnimation
import ks.bs.mysoccerting.scene.club.detail.ClubDetailFragment
import ks.bs.mysoccerting.scene.login.LoginManager
import ks.bs.mysoccerting.support.DontCare
import ks.bs.mysoccerting.test.JustCallWithCountActivity
import ks.bs.mysoccerting.test.TransparentActivity
import ks.bs.mysoccerting.test.koin.KoinActivity
import ks.bs.mysoccerting.util.TLog
import java.util.concurrent.TimeUnit


class CheatActivity : CheatBaseActivity() {

    override fun initializeMenus() {
        rightMenu.addText("/** Welcome to the cheating world **/")


        rightMenu.addButton("매칭 희망일 2일씩 증가") {
            val path = "${FirebaseUrls.club}/${LoginManager.user?.clubUid}/matchUpList/match_1563424703496_rand1393"
            val indecator = object : GenericTypeIndicator<ModelClub.MatchInfo>() {}

            RxRealTimeDB.toSingle(path, indecator) { ModelClub.MatchInfo() }
                .map {
                    it.hopeDateTimeStamp = it.hopeDateTimeStamp.minus(86_400_000 * 2)
                    it
                }
                .flatMap { RxRealTimeDB.push(path, it) }
                .subscribe({
                    println(it)
                }, {
                    println(it)
                })
        }

        rightMenu.addButton("클럽매칭 쳇 2일씩 증가") {
            val path = "${FirebaseUrls.matchChat}/match_1563411110215_rand8932"
            val indecator = object : GenericTypeIndicator<HashMap<String, ModelChat>>() {}

            RxRealTimeDB.toSingle(path, indecator) { HashMap() }
                .map {
                    it.forEach {
                        it.value.timeStamp = it.value.timeStamp.minus(86_400_000 * 2)
                    }
                    it
                }
                .flatMap { RxRealTimeDB.push("${FirebaseUrls.matchChat}/match_1563411110215_rand8932", it) }
                .subscribe({
                    println(it)
                }, {
                    println(it)
                })
        }

        rightMenu.addButton("클럽쳇 2일씩 증가") {
            val path = "${FirebaseUrls.clubChat}/${LoginManager.user?.clubUid}"
            val indecator = object : GenericTypeIndicator<HashMap<String, ModelChat>>() {}

            RxRealTimeDB.toSingle(path, indecator) { HashMap() }
                .map {
                    it.forEach {
                        it.value.timeStamp = it.value.timeStamp.minus(86_400_000 * 2)
                    }
                    it
                }
                .flatMap { RxRealTimeDB.push("${FirebaseUrls.clubChat}/${LoginManager.user?.clubUid}", it) }
                .subscribe({
                    println(it)
                }, {
                    println(it)
                })
        }

        rightMenu.addButton("타임라인 1일씩 증가") {
            RxRealTimeDB.toSingle("${FirebaseUrls.club}/${LoginManager.user?.clubUid}") { ModelClub() }
                .map { it.timeline }
                .map {
                    it.forEach {
                        it.value.timestamp = it.value.timestamp?.minus(86_400_000)
                    }
                    it
                }
                .flatMap { RxRealTimeDB.push("${FirebaseUrls.club}/${LoginManager.user?.clubUid}/timeline", it) }
                .subscribe({
                    println(it)
                }, {
                    println(it)
                })
        }

        rightMenu.addButton("클럽 정보 확인") {
            RxRealTimeDB.toSingle("${FirebaseUrls.club}/${LoginManager.user?.clubUid}") { ModelClub() }
                .subscribe({
                    println(it)
                }, {
                    println(it)
                })
        }
        rightMenu.addButton("로그인 정보 확인") {
            println(LoginManager.user?.toJson())
        }

        rightMenu.addButton("프로그래스") {
            Observable.just(DontCare)
                .flatMap { Observable.timer(2, TimeUnit.SECONDS) }
                .withProgressBallAnimation(this)
                .subscribe({

                }, {

                })

        }
        rightMenu.addButton("파이어베이스 치트") {
            FirebaseItemAddActivity::class.java.toIntent()
                .startActivity(this)
                .subscribe()
        }

        rightMenu.addButton("current login user info") {
            println(LoginManager.user?.toJson())
        }

        rightMenu.addButton("Koin") {
            KoinActivity::class.java.toIntent()
                .startActivity(this)
                .subscribe()
        }

        rightMenu.addButton("토스트 메시지") {
            TToast.show("토스트메시지 테스트 안녕하세요 안녕하세요 안녕하세요 안녕하세요 안녕하세요 안녕하세요\"안녕하세요\"안녕하세요\"")
        }

        rightMenu.addButton("다이얼로그 치트 ") {
            DialogCheatActivity::class.java.toIntent()
                .startActivity(this)
                .subscribe()
        }
        rightMenu.addButton("클럽 맴버스 정렬 ") {
            RxRealTimeDB.toSingle("${FirebaseUrls.club}/club_1563096704651_rand6608") { ModelClub() }
            .flatMap { modelClub ->
                modelClub.clubMembers[0].gradeType=ClubGradeType.ASSISTANT.name
                modelClub.clubMembers[1].gradeType=ClubGradeType.COACH.name
                modelClub.clubMembers.sortByDescending { ClubGradeType.valueOf(it.gradeType).grade }
                TLog.d(modelClub.clubMembers)
                RxRealTimeDB.push("${FirebaseUrls.club}/club_1563096704651_rand6608/clubMembers", modelClub.clubMembers)
            } .subscribe({
                    TLog.d("맴버스 정렬 성공")
                }, { e ->
                    TLog.e("맴버스 정렬 실패")
                    CrashlyticsHelper.logException(e)
                }).addTo(compositeDisposable)
        }

        rightMenu.addButton("클럽 가입 시키") {
            var clubModel: ModelClub
            var userModel: ModelUser
            RxRealTimeDB.toSingle("${FirebaseUrls.users}/BQEjzFBb0dVav2UtTbuY764xHF43") { ModelUser() }
                .zipWith(RxRealTimeDB.toSingle("${FirebaseUrls.club}/${LoginManager.user?.clubUid}") { ModelClub() })
                .flatMap { response ->
                    clubModel = response.second
                    userModel = response.first.apply {
                        clubUid = response.second.contentId
                        clubJonined = true
                        clubImageUrl = response.second.clubMarkImageUrl
                        club = response.second.clubName
                        clubMasterUid = response.second.masterUid
                    }
                    RxRealTimeDB.push("${FirebaseUrls.users}/BQEjzFBb0dVav2UtTbuY764xHF43", userModel)
                        .map {
                            clubModel.clubMemberNumber = clubModel.clubMemberNumber + 1
                            clubModel.clubMembers.add(
                                ModelClub.ModelMember(
                                    nickName = userModel.nickName ?: "",
                                    uid = userModel.uid!!,
                                    memberImageUrl = userModel.profileImageUrl ?: "",
                                    position = userModel.position?.joinToString("  "),
                                    age = userModel.age.toString(),
                                    gradeType = ClubGradeType.PLAYER.name,
                                    mercenary = false
                                )
                            )
                            clubModel
                        }
                        .flatMap { model ->
                            RxRealTimeDB.push("${FirebaseUrls.club}/${model.contentId}", model)
                        }
                }
                .subscribe({
                    TLog.d("클럽 가입 성공")
                }, { e ->
                    TLog.e("클럽 가입 실패")
                    CrashlyticsHelper.logException(e)
                }).addTo(compositeDisposable)
        }
        rightMenu.addButton("게시글 쓰기") {
            val title = "aa"
            val content = "bb"
            val postContentUid = RxRealTimeDB.generateRandomKey("post")
            val boardPostPath = "${FirebaseUrls.clubBoard}/${LoginManager.user?.clubUid}/$postContentUid"
            val model = ModelClubBoard(
                title = title,
                profileImageUrl = LoginManager.user?.profileImageUrl,
                content = content,
                nickName = "축돌이5",
                timestamp = System.currentTimeMillis(),
                contentId = postContentUid,
                uid = "HieKm5L11NZiiWDZAFzMoTiBkPY2",
                commentcount = 0,
                clubUid = "club_1563096704651_rand6608"
            )
            RxRealTimeDB.push(boardPostPath, model)
                .flatMap {
                    val requestModel = ModelMyPost(
                        contentId = model.contentId,
                        content = model.content,
                        type = PostType.CLUB_BOARD,
                        mPostPath = "${PostType.MY_POST.path}/HieKm5L11NZiiWDZAFzMoTiBkPY2/${PostType.CLUB_BOARD.type}/${model.contentId}",
                        timestamp = model.timestamp
                    )
                    RxRealTimeDB.push(requestModel.mPostPath, requestModel)
                }
                .subscribe({
                    TLog.d("게시글 작성 성공")
                }, { e ->
                    TLog.e("게시글 작성 실패")
                    CrashlyticsHelper.logException(e)
                }).addTo(compositeDisposable)

        }

        rightMenu.addButton("클럽 서치 쓰기") {

            var path = FirebaseUrls.clubSeach
            val document = RxRealTimeDB.generateRandomKey("post")

            val userInfoPath = "${FirebaseUrls.users}/${auth.currentUser?.uid}"

            RxRealTimeDB.toSingle(userInfoPath) { ModelUser() }
                .flatMap { data ->
                    val requestModel =
                        ModelClubSearch(
                            clubImageUrl = data.profileImageUrl,
                            clubName = LoginManager.user?.club,
                            hopeDate = "8월2일",
                            content = "aa",
                            position = "GK",
                            timestamp = System.currentTimeMillis(),
                            uid = "HieKm5L11NZiiWDZAFzMoTiBkPY2",
                            hopeArea = "전라남도",
                            contentId = document,
                            clubMasterUid = LoginManager.user?.clubMasterUid!!
                        )
                    Single.just(requestModel)
                }
                .flatMap { model ->
                    val requestModel = ModelMyPost(
                        content = model.content,
                        contentId = model.contentId,
                        type = PostType.CLUB_SEARCH,
                        mPostPath = "${PostType.MY_POST.path}/HieKm5L11NZiiWDZAFzMoTiBkPY2/${PostType.CLUB_SEARCH.type}/${model.contentId}",
                        timestamp = model.timestamp
                    )
                    RxRealTimeDB.push(requestModel.mPostPath, requestModel)
                        .flatMap {
                            val pushPath = "$path/$document"
                            RxRealTimeDB.push(pushPath, model)
                        }
                }
                .subscribe({
                    TLog.d("게시글 작성 성공")
                }, { e ->
                    TLog.e("게시글 작성 실패")
                    CrashlyticsHelper.logException(e)
                }).addTo(compositeDisposable)


        }

        rightMenu.addButton("클럽 서치 삭제") {
            RxRealTimeDB.toSingle("${PostType.MY_POST.path}/HieKm5L11NZiiWDZAFzMoTiBkPY2/${PostType.CLUB_SEARCH.type}") { mutableMapOf<String, ModelMyPost>() }
                .map { it.keys.toList() }
                .toObservable()
                .flatMapIterable { it }
                .flatMapCompletable {
                    RxRealTimeDB.remove("${PostType.CLUB_SEARCH.path}/$it")
                        .andThen(RxRealTimeDB.remove("${PostType.MY_POST.path}/HieKm5L11NZiiWDZAFzMoTiBkPY2/${PostType.CLUB_SEARCH.type}"))
                }
                .subscribe({
                    TLog.d("서치 삭제 성공")
                }, { e ->
                    TLog.e("서치 삭제 실패")
                    CrashlyticsHelper.logException(e)
                }).addTo(compositeDisposable)


        }
        rightMenu.addButton("클럽 매 쓰기") {

            var path = FirebaseUrls.clubMatch
            val document = RxRealTimeDB.generateRandomKey("post")

            val userInfoPath = "${FirebaseUrls.users}/${auth.currentUser?.uid}"

            RxRealTimeDB.toSingle(userInfoPath) { ModelUser() }
                .flatMap { data ->
                    val requestModel =
                        ModelClubMatch(
                            clubImageUrl = data.profileImageUrl,
                            clubName = data.club,
                            content = "aa",
                            timestamp = System.currentTimeMillis(),
                            uid = "HieKm5L11NZiiWDZAFzMoTiBkPY2",
                            matcingArea = "강원도",
                            hopeDate = "8월2일",
                            matchingPeopleNumbewr = "11 vs 11",
                            contentId = document,
                            clubUid = LoginManager.user?.clubUid!!,
                            matchHopeDateTimeStamp = 1563378455343
                        )

                    Single.just(requestModel)
                }
                .flatMap { model ->
                    val requestModel = ModelMyPost(
                        content = model.content,
                        contentId = model.contentId,
                        type = PostType.CLUB_MATCH,
                        mPostPath = "${PostType.MY_POST.path}/HieKm5L11NZiiWDZAFzMoTiBkPY2/${PostType.CLUB_MATCH.type}/${model.contentId}",
                        timestamp = model.timestamp
                    )
                    RxRealTimeDB.push(requestModel.mPostPath, requestModel)
                        .flatMap {
                            val pushPath = "$path/$document"
                            RxRealTimeDB.push(pushPath, model)
                        }
                }
                .subscribe({
                    TLog.d("게시글 작성 성공")
                }, { e ->
                    TLog.e("게시글 작성 실패")
                    CrashlyticsHelper.logException(e)
                }).addTo(compositeDisposable)


        }

        rightMenu.addButton("클럽 매치 삭제") {
            RxRealTimeDB.toSingle("${PostType.MY_POST.path}/HieKm5L11NZiiWDZAFzMoTiBkPY2/${PostType.CLUB_MATCH.type}") { mutableMapOf<String, ModelMyPost>() }
                .map { it.keys.toList() }
                .toObservable()
                .flatMapIterable { it }
                .flatMapCompletable {
                    RxRealTimeDB.remove("${PostType.CLUB_MATCH.path}/$it")
                        .andThen(RxRealTimeDB.remove("${PostType.MY_POST.path}/HieKm5L11NZiiWDZAFzMoTiBkPY2/${PostType.CLUB_MATCH.type}"))
                }
                .subscribe({
                    TLog.d("매치 삭제 성공")
                }, { e ->
                    TLog.e("매치 삭제 실패")
                    CrashlyticsHelper.logException(e)
                }).addTo(compositeDisposable)


        }

        rightMenu.addButton("카카오 로그아웃") {
            UserManagement.getInstance().requestLogout(object : LogoutResponseCallback() {
                override fun onCompleteLogout() {
                    runOnUiThread { Toast.makeText(applicationContext, "로그아웃 성공", Toast.LENGTH_SHORT).show(); }
                }
            })
        }

        rightMenu.addButton("TRANSPARENT Activity")
            .setOnClickListener {

                ActivityBundleObservable(null, TransparentActivity::class.java, Bundle())
                    .createObservable()
                    .subscribe({

                    }, {
                        logView.println(it)
                    })
            }
        rightMenu.addButton("옵저버블인텐트 TEST") {
            val extraData = JustCallWithCountActivity.ExtraData(1)
            ActivityObservable<ExtraData>(
                null,
                JustCallWithCountActivity::class.java,
                extraData,
                extraData.javaClass
            )
                .createObservable()
                .subscribe {
                    setResult(Activity.RESULT_OK, intent)
                    finish()
                }
        }
        bottomMenu.addButton("Crashlytics test") {
            //    Crashlytics.getInstance().crash()
        }

    }

    private val keyInput = mutableListOf<Char>()

    private var scanTimes: LocalTime = LocalTime()

    override fun dispatchKeyEvent(event: KeyEvent): Boolean {
        TLog.d("dispatchKeyEvent event - ${event}")

        if (event.keyCode == KeyEvent.KEYCODE_BACK
            || event.action != KeyEvent.ACTION_DOWN
        ) {
            return super.dispatchKeyEvent(event)
        }

        val char = event.unicodeChar.toChar()

        TLog.d("dispatchKeyEvent char - ${char}")

        if (char == '\n') {
            val barcode = keyInput
                .filter { it != 0.toChar() }
                .joinToString(separator = "")
            keyInput.clear()

            println("$barcode  /// ${LocalTime()}")
        } else {
            keyInput.add(char)
        }
        return true

    }

}



