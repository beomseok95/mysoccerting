package ks.bs.mysoccerting.support.cheat

import android.app.Service
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.Point
import android.view.MotionEvent
import android.view.View
import android.view.View.OnTouchListener
import android.view.WindowManager
import android.widget.FrameLayout
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.layout_floating_cheat_widget.view.*
import ks.bs.mysoccerting.BuildConfig
import ks.bs.mysoccerting.prefs.Prefs
import ks.bs.mysoccerting.R
import ks.bs.mysoccerting.scene.login.LoginActivity
import ks.bs.mysoccerting.util.AppInstance
import java.util.concurrent.TimeUnit

class CheatView(val service: Service, val params: WindowManager.LayoutParams) : FrameLayout(service) {
    private val windowManager: WindowManager
        get() = service.getSystemService(Context.WINDOW_SERVICE) as WindowManager
    private var compositDisposable = CompositeDisposable()

    init {
        View.inflate(context, R.layout.layout_floating_cheat_widget, this)

        setBuildClock()

        setSummary()

        setDetailText()

        layoutCheatExpanded.visibility = GONE

        initializeDragDrop()

        initializeButtons()
    }

    private fun setBuildClock() {
        Observable.interval(0, 1, TimeUnit.SECONDS)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                textBuildTime.text = timeSinceBuild()

                val color: Int = colorByTimeElapsedSinceBuild()

                textBuildTime.setTextColor(color)
            }.let { compositDisposable.add(it) }
    }

    private fun timeSinceBuild(): String {
        val elapsed = elapsedSinceBuild()

        val seconds = elapsed.toInt() % 60

        val minutes = elapsed.toInt() / 60

        return minutes.toString().padStart(2, '0') + ":" + seconds.toString().padStart(2, '0')
    }

    private fun colorByTimeElapsedSinceBuild(): Int {
        val minutes = elapsedSinceBuild() / 60.0
        return when {
            minutes < 1 -> Color.WHITE
            minutes < 60 * 24 -> Color.GREEN
            minutes < 60 * 48 -> Color.YELLOW
            else -> Color.RED
        }
    }

    private fun elapsedSinceBuild(): Double {
        val diff = System.currentTimeMillis() - BuildConfig.BUILD_DATE_MILLIS
        return diff / 1000.0
    }

    private fun setDetailText() {
        val serverType = if (BuildConfig.FLAVOR_TYPE) "데브버전" else "릴리즈버전"

        textDetails.text =
            """$serverType
${BuildConfig.VERSION_NAME}/${BuildConfig.VERSION_CODE}
BUILD DATE: ${BuildConfig.BUILD_DATE}"""
    }

    override fun onDetachedFromWindow() {
        compositDisposable.clear()
        super.onDetachedFromWindow()
    }

    private fun initializeDragDrop() {
        var initialX: Int = 0
        var initialY: Int = 0
        var initialTouchX: Float = 0.toFloat()
        var initialTouchY: Float = 0.toFloat()

        Observable.create<Point> { emitter ->
            //Drag and move floating view using user's touch action.
            root_container.setOnTouchListener(OnTouchListener { v, event ->
                when (event.action) {
                    MotionEvent.ACTION_DOWN -> {

                        //remember the initial position.
                        initialX = params.x
                        initialY = params.y

                        //get the touch location
                        initialTouchX = event.rawX
                        initialTouchY = event.rawY
                        return@OnTouchListener true
                    }
                    MotionEvent.ACTION_UP -> {

                        val Xdiff = (event.rawX - initialTouchX).toInt()
                        val Ydiff = (event.rawY - initialTouchY).toInt()

                        //The check for Xdiff <10 && YDiff< 10 because sometime elements moves a little while clicking.
                        //So that is click event.
                        if (Xdiff < 10 && Ydiff < 10) {
                            toggleExpansionView()
                        }
                        return@OnTouchListener true
                    }
                    MotionEvent.ACTION_MOVE -> {
                        val Xdiff = (event.rawX - initialTouchX).toInt()
                        val Ydiff = (event.rawY - initialTouchY).toInt()

                        if (Xdiff > 10 || Ydiff > 10) {
                        }

                        //Calculate the X and Y coordinates of the view.
                        params.x = initialX + (event.rawX - initialTouchX).toInt()
                        params.y = initialY + (event.rawY - initialTouchY).toInt()

                        emitter.onNext(Point(params.x, params.y))

                        //Update the layout with new X & Y coordinate
                        windowManager.updateViewLayout(this@CheatView, params)
                        return@OnTouchListener true
                    }
                }
                false
            })
        }
            .debounce(5, TimeUnit.SECONDS)
            .subscribe {
                savePosition(it)
            }.let { compositDisposable.add(it) }
    }


    private fun toggleExpansionView() {

        if (layoutCheatExpanded.visibility == View.VISIBLE) {
            layoutCheatExpanded.visibility = View.GONE
        } else {
            setDetailText()
            layoutCheatExpanded.visibility = View.VISIBLE
        }
    }

    private fun savePosition(point: Point) {
        Prefs.save {
            it.cheat.floatingPoint = point
        }
    }

    private fun setSummary() {
        textSummary.text = """${BuildConfig.FLAVOR.capitalize()}.${BuildConfig.BUILD_TYPE.capitalize()}""".trimMargin()

        if (BuildConfig.FLAVOR_TYPE) {
            textSummary.setTextColor(Color.GREEN)
        } else {
            textSummary.setTextColor(Color.RED)
        }
    }


    private fun initializeButtons() {

        buttonCheat.setOnClickListener {
            toggleExpansionView()
            startActivity(CheatActivity::class.java)
        }

       /* buttonSettings.setOnClickListener {
            toggleExpansionView()
            // startActivity(SettingsActivity::class.java)
        }*/

        buttonRestart.setOnClickListener {
            toggleExpansionView()
            AppInstance.restart()
        }

        buttonNewInstance.setOnClickListener {
            startActivity(LoginActivity::class.java)
        }


        //Set the close button
        buttonCheatClose.setOnClickListener {
            service.stopSelf()
        }

        buttonOpen.setOnClickListener {
            startActivity(LoginActivity::class.java)
        }

    }


    private fun startActivity(clazz: Class<*>) {
        val intent = Intent(service, clazz)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        service.startActivity(intent)

    }

}