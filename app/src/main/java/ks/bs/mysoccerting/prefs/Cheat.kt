package ks.bs.mysoccerting.prefs

import android.graphics.Point
import ks.bs.mysoccerting.BuildConfig

import ks.bs.mysoccerting.util.PreferencesHelper

data class Cheat(
    var floatingPoint: Point = Point(0, 0),
    var floatingCheatView: Boolean = true,
    var fakeModel: String = "",
    var fakeSerial: String = "",
    var fakeDemoMode: Boolean = false,
    var forcedUpdate: Boolean = false,
    var tdpayAutoLogin: Boolean = false,
    var fakeGcsCall: Boolean = false,
    var dummy: Boolean = false,
    var appBarButtonLarge: Boolean = false,
    var offlineApi: Boolean = false
) {


    companion object {
        val instance get() = Prefs.instance.cheat

        var saveLocalLog: Boolean
            get() = PreferencesHelper.isEmpty("SAVE_LOCAL_LOG")
            set(value) {
                PreferencesHelper.set("SAVE_LOCAL_LOG", if (value) "1" else "")
            }

        fun action(title: String = "", flag: Boolean = true, function: () -> Unit) {
            if (BuildConfig.FLAVOR_TYPE && flag) {

                function()
            }
        }
    }

}
