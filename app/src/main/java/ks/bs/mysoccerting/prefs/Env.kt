package ks.bs.mysoccerting.prefs


import ks.bs.mysoccerting.BuildConfig


object Env {
    const val isBuildType =  BuildConfig.FLAVOR_TYPE

    const val isUnderAlphaTest = BuildConfig.ALPHA_TEST || BuildConfig.DEV
}