package ks.bs.mysoccerting.prefs


import kr.nextm.api.model.type.time.KstTime
import java.util.*

enum class SupportingCountry(val title: String, val timeZone: TimeZone, val currency: String, val vatRatio: Double) {

    KR("Korea", KstTime.kstTimeZone, "KRW", 0.1),

    Nothing("Nothing", KstTime.kstTimeZone, "KRW", 0.1);


}
