package ks.bs.mysoccerting.prefs

data class Settings(
    var fcmEnabled: Boolean = true,
    var isLogined: Boolean = false,
    var loginUid: String = ""
)