﻿package ks.bs.mysoccerting.prefs

import com.google.gson.JsonObject
import ks.bs.mysoccerting.BuildConfig
import ks.bs.mysoccerting.exception.ErrorLogException
import ks.bs.mysoccerting.util.PreferencesHelper
import ks.bs.mysoccerting.util.TLog
import java.util.*

class Prefs {

    /** 앱 실행에 필요한 데이타 / 사용자가 직접 수정하지 않음. */
    data class SystemData(
        var country: SupportingCountry = SupportingCountry.Nothing
    ) {
        fun getTimeZone(): TimeZone = country.timeZone
    }

    var system = SystemData()
    var cheat = Cheat()
    var settings = Settings()

    companion object {
        private val version = BuildConfig.VERSION_SETTINGS
        private fun getPrefsKeyFor(version: Int) = "${Prefs::class.java.canonicalName}/$version"
        private val VERSION_KEY = "${Prefs::class.java.canonicalName}.VERSION"

        @JvmStatic
        val instance: Prefs
                by lazy {
                    val installedVersion = PreferencesHelper[VERSION_KEY, version]
                    if (installedVersion < version) {
                        (installedVersion until version).forEach { versionFrom ->
                            migrationPrefs(versionFrom)
                        }
                    }

                    val instance = PreferencesHelper.getObject(getPrefsKeyFor(version)) { Prefs() }

                    /**
                     * 치트 찌꺼기 남아 있어서 부작용 생길 경우 방지
                     */
                    if (!Env.isUnderAlphaTest) {
                        instance.cheat = Cheat()
                    }

                    instance
                }

        private fun migrationPrefs(versionFrom: Int) {
            val obj = PreferencesHelper.getObject<JsonObject>(getPrefsKeyFor(versionFrom)) {
                throw ErrorLogException("Cannot create JsonObject from ${getPrefsKeyFor(versionFrom)}")
            }

            val versionTo = versionFrom + 1
            PreferencesHelper.setObject(getPrefsKeyFor(versionTo), obj)
            PreferencesHelper[VERSION_KEY] = versionTo
        }

        fun save(functionPropertyChanging: (Prefs) -> Unit = {}) {
            functionPropertyChanging.invoke(instance)
            val timeBefore = System.currentTimeMillis()

            PreferencesHelper[VERSION_KEY] = version
            PreferencesHelper.setObject(getPrefsKeyFor(version), instance)

            val timeElapsed = System.currentTimeMillis() - timeBefore

            TLog.d("Prefs::save time elapsed in millis $timeElapsed")
        }

        fun reset() {
            save {
                it.system = SystemData()
                it.cheat = Cheat()
                it.settings = Settings()
            }
        }

        val system get() = instance.system
        val cheat get() = instance.cheat
        val settings get() = instance.settings
    }
}