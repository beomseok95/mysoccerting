package ks.bs.mysoccerting.firebase

import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.ListenerRegistration
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.subjects.CompletableSubject
import ks.bs.mysoccerting.BuildConfig.FLAVOR_TYPE
import ks.bs.mysoccerting.support.DontCare

object RxFireStore {
    val instance get() = FirebaseFirestore.getInstance()
    val root = if (FLAVOR_TYPE) "test" else "real"
    val basePath = "$root/soccer"

    fun generateRandomKey(): String = "time_${System.currentTimeMillis()}_rand${(Math.random() * 10000).toInt()}"


    inline fun <reified T> toSingle(
        path: String,
        noinline defaultCreator: () -> T? = { null }
    ): Single<T> {
        return toSingle(path = path, clazz = T::class.java, defaultCreator = defaultCreator)
    }

    fun <T> toSingle(
        path: String,
        clazz: Class<T>,
        defaultCreator: () -> T? = { null }
    ): Single<T> {

        var snapShot: ListenerRegistration? = null

        return Single.create<T> { emitter ->
            val pathSize = path.split("/").size
            require(pathSize % 2 == 0) { "toSingle 요청의 path는 ( / ) 를 기준으로 홀수여야 합니다.\n ex1) collection/documentNumber\nex2) collection/documentNumber/collection/documentNumber " }


            instance.document("$basePath/$path")
                .addSnapshotListener { snapshot, firebaseFirestoreException ->
                    if (snapshot == null || emitter.isDisposed) return@addSnapshotListener

                    try {
                        val newInstance: T = snapshot.toObject(clazz)
                            ?: defaultCreator()
                            ?: run {
                                emitter.onError(NullPointerException(path))
                                return@addSnapshotListener
                            }

                        emitter.onSuccess(newInstance)

                    } catch (e: Exception) {
                        emitter.onError(e)
                    }
                }
        }
            .doOnDispose {
                snapShot?.remove()
            }
    }

    inline fun <reified T> toSingleList(
        path: String,
        noinline defaultCreator: () -> T? = { null }
    ): Single<List<T>> {
        return toSingleList(path = path, clazz = T::class.java, defaultCreator = defaultCreator)
    }

    fun <T> toSingleList(
        path: String,
        clazz: Class<T>,
        defaultCreator: () -> T? = { null }
    ): Single<List<T>> {

        var snapShot: ListenerRegistration? = null

        return Single.create<List<T>> { emitter ->
            val pathSize = path.split("/").size
            require(pathSize % 2 != 0) { " toObservable 요청의 path는 (/) 를 기준으로 홀수여야 합니다. \n ex1) collection\n ex2) collection/documentNumber/collection/" }

            val responseList = mutableListOf<T>()

            instance.collection("$basePath/$path")
                .addSnapshotListener { snapshot, firebaseFirestoreException ->
                    if (snapshot == null || emitter.isDisposed) return@addSnapshotListener

                    try {
                        responseList.clear()

                        for (documentSnapshot in snapshot.documents) {

                            val newInstance: T = documentSnapshot.toObject<T>(clazz)
                                ?: defaultCreator()
                                ?: run {
                                    emitter.onError(NullPointerException(path))
                                    return@addSnapshotListener
                                }
                            responseList.add(newInstance)

                        }

                        emitter.onSuccess(responseList)

                    } catch (e: Exception) {
                        emitter.onError(e)
                    }


                }
        }
            .doOnDispose {
                snapShot?.remove()
            }
    }

    inline fun <reified T> toObservable(
        path: String,
        noinline defaultCreator: () -> T? = { null }
    ): Observable<T> {
        return toObservable(path = path, clazz = T::class.java, defaultCreator = defaultCreator)
    }

    fun <T> toObservable(
        path: String,
        clazz: Class<T>,
        defaultCreator: () -> T? = { null }
    ): Observable<T> {
        var snapShot: ListenerRegistration? = null

        return Observable.create<T> { emitter ->
            val pathSize = path.split("/").size
            require(pathSize % 2 != 0) { " toObservable 요청의 path는 (/) 를 기준으로 홀수여야 합니다. \n ex1) collection\n ex2) collection/documentNumber/collection/" }

            instance.collection("$basePath/$path")
                .addSnapshotListener { snapshot, firebaseFirestoreException ->
                    if (snapshot == null || emitter.isDisposed) return@addSnapshotListener

                    for (documentSnapshot in snapshot.documents) {
                        try {
                            val newInstance: T = documentSnapshot.toObject<T>(clazz)
                                ?: defaultCreator()
                                ?: run {
                                    emitter.onError(NullPointerException(path))
                                    return@addSnapshotListener
                                }

                            emitter.onNext(newInstance)

                        } catch (e: Exception) {
                            emitter.onError(e)
                        }
                    }
                }

        }
            .doOnDispose {
                snapShot?.remove()
            }
    }

    inline fun <reified T> toObservableList(
        path: String,
        noinline defaultCreator: () -> T? = { null }
    ): Observable<List<T>> {
        return toObservableList(path = path, clazz = T::class.java, defaultCreator = defaultCreator)
    }

    fun <T> toObservableList(
        path: String,
        clazz: Class<T>,
        defaultCreator: () -> T? = { null }
    ): Observable<List<T>> {

        var snapShot: ListenerRegistration? = null

        return Observable.create<List<T>> { emitter ->
            val pathSize = path.split("/").size
            require(pathSize % 2 != 0) { " toObservableList 요청의 path는 (/) 를 기준으로 홀수여야 합니다. \n ex1) collection\n ex2) collection/documentNumber/collection/" }

            val responseList = mutableListOf<T>()

            instance.collection("$basePath/$path")
                .addSnapshotListener { snapshot, firebaseFirestoreException ->
                    if (snapshot == null || emitter.isDisposed) return@addSnapshotListener

                    try {
                        responseList.clear()

                        for (documentSnapshot in snapshot.documents) {

                            val newInstance: T = documentSnapshot.toObject<T>(clazz)
                                ?: defaultCreator()
                                ?: run {
                                    emitter.onError(NullPointerException(path))
                                    return@addSnapshotListener
                                }
                            responseList.add(newInstance)

                        }

                        emitter.onNext(responseList)

                    } catch (e: Exception) {
                        emitter.onError(e)
                    }


                }
        }
            .doOnDispose {
                snapShot?.remove()
            }
    }

    fun push(path: String, data: Any): Single<DontCare> {
        val pathSize = path.split("/").size
        require(pathSize % 2 == 0) { " Push 요청의 path는 ( / ) 를 기준으로 짝수여야 합니다.\n ex1) collection/documentNumber\nex2) collection/documentNumber/collection/documentNumber " }

        return Single.create<DontCare> { emitter ->
            instance.document("$basePath/$path")
                .set(data)
                .addOnCompleteListener { task ->
                    if (emitter.isDisposed)
                        return@addOnCompleteListener

                    if (task.isSuccessful) {
                        emitter.onSuccess(DontCare)
                    } else {
                        emitter.onError(task.exception!!)
                    }
                }
        }
    }

    fun remove(path: String): Completable {
        val pathSize = path.split("/").size
        require(pathSize % 2 == 0) { " remove 요청의 path는 ( / ) 를 기준으로 짝수여야 합니다.\n ex1) collection/documentNumber\nex2) collection/documentNumber/collection/documentNumber " }

        val emitter = CompletableSubject.create()

        instance.document("$basePath/$path").delete()
            .addOnSuccessListener {
                if (emitter.hasObservers())
                    emitter.onComplete()
            }
            .addOnFailureListener {
                if (emitter.hasObservers())
                    emitter.onError(it)
            }

        return emitter
    }
}