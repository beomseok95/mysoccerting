package ks.bs.mysoccerting.firebase

import ks.bs.mysoccerting.BuildConfig
import ks.bs.mysoccerting.model.CollectionPath

object FirebaseUrls {
    private fun isTestUrl() = if (BuildConfig.FLAVOR_TYPE) "test" else "real"

    fun getBaseUrl() = isTestUrl()

    val clubMatch get() = "post/${CollectionPath.CLUB_MATCH.collectionPath}"
    val mercenarySeach get() = "post/${CollectionPath.MERCENARY_SEARCH.collectionPath}"
    val clubSeach get() = "post/${CollectionPath.CLUB_SEARCH.collectionPath}"
    val club get() = "club/${CollectionPath.CLUB.collectionPath}"
    val clubNames get() = "club/${CollectionPath.CLUB_NAMES.collectionPath}"
    val clubBoard get() = "club/${CollectionPath.CLUB_BOARD.collectionPath}"
    val clubChat get() = "club/${CollectionPath.CHAT.collectionPath}"
    val comment get() = CollectionPath.COMMENT.collectionPath
    val users get() = CollectionPath.USERS.collectionPath
    val myPost get() = CollectionPath.MY_POST.collectionPath
    val anonymousUserCount get() = CollectionPath.ANNOYMOUS.collectionPath
    val alram get() = CollectionPath.ALARM.collectionPath
    val timeLine get() = CollectionPath.CLUB_TIMELINE.collectionPath
    val report get() = CollectionPath.REPORT.collectionPath
    val suggestions get() = CollectionPath.SUGGESTIONS.collectionPath

    val matchChat get() = CollectionPath.MATCH_CHAT.collectionPath
}