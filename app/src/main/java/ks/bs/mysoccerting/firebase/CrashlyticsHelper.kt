package ks.bs.mysoccerting.firebase

import com.crashlytics.android.Crashlytics
import ks.bs.mysoccerting.BuildConfig
import ks.bs.mysoccerting.scene.login.LoginManager
import ks.bs.mysoccerting.util.TLog

object CrashlyticsHelper {

    fun set() {
        if (LoginManager.user != null) {
            Crashlytics.setUserName(LoginManager.user?.uid)
            Crashlytics.setUserEmail(LoginManager.user?.email)
            Crashlytics.setUserIdentifier(LoginManager.user?.uid)
        } else {
            Crashlytics.setUserName("No Login User")
        }
    }

    fun logUnimportantException(function: () -> Unit) {
        try {
            function()
        } catch (t: Throwable) {
            logException(t)
            if (BuildConfig.DEV) {
                throw t
            }
        }
    }

    fun log(msg: String) {
        Crashlytics.log(msg)
    }

    fun logException(throwable: Throwable) {
        TLog.e(throwable)
        Crashlytics.logException(throwable)
    }

    fun sendLogException(e: Exception? = null) {
        try {
            if (e == null) {
                throw RuntimeException("sendLogException Log")
            }

            Crashlytics.logException(e)

        } catch (t: Throwable) {
            logException(t)
        }
    }

}