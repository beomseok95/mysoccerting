package ks.bs.mysoccerting.firebase


import com.google.firebase.database.GenericTypeIndicator
import io.reactivex.subjects.BehaviorSubject
import io.reactivex.subjects.PublishSubject
import ks.bs.mysoccerting.model.*
import ks.bs.mysoccerting.scene.login.LoginManager


object FirebaseHelper {

    val matchClubSubject = BehaviorSubject.create<HashMap<String, ModelClubMatch>>()
    val searchMercenarySubject = BehaviorSubject.create<HashMap<String, ModelMercenarySearch>>()
    val searchClubSubject = BehaviorSubject.create<HashMap<String, ModelClubSearch>>()
    val clubBrowseSubject = BehaviorSubject.create<HashMap<String, ModelClub>>()

    fun initialize() {
        initMatchClubSnapshot()
        initSearchMercenarySnapshot()
        initSearchClubSnapshot()
        initClubBrowseSnapshot()
    }

    private fun initMatchClubSnapshot() {
        val indecatior = object : GenericTypeIndicator<HashMap<String, ModelClubMatch>>() {}
        RxRealTimeDB.toObservable(FirebaseUrls.clubMatch, indecatior) { HashMap() }
            .doOnError(CrashlyticsHelper::logException)
            .subscribe(matchClubSubject)
    }

    private fun initSearchMercenarySnapshot() {
        val indecatior = object : GenericTypeIndicator<HashMap<String, ModelMercenarySearch>>() {}
        RxRealTimeDB.toObservable(FirebaseUrls.mercenarySeach, indecatior) { HashMap() }
            .doOnError(CrashlyticsHelper::logException)
            .subscribe(searchMercenarySubject)
    }

    private fun initSearchClubSnapshot() {
        val indecatior = object : GenericTypeIndicator<HashMap<String, ModelClubSearch>>() {}
        RxRealTimeDB.toObservable(FirebaseUrls.clubSeach, indecatior) { HashMap() }
            .doOnError(CrashlyticsHelper::logException)
            .subscribe(searchClubSubject)
    }

    private fun initClubBrowseSnapshot() {
        val indecatior = object : GenericTypeIndicator<HashMap<String, ModelClub>>() {}
        RxRealTimeDB.toObservable(FirebaseUrls.club, indecatior) { HashMap() }
            .doOnError(CrashlyticsHelper::logException)
            .subscribe(clubBrowseSubject)
    }
}