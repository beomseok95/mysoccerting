package ks.bs.mysoccerting.firebase

import com.google.firebase.database.*
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.subjects.CompletableSubject
import io.reactivex.subjects.SingleSubject
import ks.bs.mysoccerting.exception.CanceledBySystemException
import ks.bs.mysoccerting.rx.toJson
import ks.bs.mysoccerting.support.DontCare

object RxRealTimeDB {
    private val instance get() = FirebaseDatabase.getInstance()
    val root get() = FirebaseUrls.getBaseUrl()

    fun generateRandomKey(filed: String): String =
        "${filed}_${System.currentTimeMillis()}_rand${(Math.random() * 10000).toInt()}"

    inline fun <reified T> toSingle(
        path: String,
        noinline defaultCreator: () -> T? = { null }
    ): Single<T> {
        return toSingle(path, object : GenericTypeIndicator<T>() {}, defaultCreator)
    }

    fun <T> toSingle(
        path: String,
        typeIndicator: GenericTypeIndicator<T>,
        defaultCreator: () -> T? = { null }
    ): Single<T> {
        val reference = instance.getReference("$root/$path")

        lateinit var listener: ValueEventListener

        return Single.create<T> { emitter ->
            listener = object : ValueEventListener {
                override fun onCancelled(error: DatabaseError) {
//                    CrashlyticsHelper.log(error.message)

                    if (emitter.isDisposed)
                        return

                    emitter.onError(CanceledBySystemException(error.message))
                }

                override fun onDataChange(snapshot: DataSnapshot) {
                    if (emitter.isDisposed)
                        return

                    try {
                        val newInstance: T = snapshot.getValue(typeIndicator)
                            ?: defaultCreator()
                            ?: run {
                                emitter.onError(NullPointerException(path))
                                return
                            }

                        emitter.onSuccess(newInstance)
                    } catch (e: Exception) {
                        CrashlyticsHelper.logException(e)
                        emitter.onError(e)
                    }
                }
            }

            reference.addListenerForSingleValueEvent(listener)
        }
            .doOnDispose {
                reference.removeEventListener(listener)
            }
    }

    inline fun <reified T> toObservable(
        path: String,
        noinline defaultCreator: () -> T? = { null }
    ): Observable<T> {
        return toObservable(path, object : GenericTypeIndicator<T>() {}, defaultCreator)
    }

    fun <T> toObservable(
        path: String,
        type: GenericTypeIndicator<T>,
        defaultCreator: () -> T? = { null }
    ): Observable<T> {
        val reference = instance.getReference("$root/$path")

        var listener: ValueEventListener? = null

        return Observable.create<T> { emitter ->
            listener = object : ValueEventListener {
                override fun onCancelled(error: DatabaseError) {
//                    CrashlyticsHelper.log(error.message)

                    if (emitter.isDisposed)
                        return

                    emitter.onError(CanceledBySystemException(error.message))
                }

                override fun onDataChange(snapshot: DataSnapshot) {
                    if (emitter.isDisposed)
                        return

                    try {
                        val newInstance: T = snapshot.getValue(type)
                            ?: defaultCreator()
                            ?: run {
                                emitter.onError(NullPointerException(path))
                                return
                            }

                        emitter.onNext(newInstance)
                    } catch (e: Exception) {
                        CrashlyticsHelper.logException(e)
                        emitter.onError(e)
                    }
                }
            }

            reference.addValueEventListener(listener!!)
        }
            .doOnDispose {
                val existListener = listener ?: return@doOnDispose
                reference.removeEventListener(existListener)
            }
    }

    fun <T> push(path: String, data: T): Single<DontCare> {
        return Single.create<DontCare> { emitter ->
            instance.getReference("$root/$path")
                .setValue(data)
                .addOnCompleteListener { task ->
                    if (emitter.isDisposed)
                        return@addOnCompleteListener

                    if (task.isSuccessful) {
                        emitter.onSuccess(DontCare)
                    } else {
                        CrashlyticsHelper.logException(task.exception!!)
                        emitter.onError(task.exception!!)
                    }
                }
        }
    }

    fun remove(path: String): Completable {
        val emitter = CompletableSubject.create()

        instance.reference.child("$root/$path").removeValue()
            .addOnSuccessListener {
                if (emitter.hasObservers())
                    emitter.onComplete()
            }
            .addOnFailureListener {
                if (emitter.hasObservers())
                    CrashlyticsHelper.logException(it)
                emitter.onError(it)
            }

        return emitter
    }

    fun counter(path: String, countValue: Int = 1): Single<Int> {
        val subject = SingleSubject.create<Int>()
        FirebaseDatabase.getInstance()
            .getReference("$root/$path")
            .runTransaction(object : Transaction.Handler {
                override fun onComplete(error: DatabaseError?, commited: Boolean, p2: DataSnapshot?) {
                    if (error != null) {
                        subject.onError(error.toException())
                        return
                    }
                    if (!commited) {
                        subject.onError(IllegalStateException("orderNo not Commited"))
                        return
                    }
                    if (p2 == null) {
                        subject.onError(IllegalStateException("snapshot null"))
                        return
                    }
                    val value = p2.getValue(Int::class.java)
                    require(value != null) {
                        "데이터베이스 값 형태가 잘못되었습니다 : ${p2.getValue(true)?.toJson()}"
                    }
                    subject.onSuccess(value)

                }


                override fun doTransaction(p0: MutableData): Transaction.Result {
                    val count = p0.getValue(Int::class.java) ?: 0
                    p0.value = count + countValue
                    return Transaction.success(p0)
                }
            })

        return subject
    }

    fun <T> pushTransaction(path: String, data: T): Completable {
        return Completable.create { emitter ->
            FirebaseDatabase.getInstance()
                .getReference("$root/$path")
                .runTransaction(object : Transaction.Handler {
                    override fun onComplete(error: DatabaseError?, commited: Boolean, p2: DataSnapshot?) {
                        if (error != null) {
                            CrashlyticsHelper.logException(error.toException())
                            emitter.onError(error.toException())
                            return
                        }
                        if (!commited) {
                            emitter.onError(IllegalStateException("orderNo not Commited"))
                            return
                        }
                        if (p2 == null) {
                            emitter.onError(IllegalStateException("snapshot null"))
                            return
                        }
                        emitter.onComplete()
                    }


                    override fun doTransaction(p0: MutableData): Transaction.Result {
                        p0.value = data
                        return Transaction.success(p0)
                    }
                })
        }


    }
}