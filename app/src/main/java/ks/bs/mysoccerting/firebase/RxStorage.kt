package ks.bs.mysoccerting.firebase

import android.net.Uri
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.storage.FirebaseStorage
import io.reactivex.Single

object RxStorage {
    val instance = FirebaseStorage.getInstance().reference
    val uid = FirebaseAuth.getInstance().currentUser!!.uid

    fun push(collectionPath: String, uri: Uri): Single<String> {
        return Single.create<String> { emitter ->
            instance.child(collectionPath).child(uid)
                .putFile(uri)
                .addOnSuccessListener {
                    it.storage.downloadUrl
                        .addOnCompleteListener {
                            emitter.onSuccess(it.result.toString())
                        }
                        .addOnFailureListener { e ->
                            emitter.onError(e)
                        }
                }
                .addOnFailureListener { e ->
                    CrashlyticsHelper.logException(e)
                    emitter.onError(e)
                }
        }
    }
}