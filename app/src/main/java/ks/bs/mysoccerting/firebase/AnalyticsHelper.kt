package ks.bs.mysoccerting.firebase

import android.os.Bundle
import com.google.firebase.analytics.FirebaseAnalytics
import kr.nextm.lib.AppInstance

object AnalyticsHelper {
    private val firebaseAnalytics: FirebaseAnalytics = FirebaseAnalytics.getInstance(AppInstance.getApplicationContext())

    fun logEventParam(name: String, payment_id: String, result_code: String, tid: String, payment_title: String) {
        val bundle = Bundle()
//        bundle.putString(PARAM_PAYMENT_ID, payment_id)
//        bundle.putString(PARAM_RESULT_CODE, result_code)
//        bundle.putInt(PARAM_TID, tid.toSafeInt())
//        bundle.putString(PARAM_PAYMENT_TITLE, payment_title)
        logEvent(name, bundle)
    }

    fun logEventSingle(name: String, paramName: String, params: String) {
        val bundle = Bundle()
        bundle.putString(paramName, params)
        logEvent(name, bundle)
    }

    fun logEvent(name: String, bundle: Bundle) {
        firebaseAnalytics.logEvent(name, bundle)
    }

    fun setUserProperty(name: String, value: String) {
        firebaseAnalytics.setUserProperty(name, value)
    }

/*
    *//** Analytics Event Name *//*
    val EVENT_PAYMENT_TYPE = "payment_type"

    val EVENT_PRE_PROCESS_ON_PAY = "preProcessOnPay"
    val EVENT_PRE_PROCESS_ON_CANCEL = "preProcessOnCancel"
    val EVENT_POST_PROCESS_ON_PAY = "postProcessOnPay"
    val EVENT_POST_PROCESS_ON_CANCEL = "postProcessOnCancel"

    val EVENT_CAT_PAY_TRANSACTION = "catPayTransaction"
    val EVENT_CAT_CANCEL_TRANSACTION = "catCancelTransaction"

    val EVENT_GET_LAST_TRANSACTION = "getLastTransaction"

    val EVENT_TPROCESS_ACTIVITY_PAY = "TProcessActivity_pay"
    val EVENT_TPROCESS_ACTIVITY_CANCEL = "TProcessActivity_cancel"

    *//** Analytics Prams Name *//*
    val PARAM_PAYMENT_ID = "payment_id"
    val PARAM_RESULT_CODE = "result_code"
    val PARAM_TID = "tid"
    val PARAM_PAYMENT_TITLE = "payment_title"
    val PARAM_RESULT = "result"*/

}