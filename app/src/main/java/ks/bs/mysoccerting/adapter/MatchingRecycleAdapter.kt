package ks.bs.mysoccerting.adapter

import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.item_base.view.*
import ks.bs.mysoccerting.BR
import ks.bs.mysoccerting.R
import ks.bs.mysoccerting.model.ModelClubMatch

class MatchingRecycleAdapter :
    androidx.recyclerview.widget.RecyclerView.Adapter<MatchingRecycleAdapter.GenericViewHolder>() {

    private var rows: List<ModelClubMatch>? = null
    lateinit var onClick: (View, ModelClubMatch) -> Unit


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GenericViewHolder {
        val binding = DataBindingUtil.inflate<ViewDataBinding>(
            LayoutInflater.from(parent.context),
            R.layout.item_club,
            parent,
            false
        )
        return GenericViewHolder(binding)
    }

    override fun onBindViewHolder(holder: GenericViewHolder, position: Int) {
        holder.bind(getRowOfPosition(position))
    }

    private fun getRowOfPosition(position: Int): ModelClubMatch {
        return rows!![position]
    }

    override fun getItemCount(): Int {
        return if (rows == null) 0 else rows!!.size
    }

    fun setRows(rows: List<ModelClubMatch>) {
        this.rows = rows
        notifyDataSetChanged()
    }


    inner class GenericViewHolder(private val binding: ViewDataBinding) : androidx.recyclerview.widget.RecyclerView.ViewHolder(binding.root) {
        fun bind(clubMatch: ModelClubMatch) {
            binding.setVariable(BR.clubMatch, clubMatch)
            binding.root.container.setOnClickListener {
                onClick(it, clubMatch)
            }
            binding.executePendingBindings()
        }
    }
}