package ks.bs.mysoccerting.adapter

import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import ks.bs.mysoccerting.BR
import ks.bs.mysoccerting.R
import ks.bs.mysoccerting.databinding.BaseViewHolder
import ks.bs.mysoccerting.databinding.LayoutRecyclderviewFooterBinding
import ks.bs.mysoccerting.databinding.LayoutRecyclderviewHeaderBinding

class HeaderFooterRecycleAdapter<ITEM : Any, B : ViewDataBinding>(
    @LayoutRes val layout: Int,
    val bindingVariableId: Int? = null)
    : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    val items = mutableListOf<ITEM>()

    private val TYPE_HEADER: Int = 0
    private val TYPE_ITEM: Int = 1
    private val TYPE_FOOTER: Int = 2

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (isPositionHeader(position) || isPositionFooter(position)) return

            holder.onBindViewHolder(items[position - 1])
    }

    fun RecyclerView.ViewHolder.onBindViewHolder(item: ITEM) {
        val b: B = DataBindingUtil.bind(itemView)!!

        if (bindingVariableId == null)
            return

        b.setVariable(bindingVariableId, item)
        itemView.visibility = View.VISIBLE

        b.executePendingBindings()
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            TYPE_ITEM -> BaseViewHolder<B>(
                layout = layout,
                parent = parent,
                bindingVariableId = bindingVariableId)

            TYPE_HEADER ->
                object : HeaderHeaderViewHolder<B>(
                    layout = R.layout.layout_recyclderview_header,
                    parent = parent,
                    bindingVariableId = BR.header) {
                    override fun onBindViewHolder(item: Any) {
                        val binding: LayoutRecyclderviewHeaderBinding = DataBindingUtil.bind(itemView)!!

                        binding.setVariable(BR.header, item)
                        itemView.visibility = View.VISIBLE
                        binding.executePendingBindings()
                    }
                }
            else -> object : HeaderHeaderViewHolder<B>(
                layout = R.layout.layout_recyclderview_footer,
                parent = parent,
                bindingVariableId = BR.footer
            ) {
                override fun onBindViewHolder(item: Any) {
                    val binding: LayoutRecyclderviewFooterBinding = DataBindingUtil.bind(itemView)!!

                    binding.setVariable(BR.footer, item)
                    itemView.visibility = View.VISIBLE
                    binding.executePendingBindings()
                }
            }
        }
    }

    override fun getItemViewType(position: Int): Int {
        return when {
            isPositionHeader(position) -> TYPE_HEADER
            isPositionFooter(position) -> TYPE_FOOTER
            else -> TYPE_ITEM
        }
    }

    private fun isPositionHeader(position: Int): Boolean {
        return position == TYPE_HEADER
    }

    private fun isPositionFooter(position: Int): Boolean {
        return position == items.size + 1
    }


    override fun getItemCount(): Int {
        return items.count() + 2
    }

    fun replaceAll(items: List<ITEM>) {
        this.items.clear()
        this.items.addAll(items)

        notifyDataSetChanged()
    }

    fun insertAll(items: List<ITEM>) {
        val sizeOld = itemCount

        this.items.addAll(items)

        val sizeNew = itemCount

        notifyItemRangeInserted(sizeOld, sizeNew)
    }

    fun add(item: ITEM) {
        this.items.add(item)
        notifyDataSetChanged()
    }

    fun clear() {
        val size = items.size
        this.items.clear()
        notifyItemRangeRemoved(0, size)
    }

    fun changeToCopyItems() {
        items.clear()
    }
}