package ks.bs.mysoccerting.adapter

import android.view.ViewGroup
import androidx.databinding.ViewDataBinding
import ks.bs.mysoccerting.databinding.BaseViewHolder

open class HeaderHeaderViewHolder<B : ViewDataBinding>(
        layout: Int,
        parent: ViewGroup,
        private val bindingVariableId: Int?
) : BaseViewHolder<B>(layout, parent, bindingVariableId) {


        class HeaderVm {}
}