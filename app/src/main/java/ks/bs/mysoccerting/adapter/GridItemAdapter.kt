package ks.bs.mysoccerting.adapter

import androidx.annotation.LayoutRes
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import ks.bs.mysoccerting.databinding.BaseRecyclerViewAdapter

class GridItemAdapter<ITEM : Any, B : ViewDataBinding>(
    @LayoutRes layout: Int,
    bindingVariableId: Int? = null,
    val spanCount: Int
) : BaseRecyclerViewAdapter<ITEM, B>(layout, bindingVariableId) {

    private var decoration: GridSpacingItemDecoration = createDecoration()

    private fun createDecoration(): GridSpacingItemDecoration {
        return GridSpacingItemDecoration(spanCount, 10, true)
    }

    override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
        super.onAttachedToRecyclerView(recyclerView)
        recyclerView.addItemDecoration(decoration)
        recyclerView.isVerticalScrollBarEnabled = true
    }

    override fun onDetachedFromRecyclerView(recyclerView: RecyclerView) {
        super.onDetachedFromRecyclerView(recyclerView)
        recyclerView.removeItemDecoration(decoration)
    }


}