package ks.bs.mysoccerting.adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import ks.bs.mysoccerting.scene.club.clubtour.ClubDetailViewPagerInterface
import ks.bs.mysoccerting.scene.club.detail.board.ClubBoardFragment
import ks.bs.mysoccerting.scene.club.detail.chat.ClubChatFragment
import ks.bs.mysoccerting.scene.club.detail.home.ClubDetailHomeFragment
import ks.bs.mysoccerting.scene.club.detail.setting.ClubSettingFragment

class ClubDetailViewPagerAdapter(
    fm: FragmentManager,
    var contentId: String,
    val viewpagerListener: ClubDetailViewPagerInterface
) : FragmentStatePagerAdapter(fm) {
    override fun getItem(position: Int): Fragment {
        return when (position) {
            0 -> {
                ClubDetailHomeFragment.newInstace(contentId)
            }
            1 -> {
                ClubBoardFragment.newInstace(contentId)
            }
            2 -> {
                ClubChatFragment.newInstace(contentId)
            }
            else -> ClubSettingFragment.newInstace(contentId, viewpagerListener)
        }
    }

    override fun getCount(): Int {
        return 4
    }
}