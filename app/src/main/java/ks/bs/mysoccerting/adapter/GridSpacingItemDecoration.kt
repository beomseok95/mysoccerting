package ks.bs.mysoccerting.adapter

import android.graphics.Rect
import android.view.View
import androidx.recyclerview.widget.RecyclerView


class GridSpacingItemDecoration(val spanCount: Int, val spacingInPixels: Int, val includeEdge: Boolean) : RecyclerView.ItemDecoration() {

    override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State) {
        val position = parent.getChildAdapterPosition(view) // item position
        val column = position % spanCount // item column

        if (includeEdge) {
            outRect.left = spacingInPixels - column * spacingInPixels / spanCount // spacing - column * ((1f / spanCount) * spacing)
            outRect.right = (column + 1) * spacingInPixels / spanCount // (column + 1) * ((1f / spanCount) * spacing)

            if (position < spanCount) { // top edge
                outRect.top = spacingInPixels
            }
            outRect.bottom = spacingInPixels // item bottom
        } else {
            outRect.left = column * spacingInPixels / spanCount // column * ((1f / spanCount) * spacing)
            outRect.right = spacingInPixels - (column + 1) * spacingInPixels / spanCount // spacing - (column + 1) * ((1f /    spanCount) * spacing)
            if (position >= spanCount) {
                outRect.top = spacingInPixels // item top
            }
        }
    }
}

