# Add project specific ProGuard rules here.
# You can control the set of applied configuration files using the
# proguardFiles setting in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}

# Uncomment this to preserve the line number information for
# debugging stack traces.
#-keepattributes SourceFile,LineNumberTable

# If you keep the line number information, uncomment this to
# hide the original source file name.
#-renamesourcefileattribute SourceFile


# Platform calls Class.forName on types which do not exist on Android to determine platform.
-dontnote retrofit2.Platform
# Platform used when running on Java 8 VMs. Will not be used at runtime.
-dontwarn retrofit2.Platform$Java8
# Retain generic type information for use by reflection by converters and adapters.
-keepattributes Signature
# Retain declared checked exceptions for use by a Proxy instance.
-keepattributes Exceptions


-keep class java.** {*;}
-keepclassmembers class java.** {*;}
-dontwarn java.**

-keep class javax.** {*;}
-keepclassmembers class javax.** {*;}
-dontwarn javax.**

-keep class org.** {*;}
-keepclassmembers class org.** {*;}
-dontwarn org.**


-keep class net.** {*;}
-keepclassmembers class net.** {*;}
-dontwarn net.**

-keep class android.** {*;}
-keepclassmembers class android.** {*;}
-dontwarn android.**

-keep class ks.bs.mysoccerting.api.** {*;}
-keepclassmembers class ks.bs.mysoccerting.api.** {*;}
-dontwarn ks.bs.mysoccerting.api.**

-keep class ks.bs.mysoccerting.model.** {*;}
-keepclassmembers class ks.bs.mysoccerting.model.** {*;}
-dontwarn ks.bs.mysoccerting.model

-keep class ks.bs.mysoccerting.util.** {*;}
-keepclassmembers class ks.bs.mysoccerting.util.** {*;}
-dontwarn ks.bs.mysoccerting.util

-keep class ks.bs.mysoccerting.prefs.** {*;}
-keepclassmembers class ks.bs.mysoccerting.prefs.** {*;}
-dontwarn ks.bs.mysoccerting.prefs


